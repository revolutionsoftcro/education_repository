﻿using System;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.IO;

namespace Servis_vozila.Models
{
    public class NLog_combine : IMoj_NLog
    {
        private Servis_vozilaEntities db = new Servis_vozilaEntities();
        public static void UndoingChangesDbContextLevel(DbContext context)
        {
            foreach (DbEntityEntry entry in context.ChangeTracker.Entries())
            {
                switch (entry.State)
                {
                    case EntityState.Modified:
                        entry.State = EntityState.Unchanged;
                        break;
                    case EntityState.Added:
                        entry.State = EntityState.Detached;
                        break;
                    case EntityState.Deleted:
                        entry.Reload();
                        break;
                    default: break;
                }
            }
        }
        public void Error(string controller, string poruka)
        {
            Moj_Nlog dblogger = new Moj_Nlog();
            dblogger.Kontroler = controller;
            dblogger.Tip = "Error";
            dblogger.Vrijeme = DateTime.Now;
            dblogger.Poruka = $"Dogodila se greška:" + poruka;

            UndoingChangesDbContextLevel(db);
            db.Moj_Nlog.Add(dblogger);
            db.SaveChanges();

            string start = @"C:\Users\trobic\Documents\TomislavRobic-Ucenje\SQL\Servis-vozila\Servis-vozila\logs\html";
            string end = $"{DateTime.Now.ToShortDateString()}.html";
            string putanja = Path.Combine(start, end);
            string temp = Path.Combine(start, "template.html");

            string text;
            if (!File.Exists(putanja))
            {
                File.Copy(temp, putanja);
                text = System.IO.File.ReadAllText(putanja);
                text = text.Replace("<!-- Datum -->", $"{DateTime.Now.ToShortDateString()}");
                File.WriteAllText(putanja, text);
            }

            text = File.ReadAllText(putanja);
            text = text.Replace("<!-- Tip -->", "<font color=\"red\">Error</font>");
            text = text.Replace("<!-- Vrijeme -->", $"<font color=\"red\">{DateTime.Now}</font>");
            text = text.Replace("<!-- Kontroler -->", $"<font color=\"red\">{controller}</font>");
            text = text.Replace("<!-- Poruka -->", $"<font color=\"red\">Dogodila se greška: {poruka}</font>");
            string red = "\n<tr >\n"
                + "<td><strong><!-- Tip --></strong></td>\n"
                + "<td><!-- Vrijeme --></td>\n"
                + "<td><!-- Kontroler --></td>\n"
                + "<td><!-- Poruka --></td>\n"
                + "</tr>\n"
                + "<!-- red -->";
            text = text.Replace("<!-- red -->", red);
            File.WriteAllText(putanja, text);
        }

        public void Info(string controller, string poruka)
        {
            Moj_Nlog dblogger = new Moj_Nlog();
            dblogger.Kontroler = controller;
            dblogger.Tip = "Info";
            dblogger.Vrijeme = DateTime.Now;
            dblogger.Poruka = poruka;

            db.Moj_Nlog.Add(dblogger);
            db.SaveChanges();

            string start = @"C:\Users\trobic\Documents\TomislavRobic-Ucenje\SQL\Servis-vozila\Servis-vozila\logs\html";
            string end = $"{DateTime.Now.ToShortDateString()}.html";
            string putanja = Path.Combine(start, end);
            string temp = Path.Combine(start, "template.html");

            string text;
            if (!File.Exists(putanja))
            {
                File.Copy(temp, putanja);
                text = System.IO.File.ReadAllText(putanja);
                text = text.Replace("<!-- Datum -->", $"{DateTime.Now.ToShortDateString()}");
                File.WriteAllText(putanja, text);
            }

            text = File.ReadAllText(putanja);
            text = text.Replace("<!-- Tip -->", "Info");
            text = text.Replace("<!-- Vrijeme -->", $"{DateTime.Now}");
            text = text.Replace("<!-- Kontroler -->", $"{controller}");
            text = text.Replace("<!-- Poruka -->", $"{poruka}");
            string red = "\n<tr >\n"
                + "<td><strong><!-- Tip --></strong></td>\n"
                + "<td><!-- Vrijeme --></td>\n"
                + "<td><!-- Kontroler --></td>\n"
                + "<td><!-- Poruka --></td>\n"
                + "</tr>\n"
                + "<!-- red -->";
            text = text.Replace("<!-- red -->", red);
            File.WriteAllText(putanja, text);
        }
    }
}