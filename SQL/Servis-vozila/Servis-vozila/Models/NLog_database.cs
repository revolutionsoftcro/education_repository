﻿using System;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;

namespace Servis_vozila.Models
{
    public class NLog_database : IMoj_NLog
    {
        private Servis_vozilaEntities db = new Servis_vozilaEntities();
        public static void UndoingChangesDbContextLevel(DbContext context)
        {
            foreach (DbEntityEntry entry in context.ChangeTracker.Entries())
            {
                switch (entry.State)
                {
                    case EntityState.Modified:
                        entry.State = EntityState.Unchanged;
                        break;
                    case EntityState.Added:
                        entry.State = EntityState.Detached;
                        break;
                    case EntityState.Deleted:
                        entry.Reload();
                        break;
                    default: break;
                }
            }
        }
        public void Error(string controller, string poruka)
        {
            Moj_Nlog dblogger = new Moj_Nlog();
            dblogger.Kontroler = controller;
            dblogger.Tip = "Error";
            dblogger.Vrijeme = DateTime.Now;
            dblogger.Poruka = $"Dogodila se greška:"+poruka;

            UndoingChangesDbContextLevel(db);
            db.Moj_Nlog.Add(dblogger);
            db.SaveChanges();
        }

        public void Info(string controller, string poruka)
        {
            Moj_Nlog dblogger = new Moj_Nlog();
            dblogger.Kontroler = controller;
            dblogger.Tip = "Info";
            dblogger.Vrijeme = DateTime.Now;
            dblogger.Poruka = poruka;

            db.Moj_Nlog.Add(dblogger);
            db.SaveChanges();
        }
    }
}