﻿namespace Servis_vozila.Models
{
    public class MojNLog
    {
        private IMoj_NLog Logger; 
        public MojNLog(IMoj_NLog dependency)
        {
            Logger = dependency;
        }

        public void Error(string controller, string poruka)
        {
            Logger.Error(controller, poruka);
        }

        public void Info(string controller, string poruka)
        {
            Logger.Info(controller, poruka);
        }
    }
}