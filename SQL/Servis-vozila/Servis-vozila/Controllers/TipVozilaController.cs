﻿using NLog;
using Servis_vozila.Models;
using System;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Web.Mvc;

namespace Servis_vozila.Controllers
{
    public class TipVozilaController : Controller
    {
        //dependencies : NLog_database() , NLog_html() ili NLog_combine()
        MojNLog MNLog =new MojNLog(new NLog_combine());

        private Servis_vozilaEntities db = new Servis_vozilaEntities();
        private static Logger logger = LogManager.GetCurrentClassLogger();

        // GET: TipVozila
        public ActionResult Index()
        {
            var tip = db.Tip.Include(t => t.Tip2);
            return View(tip.ToList());
        }

        // GET: TipVozila/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Tip tip = db.Tip.Find(id);
            if (tip == null)
            {
                return HttpNotFound();
            }
            return View(tip);
        }

        // GET: TipVozila/Create
        public ActionResult Create()
        {
            ViewBag.NadtipId = new SelectList(db.Tip, "Id", "Naziv");
            return View();
        }

        // POST: TipVozila/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,NadtipId,Naziv")] Tip tip)
        {
            if (ModelState.IsValid)
            {
                db.Tip.Add(tip);
                db.SaveChanges();

                logger.Info($"Ubacivanje u bazu: {tip.Naziv}");
                MNLog.Info(this.ControllerContext.RouteData.Values["controller"].ToString(), $"Ubacivanje u bazu: {tip.Naziv}");

                return RedirectToAction("Index");
            }

            ViewBag.NadtipId = new SelectList(db.Tip, "Id", "Naziv", tip.NadtipId);
            return View(tip);
        }

        // GET: TipVozila/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Tip tip = db.Tip.Find(id);
            if (tip == null)
            {
                return HttpNotFound();
            }
            ViewBag.NadtipId = new SelectList(db.Tip, "Id", "Naziv", tip.NadtipId);
            return View(tip);
        }

        // POST: TipVozila/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,NadtipId,Naziv")] Tip tip)
        {
            if (ModelState.IsValid)
            {
                db.Entry(tip).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.NadtipId = new SelectList(db.Tip, "Id", "Naziv", tip.NadtipId);
            return View(tip);
        }

        // GET: TipVozila/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Tip tip = db.Tip.Find(id);
            if (tip == null)
            {
                return HttpNotFound();
            }
            return View(tip);
        }

        // POST: TipVozila/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Tip tip = db.Tip.Find(id);
            db.Tip.Remove(tip);
            db.SaveChanges();

            logger.Info($"Brisanje iz baze: {tip.Naziv}");
            MNLog.Info(this.ControllerContext.RouteData.Values["controller"].ToString(), $"Brisanje iz baze: {tip.Naziv}");

            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        public static void UndoingChangesDbContextLevel(DbContext context)
        {
            foreach (DbEntityEntry entry in context.ChangeTracker.Entries())
            {
                switch (entry.State)
                {
                    case EntityState.Modified:
                        entry.State = EntityState.Unchanged;
                        break;
                    case EntityState.Added:
                        entry.State = EntityState.Detached;
                        break;
                    case EntityState.Deleted:
                        entry.Reload();
                        break;
                    default: break;
                }
            }
        }


        protected override void OnException(ExceptionContext filterContext)
        {
            Exception ex = filterContext.Exception;
            filterContext.ExceptionHandled = true;

            var controller = filterContext.RouteData.Values["controller"].ToString();
            var action = filterContext.RouteData.Values["action"].ToString();
            var model = new HandleErrorInfo(ex, controller, action);

            logger.Error($"Dogodila se greška: {model.Exception.Message}");

            MNLog.Error(this.ControllerContext.RouteData.Values["controller"].ToString(), model.Exception.Message);

            filterContext.Result = new ViewResult
            {
                ViewName = "Error",
                ViewData = new ViewDataDictionary(model)
            };
        }
    }
}
