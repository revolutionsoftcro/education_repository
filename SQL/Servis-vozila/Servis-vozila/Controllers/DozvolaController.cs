﻿using NLog;
using Servis_vozila.Models;
using System;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web.Mvc;

namespace Servis_vozila.Controllers
{
    public class DozvolaController : Controller
    {
        //dependencies : NLog_database() , NLog_html() ili NLog_combine()
        MojNLog MNLog = new MojNLog(new NLog_combine());
        private static Logger logger = LogManager.GetCurrentClassLogger();

        private Servis_vozilaEntities db = new Servis_vozilaEntities();

        // GET: Dozvola
        public ActionResult Index()
        {
            logger.Info("Ispis svih dozvola.");
            MNLog.Info(this.ControllerContext.RouteData.Values["controller"].ToString(), "Ispis svih dozvola.");
            var dozvola = db.Dozvola.Include(d => d.Tip).Include(d => d.Zaposlenik);
            return View(dozvola.ToList());
        }

        // GET: Dozvola/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Dozvola dozvola = db.Dozvola.Find(id);
            if (dozvola == null)
            {
                return HttpNotFound();
            }
            return View(dozvola);
        }

        // GET: Dozvola/Create
        public ActionResult Create()
        {
            ViewBag.TipId = new SelectList(db.Tip, "Id", "Naziv");
            ViewBag.ZapId = new SelectList(db.Zaposlenik, "Id", "PunoIme");
            return View();
        }

        // POST: Dozvola/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,ZapId,TipId")] Dozvola dozvola)
        {
            if (ModelState.IsValid)
            {
                db.Dozvola.Add(dozvola);
                db.SaveChanges();

                Zaposlenik zaposlenik = db.Zaposlenik.Find(dozvola.ZapId);
                Tip tip = db.Tip.Find(dozvola.TipId);


                logger.Info($"Ubacivanje u bazu: {zaposlenik.Ime} {zaposlenik.Prezime} {tip.Naziv}");
                MNLog.Info(this.ControllerContext.RouteData.Values["controller"].ToString(), $"Ubacivanje u bazu: {zaposlenik.Ime} {zaposlenik.Prezime} {tip.Naziv}");

                return RedirectToAction("Index");
            }

            ViewBag.TipId = new SelectList(db.Tip, "Id", "Naziv", dozvola.TipId);
            ViewBag.ZapId = new SelectList(db.Zaposlenik, "Id", "Ime", dozvola.ZapId);
            return View(dozvola);
        }

        // GET: Dozvola/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Dozvola dozvola = db.Dozvola.Find(id);
            if (dozvola == null)
            {
                return HttpNotFound();
            }
            ViewBag.TipId = new SelectList(db.Tip, "Id", "Naziv", dozvola.TipId);
            ViewBag.ZapId = new SelectList(db.Zaposlenik, "Id", "Ime", dozvola.ZapId);
            return View(dozvola);
        }

        // POST: Dozvola/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,ZapId,TipId")] Dozvola dozvola)
        {
            if (ModelState.IsValid)
            {
                db.Entry(dozvola).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.TipId = new SelectList(db.Tip, "Id", "Naziv", dozvola.TipId);
            ViewBag.ZapId = new SelectList(db.Zaposlenik, "Id", "Ime", dozvola.ZapId);
            return View(dozvola);
        }

        // GET: Dozvola/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Dozvola dozvola = db.Dozvola.Find(id);
            if (dozvola == null)
            {
                return HttpNotFound();
            }
            return View(dozvola);
        }

        // POST: Dozvola/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Dozvola dozvola = db.Dozvola.Find(id);
            db.Dozvola.Remove(dozvola);
            db.SaveChanges();

            Zaposlenik zaposlenik = db.Zaposlenik.Find(dozvola.ZapId);
            Tip tip = db.Tip.Find(dozvola.TipId);

            logger.Info($"Brisanje iz baze: {zaposlenik.Ime} {zaposlenik.Prezime} {tip.Naziv}");
            MNLog.Info(this.ControllerContext.RouteData.Values["controller"].ToString(), $"Brisanje iz baze: {zaposlenik.Ime} {zaposlenik.Prezime} {tip.Naziv}");

            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        protected override void OnException(ExceptionContext filterContext)
        {
            Exception ex = filterContext.Exception;
            filterContext.ExceptionHandled = true;

            var controller = filterContext.RouteData.Values["controller"].ToString();
            var action = filterContext.RouteData.Values["action"].ToString();
            var model = new HandleErrorInfo(ex, controller, action);

            logger.Error($"Dogodila se greška: {model.Exception.Message}");

            MNLog.Error(this.ControllerContext.RouteData.Values["controller"].ToString(), model.Exception.Message);

            filterContext.Result = new ViewResult
            {
                ViewName = "Error",
                ViewData = new ViewDataDictionary(model)
            };
        }

    }
}
