﻿using NLog;
using Servis_vozila.Models;
using System;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web.Mvc;

namespace Servis_vozila.Controllers
{
    public class VozilaController : Controller
    {
        //dependencies : NLog_database() , NLog_html() ili NLog_combine()
        MojNLog MNLog = new MojNLog(new NLog_combine());
        private static Logger logger = LogManager.GetCurrentClassLogger();

        private Servis_vozilaEntities db = new Servis_vozilaEntities();      

        public ViewResult Izbornik()
        {
            return View();
        }
        public ActionResult Terenska()
        {
            logger.Info("Ispis terenskih vozila.");
            MNLog.Info(this.ControllerContext.RouteData.Values["controller"].ToString(), "Ispis terenskih vozila.");
            var tblVozila = db.Vozilo.Where(o => o.Tip.Naziv == "Terensko" || o.Tip.Tip2.Naziv == "Terensko").ToList();
            return View("Index", tblVozila.ToList());
        }

        public ActionResult Leteca()
        {
            logger.Info("Ispis letećih vozila.");
            MNLog.Info(this.ControllerContext.RouteData.Values["controller"].ToString(), "Ispis letećih vozila.");
            var tblVozila = db.Vozilo.Where(o => o.Tip.Naziv == "Leteće" || o.Tip.Tip2.Naziv == "Leteće").ToList();
            return View("Index", tblVozila.ToList());
        }

        // GET: Vozila
        public ActionResult Index()
        {
            logger.Info("Ispis svih vozila.");
            MNLog.Info(this.ControllerContext.RouteData.Values["controller"].ToString(), "Ispis svih vozila.");
            var vozilo = db.Vozilo.Include(v => v.Tip);
            return View(vozilo.ToList());
        }

        // GET: Vozila/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Vozilo vozilo = db.Vozilo.Find(id);
            if (vozilo == null)
            {
                return HttpNotFound();
            }
            return View(vozilo);
        }

        // GET: Vozila/Create
        public ActionResult Create()
        {
            ViewBag.TipId = new SelectList(db.Tip, "Id", "Naziv");
            return View();
        }

        // POST: Vozila/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,TipId,Naziv,Boja")] Vozilo vozilo)
        {
            if (ModelState.IsValid)
            {
                db.Vozilo.Add(vozilo);
                db.SaveChanges();

                logger.Info($"Ubacivanje u bazu: {vozilo.Naziv}");
                MNLog.Info(this.ControllerContext.RouteData.Values["controller"].ToString(), $"Ubacivanje u bazu: {vozilo.Naziv}");

                return RedirectToAction("Index");
            }

            ViewBag.TipId = new SelectList(db.Tip, "Id", "Naziv", vozilo.TipId);
            return View(vozilo);
        }

        // GET: Vozila/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Vozilo vozilo = db.Vozilo.Find(id);
            if (vozilo == null)
            {
                return HttpNotFound();
            }
            ViewBag.TipId = new SelectList(db.Tip, "Id", "Naziv", vozilo.TipId);
            return View(vozilo);
        }

        // POST: Vozila/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,TipId,Naziv,Boja")] Vozilo vozilo)
        {
            if (ModelState.IsValid)
            {
                db.Entry(vozilo).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.TipId = new SelectList(db.Tip, "Id", "Naziv", vozilo.TipId);
            return View(vozilo);
        }

        // GET: Vozila/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Vozilo vozilo = db.Vozilo.Find(id);
            if (vozilo == null)
            {
                return HttpNotFound();
            }
            return View(vozilo);
        }

        // POST: Vozila/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Vozilo vozilo = db.Vozilo.Find(id);
            db.Vozilo.Remove(vozilo);
            db.SaveChanges();

            logger.Info($"Brisanje iz baze: {vozilo.Naziv}");
            MNLog.Info(this.ControllerContext.RouteData.Values["controller"].ToString(), $"Brisanje iz baze: {vozilo.Naziv}");

            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        protected override void OnException(ExceptionContext filterContext)
        {
            Exception ex = filterContext.Exception;
            filterContext.ExceptionHandled = true;

            var controller = filterContext.RouteData.Values["controller"].ToString();
            var action = filterContext.RouteData.Values["action"].ToString();
            var model = new HandleErrorInfo(ex, controller, action);

            logger.Error($"Dogodila se greška: {model.Exception.Message}");

            MNLog.Error(this.ControllerContext.RouteData.Values["controller"].ToString(), model.Exception.Message);

            filterContext.Result = new ViewResult
            {
                ViewName = "Error",
                ViewData = new ViewDataDictionary(model)
            };
        }

    }
}
