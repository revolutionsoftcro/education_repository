﻿using NLog;
using Servis_vozila.Models;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web.Mvc;

namespace Servis_vozila.Controllers
{
    public class VoziController : Controller
    {
        //dependencies : NLog_database() , NLog_html() ili NLog_combine()
        MojNLog MNLog = new MojNLog(new NLog_combine());
        private static Logger logger = LogManager.GetCurrentClassLogger();

        private Servis_vozilaEntities db = new Servis_vozilaEntities();

        // GET: Vozi
        public ActionResult Index()
        {
            logger.Info("Ispis naloga za vožnju.");
            MNLog.Info(this.ControllerContext.RouteData.Values["controller"].ToString(), "Ispis naloga za vožnju.");
            var vozi = db.Vozi.Include(v => v.Vozilo).Include(v => v.Zaposlenik);
            return View(vozi.ToList());
        }

        // GET: Vozi/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Vozi vozi = db.Vozi.Find(id);
            if (vozi == null)
            {
                return HttpNotFound();
            }
            return View(vozi);
        }

        // GET: Vozi/Create
        public ActionResult Create()
        {
            ViewBag.VoziloId = new SelectList(db.Vozilo, "Id", "Naziv");
            ViewBag.ZapId = new SelectList(db.Zaposlenik, "Id", "PunoIme");
            return View();
        }

        // POST: Vozi/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,ZapId,VoziloId")] Vozi vozi)
        {
            if (ModelState.IsValid)
            {
                db.Vozi.Add(vozi);
                db.SaveChanges();

                Zaposlenik zaposlenik = db.Zaposlenik.Find(vozi.ZapId);
                Vozilo vozilo = db.Vozilo.Find(vozi.VoziloId);

                logger.Info($"Ubacivanje u bazu: {zaposlenik.Ime} {zaposlenik.Prezime} {vozilo.Naziv}");
                MNLog.Info(this.ControllerContext.RouteData.Values["controller"].ToString(), $"Ubacivanje u bazu: {zaposlenik.Ime} {zaposlenik.Prezime} {vozilo.Naziv}");

                return RedirectToAction("Index");
            }

            ViewBag.VoziloId = new SelectList(db.Vozilo, "Id", "Naziv", vozi.VoziloId);
            ViewBag.ZapId = new SelectList(db.Zaposlenik, "Id", "PunoIme", vozi.ZapId);
            return View(vozi);
        }

        // GET: Vozi/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Vozi vozi = db.Vozi.Find(id);
            if (vozi == null)
            {
                return HttpNotFound();
            }
            ViewBag.VoziloId = new SelectList(db.Vozilo, "Id", "Naziv", vozi.VoziloId);
            ViewBag.ZapId = new SelectList(db.Zaposlenik, "Id", "Ime", vozi.ZapId);
            return View(vozi);
        }

        // POST: Vozi/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,ZapId,VoziloId")] Vozi vozi)
        {
            if (ModelState.IsValid)
            {
                db.Entry(vozi).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.VoziloId = new SelectList(db.Vozilo, "Id", "Naziv", vozi.VoziloId);
            ViewBag.ZapId = new SelectList(db.Zaposlenik, "Id", "Ime", vozi.ZapId);
            return View(vozi);
        }

        // GET: Vozi/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Vozi vozi = db.Vozi.Find(id);
            if (vozi == null)
            {
                return HttpNotFound();
            }
            return View(vozi);
        }

        // POST: Vozi/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Vozi vozi = db.Vozi.Find(id);
            db.Vozi.Remove(vozi);
            db.SaveChanges();

            Zaposlenik zaposlenik = db.Zaposlenik.Find(vozi.ZapId);
            Vozilo vozilo = db.Vozilo.Find(vozi.VoziloId);

            logger.Info($"Brisanje iz baze: {zaposlenik.Ime} {zaposlenik.Prezime} {vozilo.Naziv}");
            MNLog.Info(this.ControllerContext.RouteData.Values["controller"].ToString(), $"Brisanje iz baze: {zaposlenik.Ime} {zaposlenik.Prezime} {vozilo.Naziv}");

            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
