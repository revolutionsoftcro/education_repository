﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using NLog;
using Servis_vozila.Models;

namespace Servis_vozila.Controllers
{
    public class ZaposlenikController : Controller
    {
        //dependencies : NLog_database() , NLog_html() ili NLog_combine()
        MojNLog MNLog = new MojNLog(new NLog_combine());
        private static Logger logger = LogManager.GetCurrentClassLogger();

        private Servis_vozilaEntities db = new Servis_vozilaEntities();

        public ViewResult Izbornik()
        {
            return View();
        }

        public ActionResult Terenski()
        {
            logger.Info("Ispis terenskih zaposlenika.");
            MNLog.Info(this.ControllerContext.RouteData.Values["controller"].ToString(), "Ispis terenskih zaposlenika.");
            return View("Index", db.Zaposlenik.Where(o => o.Dozvola.Any(p => p.Tip.Naziv == "Terensko")).ToList());
        }

        public ActionResult Leteci()
        {
            logger.Info("Ispis letećih zaposlenika.");
            MNLog.Info(this.ControllerContext.RouteData.Values["controller"].ToString(), "Ispis letećih zaposlenika.");
            return View("Index", db.Zaposlenik.Where(o => o.Dozvola.Any(p => p.Tip.Naziv == "Leteće")).ToList());
        }

        public ActionResult Tenkisti()
        {
            logger.Info("Ispis tenkista.");
            MNLog.Info(this.ControllerContext.RouteData.Values["controller"].ToString(), "Ispis tenkista.");
            return View("Index", db.Zaposlenik.Where(o => o.Dozvola.Any(p => p.Tip.Naziv == "Tenk")).ToList());
        }

        public ActionResult PilotiZ()
        {
            logger.Info("Ispis pilota zrakoplova.");
            MNLog.Info(this.ControllerContext.RouteData.Values["controller"].ToString(), "Ispis pilota zrakoplova.");
            return View("Index", db.Zaposlenik.Where(o => o.Dozvola.Any(p => p.Tip.Naziv == "Zrakoplov")).ToList());
        }

        public ActionResult PilotiH()
        {
            logger.Info("Ispis pilota helikoptera.");
            MNLog.Info(this.ControllerContext.RouteData.Values["controller"].ToString(), "Ispis pilota helikoptera.");
            return View("Index", db.Zaposlenik.Where(o => o.Dozvola.Any(p => p.Tip.Naziv == "Helikopter")).ToList());
        }

        // GET: Zaposlenik
        public ActionResult Index()
        {
            logger.Info("Ispis svih zaposlenika.");
            MNLog.Info(this.ControllerContext.RouteData.Values["controller"].ToString(), "Ispis svih zaposlenika.");
            return View(db.Zaposlenik.ToList());
        }

        // GET: Zaposlenik/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Zaposlenik zaposlenik = db.Zaposlenik.Find(id);
            if (zaposlenik == null)
            {
                return HttpNotFound();
            }
            return View(zaposlenik);
        }

        // GET: Zaposlenik/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Zaposlenik/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,Ime,Prezime,Kontakt")] Zaposlenik zaposlenik)
        {
            if (ModelState.IsValid)
            {
                db.Zaposlenik.Add(zaposlenik);
                db.SaveChanges();

                logger.Info($"Ubacivanje u bazu: {zaposlenik.Ime} {zaposlenik.Prezime}");
                MNLog.Info(this.ControllerContext.RouteData.Values["controller"].ToString(), $"Ubacivanje u bazu: {zaposlenik.Ime} {zaposlenik.Prezime}");

                return RedirectToAction("Index");
            }

            return View(zaposlenik);
        }

        // GET: Zaposlenik/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Zaposlenik zaposlenik = db.Zaposlenik.Find(id);
            if (zaposlenik == null)
            {
                return HttpNotFound();
            }
            return View(zaposlenik);
        }

        // POST: Zaposlenik/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,Ime,Prezime,Kontakt")] Zaposlenik zaposlenik)
        {
            if (ModelState.IsValid)
            {
                db.Entry(zaposlenik).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(zaposlenik);
        }

        // GET: Zaposlenik/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Zaposlenik zaposlenik = db.Zaposlenik.Find(id);
            if (zaposlenik == null)
            {
                return HttpNotFound();
            }
            return View(zaposlenik);
        }

        // POST: Zaposlenik/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Zaposlenik zaposlenik = db.Zaposlenik.Find(id);
            db.Zaposlenik.Remove(zaposlenik);
            db.SaveChanges();

            logger.Info($"Brisanje iz baze: {zaposlenik.Ime} {zaposlenik.Prezime}");
            MNLog.Info(this.ControllerContext.RouteData.Values["controller"].ToString(), $"Brisanje iz baze: {zaposlenik.Ime} {zaposlenik.Prezime}");

            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        protected override void OnException(ExceptionContext filterContext)
        {
            Exception ex = filterContext.Exception;
            filterContext.ExceptionHandled = true;

            var controller = filterContext.RouteData.Values["controller"].ToString();
            var action = filterContext.RouteData.Values["action"].ToString();
            var model = new HandleErrorInfo(ex, controller, action);

            logger.Error($"Dogodila se greška: {model.Exception.Message}");

            MNLog.Error(this.ControllerContext.RouteData.Values["controller"].ToString(), model.Exception.Message);

            filterContext.Result = new ViewResult
            {
                ViewName = "Error",
                ViewData = new ViewDataDictionary(model)
            };
        }

    }
}
