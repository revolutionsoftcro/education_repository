﻿using Servis_vozila.Models;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web.Mvc;

namespace Servis_vozila.Controllers
{
    public class Moj_NlogController : Controller
    {
        private Servis_vozilaEntities db = new Servis_vozilaEntities();

        // GET: Moj_Nlog
        public ActionResult Index()
        {
            return View(db.Moj_Nlog.ToList());
        }

        // GET: Moj_Nlog/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Moj_Nlog moj_Nlog = db.Moj_Nlog.Find(id);
            if (moj_Nlog == null)
            {
                return HttpNotFound();
            }
            return View(moj_Nlog);
        }

        // GET: Moj_Nlog/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Moj_Nlog/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,Tip,Vrijeme,Kontroler,Poruka")] Moj_Nlog moj_Nlog)
        {
            if (ModelState.IsValid)
            {
                db.Moj_Nlog.Add(moj_Nlog);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(moj_Nlog);
        }

        // GET: Moj_Nlog/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Moj_Nlog moj_Nlog = db.Moj_Nlog.Find(id);
            if (moj_Nlog == null)
            {
                return HttpNotFound();
            }
            return View(moj_Nlog);
        }

        // POST: Moj_Nlog/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,Tip,Vrijeme,Kontroler,Poruka")] Moj_Nlog moj_Nlog)
        {
            if (ModelState.IsValid)
            {
                db.Entry(moj_Nlog).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(moj_Nlog);
        }

        // GET: Moj_Nlog/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Moj_Nlog moj_Nlog = db.Moj_Nlog.Find(id);
            if (moj_Nlog == null)
            {
                return HttpNotFound();
            }
            return View(moj_Nlog);
        }

        // POST: Moj_Nlog/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Moj_Nlog moj_Nlog = db.Moj_Nlog.Find(id);
            db.Moj_Nlog.Remove(moj_Nlog);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
