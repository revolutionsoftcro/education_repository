use master
go

create database dbServisVozila
go

use dbServisVozila
go

create table tblTipVozila(
Id			int
			primary key identity (1,1)
, NadtipId	int	null
, Naziv		nchar(20) not null
, foreign key (NadtipId) references tblTipVozila(Id)
)

create table tblVozila(
Id			int
			primary key identity (1,1)
, TipId		int not null
, Naziv		nchar(20) not null 
, Boja		nchar(20) not null
, foreign key (TipId) references tblTipVozila(Id)
)

create table tblVozaci(
Id			int
			primary key identity (1,1)
, TipId		int not null
, Ime		nchar(25) not null
, Prezime	nchar(25) not null
, Kontakt	nchar(25) not null
, foreign key (TipId) references tblTipVozila(Id)
)

create table tblVozi(
Id			int
			primary key identity (1,1)
, VozacId	int not null
, VoziloId  int not null
, foreign key (VozacId) references tblVozaci(Id)
, foreign key (VoziloId) references tblVozila(Id),
unique(VozacId, VoziloId)
)



