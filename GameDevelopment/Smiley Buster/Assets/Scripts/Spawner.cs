﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawner : MonoBehaviour
{
    public GameObject comet;
    public float spawnTime = 0.2f;
    public float spawnDelay = 1f;
    // Start is called before the first frame update
    void Start()
    {
        InvokeRepeating("SpawnComet", spawnTime, spawnDelay);
    }

    public void SpawnComet()
    {
        Instantiate(comet, new Vector3(Random.Range(-8f, 8f), transform.position.y, transform.position.z), Quaternion.identity);
    }
}
