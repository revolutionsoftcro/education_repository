﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MenuManager : MonoBehaviour
{
    public Player player;
    public Spawner spawner;
    public Image menu;
    public GameObject GUI;
    public GameObject gameOverPanel;
    public Text scoreText;
    public Text endScoreText;
    int score;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void UpdateScore()
    {
        scoreText.text = "Score: " + player.score;
    }

    public void StartGame()
    {
        player.enabled = true;
        spawner.enabled = true;
        menu.gameObject.SetActive(false);
        GUI.gameObject.SetActive(true);
    }

    public void EndGame()
    {
        score = player.score;
        player.gameObject.SetActive(false);
        spawner.enabled = false;
        GUI.SetActive(false);
        endScoreText.text = "Your Score: " + score;
        gameOverPanel.SetActive(true);
    }

    public void ExitToMainMenu()
    {
        SceneManager.LoadSceneAsync(0);
    }
    
    public void ExitGame()
    {
        Application.Quit();
    }
}
