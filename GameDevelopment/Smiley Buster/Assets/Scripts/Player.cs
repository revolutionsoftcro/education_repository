﻿using System.Collections;
using System.Collections.Generic;
using System.Security.Cryptography;
using UnityEditor;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Player : MonoBehaviour
{
    public float speed = 10f;
    public float rotationSpeed = 40f;
    public GameObject bulletOrigin;
    public GameObject bullet;
    public Health health;
    public int score;
    public GameObject gameOverPanel;
    MenuManager menuManager;

    // Start is called before the first frame update
    void Start()
    {
        menuManager = FindObjectOfType<MenuManager>();
    }

    // Update is called once per frame
    void Update()
    {
        if (health.health <= 0)
        {
            menuManager.EndGame();
        }
        if (Input.GetKey(KeyCode.A))
        {
            transform.Translate(Vector3.right * speed * Time.deltaTime * -1f, Space.World);
            if (transform.position.x < -9)
            {
                transform.SetPositionAndRotation(new Vector3(-transform.position.x, transform.position.y, transform.position.z), Quaternion.identity);
            }
        } else if (Input.GetKey(KeyCode.D))
        {
            transform.Translate(Vector3.right * speed * Time.deltaTime * 1f, Space.World);
            if (transform.position.x > 9)
            {
                transform.SetPositionAndRotation(new Vector3(-transform.position.x, transform.position.y, transform.position.z), Quaternion.identity);
            }
        }
        if (Input.GetMouseButtonDown(0))
        {
            Shoot();
        }
        Vector3 diff = Camera.main.ScreenToWorldPoint(Input.mousePosition) - transform.position;
        diff.Normalize();

        float rot_z = Mathf.Atan2(diff.y, diff.x) * Mathf.Rad2Deg;
        transform.rotation = Quaternion.Euler(0f, 0f, rot_z - 90);
    }

    public void Shoot()
    {
        Instantiate(bullet, new Vector2(bulletOrigin.transform.position.x, bulletOrigin.transform.position.y), gameObject.transform.rotation);
        
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag("smiley"))
        {
            health.health--;
            Destroy(other.gameObject);
        }
    }
}
