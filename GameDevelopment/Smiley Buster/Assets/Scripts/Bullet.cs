﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour
{
    public float bulletSpeed = 10f;
    Player player;
    MenuManager menuManager;
    // Start is called before the first frame update
    void Start()
    {
        Destroy(gameObject, 3f);
        player = FindObjectOfType<Player>();
        menuManager = FindObjectOfType<MenuManager>();
    }

    // Update is called once per frame
    void Update()
    {
        transform.Translate(Vector3.up * bulletSpeed * Time.deltaTime);
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag("smiley"))
        {
            Destroy(other.gameObject);
            player.score++;
            menuManager.UpdateScore();
        }
    }
}
