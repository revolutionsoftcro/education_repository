package hr.robic.tomislav.functions;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;

import static org.junit.Assert.*;

@RunWith(MockitoJUnitRunner.class)
public class FunctionsTest {

    Functions functions = null;

    @Before
    public void setUp() throws Exception {
        functions = new Functions();
    }

    @Test
    public void fetch_first_paragraph() {
        String description = "<p>test</p><p>gdfgdgdf<a>gdfgfd</a>fsdf</p>";
        assertEquals(functions.fetchFirstParagraphFromDescription(description), "test");
    }

    @After
    public void tearDown() throws Exception {
        functions = null;
    }
}