package hr.robic.tomislav.ecarsproject;

import android.view.View;

import androidx.fragment.app.Fragment;
import androidx.test.rule.ActivityTestRule;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;

import static org.junit.Assert.*;

public class HostActivityTest {

    @Rule
    public ActivityTestRule<HostActivity> mActivityTestRule = new ActivityTestRule<HostActivity>(HostActivity.class);

    private HostActivity mActivity = null;

    @Before
    public void setUp() throws Exception {
        mActivity = mActivityTestRule.getActivity();
    }

    @Test
    public void testLaunch() {
        View fragment = mActivity.findViewById(R.id.navHostFragment);
        assertNotNull(fragment);
    }

    @After
    public void tearDown() throws Exception {
        mActivity = null;
    }
}