package hr.robic.tomislav.ecarsproject.presenter;

import android.content.Context;
import android.content.Intent;

import java.util.ArrayList;
import java.util.List;

import hr.robic.tomislav.ecarsproject.ArticlesContract;
import hr.robic.tomislav.ecarsproject.ViewArticleContract;
import hr.robic.tomislav.ecarsproject.view.ArticlePagerActivity;
import hr.robic.tomislav.model.Article;

public class ArticlesPresenterImpl implements ArticlesContract.ArticlesPresenter,
        ArticlesContract.ArticlesModel.OnArticlesLoadedListener {

    private final List<Article> articles = new ArrayList<>();

    private Context context;
    private ArticlesContract.ListArticlesView view;
    private ArticlesContract.ArticlesModel model;

    public ArticlesPresenterImpl(Context context, ArticlesContract.ListArticlesView view, ArticlesContract.ArticlesModel model) {
        this.context = context;
        this.view = view;
        this.model = model;
    }

    @Override
    public void requestToLoadArticles() {
        view.showProgress();
                Thread thread = new Thread() {
            @Override
            public void run() {
                try {
                    model.loadArticles(ArticlesPresenterImpl.this::onArticlesLoaded);

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        };

        thread.start();

    }

    @Override
    public void onArticlesLoaded(List<Article> articles) {
        this.articles.clear();
        this.articles.addAll(articles);
        view.showArticles();
    }

    @Override
    public int getArticlesCount() {
        return articles.size();
    }

    @Override
    public void bindSingleArticle(ArticlesContract.SingleArticleView view, int position) {
        Article article = articles.get(position);
        view.setTitle(article.getTitle());
        view.setImage(article.getPicturePath());
    }

    @Override
    public void delete(int position) {
        Article article = articles.remove(position);
        model.deleteArticle(article);
        view.showArticles();
    }

    @Override
    public void view(int position) {
        Intent intent = new Intent(context, ArticlePagerActivity.class);
        intent.putExtra(ViewArticleContract.ArticleView.ARTICLE_POSITION, position);
        context.startActivity(intent);
    }

    @Override
    public void onDestroy() {
        view = null;
    }

}