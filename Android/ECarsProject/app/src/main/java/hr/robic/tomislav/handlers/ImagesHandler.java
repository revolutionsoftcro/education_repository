package hr.robic.tomislav.handlers;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.net.HttpURLConnection;

import hr.robic.tomislav.factory.UrlConnectionFactory;

public class ImagesHandler {

    private static final int ARTICLE_PICTURE_WIDTH = 1000;
    private static final int ARTICLE_PICTURE_HEIGHT = 750;
    private static final String EXTENSION = ".jpg";
    private static final int QUALITY = 100;

    public static String downloadImageAndStore(Context context, String url,
                                               String fileName, int timeout,
                                               String requestMethod) {

        File file = getFile(context, fileName);

        try {
            HttpURLConnection con = UrlConnectionFactory.createHttpUrlConnection(
                    url,
                    timeout,
                    requestMethod
            );
            try(InputStream is = con.getInputStream();
                FileOutputStream fos = new FileOutputStream(file)) {

                Bitmap bitmap = BitmapFactory.decodeStream(is);
                // now we have bitmap
                // we need resized bitmap to our size!!!
                Bitmap resizedBitmap = getResizedBitmap(bitmap,
                        ARTICLE_PICTURE_WIDTH, ARTICLE_PICTURE_HEIGHT);

                byte[] buffer = getBytesFromBitmap(resizedBitmap);
                fos.write(buffer);
                return file.getAbsolutePath();
            } // closed
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    private static Bitmap getResizedBitmap(Bitmap bitmap, int newWidth, int newHeight) {
        int width = bitmap.getWidth();
        int height = bitmap.getHeight();
        float scaleWidth = (float)newWidth / width;
        float scaleHeight = (float)newHeight / height;
        Matrix matrix = new Matrix();
        matrix.postScale(scaleWidth, scaleHeight);
        return Bitmap.createBitmap(bitmap, 0, 0, width, height, matrix, false);
    }

    private static byte[] getBytesFromBitmap(Bitmap bitmap) {
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, QUALITY, bos);
        return bos.toByteArray();
    }

    private static File getFile(Context context, String fileName) {
        File dir = context.getApplicationContext().getExternalFilesDir(null);
        File file = new File(dir, File.separator + fileName + EXTENSION);
        if (file.exists()) {
            file.delete();
        }
        return file;
    }

}
