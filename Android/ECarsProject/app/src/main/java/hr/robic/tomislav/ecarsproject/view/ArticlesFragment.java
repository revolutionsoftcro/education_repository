package hr.robic.tomislav.ecarsproject.view;

import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import butterknife.BindView;
import butterknife.ButterKnife;
import hr.robic.tomislav.ecarsproject.ArticlesContract;
import hr.robic.tomislav.ecarsproject.R;
import hr.robic.tomislav.ecarsproject.model.ArticlesModelImpl;
import hr.robic.tomislav.ecarsproject.presenter.ArticlesPresenterImpl;


/**
 * A simple {@link Fragment} subclass.
 */
public class ArticlesFragment extends Fragment implements ArticlesContract.ListArticlesView, ArticleTouchHelper.OnSwipedListener {

    private ArticlesContract.ArticlesPresenter presenter; // complete responsibility is mine
    private ArticlesAdapter adapter; // complete responsibility is mine

    @BindView(R.id.progressBar)
    ProgressBar progressBar;
    @BindView(R.id.rvArticles)
    RecyclerView rvArticles;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_articles, container, false);
        ButterKnife.bind(this, view);
        initPresenter();
        initAdapter();
        initTouchHelper();
        return view;
    }

    private void initTouchHelper() {
        ItemTouchHelper.SimpleCallback callback = new ArticleTouchHelper(0, ItemTouchHelper.LEFT, this);
        new ItemTouchHelper(callback).attachToRecyclerView(rvArticles);
    }

    private void initPresenter() {
        presenter = new ArticlesPresenterImpl(
                getContext(),
                this,
                new ArticlesModelImpl(getContext())
        );
        presenter.requestToLoadArticles();
    }

    private void initAdapter() {
        rvArticles.setLayoutManager(new LinearLayoutManager(getContext()));
        adapter = new ArticlesAdapter(presenter);
        rvArticles.setAdapter(adapter);
    }

    @Override
    public void showProgress() {
        progressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void showArticles() {
        progressBar.setVisibility(View.GONE);
        adapter.notifyDataSetChanged();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (presenter!=null) {
        presenter.onDestroy();}
    }

    @Override
    public void onSwiped(int position) {
        presenter.delete(position);
    }


}
