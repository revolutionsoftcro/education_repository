package hr.robic.tomislav.dao;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;

import java.util.List;

import hr.robic.tomislav.model.Article;

@Dao
public interface ArticleDao {

    @Query("SELECT * FROM article")
    List<Article> getArticles();

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insert(Article article);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertAllArticles(List<Article> articles);

    @Update
    void update(Article article);

    @Delete
    void delete(Article article);
}