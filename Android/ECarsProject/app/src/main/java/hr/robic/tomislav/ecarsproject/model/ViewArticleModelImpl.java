package hr.robic.tomislav.ecarsproject.model;

import android.content.Context;
import android.os.Handler;

import java.util.List;

import hr.robic.tomislav.dao.AppDatabase;
import hr.robic.tomislav.dao.AppExecutors;
import hr.robic.tomislav.ecarsproject.ViewArticleContract;
import hr.robic.tomislav.model.Article;

public class ViewArticleModelImpl implements ViewArticleContract.ViewArticleModel {

    private final Handler handler = new Handler();
    private Context context;

    public ViewArticleModelImpl(Context context) {
        this.context = context;
    }

    @Override
    public void loadArticles(OnArticlesLoadedListener onArticlesLoadedListener) {
        AppExecutors.getInstance().diskIO().execute(new Runnable() {
            @Override
            public void run() {
                onArticlesLoadedListener.onArticlesLoaded(AppDatabase.getInstance(context).articleDao().getArticles());
            }
        });
    }

    @Override
    public void saveArticle(Article article) {
        AppExecutors.getInstance().diskIO().execute(new Runnable() {
            @Override
            public void run() {
                AppDatabase.getInstance(context).articleDao().insert(article);
            }
        });
    }
}

