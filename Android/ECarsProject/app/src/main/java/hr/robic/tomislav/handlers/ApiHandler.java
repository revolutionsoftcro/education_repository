package hr.robic.tomislav.handlers;

import com.google.gson.Gson;
import org.json.JSONException;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;
import hr.robic.tomislav.ecarsproject.view.MapFragment;
import hr.robic.tomislav.model.Location;

public class ApiHandler  {

    public ApiHandler() throws MalformedURLException {
    }

    public static List<Location> getLocations() throws IOException, JSONException {
        Location[] locations = null;
        URL url = new URL("https://ecarsapi20200610143652.azurewebsites.net/api/Locations");
        HttpURLConnection conn = (HttpURLConnection)url.openConnection();
        conn.setRequestMethod("GET");
        conn.connect();
        int responseCode = conn.getResponseCode();

        if(responseCode != 200)
            throw new RuntimeException("HttpResponseCode: " +responseCode);
        else
        {
            Scanner sc = new Scanner(url.openStream());
            Gson gson = new Gson();

            while(sc.hasNext())
            {
                String json = sc.nextLine();

                locations = gson.fromJson(json, Location[].class);
                System.out.println(locations);
            }
            sc.close();

        }

        if (locations!=null) {
            return Arrays.asList(locations);
        }

        return null;
    }
}
