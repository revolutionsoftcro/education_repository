package hr.robic.tomislav.ecarsproject.view;

import android.graphics.Canvas;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.RecyclerView;

public class ArticleTouchHelper extends ItemTouchHelper.SimpleCallback {

    public interface OnSwipedListener {
        void onSwiped(int position);
    }

    private OnSwipedListener listener;

    public ArticleTouchHelper(int dragDirs, int swipeDirs, OnSwipedListener listener) {
        super(dragDirs, swipeDirs);
        this.listener = listener;
    }

    @Override
    public boolean onMove(@NonNull RecyclerView recyclerView, @NonNull RecyclerView.ViewHolder viewHolder, @NonNull RecyclerView.ViewHolder target) {
        return true;
    }

    @Override
    public void onSwiped(@NonNull RecyclerView.ViewHolder viewHolder, int direction) {
        listener.onSwiped(viewHolder.getAdapterPosition());
    }

    @Override
    public void onSelectedChanged(@Nullable RecyclerView.ViewHolder viewHolder, int actionState) {
        super.onSelectedChanged(viewHolder, actionState);
        if (viewHolder != null) {
            View viewForeground = ((ArticlesAdapter.ViewHolder)viewHolder).viewForeground;
            getDefaultUIUtil().onSelected(viewForeground);
        }

    }

    @Override
    public void onChildDrawOver(@NonNull Canvas c, @NonNull RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, float dX, float dY, int actionState, boolean isCurrentlyActive) {
        super.onChildDrawOver(c, recyclerView, viewHolder, dX, dY, actionState, isCurrentlyActive);
        if (viewHolder != null) {
            View viewForeground = ((ArticlesAdapter.ViewHolder)viewHolder).viewForeground;
            getDefaultUIUtil().onDrawOver(
                    c, recyclerView, viewForeground, dX, dY, actionState, isCurrentlyActive
            );
        }
    }

    @Override
    public void onChildDraw(@NonNull Canvas c, @NonNull RecyclerView recyclerView, @NonNull RecyclerView.ViewHolder viewHolder, float dX, float dY, int actionState, boolean isCurrentlyActive) {
        super.onChildDraw(c, recyclerView, viewHolder, dX, dY, actionState, isCurrentlyActive);
        if (viewHolder != null) {
            View viewForeground = ((ArticlesAdapter.ViewHolder)viewHolder).viewForeground;
            getDefaultUIUtil().onDraw(
                    c, recyclerView, viewForeground, dX, dY, actionState, isCurrentlyActive
            );
        }
    }

    @Override
    public void clearView(@NonNull RecyclerView recyclerView, @NonNull RecyclerView.ViewHolder viewHolder) {
        super.clearView(recyclerView, viewHolder);
        if (viewHolder != null) {
            View viewForeground = ((ArticlesAdapter.ViewHolder)viewHolder).viewForeground;
            getDefaultUIUtil().clearView(viewForeground);
        }
    }
}
