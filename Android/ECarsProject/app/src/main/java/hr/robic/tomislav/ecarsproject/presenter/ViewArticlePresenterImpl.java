package hr.robic.tomislav.ecarsproject.presenter;

import android.content.Context;

import org.jsoup.Jsoup;

import java.util.ArrayList;
import java.util.List;

import hr.robic.tomislav.ecarsproject.ViewArticleContract;
import hr.robic.tomislav.model.Article;

public class ViewArticlePresenterImpl implements ViewArticleContract.ViewArticlePresenter, ViewArticleContract.ViewArticleModel.OnArticlesLoadedListener {

    private final List<Article> articles = new ArrayList<>();

    private Context context;
    private ViewArticleContract.ArticleView view;
    private ViewArticleContract.ViewArticleModel model;

    public ViewArticlePresenterImpl(Context context, ViewArticleContract.ArticleView view, ViewArticleContract.ViewArticleModel model) {
        this.context = context;
        this.view = view;
        this.model = model;
    }

    @Override
    public void requestToLoadArticles() {
        model.loadArticles(this);
    }

    @Override
    public void onArticlesLoaded(List<Article> articles) {
        this.articles.clear();
        this.articles.addAll(articles);
        view.showArticle();
    }


    @Override
    public int getArticlesCount() { // i got articles cached (viewpager needs this)
        return articles.size();
    }

    @Override
    public void bindSingleArticle(ViewArticleContract.SingleArticleView view, int position) {
        Article article = articles.get(position);
        view.setTitle(article.getTitle());
        view.setDescription(Jsoup.parse(article.getDescription()).text());
        view.setRead(article.getRead());
        view.setImage(article.getPicturePath());
        view.setDateTime(article.getPublishedDateTime().format(Article.DATE_TIME_FORMATTER));
    }

    @Override
    public void toggleReadStatus(int position) {
        Article article = articles.get(position);
        article.setRead(article.getRead() != null && article.getRead() ? false : true);
        model.saveArticle(article);
    }

    @Override
    public void onDestroy() { // cleaup
        view = null;
    }

}