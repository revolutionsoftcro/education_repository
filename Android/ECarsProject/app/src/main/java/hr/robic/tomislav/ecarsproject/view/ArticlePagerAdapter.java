package hr.robic.tomislav.ecarsproject.view;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.squareup.picasso.Picasso;

import java.io.File;

import butterknife.BindView;
import butterknife.ButterKnife;
import hr.robic.tomislav.ecarsproject.R;
import hr.robic.tomislav.ecarsproject.ViewArticleContract;
import jp.wasabeef.picasso.transformations.RoundedCornersTransformation;

public class ArticlePagerAdapter extends RecyclerView.Adapter<ArticlePagerAdapter.ViewHolder> {

    private ViewArticleContract.ViewArticlePresenter presenter;

    public ArticlePagerAdapter(ViewArticleContract.ViewArticlePresenter presenter) {
        this.presenter = presenter;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.view_article, parent, false);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        handleRead(holder, position);
        presenter.bindSingleArticle(holder, position);
    }

    private void handleRead(ViewHolder holder, int position) {
        holder.ivRead.setOnClickListener((v) -> {
            presenter.toggleReadStatus(position);
            notifyDataSetChanged();
        });
    }

    @Override
    public int getItemCount() {
        return presenter.getArticlesCount();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements ViewArticleContract.SingleArticleView {

        @BindView(R.id.ivArticle)
        ImageView ivArticle;
        @BindView(R.id.ivRead)
        ImageView ivRead;
        @BindView(R.id.tvTitle)
        TextView tvTitle;
        @BindView(R.id.tvDate)
        TextView tvDate;
        @BindView(R.id.tvDescription)
        TextView tvDescription;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        @Override
        public void setTitle(String title) {
            tvTitle.setText(title);
        }

        @Override
        public void setDescription(String description) {
            tvDescription.setText(description);
        }

        @Override
        public void setImage(String path) {
            Picasso.get()
                    .load(new File(path))
                    .transform(new RoundedCornersTransformation(25, 5))
                    .into(ivArticle);
        }

        @Override
        public void setDateTime(String date) {
            tvDate.setText(date);
        }

        @Override
        public void setRead(Boolean read) {
            ivRead.setImageResource(read != null && read ? R.drawable.green_flag : R.drawable.red_flag);
        }
    }
}
