package hr.robic.tomislav.functions;

public class Functions {

    public static String fetchImageFromDescription(String description) {
        int index = description.indexOf("http");
        if (index != -1) {
            description = description.substring(index);
            index = description.indexOf("\"");
            if (index != -1) {
                description = description.substring(0, index);
            }
        }
        return description;
    }

    public static String fetchFirstParagraphFromDescription(String description) {
        int index = description.indexOf("<p>");
        if (index != -1) {
            description = description.substring(index + 3);
            index = description.indexOf("</p>");
            if (index != -1) {
                description = description.substring(0, index);
            }
        }
        return description;
    }

}
