package hr.robic.tomislav.handlers;

import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;

public class AnimationHandler {

    public static void handleAnimation(View view, int resourceId) {
        Animation animation = AnimationUtils.loadAnimation(view.getContext(), resourceId);
        view.startAnimation(animation);
    }
}
