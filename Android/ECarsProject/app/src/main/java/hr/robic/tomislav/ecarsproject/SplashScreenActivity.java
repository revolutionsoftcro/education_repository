package hr.robic.tomislav.ecarsproject;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.widget.ImageView;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import hr.robic.tomislav.handlers.AnimationHandler;
import hr.robic.tomislav.handlers.PreferenceHandler;

public class SplashScreenActivity extends AppCompatActivity {

    private final Handler handler = new Handler();
    private Runnable redirect;

    private static final long DELAY = 3000;
    @BindView(R.id.ivEcars)
    ImageView ivEcars;
    @BindView(R.id.tvMessage)
    TextView tvMessage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);

        ButterKnife.bind(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        startAnimations();
        redirect();
    }

    private void startAnimations() {
        AnimationHandler.handleAnimation(ivEcars, R.anim.bounce);
        AnimationHandler.handleAnimation(tvMessage, R.anim.shake);
    }

    private void redirect() {

        if (PreferenceHandler.getBooleanPreference(this, PreferenceHandler.DATA_IMPORTED)) {
            redirect = ()-> {
                Intent intent = new Intent(SplashScreenActivity.this, HostActivity.class);
                startActivity(intent);
            };
            handler.postDelayed(redirect, DELAY);
        } else {
            Intent intent = new Intent(this, RssService.class);
            startService(intent);
        }
    }

    @Override
    public void finish() {
        super.finish();
        handler.removeCallbacks(redirect);
    }
}
