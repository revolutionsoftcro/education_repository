package hr.robic.tomislav.parsers;

import android.content.Context;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

import hr.robic.tomislav.factory.ParserFactory;
import hr.robic.tomislav.factory.UrlConnectionFactory;
import hr.robic.tomislav.functions.Functions;
import hr.robic.tomislav.handlers.ImagesHandler;
import hr.robic.tomislav.model.Article;

public class RssArticleParser {

    private static final String RSS_URL = "https://eandt.theiet.org/rss/electric-vehicles";
    private static final int TIMEOUT = 60000;
    private static final String REQUEST_METHOD = "GET";
    private static final String ATTRIBUTE_URL = "url";
    private static final DateTimeFormatter DATE_TIME_FORMATTER = DateTimeFormatter.RFC_1123_DATE_TIME;


    public static List<Article> parse(Context context) throws IOException, XmlPullParserException {
        List<Article> articles = new ArrayList<>();

        HttpURLConnection con = UrlConnectionFactory.createHttpUrlConnection(RSS_URL, TIMEOUT, REQUEST_METHOD);
        XmlPullParser parser = ParserFactory.createXmlPullParser(con.getInputStream());

        Article article = null;
        TagType tagType = null;

        int event = parser.getEventType();

        while (event != XmlPullParser.END_DOCUMENT) {

            switch (event) {
                case XmlPullParser.START_TAG:
                    String name = parser.getName();
                    tagType = TagType.from(name);
                    break;
                case XmlPullParser.TEXT:
                    if (tagType != null) {
                        String text = parser.getText().trim();
                        switch (tagType) {
                            case ITEM:
                                article = new Article();
                                articles.add(article);
                                break;
                            case TITLE:
                                if (article != null && !text.isEmpty()) {
                                    article.setTitle(text);
                                }
                                break;
                            case DESCRIPTION:
                                if (article != null && !text.isEmpty()) {
                                    String url = Functions.fetchImageFromDescription(text);
                                    text = Functions.fetchFirstParagraphFromDescription(text);
                                    article.setDescription(text);
                                    article.setPicturePath(url);
                                    if (url != null) {
                                        String picturePath = ImagesHandler.downloadImageAndStore(
                                                context,
                                                url,
                                                Article.FILE_PREFIX + article.getTitle().hashCode(),
                                                TIMEOUT,
                                                REQUEST_METHOD
                                        );
                                        if (picturePath != null) {
                                            article.setPicturePath(picturePath);
                                        }
                                    }
                                }
                                break;
                            case PUB_DATE:
                                if (article != null && !text.isEmpty()) {
                                    text = text.replace(" +0000","GMT" );
                                    LocalDateTime publishedDateTime = LocalDateTime.parse(text, DATE_TIME_FORMATTER);
                                    article.setPublishedDateTime(publishedDateTime);
                                }
                                break;
                        }
                    }


                    break;
            }


            event = parser.next();
        }

        return articles;
    }

    private enum TagType {
        ITEM("item"),
        TITLE("title"),
        DESCRIPTION("description"),
        ENCLOSURE("enclosure"),
        PUB_DATE("pubDate");

        private final String name;

        TagType(String name) {
            this.name = name;
        }

        public static TagType from(String name) {
            for (TagType value : values()) {
                if (value.name.equals(name)) {
                    return value;
                }
            }
            return null;
        }
    }

}