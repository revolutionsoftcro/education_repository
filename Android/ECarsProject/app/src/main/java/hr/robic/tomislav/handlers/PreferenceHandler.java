package hr.robic.tomislav.handlers;

import android.content.Context;
import android.content.SharedPreferences;

import androidx.preference.PreferenceManager;

public class PreferenceHandler {

    public static final String DATA_IMPORTED = "hr.robic.tomislav.handlers.data_imported";

    public static boolean getBooleanPreference(Context context, String key) {
        return PreferenceManager.getDefaultSharedPreferences(context).getBoolean(key, false);
    }

    public static void setBooleanPreference(Context context, String key, boolean value) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        preferences
                .edit()
                .putBoolean(key, value)
                .commit();
    }
}