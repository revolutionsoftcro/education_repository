package hr.robic.tomislav.ecarsproject;

import java.util.Collection;

import hr.robic.tomislav.model.Location;

public interface MapContract {

    interface MapModel {
        interface OnPointsLoadedListener {
            void onPointsLoaded(Collection<Location> locations);
        }
        void loadPoints(OnPointsLoadedListener onPointsLoadedListener);
    }

    interface MapView {
        void addPoints(Collection<Location> locations);
    }

    interface MapPresenter {
        void requestToLoadPoints();
        void onDestroy();
    }

}