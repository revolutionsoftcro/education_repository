package hr.robic.tomislav.ecarsproject.view;

import android.content.Intent;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.viewpager2.widget.ViewPager2;

import butterknife.BindView;
import butterknife.ButterKnife;
import hr.robic.tomislav.ecarsproject.R;
import hr.robic.tomislav.ecarsproject.ViewArticleContract;
import hr.robic.tomislav.ecarsproject.model.ViewArticleModelImpl;
import hr.robic.tomislav.ecarsproject.presenter.ViewArticlePresenterImpl;

public class ArticlePagerActivity extends AppCompatActivity implements ViewArticleContract.ArticleView {

    private ViewArticleContract.ViewArticlePresenter presenter;

    private int initialPosition;

    @BindView(R.id.viewPager)
    ViewPager2 viewPager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        overridePendingTransition(android.R.anim.slide_in_left, android.R.anim.slide_out_right);
        setContentView(R.layout.activity_article_pager);

        ButterKnife.bind(this);
        handleInitialPosition(getIntent());
        initPresenter();
        handleBackMenu();

    }

    private void handleInitialPosition(Intent intent) {
        initialPosition = intent.getIntExtra(ARTICLE_POSITION, 0);
    }

    private void initPresenter() {
        presenter = new ViewArticlePresenterImpl(this, this, new ViewArticleModelImpl(this));
        presenter.requestToLoadArticles();
    }

    private void handleBackMenu() {
        getSupportActionBar().setDisplayHomeAsUpEnabled(true); // paint the arrow
    }

    @Override
    public boolean onSupportNavigateUp() { // handle the arrow
        onBackPressed();
        return super.onSupportNavigateUp();
    }

    @Override
    public void showArticle() {
        viewPager.setAdapter(new ArticlePagerAdapter(presenter));
        viewPager.setCurrentItem(initialPosition);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        presenter.onDestroy();
    }
}

