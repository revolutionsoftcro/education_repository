package hr.robic.tomislav.ecarsproject.model;

import org.json.JSONException;
import java.io.IOException;
import java.util.List;

import hr.robic.tomislav.ecarsproject.MapContract;
import hr.robic.tomislav.handlers.ApiHandler;
import hr.robic.tomislav.model.Location;

public class MapModelImpl implements MapContract.MapModel {

    @Override
    public void loadPoints(OnPointsLoadedListener onPointsLoadedListener) {
        Thread thread = new Thread(){
            public void run(){
                try {
                    List<Location> locations = ApiHandler.getLocations();
                    onPointsLoadedListener.onPointsLoaded(locations);
                } catch (IOException | JSONException e) {
                    e.printStackTrace();
                    onPointsLoadedListener.onPointsLoaded(null);
                }
            }
        };
        thread.start();
    }
}
