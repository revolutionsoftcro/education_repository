package hr.robic.tomislav.ecarsproject.view;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.Collection;

import hr.robic.tomislav.ecarsproject.MapContract;
import hr.robic.tomislav.ecarsproject.R;
import hr.robic.tomislav.ecarsproject.model.MapModelImpl;
import hr.robic.tomislav.ecarsproject.presenter.MapPresenterImpl;
import hr.robic.tomislav.model.Location;

public class MapFragment extends Fragment implements OnMapReadyCallback, MapContract.MapView {

    private MapContract.MapPresenter presenter;
    private static final float INIT_ZOOM_LEVEL = 11f;
    private GoogleMap mMap;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_map, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        SupportMapFragment mapFragment = (SupportMapFragment) getChildFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
        initPresenter();
    }

    private void initPresenter() {
        presenter = new MapPresenterImpl(this, new MapModelImpl());
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        configureMap();
        presenter.requestToLoadPoints();
    }

    private void configureMap() {
        mMap.getUiSettings().setZoomControlsEnabled(true);
        mMap.setMapType(GoogleMap.MAP_TYPE_HYBRID);
    }

    @Override
    public void addPoints(Collection<Location> locations) {
        locations.forEach(location -> addMarker(location));
    }

    private void addMarker(Location location) {
        MapFragment.this.getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                mMap.addMarker(new MarkerOptions().position(new LatLng(location.getLatitude(), location.getLongitude())).title(location.getTitle()));
                animateCamera(new LatLng(location.getLatitude(), location.getLongitude()));
            }
        });

    }
    private void animateCamera(LatLng latLng) {
        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, INIT_ZOOM_LEVEL));
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        presenter.onDestroy();
    }
}
