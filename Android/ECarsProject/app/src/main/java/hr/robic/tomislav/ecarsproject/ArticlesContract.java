package hr.robic.tomislav.ecarsproject;

import java.util.List;

import hr.robic.tomislav.model.Article;

public interface ArticlesContract {

    interface ArticlesModel {
        interface OnArticlesLoadedListener {
            void onArticlesLoaded(List<Article> articles);
        }
        void loadArticles(OnArticlesLoadedListener onArticlesLoadedListener);
        void deleteArticle(Article article);
    }

    interface ListArticlesView {
        void showProgress();
        void showArticles();
    }

    interface SingleArticleView {
        void setTitle(String title);
        void setImage(String path);
    }

    interface ArticlesPresenter {
        void requestToLoadArticles();
        int getArticlesCount();
        void bindSingleArticle(SingleArticleView view, int position);
        void delete(int position);
        void view(int position);
        void onDestroy();
    }
}
