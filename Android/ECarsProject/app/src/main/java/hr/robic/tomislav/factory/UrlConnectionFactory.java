package hr.robic.tomislav.factory;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;

public class UrlConnectionFactory {
    public static HttpURLConnection createHttpUrlConnection(String path, int timeout, String requestMethod)
            throws IOException {
        URL url = new URL(path);
        HttpURLConnection con = (HttpURLConnection)url.openConnection();
        con.setConnectTimeout(timeout);
        con.setReadTimeout(timeout);
        con.setRequestMethod(requestMethod);
        return con;
    }
}