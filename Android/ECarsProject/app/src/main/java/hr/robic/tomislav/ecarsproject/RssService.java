package hr.robic.tomislav.ecarsproject;

import android.app.IntentService;
import android.content.Intent;

import androidx.annotation.Nullable;

import java.util.List;

import hr.robic.tomislav.dao.AppDatabase;
import hr.robic.tomislav.handlers.PreferenceHandler;
import hr.robic.tomislav.model.Article;
import hr.robic.tomislav.parsers.RssArticleParser;


public class RssService extends IntentService {
    public RssService() {
        super(RssService.class.getName());
    }

    @Override
    protected void onHandleIntent(@Nullable Intent intent) {

        try {

            List<Article> articles = RssArticleParser.parse(this);
            AppDatabase.getInstance(this).articleDao().insertAllArticles(articles);
            articles = AppDatabase.getInstance(this).articleDao().getArticles();
            PreferenceHandler.setBooleanPreference(this, PreferenceHandler.DATA_IMPORTED, true);

        } catch (Exception e) {
            e.printStackTrace();
        }

        Intent i = new Intent(this, HostActivity.class);
        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(i);
    }
}
