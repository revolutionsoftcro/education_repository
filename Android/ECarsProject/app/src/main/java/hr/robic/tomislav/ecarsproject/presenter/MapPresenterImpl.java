package hr.robic.tomislav.ecarsproject.presenter;


import java.util.Collection;

import hr.robic.tomislav.ecarsproject.MapContract;
import hr.robic.tomislav.model.Location;

public class MapPresenterImpl implements MapContract.MapPresenter, MapContract.MapModel.OnPointsLoadedListener {

    private MapContract.MapView view;
    private MapContract.MapModel model;

    public MapPresenterImpl(MapContract.MapView view, MapContract.MapModel model) {
        this.view = view;
        this.model = model;
    }

    @Override
    public void requestToLoadPoints() {
        model.loadPoints(this);
    }

    @Override
    public void onDestroy() {
        view = null;
    }

    @Override
    public void onPointsLoaded(Collection<Location> locations) {
        if (locations != null) {
            view.addPoints(locations);
        }
    }
}


