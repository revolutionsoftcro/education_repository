package hr.robic.tomislav.dao;

import androidx.room.TypeConverter;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneOffset;

public class DateConverter {

    @TypeConverter
    public static LocalDateTime fromTimestamp(Long value) {
        return LocalDateTime.ofInstant(Instant.ofEpochMilli(value), ZoneOffset.UTC);
    }

    @TypeConverter
    public static Long dateToTimestamp(LocalDateTime date) {
        return date.atOffset(ZoneOffset.UTC).toInstant().toEpochMilli();
    }

}
