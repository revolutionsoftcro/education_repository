package hr.robic.tomislav.ecarsproject;

import java.util.List;

import hr.robic.tomislav.model.Article;

public interface ViewArticleContract {

    interface ViewArticleModel {
        interface OnArticlesLoadedListener {
            void onArticlesLoaded(List<Article> articles);
        }
        void loadArticles(OnArticlesLoadedListener onArticlesLoadedListener);
        void saveArticle(Article article);
    }

    interface ArticleView {
        String ARTICLE_POSITION = "hr.robic.tomislav.ecarsproject.articles.position";
        void showArticle();
    }

    interface SingleArticleView {
        void setTitle(String title);
        void setDescription(String description);
        void setImage(String path);
        void setDateTime(String date);
        void setRead(Boolean read);
    }

    interface ViewArticlePresenter {
        void requestToLoadArticles();
        int getArticlesCount();
        void bindSingleArticle(SingleArticleView view, int position);
        void toggleReadStatus(int position);
        void onDestroy();
    }

}
