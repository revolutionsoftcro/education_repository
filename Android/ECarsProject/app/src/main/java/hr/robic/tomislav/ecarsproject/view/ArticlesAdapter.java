package hr.robic.tomislav.ecarsproject.view;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.squareup.picasso.Picasso;

import java.io.File;

import butterknife.BindView;
import butterknife.ButterKnife;
import hr.robic.tomislav.ecarsproject.ArticlesContract;
import jp.wasabeef.picasso.transformations.RoundedCornersTransformation;
import hr.robic.tomislav.ecarsproject.R;

public class ArticlesAdapter extends RecyclerView.Adapter<ArticlesAdapter.ViewHolder> {

    private ArticlesContract.ArticlesPresenter presenter;

    public ArticlesAdapter(ArticlesContract.ArticlesPresenter presenter) {
        this.presenter = presenter;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.article, parent, false);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.tvTitle.setOnClickListener(v -> presenter.view(position));
        presenter.bindSingleArticle(holder, position);
    }

    @Override
    public int getItemCount() {
        return presenter.getArticlesCount();
    }

    public class ViewHolder extends RecyclerView.ViewHolder
            implements ArticlesContract.SingleArticleView {

        private static final int TITLE_SIZE = 45;

        @BindView(R.id.viewForeground)
        LinearLayout viewForeground;
        @BindView(R.id.tvTitle)
        TextView tvTitle;
        @BindView(R.id.ivImage)
        ImageView ivImage;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        @Override
        public void setTitle(String title) {
            if (title.length() > TITLE_SIZE) {
                title = title.substring(0, TITLE_SIZE - 3).concat("...");
            }

            tvTitle.setText(title);
        }

        @Override
        public void setImage(String path) {
            Picasso.get()
                    .load(new File(path))
                    .transform(new RoundedCornersTransformation(25, 5))
                    .into(ivImage);
        }
    }
}
