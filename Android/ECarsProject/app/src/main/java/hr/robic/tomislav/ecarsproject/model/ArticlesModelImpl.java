package hr.robic.tomislav.ecarsproject.model;

import android.content.Context;
import android.os.Handler;

import hr.robic.tomislav.dao.AppDatabase;
import hr.robic.tomislav.dao.AppExecutors;
import hr.robic.tomislav.ecarsproject.ArticlesContract;
import hr.robic.tomislav.model.Article;

public class ArticlesModelImpl implements ArticlesContract.ArticlesModel {

    private static final long DELAY = 500;
    private final Handler handler = new Handler();
    private Context context;

    public ArticlesModelImpl(Context context) {
        this.context = context;
    }

    @Override
    public void loadArticles(ArticlesContract.ArticlesModel.OnArticlesLoadedListener onArticlesLoadedListener) {
        onArticlesLoadedListener.onArticlesLoaded(AppDatabase.getInstance(context).articleDao().getArticles()) ;
    }

    @Override
    public void deleteArticle(Article article) {
        AppExecutors.getInstance().diskIO().execute(new Runnable() {
            @Override
            public void run() {
                AppDatabase.getInstance(context).articleDao().delete(article);
            }
        });
    }
}
