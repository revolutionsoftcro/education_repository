////////////////////////////////////
//Learning Objects and Prototypes//
//////////////////////////////////

'use strict';

//Display functions

function display() {
    for (var i = 0; i < arguments.length; i++ ) {
        if (typeof arguments[i] === 'object')
            displayObject(arguments[i]);
        else
            displayValue(arguments[i], true);
    }
}

function displayObject(object) {
    if (object == null)
      displayValue('null')
    displayValue(getTypeName(object) + ' {')
    for(var propertyName in object) {
      if (propertyName != 'constructor') {
        displayValue(propertyName + ': ' + object[propertyName], false, true);
      }
    }
    displayValue('}', true)
  }
  
  function displayValue(value, addMargin, addPadding) {
    var div = document.createElement('div');
    div.style.fontSize='32px'
    if (addMargin)
      div.style.marginBottom='30px'
    if (addPadding)
      div.style.paddingLeft='30px'
    div.textContent = value;
    document.body.appendChild(div)
  }
  
  function getTypeName(object) {
     var funcNameRegex = /function (.{1,})\(/;
     var results = (funcNameRegex).exec(object.constructor.toString());
     return (results && results.length > 1) ? results[1] : "";
  }

 // Objects and Prototypes
 
var zaposlenik = {ime: 'Pero', odjel: 'IT'}
display(zaposlenik);

zaposlenik.placa = 15800.48;
display(zaposlenik);
var a;
zaposlenik.prikaziPlacu = function() {
    display(`Plaća zaposlenika je ${Math.round( this.placa * 100 + Number.EPSILON ) / 100}.`);
}
zaposlenik.prikaziPlacu();

zaposlenik.promjeniPlacu = function(a) {
    this.placa += a;
    return Math.round( this.placa * 100 + Number.EPSILON ) / 100;
}
zaposlenik.promjeniPlacu(856.51);
zaposlenik.prikaziPlacu();
var iznos = zaposlenik.promjeniPlacu(-56.99);
display("Iznos: " + iznos);

function Zaposlenik(ime = "Novi", odjel = "Neodređen", placa = 0.0) {
    this.ime = ime;
    this.odjel = odjel;
    this.placa = placa;
    this.promjeniPlacu = function(a) {
        this.placa += a;
        display(`Nova plaća zaposlenika ${this.ime} je ${Math.round( this.placa * 100 + Number.EPSILON ) / 100}.`);
        return Math.round( this.placa * 100 + Number.EPSILON ) / 100;
    }
}

Zaposlenik.prototype.prikaziPlacu = function() {
  display(`Plaća zaposlenika ${this.ime} je ${Math.round( this.placa * 100 + Number.EPSILON ) / 100}.`);
}

var zaposlenici=[];
zaposlenici[0] = new Zaposlenik()
zaposlenici[1] = new Zaposlenik("Ivan",undefined, 7280.21);
zaposlenici[2] = new Zaposlenik("Marko","Administracija", 6550.56);
zaposlenici.forEach ( x => {
    display(x);
});

zaposlenici[1].prikaziPlacu();
zaposlenici[1].promjeniPlacu(1031.29);

//Klasa u ES6
class Auto {
    constructor(ime, boja, cijena) {
        this.ime = ime;
        this.boja = boja;
        this.cijena = cijena;
    }
    promjeniCijenu (a) {
        this.cijena+=a;
        display(`Nova cijena od ${this.ime} je ${Math.round( this.cijena * 100 + Number.EPSILON ) / 100}.`)
    }
}

var nekiAuto=new Auto("Audi", "crna", 135870.62);
nekiAuto.promjeniCijenu(-15000.76);

Zaposlenik.prototype.tvrtka="Omega";
display(zaposlenici[0].ime+' '+zaposlenici[0].tvrtka);
display(zaposlenici[1].ime+' '+zaposlenici[1].tvrtka);

Zaposlenik.prototype = { tvrtka: "Apple Inc." };
zaposlenici[3]=new Zaposlenik("Petar", "Dizajn", 31000,89)
display(zaposlenici[2].ime+' ' + zaposlenici[0].tvrtka);
display(zaposlenici[3].ime+ ' ' + zaposlenici[3].tvrtka);

Zaposlenik.prototype = "Apple Inc.";
display(zaposlenici[2].ime+' ' + zaposlenici[0].tvrtka);
display(zaposlenici[3].ime+ ' ' + zaposlenici[3].tvrtka);


function ZaposlenikBase() {}
ZaposlenikBase.prototype.grad = "Zagreb";
ZaposlenikBase.prototype.ispisiGrad = function() {
  display(`Zaposleniki ${this.ime} radi u gradu ${this.grad}`);
}
Zaposlenik.prototype = Object.create(ZaposlenikBase.prototype);
Zaposlenik.prototype.constructor = Zaposlenik;

zaposlenici[4] = new Zaposlenik("Andrija", "IT", 5400.65);
zaposlenici[4].ispisiGrad();

display(zaposlenici[2]);
display(zaposlenici[3]);
display(zaposlenici[4]);

function Direktor(ime = "Novi", odjel = "Neodređen", placa = 0.0, bonus, ured="Neodređen") {
  Zaposlenik.call(this, ime, odjel, placa);
  this.ured = ured;
  this.getBonus = function() { return bonus; }
  this.setBonus = function(newBonus) {bonus = newBonus; }
}

Direktor.prototype = Object.create(Zaposlenik.prototype);
Direktor.prototype.prikaziPlacu = function() {
  display(`Ukupna plaća direktor ${this.ime} je ${Math.round( (this.placa + this.getBonus()) * 100 + Number.EPSILON ) / 100}.`);
}

var nekiDirektor = new Direktor("Vladimir", "IT", 24000.45, 2200.12, "F14");
nekiDirektor.setBonus(2200.12);
nekiDirektor.promjeniPlacu(1500);
nekiDirektor.prikaziPlacu();


