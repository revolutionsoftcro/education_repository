﻿function pageLoad() {

    $("#PostGradForm").hide();
    $("#GradoviTableDiv").hide();
    $("#EditGradForm1").hide();
    $("#EditGradForm2").hide();
    $("#DeleteGradForm1").hide();
    $("#DeleteGradForm2").hide();
    $("#DeleteGradConfirm").hide();

    $.ajax({
        url: "http://localhost/API/api/drzava",
        method: "GET",
        crossDomain: true
    }).done(function (data) {
        var select = $(".selectDrzava");
        for (var i = 0; i < select.length; i++) {
            if ($(select[i]) != null)
                $(select[i]).children().remove();
            data.forEach(function (item) {
                $(select[i]).append('<option value="' + item.PK_Drzava +'">' + item.Naziv + '</option>');
            });
        }
    });

    $.ajax({
        url: "http://localhost/API/api/grad",
        method: "GET",
        crossDomain: true
    }).done(function (data) {
        var select = $(".selectGrad");
        for (var i = 0; i < select.length; i++) {
            if ($(select[i]) != null)
                $(select[i]).children().remove();
            data.forEach(function (item) {
                $(select[i]).append('<option value="' + item.Id + '">' + item.Grad + '</option>');
            });
        }
    });
}

pageLoad();

$(".section").hide();

$("#menu > p").click(function () {
    $(this).next().slideToggle("slow");
    $("#PostGrad > form").hide();
})

$("#GradoviTableDiv").click(function () {
    $(this).slideToggle("slow");
})

function GetGrad() {
    $.ajax({
        url: "http://localhost/API/api/grad",
        method: "GET",
        crossDomain: true
    }).done(function (data) {
        var gradoviTable = $("#GradoviTableDiv");
        var table = null;
        if ($("#GradoviTable") != null)
            $("#GradoviTable").remove();
        gradoviTable.append("<table id='GradoviTable'>");
        table = $("#GradoviTable");
        table.append("<tr>");
        $("#GradoviTable tr:first-child").append("<th> Naziv grada</th>").append("<th> Država </th>");
        data.forEach(function (item, i) {
            table.append("<tr>");
            $("#GradoviTable tr:nth-child(" + (i + 2)  + ")")
                .append("<td>" + item.Grad + "</td>")
                .append("<td>" + item.Drzava + "</td>");
        }).fail(function () {
            alert("Objekti se ne mogu dohvatiti!");
            pageLoad();
        });    
    });
}

function CreateGrad() {
    var form = $("#PostGradForm");
    var data = form.serializeArray().reduce(function (obj, item) {
        obj[item.name] = item.value;
        return obj;
    }, {});
    $.ajax({
        url: "http://localhost/API/api/grad",
        method: "POST",
        data: data,
        crossDomain: true
    }).done(function (data) {
        $("#PostGradForm").slideToggle("slow");
        pageLoad();
    }).fail(function () {
        alert("Objekt se ne može obrisati!");
        pageLoad();
    });
}

function EditGrad() {
    var form = $("#EditGradForm2");
    var data = form.serializeArray().reduce(function (obj, item) {
        obj[item.name] = item.value;
        return obj;
    }, {});
    $.ajax({
        url: "http://localhost/API/api/grad/" + data.PK_Grad,
        method: "PUT",
        data: data,
        crossDomain: true
    }).done(function (data) {
        $("#EditGradForm2").slideToggle("slow");
        pageLoad();
    }).fail(function () {
        alert("Objekt se ne može Promjeniti!");
        pageLoad();
    });
}

function DeleteGrad() {
    var form = $("#DeleteGradForm2");
    var data = form.serializeArray().reduce(function (obj, item) {
        obj[item.name] = item.value;
        return obj;
    }, {});
    $.ajax({
        url: "http://localhost/API/api/grad/" + data.PK_Grad,
        method: "DELETE",
        crossDomain: true
    }).done(function (data) {
        $("#DeleteGradForm2").slideToggle("slow");
        pageLoad();
    }).fail(function() {
        alert("Objekt se ne može obrisati!");
        pageLoad();
    });
}

function Grad({ Id, DrzavaId, Grad, Drzava }) {
    this.Id = Id;
    this.DrzavaId = DrzavaId;
    this.Grad = Grad;
    this.Drzava = Drzava;
}

$(document).on("click", "#GetGrad", function () {
    $("#DeleteGradForm1").hide();
    $("#DeleteGradForm2").hide();
    $("#PostGradForm").hide();
    $("#EditGradForm1").hide();
    $("#EditGradForm2").hide();
    $("#GradoviTableDiv").slideToggle("slow");
    GetGrad();
});


$(document).on("click", "#PostGradForm > button", function (event) {
    event.preventDefault();
    CreateGrad();
});

$(document).on("click", "#PostGrad", function () {
    $("#GradoviTableDiv").hide();
    $("#EditGradForm1").hide();
    $("#EditGradForm2").hide();
    $("#DeleteGradForm1").hide();
    $("#DeleteGradForm2").hide();
    $("#PostGradForm").slideToggle("slow");
});

$(document).on("click", "#GradCreateButton", function (event) {
    event.preventDefault();
    CreateGrad();
});

$(document).on("click", "#EditGrad", function () {
    $("#DeleteGradForm1").hide();
    $("#DeleteGradForm2").hide();
    $("#GradoviTableDiv").hide();
    $("#PostGradForm").hide();
    $("#EditGradForm2").hide();
    $("#EditGradForm1").slideToggle("slow");
});

$(document).on("click", "#GradEditButton1", function (event) {
    $("#PostGradForm1").hide();
    $("#PostGradForm2").hide();
    $("#GradoviTableDiv").hide();
    $("#PostGradForm").hide();
    $("#EditGradForm1").hide();
    var form = $("#EditGradForm1");
    let data = form.serializeArray().reduce(function (obj, item) {
        obj[item.name] = item.value;
        return obj;
    }, {});
    var gradId = $("#gradIdInput")
    gradId.val(data.Id);

    $.ajax({
        url: "http://localhost/API/api/grad/" + data.Id,
        method: "GET",
        crossDomain: true
    }).done(function (data) {
        $("#gradInput").val(data.Grad);
        $('.selectDrzava option:contains("' + data.Drzava + '")').prop("selected", true);
    });

    $("#EditGradForm2").slideToggle("slow");
});

$(document).on("click", "#GradEditButton2", function (event) {
    event.preventDefault();
    EditGrad();
});

$(document).on("click", "#DeleteGrad", function () {
    $("#DeleteGradConfirm").hide();
    $("#GradoviTableDiv").hide();
    $("#PostGradForm").hide();
    $("#EditGradForm2").hide();
    $("#EditGradForm1").hide();
    $("#DeleteGradForm1").slideToggle("slow");
});

$(document).on("click", "#GradDeleteButton", function (event) {
    event.preventDefault();
    $("#GradoviTableDiv").hide();
    $("#PostGradForm").hide();
    $("#EditGradForm1").hide();
    $("#EditGradForm2").hide();
    $("#PostGradForm1").hide();
    $("#DeleteGradForm1").hide();
    var form = $("#DeleteGradForm1");
    let data = form.serializeArray().reduce(function (obj, item) {
        obj[item.name] = item.value;
        return obj;
    }, {});

    var gradId = $("#gradIdInput2")
    gradId.val(data.Id);

    var gradIme = $("#DeleteGradIme")
    if (gradIme != null) {
        gradIme.html("");
    }

    var gradDrzava = $("#DeleteGradDrzava");
    if (gradDrzava.children() != null)
        gradDrzava.html("");

    $.ajax({
        url: "http://localhost/API/api/grad/" + data.Id,
        method: "GET",
        crossDomain: true
    }).done(function (data) {
        gradIme.append(data.Grad);
        gradDrzava.append(data.Drzava);
    });

    $("#DeleteGradForm2").slideToggle("slow");
});

$(document).on("click", "#GradDeleteCancel", function (event) {
    event.preventDefault();
    $("#GradoviTableDiv").hide();
    $("#PostGradForm").hide();
    $("#EditGradForm1").hide();
    $("#EditGradForm2").hide();
    $("#DeleteGradForm").hide();
    $("#DeleteGradForm2").slideToggle("slow");
});

$(document).on("click", "#GradDeleteConfirm", function (event) {
    event.preventDefault();
    DeleteGrad();
});

