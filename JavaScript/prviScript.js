function UnosImena()
{
    var txt;
    var name = window.prompt("Enter your name");
    if (name != null) 
    {
        txt = "Hello "+name;
    }
    document.getElementById("name").innerHTML = txt; 
}

let value;
console.log(value, typeof(value));
value = null;
console.log(value, typeof(value));

let values = [0,0,5,6 ];
values[0] = 12;
values[1] = 23;
console.log(values);
console.log(`Na indexu jedan od values je :${values[1]}`); //backtickovi alt + 7
console.log(values.length);
values.push(100);
console.log(values);
let izbacen = values.pop();
console.log("Izbačen: ",izbacen,"\nNiz: ",values);
izbacen = values.shift();
console.log("Izbačen: ",izbacen,"\nNiz: ",values);

values.push(50);
izbacen=values.splice(1,2, 31, 25, 40);
console.log("Izbačeni: ",izbacen,"\nNiz: ",values);

if (values.length < 5) {
    values.pop();
} else if (values.length === 5 || values.length<5) {
    values.push(5);
}
else {
    values.slice(1,2);
}
console.log(values); 
if (values.length < 5) {
    values.pop();
}
else if (values.length === 5) {
    values.push(5);
}
else {
    values.splice(1,2);
}
console.log(values);
if (values.length < 5) {
    values.pop();
}
else if (values.length === 5) {
    values.push(5);
} else {
    values.splice(1,2);
}

values.splice(1,1,31,25,40);
console.log(values);

values.forEach(x => {
    console.log("Value:",x + 5);
});

var newValues = values.map(function(x) {
	return x + 5;
});
newValues.forEach(x => {
    console.log(x);
});

var suma = newValues.reduce(function(suma, x) {
	return suma += x;
});
console.log(suma);

var neparni = newValues.filter(function(x) {
	return x % 2 === 1;
});
console.log(neparni);

newValues.push(-8);
newValues.push(-21);
var pozitivni = newValues.filter(function(x) {
	return x>0;
});
console.log(newValues);
console.log(pozitivni);

let x = 5;
if (x > 3 && x < 6){
    x = x+2
} else if (x == 10 || x <= 0) {
    x = x*10;
} else {
    x = x/5;
}
console.log(x);

let uvjet = "kvadriraj"
x = 5;
switch(uvjet) {
    case 'povećaj':
        x = x+1;
        break;
    case 'smanji':
        x = x-1;
        break;
    case 'kvadriraj':
        x = x*x;
        break;
    default:
        console.log('Krivi uvjet!');
        break;
}
console.log(x);

for(let i = 0; i < 5; i = i+2){
    console.log(i);
}

let count = 1;
while (count < 5){
    console.log(count);
    count++;
}

function Funkcija() {
    console.log("unutar funkcije");
}
Funkcija();
Funkcija();

function Zbroj(a,b) {
    return a+b;
}
console.log(Zbroj(2,3));
console.log(Zbroj(3,5));

let message="globalna varijabla";
function Poruka(){
    message="nesto";
    return 15;
}
let broj=Poruka();
console.log(message, broj);

function changeZaposlen(osoba) {
    if (osoba.zaposlen){
        osoba.zaposlen=false;
    } else {
        osoba.zaposlen=true;
    }    
}

let osoba = {
    ime:"Pero",
    godine:22,
    zaposlen:true
}; 
console.log(osoba.ime);
console.log(osoba.godine);
changeZaposlen(osoba);
if(osoba.zaposlen) {
    console.log("Zaposlen!");
} else {
    console.log("Nezaposlen!");
}

let osobe= [ {
        ime:"Pero",
        godine:22,
        zaposlen:true
    }, {
        ime:"Marko",
        godine:20,
        zaposlen:true
    }
];
console.log(osobe[0].ime);
console.log(osobe[1].godine);
changeZaposlen(osobe[1]);
console.log(osobe[1].zaposlen);

let randomBroj = Math.random()*53;
randomBroj = Math.trunc(randomBroj);
console.log(randomBroj);

let datum = new Date();
console.log(datum);
console.log(datum.toLocaleString());

let rijec = "    neka riječ   ";
console.log(rijec, "x");
rijec  =rijec.trim();
console.log(rijec, "x");
rijec = rijec.toUpperCase();
console.log(rijec);

let textArea = document.getElementById('text-area');
textArea.innerText = 'Promjenjen text!';

textArea.style.display = 'none';
let okButton = document.getElementById('ok-button');
okButton.addEventListener('click', function() {
    textArea.style.display = 'block';
    textArea.innerText = 'Promjenjen text!';
})

textArea.addEventListener('click', function() {
    textArea.innerText = 'Kliknuo na text!';
})

function Pozdrav()
{
  var cont = document.getElementById("container");
  var ime = document.getElementById("ime").value;
  cont.innerHTML = "<br />" + "Pozdrav " + ime + "!" + "<br />";
}