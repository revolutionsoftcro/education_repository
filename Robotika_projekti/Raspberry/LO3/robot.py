from collections import deque
from imutils.video import VideoStream
import numpy as np
import argparse
import cv2
import imutils
import time
import RPi.GPIO as gpio
gpio.setwarnings(False)

gpio.setmode(gpio.BCM)

gpio.setup(2, gpio.OUT)
gpio.setup(3, gpio.OUT)
gpio.setup(4, gpio.OUT)
gpio.setup(17, gpio.OUT)
gpio.setup(22, gpio.OUT)
gpio.setup(27, gpio.OUT)

l=gpio.PWM(2, 50)
r=gpio.PWM(22, 50)

l.start(100)
r.start(100)

ap = argparse.ArgumentParser()
ap.add_argument("-v", "--video",
    help="path to the (optional) video file")
ap.add_argument("-b", "--buffer", type=int, default=64,
    help="max buffer size")
args = vars(ap.parse_args())

greenLower = (50, 100, 20)
greenUpper = (70, 255, 255)
pts = deque(maxlen=args["buffer"])

class Motor:
    def driveForward(speed):
        l.ChangeDutyCycle(speed)
        r.ChangeDutyCycle(speed)
        gpio.output(2, gpio.HIGH)
        gpio.output(3, gpio.LOW)
        gpio.output(4, gpio.HIGH)
        gpio.output(22, gpio.HIGH)
        gpio.output(17, gpio.LOW)
        gpio.output(27, gpio.HIGH)
    
    def driveRight():
        gpio.output(2, gpio.HIGH)
        gpio.output(3, gpio.LOW)
        gpio.output(4, gpio.HIGH)
        
    def driveLeft():
        gpio.output(22, gpio.HIGH)
        gpio.output(17, gpio.LOW)
        gpio.output(27, gpio.HIGH)
        
    def stop():
        gpio.output(2, gpio.LOW)
        gpio.output(3, gpio.LOW)
        gpio.output(4, gpio.LOW)
        gpio.output(22, gpio.LOW)
        gpio.output(17, gpio.LOW)
        gpio.output(27, gpio.LOW)

class Camera:
    def init():
        if not args.get("video", False):
            vs = VideoStream(src=0).start()
        else:
            vs = cv2.VideoCapture(args["video"])

        time.sleep(2.0)
        return vs
    
    def getFrame(vs):
        frame = vs.read()
        frame = frame[1] if args.get("video", False) else frame
        return frame
    
    def detectColor(color, p, frame):

        frame = imutils.resize(frame, width=600)

        cv2.imshow("Camera", frame)

        blurred = cv2.GaussianBlur(frame, (11, 11), 0)

        hsv = cv2.cvtColor(blurred, cv2.COLOR_BGR2HSV)
        
        if (color=="green"):
            mask = cv2.inRange(hsv, greenLower, greenUpper)
        else:
            print("Kriva boja!")
            return False

        mask = cv2.erode(mask, None, iterations=2)

        mask = cv2.dilate(mask, None, iterations=4)
        
        cnts = cv2.findContours(mask.copy(), cv2.RETR_EXTERNAL,
            cv2.CHAIN_APPROX_SIMPLE)
        cnts = imutils.grab_contours(cnts)
        center = None

        if len(cnts) > 0:
            c = max(cnts, key=cv2.contourArea)
            ch, cw, cc = frame.shape
            x, y, w, h = cv2.boundingRect(c)
            
            rect = cv2.minAreaRect(c)
            
            (rx, ry) = rect[1]
            
            percentage = ((rx * ry) / (cw * ch)) * 100
            
            if percentage > p:
                return True
            else:
                return False
        else:
            return False
            
    def key():
        return cv2.waitKey(1) & 0xFF
    
    def closeAll():
        cv2.destroyAllWindows()
        