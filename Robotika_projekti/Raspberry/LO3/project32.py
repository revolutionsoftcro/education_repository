import robot
import time

vs = robot.Camera.init()

while True:
    
    frame = robot.Camera.getFrame(vs)
    if frame is None:
        break
    
    if robot.Camera.detectColor("blue", 25, frame) and robot.Camera.detectColor("orange", 25, frame):
        robot.Motor.driveRight()
        time.sleep(2.3)
        robot.Motor.stop()

    elif robot.Camera.detectColor("green", 50, frame):
        robot.Motor.driveForward(90)
        time.sleep(0.7)
        robot.Motor.stop()
        
    elif robot.Camera.detectColor("yellow", 50, frame):
        robot.Motor.driveBackward(90)
        time.sleep(0.7)
        robot.Motor.stop()            

    if robot.Camera.key() == ord("q"):
        break

robot.Motor.stop()
robot.Camera.closeAll()

