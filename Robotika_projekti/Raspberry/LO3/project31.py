import robot
import time

vs = robot.Camera.init()

while True:
    
    robot.Motor.driveLeft()
    time.sleep(1)
    robot.Motor.stop()
    time.sleep(0.5)
    
    frame = robot.Camera.getFrame(vs)
    if frame is None:
        break
    
    if robot.Camera.detectColor("green", 50, frame):
        robot.Motor.driveForward(90)
        time.sleep(0.7)
        robot.Motor.stop()
        break;

    if robot.Camera.key() == ord("q"):
        break

robot.Motor.stop()
robot.Camera.closeAll()
