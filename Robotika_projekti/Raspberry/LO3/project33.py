import robot
import time

vs = robot.Camera.init()

while True:
    
    frame = robot.Camera.getFrame(vs)
    if frame is None:
        break
    g = False
    y = False
    r = False
    
    g = robot.Camera.detectColor("green", 1, frame)
    y = robot.Camera.detectColor("yellow", 1, frame)
    r = robot.Camera.detectColor("red", 1, frame)

    if (g or (y and r)) and not (g and y):
        if y and r:
            time.sleep(1)
            print("YR")
        elif y:
            print("GY-stoji")
        else:
            print("G")
        for i in range(6):
            robot.Motor.driveForward(100)
            time.sleep(0.7)
            robot.Motor.stop()
            frame = robot.Camera.getFrame(vs)
            
            g = robot.Camera.detectColor("green", 1, frame)
            y = robot.Camera.detectColor("yellow", 1, frame)
            r = robot.Camera.detectColor("red", 1, frame)
            
            if frame is None:
                break
            if r:
                print("R")
                break
            if g and y:
                robot.Motor.driveForward(85)
                time.sleep(0.4)
                robot.Motor.stop()
                print(i, "YG-usporavanje")
                time.sleep(0.3)
                break

            print(i, "nije izasao")
            time.sleep(1.2)

    if robot.Camera.key() == ord("q"):
        break

robot.Motor.stop()
robot.Camera.closeAll()


