import robot
import time

vs = robot.Camera.init()

while True:
    frame = robot.Camera.getFrame(vs)
    
    if frame is None:
        break

    if robot.Camera.track_ball(frame):
        break
    else:
        robot.Motor.driveLeft()
        time.sleep(0.2)
        robot.Motor.stop()
        time.sleep(0.2)
    
    if robot.Camera.key() == ord("q"):
        break

robot.Motor.stop()
robot.Camera.closeAll()

