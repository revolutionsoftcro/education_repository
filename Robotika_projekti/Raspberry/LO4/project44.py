import robot
import time

vs = robot.Camera.init()
i=12

while i>0:        
    
    frame = robot.Camera.getFrame(vs)
    if frame is None:
        break
    
    robot.Motor.driveForward(100)
    time.sleep(0.3)
    robot.Motor.stop() 
    turn = robot.Camera.detectArrow(frame)
    time.sleep(2) 
    
    if turn == "L":
        print("L")
        robot.Motor.driveLeft()
        time.sleep(1)
    
    if turn == "R":
        print("R")
        robot.Motor.driveRight()
        time.sleep(1) 
    
    i=i-1
    if robot.Camera.key() == ord("q"):
        break

robot.Motor.stop()
robot.Camera.closeAll()