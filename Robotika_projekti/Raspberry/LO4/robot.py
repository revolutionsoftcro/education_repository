from collections import deque
from imutils.video import VideoStream
import numpy as np
import argparse
import cv2
import imutils
import time
import RPi.GPIO as gpio
gpio.setwarnings(False)

gpio.setmode(gpio.BCM)

gpio.setup(2, gpio.OUT)
gpio.setup(3, gpio.OUT)
gpio.setup(4, gpio.OUT)
gpio.setup(17, gpio.OUT)
gpio.setup(22, gpio.OUT)
gpio.setup(27, gpio.OUT)

l=gpio.PWM(2, 50)
r=gpio.PWM(22, 50)

l.start(100)
r.start(100)

ap = argparse.ArgumentParser()
ap.add_argument("-v", "--video",
    help="path to the (optional) video file")
ap.add_argument("-b", "--buffer", type=int, default=64,
    help="max buffer size")
args = vars(ap.parse_args())

greenLower = (56, 95, 95)
greenUpper = (75, 255, 255)
yellowLower = (25, 50, 140)
yellowUpper = (45, 255, 255)
redLower = (170, 70, 120)
redUpper = (179, 255, 255)
blueLower = (90, 50, 150)
blueUpper = (110, 255, 255)
darkBlueLower = (100, 120, 57)
darkBlueUpper = (120, 255, 255)
orangeLower = (0, 159, 31)
orangeUpper = (14, 255, 255)
pts = deque(maxlen=args["buffer"])

class Motor:
    def driveForward(speed):
        l.ChangeDutyCycle(speed)
        r.ChangeDutyCycle(speed)
        gpio.output(2, gpio.HIGH)
        gpio.output(3, gpio.LOW)
        gpio.output(4, gpio.HIGH)
        gpio.output(22, gpio.HIGH)
        gpio.output(17, gpio.LOW)
        gpio.output(27, gpio.HIGH)
        
    def driveBackward(speed):
        l.ChangeDutyCycle(speed)
        r.ChangeDutyCycle(speed)
        gpio.output(2, gpio.HIGH)
        gpio.output(3, gpio.HIGH)
        gpio.output(4, gpio.LOW)
        gpio.output(22, gpio.HIGH)
        gpio.output(17, gpio.HIGH)
        gpio.output(27, gpio.LOW)
    
    def driveRight():
        gpio.output(2, gpio.HIGH)
        gpio.output(3, gpio.LOW)
        gpio.output(4, gpio.HIGH)
        
    def driveLeft():
        gpio.output(22, gpio.HIGH)
        gpio.output(17, gpio.LOW)
        gpio.output(27, gpio.HIGH)
        
    def stop():
        gpio.output(2, gpio.LOW)
        gpio.output(3, gpio.LOW)
        gpio.output(4, gpio.LOW)
        gpio.output(22, gpio.LOW)
        gpio.output(17, gpio.LOW)
        gpio.output(27, gpio.LOW)

class Camera:
    def init():
        if not args.get("video", False):
            vs = VideoStream(src=0).start()
        else:
            vs = cv2.VideoCapture(args["video"])
        time.sleep(2.0)
        return vs
    
    def getFrame(vs):
        frame = vs.read()
        frame = frame[1] if args.get("video", False) else frame
        return frame
    
    def detectColor(color, p, frame):

        frame = imutils.resize(frame, width=600)
        
        cv2.imshow("Camera", frame)

        blurred = cv2.GaussianBlur(frame, (11, 11), 0)

        hsv = cv2.cvtColor(blurred, cv2.COLOR_BGR2HSV)
        
        if color == "green":
            mask = cv2.inRange(hsv, greenLower, greenUpper)
        elif color == "yellow":
            mask = cv2.inRange(hsv, yellowLower, yellowUpper)
        elif color == "red":
            mask = cv2.inRange(hsv, redLower, redUpper)
        elif color == "blue":
            mask = cv2.inRange(hsv, blueLower, blueUpper)
        elif color == "darkblue":
            mask = cv2.inRange(hsv, darkBlueLower, darkBlueUpper)
        elif color == "orange":
            mask = cv2.inRange(hsv, orangeLower, orangeUpper)
        else:
            return False

        mask = cv2.erode(mask, None, iterations=2)

        mask = cv2.dilate(mask, None, iterations=4)
                
        cnts = cv2.findContours(mask.copy(), cv2.RETR_EXTERNAL,
            cv2.CHAIN_APPROX_SIMPLE)
        cnts = imutils.grab_contours(cnts)
        center = None

        if len(cnts) > 0:
            c = max(cnts, key=cv2.contourArea)
            ch, cw, cc = frame.shape
            x, y, w, h = cv2.boundingRect(c)
            
            rect = cv2.minAreaRect(c)
            
            (rx, ry) = rect[1]
            
            percentage = ((rx * ry) / (cw * ch)) * 100
            
            if percentage > p:
                return True
            else:
                return False
        else:
            return False
        
    def detectArrow(frame):
        frame_gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
        frame_bin = cv2.adaptiveThreshold(frame_gray, 255, cv2.ADAPTIVE_THRESH_GAUSSIAN_C, cv2.THRESH_BINARY, 151, 2)
        _, contours, _ = cv2.findContours(frame_bin, cv2.RETR_LIST, cv2.CHAIN_APPROX_SIMPLE)
        min_area = 3000
        contours = [el for el in contours if cv2.contourArea(el) >= min_area]

        for c in contours:
            perimeter = cv2.arcLength(c, True)
            approximation = cv2.approxPolyDP(c, 0.01 * perimeter, True)
            rightmost = tuple(approximation[approximation[:, :, 0].argmax()][0])
            leftmost = tuple(approximation[approximation[:, :, 0].argmin()][0])
            topmost = tuple(approximation[approximation[:, :, 1].argmin()][0])
            bottommost = tuple(approximation[approximation[:, :, 1].argmax()][0])
            center = tuple([((rightmost[0]+leftmost[0]) // 2), ((rightmost[1]+leftmost[1]) // 2)])
            height = bottommost[1] - topmost[1]
            width = rightmost[0] - leftmost[0]
            
            if len(approximation) == 7 and (15*height)<(10*width) and (10*width)<(19*height) and width>200:
                right = 0
                left = 0
                cv2.circle(frame, center, 8, (0, 50, 255), -1)
                for i in range (len(approximation)):
                    if approximation[i][0][0] > center[0]:
                        right = right + 1
                    elif approximation[i][0][0] < center[0]:
                        left = left + 1

                if right == 5:
                    return "R"
                elif left == 5:
                    return "L"
        return "F"
    def detectSmile(gray, frame):
        face_cascade = cv2.CascadeClassifier('haarcascade_frontalface_default.xml') 
        eye_cascade = cv2.CascadeClassifier('haarcascade_eye.xml') 
        smile_cascade = cv2.CascadeClassifier('haarcascade_smile.xml') 
        faces = face_cascade.detectMultiScale(gray, 1.3, 5) 
        for (x, y, w, h) in faces: 
            cv2.rectangle(frame, (x, y), ((x + w), (y + h)), (255, 0, 0), 2) 
            roi_gray = gray[y:y + h, x:x + w] 
            roi_color = frame[y:y + h, x:x + w] 
            smiles = smile_cascade.detectMultiScale(roi_color, 2, 22) 
      
            for (sx, sy, sw, sh) in smiles: 
                cv2.rectangle(frame, (sx, sy), ((sx + sw), (sy + sh)), (0, 0, 255), 2)
                print("Found smile at:",sx,sy,sw,sh)
                return True
        cv2.imshow("Camera", frame)
        
    def track_ball(frame) :
        frame = imutils.resize(frame, width=600)
        cv2.imshow("Camera", frame)
        
        blurred = cv2.GaussianBlur(frame, (11, 11), 0)
        hsv = cv2.cvtColor(blurred, cv2.COLOR_BGR2HSV)
        mask = cv2.inRange(hsv, orangeLower, orangeUpper)
        mask = cv2.erode(mask, None, iterations=2)
        mask = cv2.dilate(mask, None, iterations=4)
        
        cv2.imshow("dilated", mask)
        
        cnts = cv2.findContours(mask.copy(), cv2.RETR_EXTERNAL,
            cv2.CHAIN_APPROX_SIMPLE)
        cnts = imutils.grab_contours(cnts)
        center = None
        
        if len(cnts) > 0:
            c = max(cnts, key=cv2.contourArea)
            ((x, y), radius) = cv2.minEnclosingCircle(c)
            M = cv2.moments(c)
            center = (int(M["m10"] / M["m00"]), int(M["m01"] / M["m00"]))
            ch, cw, cc = frame.shape
            framecenter = (int(cw / 2), int(ch / 2))
            
            x1 = framecenter[0] - 25
            x2 = framecenter[0] + 25
            
            if radius > 20:
                if (center[0] > x1) and (center[0] < x2):
                    print("C")
                    Motor.driveForward(90)
                    time.sleep(0.7)
                    Motor.stop()
                    return True
                elif (center[0] < x1):
                    print("L")
                    Motor.driveLeft()
                    time.sleep(0.15)
                    Motor.stop()
                else:
                    print("R")
                    Motor.driveRight()
                    time.sleep(0.15)
                    Motor.stop()               
            else:
                return False
        else:
            return False
        return False
        
    def key():
        return cv2.waitKey(1) & 0xFF
    
    def closeAll():
        cv2.destroyAllWindows()
        