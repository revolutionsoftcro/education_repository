import robot
import time
import cv2

vs = robot.Camera.init()
i = 0
start = time.time()
while True:
    frame = robot.Camera.getFrame(vs)
    if frame is None:
        break
    frame_gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
    rotate = robot.Camera.detectSmile(frame_gray, frame)
    
    if rotate == True:
        robot.Motor.driveLeft()
        time.sleep(1.9)
        robot.Motor.stop()
    i = i+1
    if robot.Camera.key() == ord("q"):
        break
    
end = time.time()
seconds = end-start
fps = i / seconds
fps_str = str(fps)
print("Average FPS:", fps_str)
f = open("fps.txt", "w")
f.write(fps_str)
f.close()


robot.Camera.closeAll()

