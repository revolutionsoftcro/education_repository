#include "Motor.h"

#define IR_SENSOR_1 2 //lijevo
#define IR_SENSOR_2 3 //lijevo
#define IR_SENSOR_3 4 //sredina
#define IR_SENSOR_4 5 //desno
#define IR_SENSOR_5 6 //desno
#define IR_SENSOR_6 7 //ovo je senzor ispred robota i taj ne koristimo

Motor motor(22, 23, 28, 25, 24, 29);

int ir1;
int ir2;
int ir3;
int ir4;
int ir5;

void setup() {
  pinMode(IR_SENSOR_1, INPUT);
  pinMode(IR_SENSOR_2, INPUT);
  pinMode(IR_SENSOR_3, INPUT);
  pinMode(IR_SENSOR_4, INPUT);
  pinMode(IR_SENSOR_5, INPUT);
  delay(6000);
}

void loop() {
  ir1 = digitalRead(IR_SENSOR_1);
  ir2 = digitalRead(IR_SENSOR_2);
  ir3 = digitalRead(IR_SENSOR_3);
  ir4 = digitalRead(IR_SENSOR_4);
  ir5 = digitalRead(IR_SENSOR_5); 
  
  if(ir1 || ir2) {
      motor.start_turning_left(255, 0);
  } else if (ir4 || ir5) {
      motor.start_turning_right(255, 0);
  } else {
      motor.start_driving_forward(255);
  }
  
}
