#include "Motor.h"

#define IR_SENSOR_6 7 //ovo je senzor ispred robota

int ir;

Motor motor(22, 23, 28, 25, 24, 29);
int i;

void setup() {
  pinMode(IR_SENSOR_6, INPUT);
  delay(6000);
  i=1;
}

void loop() {

  if (i) {
      ir = digitalRead(IR_SENSOR_6);
      if(!ir) {
        motor.stop_both_motors();
        delay(1000);
        motor.start_turning_left(255, 0);
        delay(2000);
        motor.stop_both_motors();
        delay(1000);
        motor.start_driving_backward(255);
        delay(1000);
        motor.stop_both_motors();
        i=0;
      }
      
      if (i) {
        motor.start_driving_forward(255);
      } else {
        motor.stop_both_motors();
      }     
  }
}
