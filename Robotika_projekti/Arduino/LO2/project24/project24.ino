#include "Motor.h"

#define IR_SENSOR_6 7 //ovo je senzor ispred robota

#define TRIG_L 8
#define ECHO_L 9

#define TRIG_R 10
#define ECHO_R 11

Motor motor(22, 23, 28, 25, 24, 29);

int ir;
unsigned long duration_l;
unsigned long distance_l;
unsigned long duration_r;
unsigned long distance_r;
int i;

void setup() {
  pinMode(IR_SENSOR_6, INPUT);
  
  pinMode(TRIG_L, OUTPUT);
  pinMode(ECHO_L, INPUT);

  pinMode(TRIG_R, OUTPUT);
  pinMode(ECHO_R, INPUT);
  
  delay(6000);
  i=1;
}

void measure_distance_L() {
  
    digitalWrite(TRIG_L, LOW);
    delayMicroseconds(5);
    digitalWrite(TRIG_L, HIGH);
    delayMicroseconds(10);
    digitalWrite(ECHO_L, LOW);

    duration_l = pulseIn(ECHO_L, HIGH); 
    distance_l = (duration_l / 2) / 29.1;
}

void measure_distance_R() {
  
    digitalWrite(TRIG_R, LOW);
    delayMicroseconds(5);
    digitalWrite(TRIG_R, HIGH);
    delayMicroseconds(10);
    digitalWrite(ECHO_R, LOW);

    duration_r = pulseIn(ECHO_R, HIGH); 
    distance_r = (duration_r / 2) / 29.1;
}

void loop() {

  if (i) {
      measure_distance_L();
      measure_distance_R();
      if ((distance_l + distance_r)>40){
        i=0;
      }

      if (!digitalRead(IR_SENSOR_6)) {
        motor.stop_both_motors();
        delay(1000);
        if (distance_l < distance_r){
            motor.start_turning_right(255, 0);
            delay(1000);
        }
        else {
            motor.start_turning_left(255, 0);
            delay(1000);
        }
      }

      
      if (distance_l<3) {
        motor.stop_both_motors();
        delay(1000);
        motor.start_turning_right(255, 0);
        delay(1000);
        motor.start_driving_forward(255);
        delay(1000);
        motor.start_turning_left(255, 0);
        delay(1000);
      }
      
      if (distance_r<3) {
        motor.stop_both_motors();
        delay(1000);
        motor.start_turning_left(255, 0);
        delay(1000);
        motor.start_driving_forward(255);
        delay(1000);
        motor.start_turning_right(255, 0);
        delay(1000);
      }
      
      if (i) {
        motor.start_driving_forward(255);
      } else {
        motor.stop_both_motors();
      }
  }
}
