#include "Motor.h"

#define IR_SENSOR_1 2
#define IR_SENSOR_2 3
#define IR_SENSOR_3 4
#define IR_SENSOR_4 5
#define IR_SENSOR_5 6
#define IR_SENSOR_6 7

#define TRIG_L 8
#define ECHO_L 9

#define TRIG_R 10
#define ECHO_R 11

const unsigned long MOTOR_RUN_TIME = 5000;

const unsigned long SENSOR_SAMPLE_TIME = 250; //read sensors two times per second

unsigned long duration_l;
unsigned long distance_l;
unsigned long duration_r;
unsigned long distance_r;

const byte STATE_IDLE = 1;
const byte STATE_ROTATING_LEFT = 2;
const byte STATE_ROTATING_RIGHT = 3;

Motor motor(22, 23, 28, 25, 24, 29);
byte current_state;
unsigned long state_start_time;
unsigned long sensor_read_start_time;

void setup() {
  Serial.begin(9600);
  current_state = STATE_IDLE;
  pinMode(IR_SENSOR_1, INPUT);
  pinMode(IR_SENSOR_2, INPUT);
  pinMode(IR_SENSOR_3, INPUT);
  pinMode(IR_SENSOR_4, INPUT);
  pinMode(IR_SENSOR_5, INPUT);
  pinMode(IR_SENSOR_6, INPUT);

  pinMode(TRIG_L, OUTPUT);
  pinMode(ECHO_L, INPUT);

  pinMode(TRIG_R, OUTPUT);
  pinMode(ECHO_R, INPUT);
}

void measure_distance_L() {
  
    digitalWrite(TRIG_L, LOW);
    delayMicroseconds(5);
    digitalWrite(TRIG_L, HIGH);
    delayMicroseconds(10);
    digitalWrite(ECHO_L, LOW);

    duration_l = pulseIn(ECHO_L, HIGH); 
    distance_l = (duration_l / 2) / 29.1;
}

void measure_distance_R() {
  
    digitalWrite(TRIG_R, LOW);
    delayMicroseconds(5);
    digitalWrite(TRIG_R, HIGH);
    delayMicroseconds(10);
    digitalWrite(ECHO_R, LOW);

    duration_r = pulseIn(ECHO_R, HIGH); 
    distance_r = (duration_r / 2) / 29.1;
}

void loop() { 
  //Serial.println(current_state);
  switch (current_state) {
    case STATE_IDLE:
      motor.start_turning_left(motor.MAX_SPEED, motor.HARD);
      current_state = STATE_ROTATING_LEFT;
      state_start_time = millis();
      break;
      
    case STATE_ROTATING_LEFT:
      if (millis() - state_start_time >= MOTOR_RUN_TIME) {
        motor.start_turning_right(motor.MAX_SPEED, motor.HARD);
        current_state = STATE_ROTATING_RIGHT;
        state_start_time = millis();
      }
      break;

    case STATE_ROTATING_RIGHT:
      if (millis() - state_start_time >= MOTOR_RUN_TIME) {
        motor.start_turning_left(motor.MAX_SPEED, motor.HARD);
        current_state = STATE_ROTATING_LEFT;
        state_start_time = millis();
      }
      break;
  }

  if (millis() - sensor_read_start_time >= SENSOR_SAMPLE_TIME) {

    sensor_read_start_time = millis();
    
    if (digitalRead(IR_SENSOR_1) == LOW) {

        Serial.print("* ");
    }
    else {

        Serial.print("  ");
    }
    if (digitalRead(IR_SENSOR_2) == LOW) {

        Serial.print("* ");
    }
    else {

        Serial.print("  ");
    }
    if (digitalRead(IR_SENSOR_3) == LOW) {

        Serial.print("* ");
    }
    else {

        Serial.print("  ");
    }
    if (digitalRead(IR_SENSOR_4) == LOW) {

        Serial.print("* ");
    }
    else {

        Serial.print("  ");
    }

    if (digitalRead(IR_SENSOR_5) == LOW) {

        Serial.print("* ");
    }
    else {

        Serial.print("  ");
    }
    if (digitalRead(IR_SENSOR_6) == LOW) {

        Serial.print("* ");
    }
    else {

        Serial.print("  ");
    }
    measure_distance_L();
    Serial.print(distance_l);

    Serial.print(" ");
    measure_distance_R();
    Serial.print(distance_r);
    Serial.println(" ");
  }
}
