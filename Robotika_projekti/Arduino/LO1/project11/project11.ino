#include "Motor.h"

Motor motor(22, 23, 28, 25, 24, 29);
int i;

void setup() {
  delay(6000);
  i=4;
}

void loop() { 
      if (i>=0) {
        delay(1000);
        motor.start_turning_right(255, 0.80);
        delay(1000);
        motor.start_turning_left(255, 0.80);
        i--; 
        if (i<0) {
          motor.stop_both_motors();
        }
      }
}
