#include "Motor.h"

#define LED_BUILTIN 8 //ne znam na kojem je pinu

Motor motor(22, 23, 28, 25, 24, 29);
int i;

void setup() {
  pinMode(LED_BUILTIN, OUTPUT);
  delay(6000);
  i=1;
}

void loop() { 
      if (i<=8) {
        motor.start_turning_left(255, 0);
        delay(2000);
        motor.start_turning_right(255, 0);
        delay(2000);
        for(int j=0; j<i ; j++) {
          digitalWrite(LED_BUILTIN, HIGH);   
          delay(500);                       
          digitalWrite(LED_BUILTIN, LOW);
          }
        motor.stop_both_motors();       
        i=i+1; 
      }
}
