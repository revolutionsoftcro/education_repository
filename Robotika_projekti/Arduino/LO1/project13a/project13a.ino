#include "Motor.h"

Motor motor(22, 23, 28, 25, 24, 29);
int i;

void setup() {
  delay(6000);
  i=1;
}

void loop() { 
      if (i) {
        motor.start_driving_forward(255);
        delay(1000);
        motor.start_turning_left(255, 0);
        delay(10000);
        motor.start_driving_forward(255);
        delay(1000);
        motor.start_turning_right(255, 0);
        delay(10000);
        motor.start_driving_forward(255);
        delay(1000);
        motor.stop_both_motors();
        i=0; 
      }
}
