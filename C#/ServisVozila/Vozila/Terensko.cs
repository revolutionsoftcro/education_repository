﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Vozila
{
    public class Terensko : IVozilo
    {
        public string Ime { get; set; }
        public Guid Id { get; set; }

        public Terensko(string ime )
        {
            Ime = ime;
        }

    }
}
