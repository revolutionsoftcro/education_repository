﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Vozila
{
    public interface IVozilo
    {
        Guid Id { get; set; }
        string Ime { get; set; }

    }
}
