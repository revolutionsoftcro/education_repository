﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Vozila
{
    public class VozilaLista
    {
        private List<IVozilo> _list = new List<IVozilo>();
        public int Count { get; set; }
        public VozilaLista()
        {
            Count = 0;
        }

        public void Add(IVozilo vozilo)
        {
            Count++;
            _list.Add(vozilo);
        }
        public void Remove(IVozilo vozilo)
        {
            _list.Remove(vozilo);
            Count--;
        }

        public IEnumerable<IVozilo> GetSvaVozila()
        {
            return _list;
        }

        public IEnumerable<IVozilo> GetTerenskaVozila()
        {
            return _list.Where(x => x is Terensko);
        }

        public IEnumerable<IVozilo> GetLetecaVozila()
        {
            return _list.Where(x => x is Letece);
        }

    }
}
