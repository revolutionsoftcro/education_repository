﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Vozila
{
    public class Letece : IVozilo
    {
        public string Ime { get; set; }
        public Guid Id { get; set; }

        public Letece(string ime)
        {
            Ime = ime;
        }

    }
}
