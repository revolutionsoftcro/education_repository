﻿using System;
using System.Collections.Generic;
using Vozila;
using Zaposlenici;

namespace ServisVozila
{
    class Program
    {
        static void Main(string[] args)
        {

            Tenk T1 = new Tenk("T1");
            Tenk T2 = new Tenk("T2");
            Tenk T3 = new Tenk("T3");

            Zrakoplov Z1 = new Zrakoplov("Z1");
            Zrakoplov Z2 = new Zrakoplov("Z2");

            Helikopter H1 = new Helikopter("H1");

            VozilaLista vozila = new VozilaLista();

            vozila.Add(T1);
            vozila.Add(T2);
            vozila.Add(T3);
            vozila.Add(Z1);
            vozila.Add(Z2);
            vozila.Add(H1);


            Zaposlenik tenkist1 = new Zaposlenik("Marko", new Dozvola(typeof(Tenk)));
            Zaposlenik tenkist2 = new Zaposlenik("Marija", new Dozvola(typeof(Tenk)));
            Zaposlenik tenkist3 = new Zaposlenik("Bruno", new Dozvola(typeof(Tenk)));

            Zaposlenik pilotZrakoplova1 = new Zaposlenik("Franjo", new Dozvola(typeof(Zrakoplov)));
            Zaposlenik pilotZrakoplova2 = new Zaposlenik("Franka", new Dozvola(typeof(Zrakoplov)));

            Zaposlenik pilotHelikoptera1 = new Zaposlenik("Josip", new Dozvola(typeof(Helikopter)));
            Zaposlenik pilotHelikoptera2 = new Zaposlenik("Lucija", new Dozvola(typeof(Helikopter)));

            Zaposlenik pilot1 = new Zaposlenik("Martina", new Dozvola(typeof(Letece)));
            Zaposlenik pilot2 = new Zaposlenik("Luka", new Dozvola(typeof(Letece)));

            Zaposlenik terenski1= new Zaposlenik("Alen", new Dozvola(typeof(Terensko)));
            Zaposlenik terenski2 = new Zaposlenik("Tea", new Dozvola(typeof(Terensko)));

            Zaposlenik pilotITerenski = new Zaposlenik("Tomislav", new Dozvola(typeof(Letece)));
            pilotITerenski.DodajDozvolu(new Dozvola(typeof(Terensko)));


            ZaposleniciLista zaposlenici = new ZaposleniciLista();

            zaposlenici.Add(tenkist1);
            zaposlenici.Add(tenkist2);
            zaposlenici.Add(tenkist3);
            zaposlenici.Add(pilotZrakoplova1);
            zaposlenici.Add(pilotZrakoplova2);
            zaposlenici.Add(pilotHelikoptera1);
            zaposlenici.Add(pilotHelikoptera2);
            zaposlenici.Add(pilot1);
            zaposlenici.Add(pilot2);
            zaposlenici.Add(terenski1);
            zaposlenici.Add(terenski2);
            zaposlenici.Add(pilotITerenski);




            int odabir = 1;
            while (odabir != 0)
            {
                Console.WriteLine();
                Console.WriteLine("----Servis vozila----");
                Console.WriteLine();
                Console.WriteLine("  Odaberite ispis: ");
                Console.WriteLine("  0--Izlaz");
                Console.WriteLine("  1--Terenska vozila");
                Console.WriteLine("  2--Leteća vozila");
                Console.WriteLine("  3--Sva vozila");
                Console.WriteLine("  4--Terenski vozači");
                Console.WriteLine("  5--Piloti");
                Console.WriteLine("  6--Svi zaposlenici");

                Console.WriteLine();
                Console.Write("  Odabir: ");
                odabir=int.Parse(Console.ReadLine());

                switch (odabir)
                {
                    case 1:
                        Console.WriteLine();
                        Console.WriteLine("  Terenska :");
                        PrintVozila(vozila.GetTerenskaVozila());
                        break;
                    case 2:
                        Console.WriteLine();
                        Console.WriteLine("  Leteća :");
                        PrintVozila(vozila.GetLetecaVozila());
                        break;
                    case 3:
                        Console.WriteLine();
                        Console.WriteLine("  Sva :");
                        PrintVozila(vozila.GetSvaVozila());
                        break;
                    case 4:
                        Console.WriteLine();
                        Console.WriteLine("  Terenski vozači :");
                        PrintZaposlenika(zaposlenici.GetTerenskiZaposlenici());
                        break;
                    case 5:
                        Console.WriteLine();
                        Console.WriteLine("  Piloti :");
                        PrintZaposlenika(zaposlenici.GetLeteciZaposlenici());
                        break;
                    case 6:
                        Console.WriteLine();
                        Console.WriteLine("  Sva :");
                        PrintZaposlenika(zaposlenici.GetSviZaposlenici());
                        break;
                    case 0:
                        Console.WriteLine("  Kraj programa!");
                        break;

                }
            }
        }

        static void PrintZaposlenika(IEnumerable<IZaposlenik> lista)
        {
            foreach(var zaposlenik in lista)
            {
                Console.WriteLine("  "+zaposlenik.Ime);
            }
        }

        static void PrintVozila(IEnumerable<IVozilo> lista)
        {
            foreach (var vozilo in lista)
            {
                Console.WriteLine("  "+vozilo.Ime);
            }
        }
    }
}
