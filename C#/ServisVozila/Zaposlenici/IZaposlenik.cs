﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Zaposlenici
{
    public interface IZaposlenik
    {
        Guid Id { get; set; }
        string Ime { get; set; }
        List<Dozvola> Dozvole { get; set; }

        void DodajDozvolu(Dozvola d);
        bool JeTerenskiVozac();
        bool JePilot();
    }
}
