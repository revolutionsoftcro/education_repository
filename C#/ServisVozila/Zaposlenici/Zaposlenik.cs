﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Vozila;

namespace Zaposlenici
{
    public class Zaposlenik : IZaposlenik
    {
        public string Ime { get; set; }
        public List<Dozvola> Dozvole { get; set; }
        public Guid Id { get; set; }

        public Zaposlenik(string ime)
        {
            Dozvole = new List<Dozvola>();
            Ime = ime;
        }

        public Zaposlenik(string ime, Dozvola dozvola)
        {
            Ime = ime;
            Dozvole = new List<Dozvola>();
            Dozvole.Add(dozvola);
        }

        public void DodajDozvolu(Dozvola d)
        {
            Dozvole.Add(d);
        }

        public bool JeTerenskiVozac()
        {
            if (Dozvole.Any(x => x.TipDozvole == typeof(Terensko)))
            {
                return true;
            }
            return false;

        }

        public bool JePilot()
        {
            if(Dozvole.Any(x=>x.TipDozvole==typeof(Letece)))
            {
                return true;
            }
            return false;
        }
    }
}
