﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Vozila;

namespace Zaposlenici
{
    public class ZaposleniciLista
    {
        List<IZaposlenik> _list = new List<IZaposlenik>();

        public void Add(IZaposlenik zaposlenik)
        {
            _list.Add(zaposlenik);
        }

        public void Remove(IZaposlenik zaposlenik)
        {
            _list.Remove(zaposlenik);
        }

        public IEnumerable<IZaposlenik> GetSviZaposlenici()
        {
            return _list;
        }

        public IEnumerable<IZaposlenik> GetTerenskiZaposlenici()
        {
            return _list.Where(x => x.JeTerenskiVozac());
        }

        public IEnumerable<IZaposlenik> GetLeteciZaposlenici()
        {
            return _list.Where(x => x.JePilot());
        }


    }
}
