﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Zaposlenici
{
    public class Dozvola
    {
        public Type TipDozvole { get; set; }
        public Dozvola(Type tipDozvole)
        {
            TipDozvole = tipDozvole;
        }
    }
}
