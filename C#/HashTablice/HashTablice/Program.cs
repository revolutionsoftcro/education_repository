﻿using System;
using System.Collections.Generic;

namespace HashTablice
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("------Hash tablice------");
            Console.WriteLine();


            HashSet<string> hs = new HashSet<string>();

            hs.Add("Marko");
            hs.Add("Marija");
            hs.Add("Luka");
            hs.Add("Lucija");
            hs.Add("Luka");

            Console.WriteLine("Hash Set:");
            Console.WriteLine($"Broj elemenata u hash tablici je {hs.Count}.");
            foreach(var item in hs)
            {
                Console.WriteLine("Vrijednost :"+item+" // Hash: "+item.GetHashCode());
            }

        }
    }
}
