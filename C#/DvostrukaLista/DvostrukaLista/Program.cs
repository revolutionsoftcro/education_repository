﻿using System;

namespace DvostrukaLista
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("------Dvostruka lista------");
            Console.WriteLine();

            DoublyLinkedList<int> L = new DoublyLinkedList<int>();

            L.Clear();
            Console.WriteLine("L.Clear();");
            L.Print();

            L.AddFirst(2);
            Console.WriteLine("L.AddFirst(2);");
            L.Print();

            L.AddFirst(1);
            Console.WriteLine("L.AddFirst(1);");
            L.Print();

            L.AddLast(3);
            Console.WriteLine("L.AddLast(3);");
            L.Print();

            L.AddLast(4);
            Console.WriteLine("L.AddLast(4);");
            L.Print();

            L.AddLast(5);
            Console.WriteLine("L.AddLast(5);");
            L.Print();

            L.AddLast(6);
            Console.WriteLine("L.AddLast(6);");
            L.Print();

            L.RemoveLast();
            Console.WriteLine("L.RemoveLast();");
            L.Print();

            L.RemoveFirst();
            Console.WriteLine("L.RemoveFirst();");
            L.Print();

            L.Remove(4);
            Console.WriteLine("L.Remove(4);");
            L.Print();

        }
    }
}
