﻿using System;

namespace NodeChains
{
    class Program
    {
        static void Main(string[] args)
        {
            //Node
            Node<int> first = new Node<int> { Value = 3 };
            Node<int> middle= new Node<int> { Value = 5 };
            first.Next = middle;
            Node<int> last=new Node<int> { Value = 7 };
            middle.Next = last;

            NodeOp<int>.Print(first);
            Console.WriteLine();

            //SingleLinkedList
            SingleLinkedList<int> L =new SingleLinkedList<int>();
            Node<int> n = new Node<int>();
            n.Value = 2;
            L.AddFirst(n);
            L.Print();

            Node<int> n1 = new Node<int>();
            n1.Value = 3;
            L.AddFirst(n1);
            L.Print();

            Node<int> n2 = new Node<int>();
            n2.Value = 4;
            L.AddFirst(n2);
            L.Print();

            Node<int> n3 = new Node<int>();
            n3.Value = 5;
            L.AddLast(n3);
            L.Print();

            L.RemoveFirst();
            L.Print();

            L.RemoveLast();

            foreach(var b in L)
            {
                Console.WriteLine(b);
            }


        }
    }
}
