﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NodeChains
{
    public static class NodeOp<T>
    {
        public static void Print(Node<T> n)
        {
            while(n!=null)
            {
                Console.WriteLine(n.Value);
                n = n.Next;
            }
        }
    }
}
