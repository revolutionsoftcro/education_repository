﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace NodeChains
{
    public class SingleLinkedList<T>: ICollection<T>
    {
        public Node<T> Head { get; private set; }
        public Node<T> Tail { get; private set; }
        public int Count { get; private set; }

        public SingleLinkedList()
        {
            Count = 0;
        }

        public SingleLinkedList(Node<T> n)
        {
            Head = n;
            Tail = Head;
            Count = 1;
        }
        public SingleLinkedList(T value):this(new Node<T>(value))
        {
        }

        //-----------------Print-----------------
        public void Print()
        {
            Node<T> n;
            n = Head;
            Console.WriteLine("Print:");
            Console.Write("HEAD-->");
            if (n == null)
            {
                Console.Write("null");
            }
            else
            {
                while (n != null)
                {
                    if (n.Next == null)
                    {
                        Console.Write(n.Value);
                    }
                    else
                    {
                        Console.Write(n.Value + "-->");
                    }
                    n = n.Next;
                }
            }
            Console.WriteLine("<--TAIL");
            Console.WriteLine("\n");
        }

        //-----------------Add-----------------
        public void Add(T value)
        {
            AddFirst(value);
        }
        public void AddFirst(T value)
        {
            AddFirst(new Node<T>(value));
        }
        public void AddFirst(Node<T> n)
        {
            Node<T> temp = Head; 
            Head = n;
            Head.Next = temp;
            Count++;

            if (Count == 1)
            {
                Tail = Head;
            }
            
        }

        public void AddLast(T value)
        {
            AddLast(new Node<T>(value));
        }
        public void AddLast(Node<T> n)
        {
            if (Count == 0)
            {
                Head=n;
            }
            else
            {
                Tail.Next = n;
            }
            Tail = n;
            Count++;
        }

        //-----------------Remove-----------------
        public void RemoveLast()
        {
            if (Count > 0)
            {
                if (Count == 1)
                {
                    Head = null;
                    Tail = null;
                }
                else
                {
                    Node<T> n = Head;
                    while (n.Next != Tail)
                    {
                        n = n.Next;
                    }
                    n.Next = null;
                    Tail = n;
                }
                Count--;
            }
        }

        public void RemoveFirst()
        {
            if (Count>0)
            {
                Head = Head.Next;
                Count--;
                if (Count == 0)
                {
                    Tail = null;
                }
            }
        }

        public bool Remove(T item)
        {
            Node<T> previous = null;
            Node<T> n = Head;

            while (n!=null)
            {
                if (n.Value.Equals(item))
                {
                    if (previous != null)
                    {
                        previous.Next = n.Next;
                        if (n.Next == null)
                        {
                            Tail = previous;
                        }
                        Count--;
                    }
                    else
                    {
                        RemoveFirst();
                    }
                    return true;
                }
                previous = n;
                n = n.Next;
            }
            return false; 
        }

        //-----------------Contains-----------------
        public bool Contains(T value)
        {
            Node<T> n = Head;
            while (n != null)
            {
                if (n.Value.Equals(value))
                {
                    return true;
                }
                n = n.Next;
            }
            return false;
        }

        //-----------------Copy-----------------
        public void CopyTo(T[] L, int index)
        {
            Node<T> n = Head;
            while (n != null)
            {
                L[index++] = n.Value;
                n = n.Next;
            }
        }

        //-----------------IsReadOnly-----------------
        public bool IsReadOnly
        {
            get
            {
                return false;
            }
        }


        //-----------------Enumerate(da možemo prolazit kroz listu sa foreach naredbom)-----------------

        public IEnumerator<T> GetEnumerator()
        {
            Node<T> n = Head;
            while (n != null)
            {
                yield return n.Value;
                n = n.Next;
            }
        }
        IEnumerator IEnumerable.GetEnumerator()
        {
            return ((IEnumerable<T>)this).GetEnumerator();
        }

        //-----------------Clear-----------------
        public void Clear()
        {
            Head = null;
            Tail = null;
            Count = 0;
        }
    }
}
