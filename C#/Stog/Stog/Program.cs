﻿using System;
using System.Collections.Generic;

namespace Stog
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("------Stog------");
            Console.WriteLine();

            Console.WriteLine("Primjer Postfix kalkulatora za 567*+1- :") ;

            List<string> L = new List<string>();
            Stack<int> S = new Stack<int>();

            L.Add("5");
            L.Add("6");
            L.Add("7");
            L.Add("*");
            L.Add("+");
            L.Add("1");
            L.Add("-");

            foreach (var token in L)
            {
                int value;
                if(int.TryParse(token, out value))
                {
                    S.Push(value);
                }
                else
                {
                    int r = S.Pop();
                    int l = S.Pop();

                    switch(token)
                    {
                        case "+" :
                            S.Push(l + r);
                            Console.WriteLine($"{l}+{r}={l+r}");
                            break;
                        case "-":
                            S.Push(l - r);
                            Console.WriteLine($"{l}-{r}={l - r}");
                            break;
                        case "*":
                            S.Push(l * r);
                            Console.WriteLine($"{l}*{r}={l * r}");
                            break;
                        case "/":
                            S.Push(l * r);
                            Console.WriteLine($"{l}/{r}={l / r}");
                            break;
                        case "%":
                            S.Push(l % r);
                            Console.WriteLine($"{l}%{r}={l % r}");
                            break;
                        default:
                            throw new ArgumentException(string.Format($"Nepoznata operacija: {token}"));
                    }
                }
            }
            Console.WriteLine($"Rezultat je {S.Pop()}");
        }
    }
}
