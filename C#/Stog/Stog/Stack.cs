﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;

namespace Stog
{
    public class Stack<T> :
        IEnumerable<T>
    {
        private LinkedList<T> _list = new LinkedList<T>();
        public int Count
        {
            get
            {
                return _list.Count;
            }
        }

        public void Push(T item)
        {
            _list.AddFirst(item);
        }

        public T Pop()
        {
            if (_list.Count==0)
            {
                throw new InvalidOperationException("Stog je prazan");
            }

            T value = _list.First.Value;
            _list.RemoveFirst();

            return value;
        }

        public T Peek()
        {
            if (_list.Count == 0)
            {
                throw new InvalidOperationException("Stog je prazan");
            }

            return _list.First.Value;
        }

        public void Clear()
        {
            _list.Clear();
        }

        public IEnumerator<T> GetEnumerator()
        {
            return _list.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return _list.GetEnumerator();
        }
    }
}
