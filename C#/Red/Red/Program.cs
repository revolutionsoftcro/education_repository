﻿using System;

namespace Red
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("------Red------");
            Console.WriteLine();

            Queue<int> Q1 = new Queue<int>();

            Q1.Clear();

            Q1.Enqueue(1);
            Q1.Enqueue(15);
            Q1.Enqueue(2);
            Q1.Enqueue(8);
            Q1.Enqueue(3);

            foreach (var item in Q1)
            {
                Console.WriteLine($"Dequeue: {item}");
            }
            Console.WriteLine();

            Console.WriteLine("------Prioritetni red------");
            Console.WriteLine();

            PriorityQueue<int> Q2 = new PriorityQueue<int>();

            Q2.Clear();

            Q2.Enqueue(1);
            Q2.Enqueue(15);
            Q2.Enqueue(2);
            Q2.Enqueue(8);
            Q2.Enqueue(3);

            foreach (var item in Q2)
            {
                Console.WriteLine($"Dequeue: {item}");
            }


        }
    }
}
