﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;

namespace Stablo
{
    public class BinaryTree<T> :
        IEnumerable<T> where T : IComparable<T>
    {
        private Node<T> _head;
        private int _count;

        public int Count
        {
            get
            {
                return _count;
            }
        }

        public void Add(T value)
        {
            if (_head == null)
            {
                _head = new Node<T>(value);
            }
            else
            {
                AddTo(_head, value);
            }

            _count++;
        }

        private void AddTo(Node<T> node, T value)
        {
            if (value.CompareTo(node.Value) < 0)
            {
                if (node.Left == null)
                {
                    node.Left = new Node<T>(value);
                }
                else
                {
                    AddTo(node.Left, value);
                }
            }
            else
            {
                if(node.Right==null)
                {
                    node.Right = new Node<T>(value);
                }
                else
                {
                    AddTo(node.Right, value);
                }
            }
        }

        public bool Contains(T value)
        {
            Node<T> parent;
            return FindWithParent(value, out parent) != null;
        }

        private Node<T> FindWithParent(T value, out Node<T> parent)
        {
            Node<T> current = _head;
            parent = null;

            while (current!=null)
            {
                int result = current.CompareTo(value);

                if (result<0)
                {
                    parent = current;
                    current = current.Left;
                }
                else if (result>0)
                {
                    parent = current;
                    current = current.Right;
                }
                else
                {
                    break;
                }
            }
            return current;
        }

        public bool Remove(T value)
        {
            Node<T> current, parent;
            current = FindWithParent(value, out parent);

            if (current==null)
            {
                return false;
            }

            _count--;

            if (current.Right==null)
            {
                if (parent==null)
                {
                    _head = current.Left;
                }
                else
                {
                    int result = parent.CompareTo(current.Value);
                    if (result>0)
                    {
                        parent.Left = current.Left;
                    }
                    else //jednake elemente tretiramo kao veće
                    {
                        parent.Right = current.Left;
                    }
                }
            }
            else if(current.Right.Left==null)
            {
                current.Right.Left = current.Left;

                if (parent == null)
                {
                    _head = current.Right;
                }
                else
                {
                    int result = parent.CompareTo(current.Value);
                    if (result > 0)
                    {
                        parent.Left = current.Right;
                    }
                    else //jednake elemente tretiramo kao veće
                    {
                        parent.Right = current.Right;
                    }
                }
            }
            else
            {
                Node<T> leftmost = current.Right.Left;
                Node<T> leftmostParent = current.Right;

                while (leftmost.Left!=null)
                {
                    leftmostParent = leftmost;
                    leftmost = leftmost.Left;
                }

                leftmostParent.Left = leftmost.Right;

                leftmost.Left = current.Left;
                leftmost.Right = current.Right;

                if (parent == null)
                {
                    _head = current.Right;
                }
                else
                {
                    int result = parent.CompareTo(current.Value);
                    if (result > 0)
                    {
                        parent.Left = leftmost;
                    }
                    else //jednake elemente tretiramo kao veće
                    {
                        parent.Right = leftmost;
                    }
                }
            }
            return true;

        }

        public void PreVisitPrint()
        {
            Console.WriteLine();
            Console.Write("Preorder: ");
            PreVisitPrint(_head);
            Console.WriteLine();
        }
        private void PreVisitPrint(Node<T> node)
        {
            if (node != null)
            {
                Console.Write($"{node.Value}  ");
                PreVisitPrint(node.Left);
                PreVisitPrint(node.Right);
            }

        }

        public void InVisitPrint()
        {
            Console.WriteLine();
            Console.Write("Inorder: ");
            InVisitPrint(_head);
            Console.WriteLine();
        }

        private void InVisitPrint(Node<T> node)
        {
            if (node != null)
            {                
                InVisitPrint(node.Left);
                Console.Write($"{node.Value}  ");
                InVisitPrint(node.Right);
            }

        }

        public void PostVisitPrint()
        {
            Console.WriteLine();
            Console.Write("Postorder: ");
            PostVisitPrint(_head);
            Console.WriteLine();
        }

        private void PostVisitPrint(Node<T> node)
        {
            if (node != null)
            {               
                PostVisitPrint(node.Left);
                PostVisitPrint(node.Right);
                Console.Write($"{node.Value}  ");
            }
        }

        public void Clear()
        {
            _head = null;
            _count = 0;
        }

        public IEnumerator<T> InVisit()
        {
            if (_head != null)
            {
                Stack<Node<T>> stack = new Stack<Node<T>>();
                Node<T> current = _head;

                bool goLeftNext = true;

                stack.Push(current);

                //pomoću stoga simuliramo rekurziju
                while (stack.Count>0)
                {
                    if (goLeftNext)
                    {
                        while (current.Left!=null)
                        {
                            stack.Push(current);
                            current = current.Left;
                        }
                    }

                    yield return current.Value;

                    if (current.Right!=null)
                    {
                        current = current.Right;
                        goLeftNext = true;
                    }
                    else
                    {
                        current = stack.Pop();
                        goLeftNext = false;
                    }
                }
            }
        }


        public IEnumerator<T> GetEnumerator()
        {
            return InVisit();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }
    }
}
