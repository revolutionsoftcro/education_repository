﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Stablo
{
    public class Node<T> :
        IComparable<T> where T : IComparable<T>
    {
        public Node<T> Left { get; set; }
        public Node<T> Right { get; set; }
        public T Value { get; set; }

        public Node(T value)
        {
            Value = value;
        }

        public int CompareTo(T other)
        {
            return Value.CompareTo(other);
        }
    }
}
