﻿using System;

namespace Stablo
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("------Stablo------");
            Console.WriteLine();

            BinaryTree<int> T =new BinaryTree<int>();

            T.Add(15);
            T.Add(8);
            T.Add(20);
            T.Add(17);
            T.Add(16);
            T.Add(19);
            T.Add(25);
            T.Add(28);

            T.PreVisitPrint();
            T.InVisitPrint();
            T.PostVisitPrint();

            Console.WriteLine();
            Console.Write("Foreach : ");
            foreach (var x in T)
            {
                Console.Write($"{x}  ");
            }
            Console.WriteLine();
            
        }
    }
}
