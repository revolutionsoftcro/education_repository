﻿using NLog;
using NLog.Config;
using NLog.Targets;

namespace Nlog1
{
    class Program
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();
        static void C()
        {
            logger.Info("Info CCC");
        }
        static void B()
        {
            logger.Trace("Trace BBB");
            logger.Debug("Debug BBB");
            logger.Info("Info BBB");
            C();
            logger.Warn("Warn BBB");
            logger.Error("Error BBB");
            logger.Fatal("Fatal BBB");
        }
        static void A()
        {
            logger.Trace("Trace AAA");
            logger.Debug("Debug AAA");
            logger.Info("Info AAA");
            B();
            logger.Warn("Warn AAA");
            logger.Error("Error AAA");
            logger.Fatal("Fatal AAA");
        }
        static void Main(string[] args)
        {
            logger.Trace("This is a Trace message");
            logger.Debug("This is a Debug message");
            logger.Info("This is an Info message");
            A();
            logger.Warn("This is a Warn message");
            logger.Error("This is an Error message");
            logger.Fatal("This is a Fatal error message");

            //// Step 1. Create configuration object

            //LoggingConfiguration config = new LoggingConfiguration();

            //// Step 2. Create targets and add them to the configuration

            //ColoredConsoleTarget consoleTarget = new ColoredConsoleTarget();
            //config.AddTarget("console", consoleTarget);

            //FileTarget fileTarget = new FileTarget();
            //config.AddTarget("file", fileTarget);

            //// Step 3. Set target properties

            //consoleTarget.Layout = "${date:format=HH\\:MM\\:ss} ${logger} ${message}";
            //fileTarget.FileName = "${basedir}/file.txt";
            //fileTarget.Layout = "${message}";

            //// Step 4. Define rules

            //LoggingRule rule1 = new LoggingRule("*", LogLevel.Debug, consoleTarget);
            //config.LoggingRules.Add(rule1);

            //LoggingRule rule2 = new LoggingRule("*", LogLevel.Debug, fileTarget);
            //config.LoggingRules.Add(rule2);

            //// Step 5. Activate the configuration

            //LogManager.Configuration = config;

            //// Example usage

            //Logger logger1 = LogManager.GetLogger("Example");
            //logger1.Trace("trace log message");
            //logger1.Debug("debug log message");
            //logger1.Info("info log message");
            //logger1.Warn("warn log message");
            //logger1.Error("error log message");
            //logger1.Fatal("fatal log message");
        }
    }
}
