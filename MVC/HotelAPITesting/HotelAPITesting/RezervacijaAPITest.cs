﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Configuration;
using System.Net;
using System.Net.Http;
using System.Text;
using Testing_Resources;

namespace HotelAPITesting
{
    [TestClass]
    public class RezervacijaAPITest
    {      
        private readonly string _route="api/rezervacija";
        private readonly string _route1 = "api/rezervacija/racuni-rezervacija";

        private static readonly string _serverRoute = ConfigurationManager.AppSettings.Get("APIRoute").ToString();
        private static readonly HttpClient _client = Helper.GetClient(_serverRoute);


        [TestMethod]
        public void RezervacijaGetAllTest()
        {
            using (var response= _client.GetAsync(_route).Result)
            {
                Assert.IsNotNull(response, "Response je null");
                Assert.IsTrue(response.StatusCode == HttpStatusCode.OK, "Status nije dobar!");
                var rezervacije=JsonConvert.DeserializeObject<List<RezervacijaDTO>>(response.Content.ReadAsStringAsync().Result);
                Assert.AreNotEqual(0, rezervacije.Count, "Prazna lista!");
                Assert.IsInstanceOfType(rezervacije, typeof(List<RezervacijaDTO>), "Krivi model!");
            }
        }

        [TestMethod]
        public void RacuniGetTest()
        {
            using (var response = _client.GetAsync(_route1).Result)
            {
                Assert.IsNotNull(response, "Response je null");
                Assert.IsTrue(response.StatusCode == HttpStatusCode.OK, "Status nije dobar!");
                var rezervacije = JsonConvert.DeserializeObject<List<RacuniRezervacijaDTO>>(response.Content.ReadAsStringAsync().Result);
                Assert.AreNotEqual(0, rezervacije.Count, "Prazna lista!");
                Assert.IsInstanceOfType(rezervacije, typeof(List<RacuniRezervacijaDTO>), "Krivi model!");
            }
        }

        [TestMethod]
        public void RezervacijaGetNepostojecaRezervacijaTest()
        {
            using (var response = _client.GetAsync(_route + $"/{TestData.nepostojeciId}").Result)
            {
                Assert.IsNotNull(response, "Response je null");
                Assert.IsTrue(response.StatusCode == HttpStatusCode.NotFound, "Status nije dobar!");
            }
        }

        [TestMethod]
        public void RezervacijaGetTest()
        {
            using (var response = _client.GetAsync(_route+$"/{TestData.novaRezervacijaId}").Result)
            {
                Assert.IsNotNull(response, "Response je null");
                Assert.IsTrue(response.StatusCode == HttpStatusCode.OK, "Status nije dobar!");
                var rezervacija = JsonConvert.DeserializeObject<RezervacijaDTO>(response.Content.ReadAsStringAsync().Result);
                Assert.IsInstanceOfType(rezervacija, typeof(RezervacijaDTO), "Krivi model!");
            }
        }

        [TestMethod]
        public void RezervacijaOldPostTest()
        {
            var jsonDTO = new StringContent(JsonConvert.SerializeObject(TestData.rezervacijaOld), Encoding.UTF8, "application/json");
            using (var response = _client.PostAsync(_route, jsonDTO).Result)
            {
                Assert.IsNotNull(response, "Response je null");
                Assert.AreEqual(HttpStatusCode.BadRequest, response.StatusCode, "Ne možete napraviti rezervaciju za datum koji je već prošao!", "Star datum dolaska ne vraća BadRequest!");
            }
        }

        [TestMethod]
        public void RezervacijaOdlazakPostTest()
        {
            var jsonDTO = new StringContent(JsonConvert.SerializeObject(TestData.rezervacijaOdlazak), Encoding.UTF8, "application/json");
            using (var response = _client.PostAsync(_route, jsonDTO).Result)
            {
                Assert.IsNotNull(response, "Response je null");
                Assert.AreEqual(HttpStatusCode.BadRequest, response.StatusCode, "Datum odlaska mora biti nakon datuma dolaska!", "Loš datum odlaska ne vraća BadRequest!");
            }
        }

        [TestMethod]
        public void RezervacijaNepostojeciGostPostTest()
        {
            var jsonDTO = new StringContent(JsonConvert.SerializeObject(TestData.rezervacijaNepostojeciGost), Encoding.UTF8, "application/json");
            using (var response = _client.PostAsync(_route, jsonDTO).Result)
            {
                Assert.IsNotNull(response, "Response je null");
                Assert.AreEqual(HttpStatusCode.BadRequest, response.StatusCode, "Taj gost ne postoji u bazi!", "Nepostojeći gost nije vratio BadRequest!");
            }
        }

        [TestMethod]
        public void RezervacijaNepostojeciHotelPostTest()
        {
            var jsonDTO = new StringContent(JsonConvert.SerializeObject(TestData.rezervacijaNepostojeciHotel), Encoding.UTF8, "application/json");
            using (var response = _client.PostAsync(_route, jsonDTO).Result)
            {
                Assert.IsNotNull(response, "Response je null");
                Assert.AreEqual(HttpStatusCode.BadRequest, response.StatusCode, "Taj hotel ne postoji u bazi!", "Nepostojeći hotel nije vratio BadRequest!");
            }
        }

        [TestMethod]
        public void RezervacijaNepostojeciTipPostTest()
        {
            var jsonDTO = new StringContent(JsonConvert.SerializeObject(TestData.rezervacijaTipSobe), Encoding.UTF8, "application/json");
            using (var response = _client.PostAsync(_route, jsonDTO).Result)
            {
                Assert.IsNotNull(response, "Response je null");
                Assert.AreEqual(HttpStatusCode.BadRequest, response.StatusCode, "Taj tip sobe ne postoji u odabranom hotelu!", "Nepostojeći tip sobe nije vratio BadRequest!");
            }
        }

        [TestMethod]
        public void RezervacijaNepostojeciTerminTest()
        {
            var jsonDTO = new StringContent(JsonConvert.SerializeObject(TestData.rezervacijaTermin), Encoding.UTF8, "application/json");
            using (var response = _client.PostAsync(_route, jsonDTO).Result)
            {
                Assert.IsNotNull(response, "Response je null");
                Assert.AreEqual(HttpStatusCode.BadRequest, response.StatusCode, "Nema slobodnih soba tog tipa za odabrani termin!", "Nepostojeći termin nije vratio BadRequest!");
            }
        }

        [TestMethod]
        public void RezervacijaOkTest()
        {
            int id;
            if (TestData.deleteBool)
            {
                id = TestData.novaRezervacijaId + 2;
            }
            else
            {
                id= TestData.novaRezervacijaId + 1;
            }

            var jsonDTO = new StringContent(JsonConvert.SerializeObject(TestData.rezervacijaOk), Encoding.UTF8, "application/json");
            using (var response = _client.PostAsync(_route, jsonDTO).Result)
            {
                Assert.IsNotNull(response, "Response je null");
                Assert.AreEqual(HttpStatusCode.Created, response.StatusCode, "Status nije dobar!");
                var rezervacija = JsonConvert.DeserializeObject<RezervacijaDTO>(response.Content.ReadAsStringAsync().Result);
                Assert.AreEqual(id, rezervacija.Id, "Krivi Id od napravljenog entiteta!");

            }
            var apiResponse = _client.DeleteAsync(string.Format("api/rezervacija/{0}", id)).Result;
        }

        [TestMethod]
        public void RezervacijaDeleteNepostojecaRezervacijaTest()
        {
            using (var response = _client.DeleteAsync(_route + $"/{TestData.nepostojeciId}").Result)
            {
                Assert.IsNotNull(response, "Response je null");
                Assert.IsTrue(response.StatusCode == HttpStatusCode.NotFound, "Status nije dobar!");
            }
        }

        [TestMethod]
        public void RezervacijaDeleteTest()
        {
            TestData.deleteBool = true;
            var id = TestData._sendData<RezervacijaDTO>("rezervacija", TestData.rezervacijaDelete, _serverRoute, "Id");
            using (var response = _client.DeleteAsync(_route + $"/{id}").Result)
            {
                Assert.IsNotNull(response, "Response je null");
                Assert.IsTrue(response.StatusCode == HttpStatusCode.OK, "Status nije dobar!");
                var rezervacija = JsonConvert.DeserializeObject<RezervacijaDTO>(response.Content.ReadAsStringAsync().Result);
                Assert.AreEqual(id, rezervacija.Id, "Krivi Id od obrisanog entiteta!");
            }
        }

    }
}
