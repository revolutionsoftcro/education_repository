﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Configuration;
using Testing_Resources;

namespace HotelAPITesting
{
    [TestClass]
    public class Initialize
    {
        private static readonly string _serverRoute = ConfigurationManager.AppSettings.Get("APIRoute").ToString();

        [AssemblyInitialize]
        public static void AssemblyInitialize(TestContext tc)
        {
            TestData.NapuniBazu(_serverRoute);
        }

        [AssemblyCleanup]
        public static void AssemblyCleanup()
        {
            TestData.IsprazniBazu(_serverRoute);
        }

    }
}
