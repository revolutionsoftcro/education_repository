﻿using System;
using System.Net.Http;

namespace Testing_Resources
{
    public static class Helper
    {
        private static HttpClient _Client;

        public static HttpClient GetClient(string url)
        {
            if (_Client != null)
                return _Client;
            else
            {
                _Client = new HttpClient
                {
                    BaseAddress = new Uri(url)
                };
                return _Client;
            }
        }
    }
}