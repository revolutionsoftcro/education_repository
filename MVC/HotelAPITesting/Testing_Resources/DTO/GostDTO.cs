﻿namespace Testing_Resources
{
    public class GostDTO
    {
        public int? Id { get; set; }
        public int GradId { get; set; }
        public string Ime { get; set; }
        public string Prezime { get; set; }
        public string Mobitel { get; set; }
        public string Email { get; set; }
        public string Adresa { get; set; }
    }
}