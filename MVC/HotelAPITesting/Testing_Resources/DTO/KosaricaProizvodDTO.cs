﻿namespace Testing_Resources
{
    public class KosaricaProizvodDTO
    {
        public int? Id { get; set; }
        public int ProizvodId { get; set; }
        public string Naziv { get; set; }
        public decimal Kolicina { get; set; }
        public decimal Iznos { get; set; }
        public int? KosaricaId { get; set; }
    }
}