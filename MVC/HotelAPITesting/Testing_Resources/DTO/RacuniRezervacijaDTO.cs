﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Testing_Resources
{
    public class RacuniRezervacijaDTO
    {
        public int Id { get; set; }
        public string Ime { get; set; }
        public string Prezime { get; set; }
        public string  Hotel { get; set; }
        public int BrojSobe { get; set; }
        public decimal UkupanIznos { get; set; }
        public decimal PreostaliDug { get; set; }
    }
}