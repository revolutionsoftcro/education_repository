﻿namespace Testing_Resources
{
    public class UslugeProizvodiDTO
    {
        public int? Id { get; set; }
        public int HotelId { get; set; }
        public string Naziv { get; set; }
        public string Tip { get; set; }
        public string Mjera { get; set; }
        public decimal JedinicnaCijena { get; set; }
    }
}