//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Testing_Resources
{
    using System.Collections.Generic;

    public partial class Racun
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Racun()
        {
            this.StavkaRacun = new HashSet<StavkaRacun>();
        }
    
        public int PK_Racun { get; set; }
        public int FK_Racun_Rezervacija_PK { get; set; }
        public bool Soba { get; set; }
        public decimal Iznos { get; set; }
        public bool isPaid { get; set; }
        public System.DateTime Datum_izrade { get; set; }
    
        public virtual Rezervacija Rezervacija { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<StavkaRacun> StavkaRacun { get; set; }
    }
}
