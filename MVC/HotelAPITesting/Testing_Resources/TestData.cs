﻿using Newtonsoft.Json;
using System;
using System.Net.Http;
using System.Text;

namespace Testing_Resources
{
    public static class TestData
    {
        public static int novaDrzavaId;
        public static int noviGradId;
        public static int noviGost1Id;
        public static int noviGost2Id;
        public static int noviHotelId;
        public static int noviTipSobeId;
        public static int novaSobaId;
        public static int novaRezervacijaId;
        public static int nepostojeciId = 0;
        public static RezervacijaDTO rezervacijaOld;
        public static RezervacijaDTO rezervacijaOdlazak;
        public static RezervacijaDTO rezervacijaNepostojeciGost;
        public static RezervacijaDTO rezervacijaNepostojeciHotel;
        public static RezervacijaDTO rezervacijaTipSobe;
        public static RezervacijaDTO rezervacijaTermin;
        public static RezervacijaDTO rezervacijaOk;
        public static RezervacijaDTO rezervacijaDelete;
        public static bool deleteBool = false;


        public static Drzava novaDrzava() 
        {
            return new Drzava()
            {
                Naziv = "DrzavaTest"
            };            
        }

        public static GradDTO noviGrad() 
        {
            return new GradDTO()
            {
                Grad = "GradTest"
            };            
        }

        public static GostDTO noviGost1()
        {
            return new GostDTO()
            {
                Ime = "Gost1Test",
                Prezime = "Test1",
                Adresa = "TestAdresa",
                Email = "TestEmail",
                Mobitel = "TestMobitel"
            };
        }

        public static GostDTO noviGost2()
        {
            return new GostDTO()
            {
                Ime = "Gost2Test",
                Prezime = "Test2",
                Adresa = "TestAdresa",
                Email = "TestEmail",
                Mobitel = "TestMobitel"
            };
        }

        public static HotelDTO noviHotel()
        {
            return new HotelDTO()
            {
                Naziv = "HotelTest",
                Adresa = "TestAdresa",
                BrojZvjezdica = 5,
                Email = "TestEmail",
                Telefon = "TestTelefon"
            };
        }
            

        public static TipSobe noviTipSobe()
        {
            return new TipSobe()
            {
                Naziv = "TipTest",
                Balkon = true,
                WIFI = true,
                Televizor = true,
                Telefon = true
            };
        }

        public static SobaDTO novaSoba()
        {
            return new SobaDTO()
            {
                Cijena=888
            };
        }

        public static RezervacijaDTO novaRezervacija()
        {
            return new RezervacijaDTO()
            {
                Dolazak=new DateTime(2019, 10, 11),
                Odlazak = new DateTime(2019, 10, 21),
                Popust=21
            };
        }

        public static RezervacijaDTO createRezervacijaOld()
        {
            return new RezervacijaDTO()
            {
                Dolazak = new DateTime(2010, 10, 3),
                Odlazak = new DateTime(2010, 10, 5),
                Popust = 21
            };
        }

        public static RezervacijaDTO createRezervacijaOdlazak()
        {
            return new RezervacijaDTO()
            {
                Dolazak = new DateTime(2019, 10, 3),
                Odlazak = new DateTime(2019, 10, 1),
                Popust = 21
            };
        }

        public static RezervacijaDTO createRezervacijaTipSobe()
        {
            return new RezervacijaDTO()
            {
                TipSobeId = 9999,
                Dolazak = new DateTime(2019, 10, 11),
                Odlazak = new DateTime(2019, 10, 21),
                Popust = 21
            };
        }

        public static RezervacijaDTO createRezervacijaTermin()
        {
            return new RezervacijaDTO()
            {
                Dolazak = new DateTime(2019, 10, 13),
                Odlazak = new DateTime(2019, 10, 18),
                Popust = 21
            };
        }

        public static RezervacijaDTO createRezervacijaOk()
        {
            return new RezervacijaDTO()
            {
                Dolazak = new DateTime(2019, 10, 22),
                Odlazak = new DateTime(2019, 10, 28),
                Popust = 21
            };
        }

        public static RezervacijaDTO createRezervacijaDelete()
        {
            return new RezervacijaDTO()
            {
                Dolazak = new DateTime(2019, 10, 22),
                Odlazak = new DateTime(2019, 10, 28),
                Popust = 21
            };
        }

        public static int _sendData<T>(string routeName, object data, string _serverRoute, string key)
        {
            var jsonDTO = new StringContent(JsonConvert.SerializeObject(data), Encoding.UTF8, "application/json");
            using (var response = Helper.GetClient(_serverRoute).PostAsync(string.Format("api/{0}", routeName), jsonDTO).Result)
            {
                var responseJSON = response.Content.ReadAsStringAsync().Result;
                T responseObject = JsonConvert.DeserializeObject<T>(responseJSON);
                return (int)responseObject.GetType().GetProperty(key).GetValue(responseObject, null);
            }
        }

        public static void NapuniBazu(string _serverRoute)
        {
            novaDrzavaId = _sendData<Drzava>("drzava", novaDrzava(), _serverRoute, "PK_Drzava");

            GradDTO grad = noviGrad();
            grad.DrzavaId = novaDrzavaId;
            noviGradId = _sendData<GradDTO>("grad", grad, _serverRoute, "Id");

            GostDTO gost1 = noviGost1();
            gost1.GradId = noviGradId;
            noviGost1Id = _sendData<GostDTO>("gost", gost1, _serverRoute, "Id");

            GostDTO gost2 = noviGost2();
            gost2.GradId = noviGradId;
            noviGost2Id = _sendData<GostDTO>("gost", gost2, _serverRoute, "Id");

            HotelDTO hotel = noviHotel();
            hotel.GradId = noviGradId;
            noviHotelId = _sendData<HotelDTO>("hotel", hotel, _serverRoute, "Id");

            noviTipSobeId= _sendData<TipSobe>("tipsobe", noviTipSobe(), _serverRoute, "PK_TipSobe");

            SobaDTO soba = novaSoba();
            soba.HotelId = noviHotelId;
            soba.TipId = noviTipSobeId;
            novaSobaId = _sendData<SobaDTO>("soba", soba, _serverRoute, "Broj");

            RezervacijaDTO rezervacija = novaRezervacija();
            rezervacija.GostId = noviGost1Id;
            rezervacija.TipSobeId = noviTipSobeId;
            rezervacija.HotelId = noviHotelId;
            novaRezervacijaId = _sendData<RezervacijaDTO>("rezervacija", rezervacija, _serverRoute, "Id");

            rezervacijaOld = createRezervacijaOld();
            rezervacijaOld.GostId = noviGost2Id;
            rezervacijaOld.HotelId = noviHotelId;
            rezervacijaOld.TipSobeId = noviTipSobeId;

            rezervacijaOdlazak = createRezervacijaOdlazak();
            rezervacijaOdlazak.GostId = noviGost2Id;
            rezervacijaOdlazak.HotelId = noviHotelId;
            rezervacijaOdlazak.TipSobeId = noviTipSobeId;

            rezervacijaNepostojeciGost = createRezervacijaOk();
            rezervacijaNepostojeciGost.GostId = nepostojeciId;
            rezervacijaNepostojeciGost.HotelId = noviHotelId;
            rezervacijaNepostojeciGost.TipSobeId = noviTipSobeId;

            rezervacijaNepostojeciHotel = createRezervacijaOk();
            rezervacijaNepostojeciHotel.GostId = noviGost2Id;
            rezervacijaNepostojeciHotel.HotelId = nepostojeciId;
            rezervacijaNepostojeciHotel.TipSobeId = noviTipSobeId;

            rezervacijaTipSobe = createRezervacijaTipSobe();
            rezervacijaTipSobe.GostId = noviGost2Id;
            rezervacijaTipSobe.HotelId = noviHotelId;
            rezervacijaTipSobe.TipSobeId = noviTipSobeId;

            rezervacijaTermin = createRezervacijaTermin();
            rezervacijaTermin.GostId = noviGost2Id;
            rezervacijaTermin.HotelId = noviHotelId;
            rezervacijaTermin.TipSobeId = noviTipSobeId;

            rezervacijaOk = createRezervacijaOk();
            rezervacijaOk.GostId = noviGost2Id;
            rezervacijaOk.HotelId = noviHotelId;
            rezervacijaOk.TipSobeId = noviTipSobeId;

            rezervacijaDelete = createRezervacijaDelete();
            rezervacijaDelete.GostId = noviGost2Id;
            rezervacijaDelete.HotelId = noviHotelId;
            rezervacijaDelete.TipSobeId = noviTipSobeId;


        }

        public static void IsprazniBazu(string _serverRoute)
        {
            var client = Helper.GetClient(_serverRoute);
            var apiResponse = client.DeleteAsync(string.Format("api/rezervacija/{0}", novaRezervacijaId)).Result;
            apiResponse = client.DeleteAsync(string.Format("api/soba/{0}", novaSobaId)).Result;
            apiResponse = client.DeleteAsync(string.Format("api/tipsobe/{0}", noviTipSobeId)).Result;
            apiResponse = client.DeleteAsync(string.Format("api/hotel/{0}", noviHotelId)).Result;
            apiResponse = client.DeleteAsync(string.Format("api/gost/{0}", noviGost2Id)).Result;
            apiResponse = client.DeleteAsync(string.Format("api/gost/{0}", noviGost1Id)).Result;
            apiResponse = client.DeleteAsync(string.Format("api/grad/{0}", noviGradId)).Result;
            apiResponse = client.DeleteAsync(string.Format("api/drzava/{0}", novaDrzavaId)).Result;
        }
    }
}
