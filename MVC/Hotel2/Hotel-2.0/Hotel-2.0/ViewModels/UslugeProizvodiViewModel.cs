﻿using Hotel_2._0.Models;
using System.Web.Mvc;

namespace Hotel_2._0.ViewModels
{
    public class UslugeProizvodiViewModel : UslugeProizvodi
    {
        public SelectList ListaHotela { get; set; }
    }
}