﻿using Hotel_2._0.Models;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace Hotel_2._0.ViewModels
{
    public class RezervacijaViewModel : Rezervacija
    {
        public SelectList ListaGostiju { get; set; }
        public SelectList ListaHotela { get; set; }
        public SelectList ListaTipova { get; set; }
        public SelectList ListaSoba { get; set; }

        [Required]
        [DisplayName("Hotel")]
        public int PK_Hotel { get; set; }

        [Required]
        [DisplayName("Tip sobe")]
        public int PK_TipSobe { get; set; }

        public bool OdlazakBool { get; set; }
        public bool TipSobeBool { get; set; }
        public bool TerminBool { get; set; }
    }
}