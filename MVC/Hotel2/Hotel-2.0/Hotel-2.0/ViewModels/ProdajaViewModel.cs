﻿using Hotel_2._0.Models;
using System.Collections.Generic;

namespace Hotel_2._0.ViewModels
{
    public class ProdajaViewModel
    {
        public string Kupac { get; set; }
        public IEnumerable<UslugeProizvodi> Items { get; set; }
    }
}