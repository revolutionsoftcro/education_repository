﻿using Hotel_2._0.Models;
using System.Web.Mvc;

namespace Hotel_2._0.ViewModels
{
    public class SobaViewModel : Soba
    {
        public SelectList ListaTipovaSoba { get; set; }
        public SelectList ListaHotela { get; set; }


    }
}