﻿using System.Collections.Generic;

namespace Hotel_2._0.ViewModels
{
    public class KosaricaViewModel
    {
        public string Kupac { get; set; }
        public decimal UkupnaCijena { get; set; }
        public List<KosaricaProizvodViewModel> Items { get; set; }
    }
}