﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace Hotel_2._0.ViewModels
{
    public class SobaRezervacijaViewModel
    {
        public int PK_Soba { get; set; }

        [Required]
        [DisplayName("Tip Sobe")]
        public string TipSobe { get; set; }

        [Required]
        public decimal Cijena { get; set; }
        public int GostPK { get; set; }
        public DateTime Dolazak { get; set; }
        public DateTime Odlazak { get; set; }
        public Nullable<decimal> Popust { get; set; }
    }
}