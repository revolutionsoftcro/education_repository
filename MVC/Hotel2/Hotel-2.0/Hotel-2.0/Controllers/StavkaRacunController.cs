﻿using Hotel_2._0.Models;
using System;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web.Mvc;

namespace Hotel_2._0.Controllers
{
    public class StavkaRacunController : Controller
    {
        private Hotel2Entities db = new Hotel2Entities();
        MojNLog MNLog = new MojNLog(new NLog_html());

        // GET: StavkaRacun
        public ActionResult Index()
        {
            var stavkaRacun = db.StavkaRacun.Include(s => s.Racun).Include(s => s.UslugeProizvodi);
            return View(stavkaRacun.ToList());
        }

        // GET: StavkaRacun/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            StavkaRacun stavkaRacun = db.StavkaRacun.Find(id);
            if (stavkaRacun == null)
            {
                return HttpNotFound();
            }
            return View(stavkaRacun);
        }


        // GET: StavkaRacun/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            StavkaRacun stavkaRacun = db.StavkaRacun.Find(id);
            if (stavkaRacun == null)
            {
                return HttpNotFound();
            }
            return View(stavkaRacun);
        }

        // POST: StavkaRacun/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            StavkaRacun stavkaRacun = db.StavkaRacun.Find(id);
            db.StavkaRacun.Remove(stavkaRacun);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        internal void NapraviStavke(int pK_Racun)
        {
            foreach(var proizvod in db.Kosarica.Where(k=>k.Trenutna==true).Select(p=>p.KosaricaProizvod).FirstOrDefault())
            {
                if (proizvod.Deleted==false)
                {
                    var stavka = new StavkaRacun()
                    {
                        FK_StavkaRacun_Racun_PK = pK_Racun,
                        FK_StavkaRacun_UslugeProizvodi_PK = proizvod.FK_KosaricaProizvod_UslugeProizvodi_PK,
                        Kolicina=proizvod.Kolicina,
                        Iznos=proizvod.Iznos,
                        Datum_izrade=DateTime.Now
                    };

                    db.StavkaRacun.Add(stavka);
                    db.SaveChanges();
                }  
            }
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        protected override void OnException(ExceptionContext filterContext)
        {
            Exception ex = filterContext.Exception;
            filterContext.ExceptionHandled = true;

            var controller = filterContext.RouteData.Values["controller"].ToString();
            var action = filterContext.RouteData.Values["action"].ToString();
            var model = new HandleErrorInfo(ex, controller, action);

            MNLog.Error(this.ControllerContext.RouteData.Values["controller"].ToString(), model.Exception.Message);

            filterContext.Result = new ViewResult
            {
                ViewName = "Error",
                ViewData = new ViewDataDictionary(model)
            };
        }
    }
}
