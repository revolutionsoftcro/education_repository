﻿using Hotel_2._0.Models;
using Hotel_2._0.ViewModels;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web.Mvc;

namespace Hotel_2._0.Controllers
{
    public class RezervacijaController : Controller
    {
        private Hotel2Entities db = new Hotel2Entities();
        MojNLog MNLog = new MojNLog(new NLog_html());

        // GET: Rezervacija
        public ActionResult Index()
        {
            var rezervacija = db.Rezervacija.Include(r => r.Gost).Include(r => r.Soba);
            return View(rezervacija.ToList());
        }

        // GET: Rezervacija/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Rezervacija rezervacija = db.Rezervacija.Find(id);
            if (rezervacija == null)
            {
                return HttpNotFound();
            }
            return View(rezervacija);
        }

        public ActionResult Create()
        {
            var model = new RezervacijaViewModel();
            model.ListaGostiju = new SelectList(db.Gost.OrderBy(g => g.Ime).ThenBy(g => g.Prezime), "PK_Gost", "ImePrezimeGrad");
            model.ListaHotela = new SelectList(db.Hotel.OrderBy(h => h.Naziv), "PK_Hotel", "Naziv");
            model.ListaTipova = new SelectList(db.TipSobe, "PK_TipSobe", "Naziv");
            model.TerminBool = true;
            model.OdlazakBool = true;
            model.TipSobeBool = true;
            return View(model);
        }

        [HttpPost]
        public ActionResult Create(RezervacijaViewModel model)
        {
            if (ModelState.IsValid)
            {
                if (model.Dolazak > model.Odlazak)
                {
                    model.ListaGostiju = new SelectList(db.Gost.OrderBy(g => g.Ime).ThenBy(g => g.Prezime), "PK_Gost", "ImePrezimeGrad");
                    model.ListaHotela = new SelectList(db.Hotel.OrderBy(h => h.Naziv), "PK_Hotel", "Naziv");
                    model.ListaTipova = new SelectList(db.TipSobe, "PK_TipSobe", "Naziv");
                    model.TerminBool = true;
                    model.OdlazakBool = false;
                    model.TipSobeBool = true;
                    return View(model);
                }

                if (!(db.Soba.Where(s => s.FK_Soba_Hotel_PK == model.PK_Hotel && s.FK_Soba_TipSobe_PK == model.PK_TipSobe).Any()))
                {
                    model.ListaGostiju = new SelectList(db.Gost.OrderBy(g => g.Ime).ThenBy(g => g.Prezime), "PK_Gost", "ImePrezimeGrad");
                    model.ListaHotela = new SelectList(db.Hotel.OrderBy(h => h.Naziv), "PK_Hotel", "Naziv");
                    model.ListaTipova = new SelectList(db.TipSobe, "PK_TipSobe", "Naziv");
                    model.TerminBool = true;
                    model.OdlazakBool = true;
                    model.TipSobeBool = false;
                    return View(model);
                }

                Soba soba = new Soba();

                if (db.Soba.Where(s => s.FK_Soba_Hotel_PK == model.PK_Hotel && !(s.Rezervacija.Any(r => !(r.Odlazak <= model.Dolazak || r.Dolazak >= model.Odlazak)))).Any())
                {
                    soba = db.Soba.Where(s => s.FK_Soba_Hotel_PK == model.PK_Hotel && !(s.Rezervacija.Any(r => !(r.Odlazak <= model.Dolazak || r.Dolazak >= model.Odlazak)))).FirstOrDefault();
                }
                else
                {
                    model.ListaGostiju = new SelectList(db.Gost.OrderBy(g => g.Ime).ThenBy(g => g.Prezime), "PK_Gost", "ImePrezimeGrad");
                    model.ListaHotela = new SelectList(db.Hotel.OrderBy(h => h.Naziv), "PK_Hotel", "Naziv");
                    model.ListaTipova = new SelectList(db.TipSobe, "PK_TipSobe", "Naziv");
                    model.TerminBool = false;
                    model.OdlazakBool = true;
                    model.TipSobeBool = true;
                    return View(model);
                }

                Rezervacija rezervacija = new Rezervacija()
                {
                    FK_Rezervacija_Gost_PK = model.FK_Rezervacija_Gost_PK,
                    FK_Rezervacija_Soba_PK = soba.PK_Soba,
                    Dolazak = (DateTime)model.Dolazak,
                    Odlazak = (DateTime)model.Odlazak,
                    Popust = model.Popust,
                    Datum_izrade = DateTime.Now
                };

                db.Rezervacija.Add(rezervacija);
                db.SaveChanges();

                var racunSobe = new RacunController();
                racunSobe.NapraviSobaRn(rezervacija.PK_Rezervacija, soba.Cijena, rezervacija.Popust);
                return RedirectToAction("Index");

            }
            model.ListaGostiju = new SelectList(db.Gost.OrderBy(g => g.Ime).ThenBy(g => g.Prezime), "PK_Gost", "ImePrezimeGrad");
            model.ListaHotela = new SelectList(db.Hotel.OrderBy(h => h.Naziv), "PK_Hotel", "Naziv");
            model.ListaTipova = new SelectList(db.TipSobe, "PK_TipSobe", "Naziv");
            model.TerminBool = true;
            model.OdlazakBool = true;
            model.TipSobeBool = true;
            return View(model);
        }

        // GET: Rezervacija/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Rezervacija rezervacija = db.Rezervacija.Find(id);
            if (rezervacija == null)
            {
                return HttpNotFound();
            }
            var model = new RezervacijaViewModel();
            model.ListaGostiju = new SelectList(db.Gost.OrderBy(g => g.Ime).ThenBy(g => g.Prezime), "PK_Gost", "ImePrezimeGrad");
            model.ListaSoba = new SelectList(db.Soba.OrderBy(s => s.Hotel.Naziv).ThenBy(s => s.TipSobe.Naziv).ThenBy(s => s.PK_Soba), "PK_Soba", "HotelTipBroj");
            return View(model);
        }

        // POST: Rezervacija/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "PK_Rezervacija,FK_Rezervacija_Soba_PK,FK_Rezervacija_Gost_PK,Dolazak,Odlazak,Popust,Datum_izrade")] Rezervacija rezervacija)
        {
            if (ModelState.IsValid)
            {
                db.Entry(rezervacija).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            var model = new RezervacijaViewModel();
            model.ListaGostiju = new SelectList(db.Gost.OrderBy(g => g.Ime).ThenBy(g => g.Prezime), "PK_Gost", "ImePrezimeGrad");
            model.ListaSoba = new SelectList(db.Soba.OrderBy(s => s.Hotel.Naziv).ThenBy(s => s.TipSobe.Naziv).ThenBy(s => s.PK_Soba), "PK_Soba", "HotelTipBroj");
            return View(model);
        }

        // GET: Rezervacija/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Rezervacija rezervacija = db.Rezervacija.Find(id);
            if (rezervacija == null)
            {
                return HttpNotFound();
            }
            return View(rezervacija);
        }

        // POST: Rezervacija/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Rezervacija rezervacija = db.Rezervacija.Find(id);
            db.Rezervacija.Remove(rezervacija);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        public ActionResult RezervacijeRacuni()
        {
            List<RezervacijeRnViewModel> rezervacije = new List<RezervacijeRnViewModel>();
            foreach (var rezervacija in db.Rezervacija.Include(x => x.Gost).Include(x => x.Soba.Hotel).Include(x => x.Racun))
            {
                var rez = new RezervacijeRnViewModel();
                rez.PK_Rezervacija = rezervacija.PK_Rezervacija;
                rez.Ime = rezervacija.Gost.Ime;
                rez.Prezime = rezervacija.Gost.Prezime;
                rez.Hotel = rezervacija.Soba.Hotel.Naziv;
                rez.UkupnaSuma = 0;
                rez.ZaPlatiti = 0;
                foreach (var racun in rezervacija.Racun)
                {
                    if (racun.Soba == true)
                    {
                        rez.UkupnaSuma += racun.Iznos;
                        if (racun.isPaid == false)
                        {
                            rez.ZaPlatiti += racun.Iznos;
                        }
                    }
                    else
                    {
                        foreach (var podracun in racun.StavkaRacun)
                        {
                            rez.UkupnaSuma += podracun.Iznos;
                            if (racun.isPaid == false)
                            {
                                rez.ZaPlatiti += podracun.Iznos;
                            }
                        }
                    }
                }
                rezervacije.Add(rez);
            }
            return View(rezervacije);
        }

        public ActionResult NeplaceniRacuni()
        {
            List<RezervacijeRnViewModel> rezervacije = new List<RezervacijeRnViewModel>();
            foreach (var rezervacija in db.Rezervacija.Include(x => x.Gost).Include(x => x.Soba.Hotel).Include(x => x.Racun))
            {
                var rez = new RezervacijeRnViewModel();
                rez.PK_Rezervacija = rezervacija.PK_Rezervacija;
                rez.Ime = rezervacija.Gost.Ime;
                rez.Prezime = rezervacija.Gost.Prezime;
                rez.Hotel = rezervacija.Soba.Hotel.Naziv;
                rez.UkupnaSuma = 0;
                rez.ZaPlatiti = 0;
                foreach (var racun in rezervacija.Racun)
                {
                    if (racun.Soba == true)
                    {
                        rez.UkupnaSuma += racun.Iznos;
                        if (racun.isPaid == false)
                        {
                            rez.ZaPlatiti += racun.Iznos;
                        }
                    }
                    else
                    {
                        foreach (var podracun in racun.StavkaRacun)
                        {
                            rez.UkupnaSuma += podracun.Iznos;
                            if (racun.isPaid == false)
                            {
                                rez.ZaPlatiti += podracun.Iznos;
                            }
                        }
                    }
                }

                if (rez.ZaPlatiti != 0)
                {
                    rezervacije.Add(rez);
                }
            }
            return View("RezervacijeRacuni", rezervacije);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        protected override void OnException(ExceptionContext filterContext)
        {
            Exception ex = filterContext.Exception;
            filterContext.ExceptionHandled = true;

            var controller = filterContext.RouteData.Values["controller"].ToString();
            var action = filterContext.RouteData.Values["action"].ToString();
            var model = new HandleErrorInfo(ex, controller, action);

            MNLog.Error(this.ControllerContext.RouteData.Values["controller"].ToString(), model.Exception.Message);

            filterContext.Result = new ViewResult
            {
                ViewName = "Error",
                ViewData = new ViewDataDictionary(model)
            };
        }
    }
}
