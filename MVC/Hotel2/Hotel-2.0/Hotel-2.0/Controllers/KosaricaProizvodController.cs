﻿using Hotel_2._0.Models;
using Hotel_2._0.ViewModels;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web.Mvc;

namespace Hotel_2._0.Controllers
{
    public class KosaricaProizvodController : Controller
    {
        private Hotel2Entities db = new Hotel2Entities();
        MojNLog MNLog = new MojNLog(new NLog_html());

        // GET: KosaricaProizvod
        public ActionResult Index()
        {
            var kosaricaProizvod = db.KosaricaProizvod.Include(k => k.Kosarica).Include(k => k.UslugeProizvodi);
            return View(kosaricaProizvod.ToList());
        }

        [HttpPost]
        public ActionResult AddToCart()
        {
            var keys = Request.Form.AllKeys;
            var id = int.Parse(Request.Form.Get(keys[1]));
            var kolicina = decimal.Parse(Request.Form.Get(keys[0]));
            var item = new KosaricaProizvod()
            {
                FK_KosaricaProizvod_Kosarica_PK = db.Kosarica.Where(k => k.Trenutna == true).Select(k => k.PK_Kosarica).FirstOrDefault(),
                FK_KosaricaProizvod_UslugeProizvodi_PK = id,
                Deleted = false,
                Kolicina = kolicina,
                Iznos = kolicina * db.UslugeProizvodi.Where(p => p.PK_UslugeProizvodi == id).Select(c => c.CijenaPoMjeri).FirstOrDefault(),
                Datum_izrade = DateTime.Now
            };

            db.KosaricaProizvod.Add(item);
            db.SaveChanges();

            var kosarica = new KosaricaController();
            kosarica.UpdateIznos(item.FK_KosaricaProizvod_Kosarica_PK, item.Iznos);

            return RedirectToAction("Prodaja", "UslugeProizvodi");
        }

        [HttpPost]
        public ActionResult RemoveFromCart()
        {
            var keys = Request.Form.AllKeys;
            var Id = int.Parse(Request.Form.Get(keys[0]));

            var proizvod_b = new KosaricaProizvod();
            proizvod_b = db.KosaricaProizvod.Where(p => p.PK_KosaricaProizvod == Id).FirstOrDefault();
            proizvod_b.Deleted = true;
            proizvod_b.Datum_brisanja = DateTime.Now;
            db.Entry(proizvod_b).State = EntityState.Modified;
            db.SaveChanges();

            var kosarica = new KosaricaController();
            kosarica.UpdateIznos(proizvod_b.FK_KosaricaProizvod_Kosarica_PK, (0 - proizvod_b.Iznos));

            var kosaricaViewModel = new KosaricaViewModel();
            Rezervacija rez = db.Kosarica.Where(k => k.Trenutna == true).Select(k => k.Rezervacija).FirstOrDefault();
            kosaricaViewModel.Kupac = rez.HotelGostSoba;
            kosaricaViewModel.UkupnaCijena = db.Kosarica.Where(k => k.Trenutna == true).Select(k => k.Iznos).FirstOrDefault();
            kosaricaViewModel.Items = new List<KosaricaProizvodViewModel>();

            foreach (var proizvod in db.KosaricaProizvod.Where(k => k.Kosarica.Trenutna == true && k.Deleted == false))
            {
                var kosaricaProizvodViewModel = new KosaricaProizvodViewModel()
                {
                    Naziv = proizvod.UslugeProizvodi.Naziv,
                    Tip = proizvod.UslugeProizvodi.Tip,
                    CijenaPoMjeri = proizvod.UslugeProizvodi.CijenaPoMjeri,
                    Mjera = proizvod.UslugeProizvodi.Mjera,
                    Kolicina = proizvod.Kolicina,
                    Iznos = proizvod.Iznos,
                    Id = proizvod.PK_KosaricaProizvod
                };

                kosaricaViewModel.Items.Add(kosaricaProizvodViewModel);
            }

            return RedirectToAction("Cart", "Kosarica");
        }

        // GET: KosaricaProizvod/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            KosaricaProizvod kosaricaProizvod = db.KosaricaProizvod.Find(id);
            if (kosaricaProizvod == null)
            {
                return HttpNotFound();
            }
            return View(kosaricaProizvod);
        }


        // GET: KosaricaProizvod/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            KosaricaProizvod kosaricaProizvod = db.KosaricaProizvod.Find(id);
            if (kosaricaProizvod == null)
            {
                return HttpNotFound();
            }
            return View(kosaricaProizvod);
        }

        // POST: KosaricaProizvod/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            KosaricaProizvod kosaricaProizvod = db.KosaricaProizvod.Find(id);
            db.KosaricaProizvod.Remove(kosaricaProizvod);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        protected override void OnException(ExceptionContext filterContext)
        {
            Exception ex = filterContext.Exception;
            filterContext.ExceptionHandled = true;

            var controller = filterContext.RouteData.Values["controller"].ToString();
            var action = filterContext.RouteData.Values["action"].ToString();
            var model = new HandleErrorInfo(ex, controller, action);

            MNLog.Error(this.ControllerContext.RouteData.Values["controller"].ToString(), model.Exception.Message);

            filterContext.Result = new ViewResult
            {
                ViewName = "Error",
                ViewData = new ViewDataDictionary(model)
            };
        }
    }
}
