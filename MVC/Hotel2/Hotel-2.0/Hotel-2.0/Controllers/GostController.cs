﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Hotel_2._0.Models;
using Hotel_2._0.ViewModels;

namespace Hotel_2._0.Controllers
{
    public class GostController : Controller
    {
        private Hotel2Entities db = new Hotel2Entities();
        MojNLog MNLog = new MojNLog(new NLog_html());

        // GET: Gost
        public ActionResult Index()
        {
            var gost = db.Gost.Include(g => g.Grad);
            return View(gost.ToList());
        }

        // GET: Gost/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Gost gost = db.Gost.Find(id);
            if (gost == null)
            {
                return HttpNotFound();
            }
            return View(gost);
        }

        // GET: Gost/Create
        public ActionResult Create()
        {
            var model = new GostViewModel();
            model.ListaGradova=new SelectList(db.Grad.OrderBy(g=>g.Naziv), "PK_Grad", "Naziv");
            return View(model);
        }

        // POST: Gost/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "PK_Gost,Ime,Prezime,Mobitel,Email,FK_Gost_Grad_PK,Adresa")] Gost gost)
        {
            if (ModelState.IsValid)
            {
                db.Gost.Add(gost);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            var model = new GostViewModel();
            model.ListaGradova = new SelectList(db.Grad.OrderBy(g => g.Naziv), "PK_Grad", "Naziv");
            return View(model);
        }

        // GET: Gost/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Gost gost = db.Gost.Find(id);
            if (gost == null)
            {
                return HttpNotFound();
            }
            var model = new GostViewModel();
            model.ListaGradova = new SelectList(db.Grad.OrderBy(g => g.Naziv), "PK_Grad", "Naziv");
            return View(model);
        }

        // POST: Gost/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "PK_Gost,Ime,Prezime,Mobitel,Email,FK_Gost_Grad_PK,Adresa")] Gost gost)
        {
            if (ModelState.IsValid)
            {
                db.Entry(gost).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            var model = new GostViewModel();
            model.ListaGradova = new SelectList(db.Grad.OrderBy(g => g.Naziv), "PK_Grad", "Naziv");
            return View(model);
        }

        // GET: Gost/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Gost gost = db.Gost.Find(id);
            if (gost == null)
            {
                return HttpNotFound();
            }
            return View(gost);
        }

        // POST: Gost/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Gost gost = db.Gost.Find(id);
            db.Gost.Remove(gost);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        protected override void OnException(ExceptionContext filterContext)
        {
            Exception ex = filterContext.Exception;
            filterContext.ExceptionHandled = true;

            var controller = filterContext.RouteData.Values["controller"].ToString();
            var action = filterContext.RouteData.Values["action"].ToString();
            var model = new HandleErrorInfo(ex, controller, action);

            MNLog.Error(this.ControllerContext.RouteData.Values["controller"].ToString(), model.Exception.Message);

            filterContext.Result = new ViewResult
            {
                ViewName = "Error",
                ViewData = new ViewDataDictionary(model)
            };
        }
    }
}
