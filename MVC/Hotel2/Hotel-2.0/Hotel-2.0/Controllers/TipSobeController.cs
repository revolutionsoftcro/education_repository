﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Hotel_2._0.Models;

namespace Hotel_2._0.Controllers
{
    public class TipSobeController : Controller
    {
        private Hotel2Entities db = new Hotel2Entities();
        MojNLog MNLog = new MojNLog(new NLog_html());

        // GET: TipSobe
        public ActionResult Index()
        {
            return View(db.TipSobe.ToList());
        }

        // GET: TipSobe/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TipSobe tipSobe = db.TipSobe.Find(id);
            if (tipSobe == null)
            {
                return HttpNotFound();
            }
            return View(tipSobe);
        }

        // GET: TipSobe/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: TipSobe/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "PK_TipSobe,Naziv,Televizor,Telefon,Balkon,WIFI")] TipSobe tipSobe)
        {
            if (ModelState.IsValid)
            {
                db.TipSobe.Add(tipSobe);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(tipSobe);
        }

        // GET: TipSobe/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TipSobe tipSobe = db.TipSobe.Find(id);
            if (tipSobe == null)
            {
                return HttpNotFound();
            }
            return View(tipSobe);
        }

        // POST: TipSobe/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "PK_TipSobe,Naziv,Televizor,Telefon,Balkon,WIFI")] TipSobe tipSobe)
        {
            if (ModelState.IsValid)
            {
                db.Entry(tipSobe).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(tipSobe);
        }

        // GET: TipSobe/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TipSobe tipSobe = db.TipSobe.Find(id);
            if (tipSobe == null)
            {
                return HttpNotFound();
            }
            return View(tipSobe);
        }

        // POST: TipSobe/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            TipSobe tipSobe = db.TipSobe.Find(id);
            db.TipSobe.Remove(tipSobe);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        protected override void OnException(ExceptionContext filterContext)
        {
            Exception ex = filterContext.Exception;
            filterContext.ExceptionHandled = true;

            var controller = filterContext.RouteData.Values["controller"].ToString();
            var action = filterContext.RouteData.Values["action"].ToString();
            var model = new HandleErrorInfo(ex, controller, action);

            MNLog.Error(this.ControllerContext.RouteData.Values["controller"].ToString(), model.Exception.Message);

            filterContext.Result = new ViewResult
            {
                ViewName = "Error",
                ViewData = new ViewDataDictionary(model)
            };
        }
    }
}
