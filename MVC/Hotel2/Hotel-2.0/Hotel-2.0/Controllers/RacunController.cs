﻿using Hotel_2._0.Models;
using Hotel_2._0.ViewModels;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web.Mvc;

namespace Hotel_2._0.Controllers
{
    public class RacunController : Controller
    {
        private Hotel2Entities db = new Hotel2Entities();
        MojNLog MNLog = new MojNLog(new NLog_html());

        // GET: Racun
        public ActionResult Index()
        {
            var racun = db.Racun.Include(r => r.Rezervacija);
            return View(racun.ToList());
        }

        // GET: Racun/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Racun racun = db.Racun.Find(id);
            if (racun == null)
            {
                return HttpNotFound();
            }
            return View(racun);
        }


        // GET: Racun/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Racun racun = db.Racun.Find(id);
            if (racun == null)
            {
                return HttpNotFound();
            }
            return View(racun);
        }

        // POST: Racun/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Racun racun = db.Racun.Find(id);
            db.Racun.Remove(racun);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        public void NapraviSobaRn(int rezervacija, decimal iznos, decimal? popust)
        {
            if (popust != null)
            {
                iznos = iznos * ((100 - (decimal)popust) / 100);
            }
            Racun racun = new Racun()
            {
                FK_Racun_Rezervacija_PK = rezervacija,
                Iznos = iznos,
                Soba = true,
                isPaid = false,
                Datum_izrade = DateTime.Now
            };
            db.Racun.Add(racun);
            db.SaveChanges();
        }

        internal void NapraviRn()
        {
            var racun = new Racun
            {
                FK_Racun_Rezervacija_PK = db.Kosarica.Where(k => k.Trenutna == true).Select(k => k.FK_Kosarica_Rezervacija_PK).FirstOrDefault(),
                Soba = false,
                isPaid=false,
                Iznos=db.Kosarica.Where(k=>k.Trenutna==true).Select(k=>k.Iznos).FirstOrDefault(),
                Datum_izrade=DateTime.Now
            };
            db.Racun.Add(racun);
            db.SaveChanges();
            var stavke = new StavkaRacunController();
            stavke.NapraviStavke(racun.PK_Racun);

        }

        public ActionResult RacuniView(int? id)
        {
            var racuni = new List<RacuniViewModel>();
            foreach (var racun in db.Racun.Include(x => x.StavkaRacun).Include(x => x.Rezervacija).Where(x => x.FK_Racun_Rezervacija_PK == id))
            {
                var racuniViewModel = new RacuniViewModel();
                racuniViewModel.PK_Racun = racun.PK_Racun;
                racuniViewModel.isPaid = racun.isPaid;
                racuniViewModel.Iznos = 0;

                if (racun.Soba == true)
                {
                    racuniViewModel.Iznos += racun.Iznos;
                }
                else
                {
                    foreach (var podracun in racun.StavkaRacun)
                    {
                        racuniViewModel.Iznos += podracun.Iznos;
                    }
                }

                racuni.Add(racuniViewModel);
            }


            return View(racuni);
        }

        public ActionResult Plati(int? id)
        {
            var racun = db.Racun.Where(r => r.PK_Racun == id).FirstOrDefault();
            racun.isPaid = true;
            db.Entry(racun).State = EntityState.Modified;
            db.SaveChanges();
            return RedirectToAction("RacuniView", new { id = racun.FK_Racun_Rezervacija_PK });
        }

        protected override void OnException(ExceptionContext filterContext)
        {
            Exception ex = filterContext.Exception;
            filterContext.ExceptionHandled = true;

            var controller = filterContext.RouteData.Values["controller"].ToString();
            var action = filterContext.RouteData.Values["action"].ToString();
            var model = new HandleErrorInfo(ex, controller, action);

            MNLog.Error(this.ControllerContext.RouteData.Values["controller"].ToString(), model.Exception.Message);

            filterContext.Result = new ViewResult
            {
                ViewName = "Error",
                ViewData = new ViewDataDictionary(model)
            };
        }
    }
}
