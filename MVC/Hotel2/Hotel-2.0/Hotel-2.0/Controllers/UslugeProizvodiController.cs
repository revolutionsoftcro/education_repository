﻿using Hotel_2._0.Models;
using Hotel_2._0.ViewModels;
using System;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web.Mvc;

namespace Hotel_2._0.Controllers
{
    public class UslugeProizvodiController : Controller
    {
        private Hotel2Entities db = new Hotel2Entities();
        MojNLog MNLog = new MojNLog(new NLog_html());

        // GET: UslugeProizvodi
        public ActionResult Index()
        {
            var uslugeProizvodi = db.UslugeProizvodi.Include(u => u.Hotel);
            return View(uslugeProizvodi.ToList());
        }

        public ActionResult OdabirRezervacije()
        {
            db.Rezervacija.Include(x => x.Soba.Hotel).Include(x => x.Soba.Hotel.Grad).Include(x => x.Gost);
            var model = new OdabirRezervacije();
            model.ListaRezervacija = new SelectList(db.Rezervacija
                .OrderBy(d => d.Soba.Hotel.Grad.Naziv).ThenBy(g => g.Soba.Hotel.Naziv).ThenBy(g => g.Gost.Ime).ThenBy(g => g.Gost.Prezime), "PK_Rezervacija", "HotelGostSoba");
            return View(model);
        }

        public ActionResult Prodaja()
        {
            var hotel = db.Kosarica.Where(k => k.Trenutna == true).Select(h => h.Rezervacija.Soba.FK_Soba_Hotel_PK).FirstOrDefault();
            var uslugeProizvodi = db.UslugeProizvodi.Where(p => p.FK_UslugeProizvodi_Hotel_PK == hotel).Include(u => u.Hotel);
            var prodaja = new ProdajaViewModel();

            var rezervacijaId = db.Kosarica.Where(k => k.Trenutna == true).Select(r => r.FK_Kosarica_Rezervacija_PK).FirstOrDefault();
            var rez = db.Rezervacija.Where(r => r.PK_Rezervacija == rezervacijaId).FirstOrDefault();

            prodaja.Kupac = rez.HotelGostSoba;
            prodaja.Items = uslugeProizvodi;

            return View(prodaja);
        }

        [HttpPost]
        public ActionResult Prodaja(OdabirRezervacije rezervacija)
        {
            var kosarica = new KosaricaController();
            kosarica.Napravi((int)rezervacija.PK_Rezervacija);
            int hotel = db.Rezervacija.Where(r => r.PK_Rezervacija == rezervacija.PK_Rezervacija).Select(r => r.Soba.Hotel.PK_Hotel).FirstOrDefault();

            var uslugeProizvodi = db.UslugeProizvodi.Where(p => p.FK_UslugeProizvodi_Hotel_PK == hotel).Include(u => u.Hotel);
            var prodaja = new ProdajaViewModel();

            var rez = db.Rezervacija.Where(r => r.PK_Rezervacija == rezervacija.PK_Rezervacija).FirstOrDefault();

            prodaja.Kupac = rez.HotelGostSoba;
            prodaja.Items = uslugeProizvodi;

            return View(prodaja);
        }




        // GET: UslugeProizvodi/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            UslugeProizvodi uslugeProizvodi = db.UslugeProizvodi.Find(id);
            if (uslugeProizvodi == null)
            {
                return HttpNotFound();
            }
            return View(uslugeProizvodi);
        }

        // GET: UslugeProizvodi/Create
        public ActionResult Create()
        {
            var model = new UslugeProizvodiViewModel();
            model.ListaHotela = new SelectList(db.Hotel.OrderBy(h => h.Naziv), "PK_Hotel", "Naziv");
            return View(model);
        }

        // POST: UslugeProizvodi/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "PK_UslugeProizvodi,Naziv,Tip,FK_UslugeProizvodi_Hotel_PK,CijenaPoMjeri,Mjera")] UslugeProizvodi uslugeProizvodi)
        {
            if (ModelState.IsValid)
            {
                db.UslugeProizvodi.Add(uslugeProizvodi);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            var model = new UslugeProizvodiViewModel();
            model.ListaHotela = new SelectList(db.Hotel.OrderBy(h => h.Naziv), "PK_Hotel", "Naziv");
            return View(model);
        }

        // GET: UslugeProizvodi/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            UslugeProizvodi uslugeProizvodi = db.UslugeProizvodi.Find(id);
            if (uslugeProizvodi == null)
            {
                return HttpNotFound();
            }
            var model = new UslugeProizvodiViewModel();
            model.ListaHotela = new SelectList(db.Hotel.OrderBy(h => h.Naziv), "PK_Hotel", "Naziv");
            return View(model);
        }

        // POST: UslugeProizvodi/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "PK_UslugeProizvodi,Naziv,Tip,FK_UslugeProizvodi_Hotel_PK,CijenaPoMjeri,Mjera")] UslugeProizvodi uslugeProizvodi)
        {
            if (ModelState.IsValid)
            {
                db.Entry(uslugeProizvodi).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            var model = new UslugeProizvodiViewModel();
            model.ListaHotela = new SelectList(db.Hotel.OrderBy(h => h.Naziv), "PK_Hotel", "Naziv");
            return View(model);
        }

        // GET: UslugeProizvodi/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            UslugeProizvodi uslugeProizvodi = db.UslugeProizvodi.Find(id);
            if (uslugeProizvodi == null)
            {
                return HttpNotFound();
            }
            return View(uslugeProizvodi);
        }

        // POST: UslugeProizvodi/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            UslugeProizvodi uslugeProizvodi = db.UslugeProizvodi.Find(id);
            db.UslugeProizvodi.Remove(uslugeProizvodi);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        protected override void OnException(ExceptionContext filterContext)
        {
            Exception ex = filterContext.Exception;
            filterContext.ExceptionHandled = true;

            var controller = filterContext.RouteData.Values["controller"].ToString();
            var action = filterContext.RouteData.Values["action"].ToString();
            var model = new HandleErrorInfo(ex, controller, action);

            MNLog.Error(this.ControllerContext.RouteData.Values["controller"].ToString(), model.Exception.Message);

            filterContext.Result = new ViewResult
            {
                ViewName = "Error",
                ViewData = new ViewDataDictionary(model)
            };
        }
    }
}
