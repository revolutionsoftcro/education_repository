﻿using Hotel_2._0.Models;
using Hotel_2._0.ViewModels;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web.Mvc;

namespace Hotel_2._0.Controllers
{
    public class KosaricaController : Controller
    {
        private Hotel2Entities db = new Hotel2Entities();
        MojNLog MNLog = new MojNLog(new NLog_html());

        public ActionResult Cart()
        {
            var kosaricaViewModel = new KosaricaViewModel();
            Rezervacija rez = db.Kosarica.Where(k => k.Trenutna == true).Select(k => k.Rezervacija).FirstOrDefault();
            kosaricaViewModel.Kupac = rez.HotelGostSoba;
            kosaricaViewModel.UkupnaCijena = db.Kosarica.Where(k => k.Trenutna == true).Select(k => k.Iznos).FirstOrDefault();
            kosaricaViewModel.Items = new List<KosaricaProizvodViewModel>();

            foreach (var proizvod in db.KosaricaProizvod.Where(k => k.Kosarica.Trenutna == true && k.Deleted == false))
            {
                var kosaricaProizvodViewModel = new KosaricaProizvodViewModel()
                {
                    Naziv = proizvod.UslugeProizvodi.Naziv,
                    Tip = proizvod.UslugeProizvodi.Tip,
                    CijenaPoMjeri = proizvod.UslugeProizvodi.CijenaPoMjeri,
                    Mjera = proizvod.UslugeProizvodi.Mjera,
                    Kolicina = proizvod.Kolicina,
                    Iznos = proizvod.Iznos,
                    Id = proizvod.PK_KosaricaProizvod
                };

                kosaricaViewModel.Items.Add(kosaricaProizvodViewModel);
            }


            return View(kosaricaViewModel);
        }

        public ActionResult Terminate()
        {
            var kosarica = new Kosarica();
            kosarica = db.Kosarica.Where(k => k.Trenutna == true).FirstOrDefault();
            kosarica.Trenutna = false;
            kosarica.Otkazana = true;
            db.Entry(kosarica).State = EntityState.Modified;
            db.SaveChanges();

            return RedirectToAction("Index", "Home");
        }

        public ActionResult Checkout()
        {
            var kosarica = db.Kosarica.Where(k => k.Trenutna == true).FirstOrDefault();
            if (db.Kosarica.Where(k => k.Trenutna == true && k.Iznos != 0).Any())
            {
                var racun = new RacunController();
                racun.NapraviRn();
                kosarica.Trenutna = false;
                db.Entry(kosarica).State = EntityState.Modified;
                db.SaveChanges();
            }
            else
            {
                kosarica.Otkazana = true;
            }

            kosarica.Trenutna = false;
            db.Entry(kosarica).State = EntityState.Modified;
            db.SaveChanges();
            return RedirectToAction("Index", "Home");
        }

        public ActionResult Index()
        {
            var kosarica = db.Kosarica.Include(k => k.Rezervacija);
            return View(kosarica.ToList());
        }

        // GET: Kosarica/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Kosarica kosarica = db.Kosarica.Find(id);
            if (kosarica == null)
            {
                return HttpNotFound();
            }
            return View(kosarica);
        }

        // GET: Kosarica/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Kosarica kosarica = db.Kosarica.Find(id);
            if (kosarica == null)
            {
                return HttpNotFound();
            }
            return View(kosarica);
        }

        // POST: Kosarica/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Kosarica kosarica = db.Kosarica.Find(id);
            db.Kosarica.Remove(kosarica);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        internal void Napravi(int pK_Rezervacija)
        {
            if (db.Kosarica.Where(k => k.Trenutna == true).Any())
            {
                var kosarica_t = db.Kosarica.Where(k => k.Trenutna == true).FirstOrDefault();
                kosarica_t.Trenutna = false;
                kosarica_t.Otkazana = true;
                db.Entry(kosarica_t).State = EntityState.Modified;
                db.SaveChanges();
            }

            Kosarica kosarica = new Kosarica()
            {
                FK_Kosarica_Rezervacija_PK = pK_Rezervacija,
                Trenutna = true,
                Otkazana = false,
                Iznos = 0,
                Datum_izrade = DateTime.Now

            };

            db.Kosarica.Add(kosarica);
            db.SaveChanges();
        }

        internal void UpdateIznos(int kosaricaId, decimal iznos)
        {
            var kosarica = db.Kosarica.Where(k => k.PK_Kosarica == kosaricaId).FirstOrDefault();
            kosarica.Iznos += iznos;
            db.Entry(kosarica).State = EntityState.Modified;
            db.SaveChanges();
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        protected override void OnException(ExceptionContext filterContext)
        {
            Exception ex = filterContext.Exception;
            filterContext.ExceptionHandled = true;

            var controller = filterContext.RouteData.Values["controller"].ToString();
            var action = filterContext.RouteData.Values["action"].ToString();
            var model = new HandleErrorInfo(ex, controller, action);

            MNLog.Error(this.ControllerContext.RouteData.Values["controller"].ToString(), model.Exception.Message);

            filterContext.Result = new ViewResult
            {
                ViewName = "Error",
                ViewData = new ViewDataDictionary(model)
            };
        }
    }
}
