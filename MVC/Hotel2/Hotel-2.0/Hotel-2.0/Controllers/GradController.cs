﻿using Hotel_2._0.Models;
using Hotel_2._0.ViewModels;
using System;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web.Mvc;

namespace Hotel_2._0.Controllers
{
    public class GradController : Controller
    {
        private Hotel2Entities db = new Hotel2Entities();
        MojNLog MNLog = new MojNLog(new NLog_html());

        // GET: Grad
        public ActionResult Index()
        {
            var grad = db.Grad.Include(g => g.Drzava);
            return View(grad.ToList());
        }

        // GET: Grad/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Grad grad = db.Grad.Find(id);
            if (grad == null)
            {
                return HttpNotFound();
            }
            return View(grad);
        }

        // GET: Grad/Create
        public ActionResult Create()
        {
            var model = new GradViewModel();
            model.ListaDrzava = new SelectList(db.Drzava.OrderBy(d => d.Naziv), "PK_Drzava", "Naziv");
            return View(model);
        }

        // POST: Grad/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "PK_Grad,Naziv,FK_Grad_Drzava_PK")] Grad grad)
        {
            if (ModelState.IsValid)
            {
                db.Grad.Add(grad);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            var model = new GradViewModel();
            model.ListaDrzava = new SelectList(db.Drzava.OrderBy(d => d.Naziv), "PK_Drzava", "Naziv");
            return View(model);
        }

        // GET: Grad/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Grad grad = db.Grad.Find(id);
            if (grad == null)
            {
                return HttpNotFound();
            }
            var model = new GradViewModel();
            model.ListaDrzava = new SelectList(db.Drzava.OrderBy(d => d.Naziv), "PK_Drzava", "Naziv");
            return View(model);
        }

        // POST: Grad/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "PK_Grad,Naziv,FK_Grad_Drzava_PK")] Grad grad)
        {
            if (ModelState.IsValid)
            {
                db.Entry(grad).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            var model = new GradViewModel();
            model.ListaDrzava = new SelectList(db.Drzava.OrderBy(d => d.Naziv), "PK_Drzava", "Naziv");
            return View(model);
        }

        // GET: Grad/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Grad grad = db.Grad.Find(id);
            if (grad == null)
            {
                return HttpNotFound();
            }
            return View(grad);
        }

        // POST: Grad/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Grad grad = db.Grad.Find(id);
            db.Grad.Remove(grad);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        protected override void OnException(ExceptionContext filterContext)
        {
            Exception ex = filterContext.Exception;
            filterContext.ExceptionHandled = true;

            var controller = filterContext.RouteData.Values["controller"].ToString();
            var action = filterContext.RouteData.Values["action"].ToString();
            var model = new HandleErrorInfo(ex, controller, action);

            MNLog.Error(this.ControllerContext.RouteData.Values["controller"].ToString(), model.Exception.Message);

            filterContext.Result = new ViewResult
            {
                ViewName = "Error",
                ViewData = new ViewDataDictionary(model)
            };
        }
    }
}
