﻿namespace Hotel_2._0.Controllers
{
    public interface IMoj_NLog
    {
        void Info(string controller, string poruka);
        void Error(string controller, string poruka);
    }
}
