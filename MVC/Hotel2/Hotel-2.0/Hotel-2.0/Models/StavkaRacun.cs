//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Hotel_2._0.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class StavkaRacun
    {
        public int PK_UslugaRacun { get; set; }
        public int FK_StavkaRacun_Racun_PK { get; set; }
        public Nullable<int> FK_StavkaRacun_UslugeProizvodi_PK { get; set; }
        public Nullable<decimal> Kolicina { get; set; }
        public decimal Iznos { get; set; }
        public System.DateTime Datum_izrade { get; set; }
    
        public virtual Racun Racun { get; set; }
        public virtual UslugeProizvodi UslugeProizvodi { get; set; }
    }
}
