USE master
GO

CREATE DATABASE Hotel2
GO

USE Hotel2
GO

CREATE TABLE Drzava (
PK_Drzava			 						INT
											PRIMARY KEY IDENTITY(1,1)
, Naziv										NVARCHAR(30) NOT NULL
)

CREATE TABLE Grad (	
PK_Grad			 							INT
											PRIMARY KEY IDENTITY(1,1)
, Naziv										NVARCHAR(30) NOT NULL
, FK_Grad_Drzava_PK							INT NOT NULL 
											FOREIGN KEY REFERENCES Drzava(PK_Drzava)
)

CREATE TABLE Hotel (
PK_Hotel									INT
											PRIMARY KEY IDENTITY(1,1)
, Naziv										NVARCHAR(30) NOT NULL
, BrojZvjezdica								SMALLINT NOT NULL
, Telefon									NVARCHAR(30) NOT NULL
, Email										NVARCHAR(30) NOT NULL
, FK_Hotel_Grad_PK							INT NOT NULL
											FOREIGN KEY REFERENCES Grad(PK_Grad)						
, Adresa									NVARCHAR (60) NOT NULL
)

CREATE TABLE TipSobe (
PK_TipSobe 									INT
											PRIMARY KEY IDENTITY(1,1)
, Naziv										NVARCHAR(30) NOT NULL
, Televizor									BIT NOT NULL
, Telefon									BIT NOT NULL
, Balkon									BIT NOT NULL
, WIFI										BIT NOT NULL
)

CREATE TABLE Soba (
PK_Soba  									INT 
											PRIMARY KEY IDENTITY(1,1) 
, FK_Soba_TipSobe_PK 						INT NOT NULL
											FOREIGN KEY REFERENCES TipSobe(PK_TipSobe)	
, FK_Soba_Hotel_PK 							INT NOT NULL
											FOREIGN KEY REFERENCES Hotel(PK_Hotel)	
, Cijena									DECIMAL NOT NULL
)

CREATE TABLE Gost (
PK_Gost   									INT
											PRIMARY KEY IDENTITY(1,1)
, Ime										NVARCHAR(30) NOT NULL
, Prezime									NVARCHAR(30) NOT NULL
, Mobitel									NVARCHAR(30) NOT NULL
, Email										NVARCHAR(30) NOT NULL
, FK_Gost_Grad_PK							INT NOT NULL
											FOREIGN KEY REFERENCES Grad(PK_Grad)
, Adresa									NVARCHAR(30) NOT NULL
)

CREATE TABLE Rezervacija (
PK_Rezervacija   							INT
											PRIMARY KEY IDENTITY(1,1)
, FK_Rezervacija_Soba_PK					INT NOT NULL
											FOREIGN KEY REFERENCES Soba(PK_Soba)
, FK_Rezervacija_Gost_PK					INT NOT NULL
											FOREIGN KEY REFERENCES Gost(PK_Gost)
, Dolazak									DATETIME NOT NULL
, Odlazak									DATETIME NOT NULL
, Popust									DECIMAL NULL
, Datum_izrade								DATETIME NOT NULL
)

CREATE TABLE UslugeProizvodi (
PK_UslugeProizvodi   						INT
											PRIMARY KEY IDENTITY(1,1)
, Naziv										NVARCHAR(30) NOT NULL
, Tip										NVARCHAR(30) NOT NULL
, FK_UslugeProizvodi_Hotel_PK 				INT NOT NULL
											FOREIGN KEY REFERENCES Hotel(PK_Hotel)	
, CijenaPoMjeri								DECIMAL NOT NULL
, Mjera										NVARCHAR(20) NOT NULL
)

CREATE TABLE Racun (
PK_Racun    								INT
											PRIMARY KEY IDENTITY(1,1)
, FK_Racun_Rezervacija_PK 					INT NOT NULL
											FOREIGN KEY REFERENCES Rezervacija(PK_Rezervacija)
, Soba										BIT NOT NULL
, Iznos										DECIMAL NOT NULL
, isPaid									BIT NOT NULL
, Datum_izrade								DATETIME NOT NULL
)


CREATE TABLE StavkaRacun (
PK_UslugaRacun    							INT
											PRIMARY KEY IDENTITY(1,1)
, FK_StavkaRacun_Racun_PK 					INT NOT NULL
											FOREIGN KEY REFERENCES Racun(PK_Racun)
, FK_StavkaRacun_UslugeProizvodi_PK 		INT NULL
											FOREIGN KEY REFERENCES UslugeProizvodi(PK_UslugeProizvodi)
, Kolicina									DECIMAL NULL
, Iznos										DECIMAL NOT NULL
, Datum_izrade								DATETIME NOT NULL
)

CREATE TABLE Kosarica (
PK_Kosarica	    							INT
											PRIMARY KEY IDENTITY(1,1)
, FK_Kosarica_Rezervacija_PK 				INT NOT NULL
											FOREIGN KEY REFERENCES Rezervacija(PK_Rezervacija)
, Trenutna									BIT NOT NULL
, Otkazana									BIT NOT NULL
, Iznos										DECIMAL NOT NULL
, Datum_izrade								DATETIME NOT NULL
)

CREATE TABLE KosaricaProizvod (
PK_KosaricaProizvod    						INT
											PRIMARY KEY IDENTITY(1,1)
, FK_KosaricaProizvod_Kosarica_PK 			INT NOT NULL
											FOREIGN KEY REFERENCES Kosarica(PK_Kosarica)
, FK_KosaricaProizvod_UslugeProizvodi_PK 	INT NOT NULL
											FOREIGN KEY REFERENCES UslugeProizvodi(PK_UslugeProizvodi)	
, Kolicina									DECIMAL NOT NULL
, Iznos										DECIMAL NOT NULL
, Deleted									BIT NOT NULL
, Datum_brisanja							DATETIME NULL
, Datum_izrade								DATETIME NOT NULL
)






