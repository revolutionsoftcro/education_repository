USE master
GO

CREATE DATABASE Hotel
GO

USE Hotel
GO

CREATE TABLE Drzava (
PK_Drzava			 					INT
										PRIMARY KEY IDENTITY(1,1)
, Naziv									NVARCHAR(30)
)

CREATE TABLE Grad (
PK_Grad			 						INT
										PRIMARY KEY IDENTITY(1,1)
, Naziv									NVARCHAR(30)
, FK_Grad_Drzava_PK						INT
										FOREIGN KEY REFERENCES Drzava(PK_Drzava)
)

CREATE TABLE Hotel (
PK_Hotel								INT
										PRIMARY KEY IDENTITY(1,1)
, Naziv									NVARCHAR(30)
, BrojZvjezdica							SMALLINT
, Telefon								NVARCHAR(30)
, Email									NVARCHAR(30)
, FK_Hotel_Grad_PK						INT
										FOREIGN KEY REFERENCES Grad(PK_Grad)						
, Adresa								NVARCHAR (60)
)

CREATE TABLE TipSobe (
PK_TipSobe 								INT
										PRIMARY KEY IDENTITY(1,1)
, Naziv									NVARCHAR(30)
, Televizor								BIT
, Telefon								BIT
, Balkon								BIT
, WIFI									BIT
)

CREATE TABLE Soba (
PK_Soba  								INT
										PRIMARY KEY IDENTITY(1,1)
, FK_Soba_TipSobe_PK 					INT
										FOREIGN KEY REFERENCES TipSobe(PK_TipSobe)	
, FK_Soba_Hotel_PK 						INT
										FOREIGN KEY REFERENCES Hotel(PK_Hotel)	
, Cijena								DECIMAL
)

CREATE TABLE Gost (
PK_Gost   								INT
										PRIMARY KEY IDENTITY(1,1)
, Ime									NVARCHAR(30)
, Prezime								NVARCHAR(30)
, Mobitel								NVARCHAR(30)
, Email									NVARCHAR(30)
, FK_Gost_Grad_PK						INT
										FOREIGN KEY REFERENCES Grad(PK_Grad)
, Adresa								NVARCHAR(30)
)

CREATE TABLE Rezervacija (
PK_Rezervacija   						INT
										PRIMARY KEY IDENTITY(1,1)
, FK_Rezervacija_Soba_PK				INT
										FOREIGN KEY REFERENCES Soba(PK_Soba)
, FK_Rezervacija_Gost_PK				INT
										FOREIGN KEY REFERENCES Gost(PK_Gost)
, Dolazak								DATETIME
, Odlazak								DATETIME
, Popust								DECIMAL
)

CREATE TABLE UslugeProizvodi (
PK_UslugeProizvodi   					INT
										PRIMARY KEY IDENTITY(1,1)
, Naziv									NVARCHAR(30)
, Tip									NVARCHAR(30)
, FK_UslugeProizvodi_Hotel_PK 			INT
										FOREIGN KEY REFERENCES Hotel(PK_Hotel)	
, CijenaPoMjeri							DECIMAL
, Mjera									NVARCHAR(20)
)

CREATE TABLE Racun (
PK_Racun    							INT
										PRIMARY KEY IDENTITY(1,1)
, FK_Racun_Rezervacija_PK 				INT
										FOREIGN KEY REFERENCES Rezervacija(PK_Rezervacija)
, isPaid								BIT
)


CREATE TABLE UslugeProizvodiRacun (
PK_UslugaRacun    						INT
										PRIMARY KEY IDENTITY(1,1)
, FK_UslugaRacun_Racun_PK 				INT
										FOREIGN KEY REFERENCES Racun(PK_Racun)
, FK_UslugaRacun_UslugeProizvodi_PK 	INT
										FOREIGN KEY REFERENCES UslugeProizvodi(PK_UslugeProizvodi)	
, Kolicina								DECIMAL
)

CREATE TABLE SobaRacun (
PK_SobaRacun    						INT
										PRIMARY KEY IDENTITY(1,1)
, FK_SobaRacun_Racun_PK 				INT
										FOREIGN KEY REFERENCES Racun(PK_Racun)
, FK_SobaRacun_Soba_PK					INT
										FOREIGN KEY REFERENCES Soba(PK_Soba)	
)




