﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace HotelMVC.ViewModels
{
    public class SobaTipViewModel
    {
        [Required]
        [DisplayName("Hotel")]
        public Nullable<int> FK_Soba_Hotel_PK { get; set; }

        [Required]
        [DisplayName("Naziv za tip sobe")]
        public string TipNaziv { get; set; }

        [Required]
        public Nullable<bool> Televizor { get; set; }

        [Required]
        public Nullable<bool> Telefon { get; set; }

        [Required]
        public Nullable<bool> Balkon { get; set; }

        [Required]
        public Nullable<bool> WIFI { get; set; }

        [Required]
        public Nullable<decimal> Cijena { get; set; }
    }
}