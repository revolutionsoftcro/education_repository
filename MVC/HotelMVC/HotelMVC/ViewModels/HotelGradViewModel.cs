﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace HotelMVC.ViewModels
{
    public class HotelGradViewModel
    {
        [Required]
        public string Naziv { get; set; }

        [Required]
        [DisplayName("Broj zvjezdica")]
        public short BrojZvjezdica { get; set; }

        [Required]
        public string Telefon { get; set; }

        [Required]
        [DataType(DataType.EmailAddress, ErrorMessage = "E-mail is not valid")]
        public string Email { get; set; }

        [Required]
        public string Grad { get; set; }

        [Required]
        [DisplayName("Država")]
        public string Drzava { get; set; }

        [Required]
        public string Adresa { get; set; }
    }
}