﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace HotelMVC.ViewModels
{
    public class OdabirRezervacije
    {
        [DisplayName("Rezervacija")]
        [Required]
        public int PK_Rezervacija { get; set; }
    }
}