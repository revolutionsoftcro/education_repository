﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace HotelMVC.ViewModels
{
    public class HotelRezervacija1ViewModel
    {
        [Required]
        public string Ime { get; set; }

        [Required]
        public string Prezime { get; set; }

        [Required]
        public string Mobitel { get; set; }

        [Required]
        [DataType(DataType.EmailAddress, ErrorMessage = "E-mail is not valid")]
        public string Email { get; set; }

        [Required]
        [DisplayName("Grad")]
        public Nullable<int> PK_Grad { get; set; }

        [Required]
        public string Adresa { get; set; }

        [Required]
        [DisplayName("Hotel")]
        public Nullable<int> PK_Hotel { get; set; }

        [Required]
        [DisplayName("Tip Sobe")]
        public Nullable<int> PK_TipSobe { get; set; }

        [Required]
        [FutureDate]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public DateTime Dolazak { get; set; }

        [Required]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public DateTime Odlazak { get; set; }

        [Required]
        public decimal Popust { get; set; }
    }
}