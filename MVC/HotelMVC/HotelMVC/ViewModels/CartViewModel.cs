﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HotelMVC.ViewModels
{
    public class CartViewModel
    {
        public int Id { get; set; }
        public string Naziv { get; set; }
        public string Tip { get; set; }
        public Nullable<decimal> CijenaPoMjeri { get; set; }
        public string Mjera { get; set; }
        public decimal Kolicina { get; set; }
    }
}