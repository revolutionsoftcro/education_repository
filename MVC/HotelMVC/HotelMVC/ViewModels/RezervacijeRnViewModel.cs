﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace HotelMVC.ViewModels
{
    public class RezervacijeRnViewModel
    {
        [Key]
        public int PK_Rezervacija { get; set; }
        public string Ime { get; set; }
        public string Prezime { get; set; }
        public string Hotel { get; set; }
        [DisplayName("Ukupan iznos")]
        public decimal? UkupnaSuma { get; set; }
        [DisplayName("Preostalo za platiti")]
        public decimal? ZaPlatiti { get; set; }
    }
}