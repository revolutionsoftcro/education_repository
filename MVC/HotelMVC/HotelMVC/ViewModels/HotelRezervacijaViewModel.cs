﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace HotelMVC.ViewModels
{
    public class HotelRezervacijaViewModel
    {
        [Required]
        [DisplayName("Gost")]
        public Nullable<int> PK_Gost { get; set; }

        [Required]
        [DisplayName("Hotel")]
        public Nullable<int> PK_Hotel { get; set; }

        [Required]
        [DisplayName("Tip Sobe")]
        public Nullable<int> PK_TipSobe { get; set; }

        [Required]
        [FutureDate]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public DateTime Dolazak { get; set; }

        [Required]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public DateTime Odlazak { get; set; }

        [Required]
        public decimal Popust { get; set; }
    }
}