﻿using HotelMVC.Models;
using HotelMVC.ViewModels;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web.Mvc;

namespace HotelMVC.Controllers
{
    public class UslugeProizvodiController : Controller
    {
        private HotelEntities db = new HotelEntities();
        MojNLog MNLog = new MojNLog(new NLog_html());
        public static List<KupljenProizvod> proizvodi = new List<KupljenProizvod>();

        // GET: UslugeProizvodi
        public ActionResult Index()
        {
            var uslugeProizvodi = db.UslugeProizvodi.Include(u => u.Hotel);
            return View(uslugeProizvodi.ToList());
        }

        public ActionResult Prodaja()
        {
            var uslugeProizvodi = db.UslugeProizvodi.Include(u => u.Hotel);
            return View(uslugeProizvodi.ToList());
        }

        public ActionResult OdabirRezervacije()
        {
            db.Rezervacija.Include(x => x.Soba.Hotel).Include(x => x.Soba.Hotel.Grad).Include(x => x.Gost);
            ViewBag.PK_Rezervacija = new SelectList(db.Rezervacija
                .OrderBy(d => d.Soba.Hotel.Grad.Naziv).ThenBy(g => g.Soba.Hotel.Naziv).ThenBy(g => g.Gost.Ime).ThenBy(g => g.Gost.Prezime), "PK_Rezervacija", "GradHotelGost");
            return View();
        }

        [HttpPost]
        public ActionResult OdabirRezervacije(OdabirRezervacije rezervacija)
        {
            Session["RezervacijaId"] = (int)rezervacija.PK_Rezervacija;
            Rezervacija rez = db.Rezervacija.Where(x => x.PK_Rezervacija == rezervacija.PK_Rezervacija).FirstOrDefault();
            string s= rez.GradHotelGost;
            Session["Kupac"] = rez.GradHotelGost;
            var uslugeProizvodi = db.UslugeProizvodi.Include(u => u.Hotel);
            return View("Prodaja", uslugeProizvodi.ToList());
        }

        public ActionResult Cart()
        {
            List<CartViewModel> sadrzajKosarice = new List<CartViewModel>();
            if (Session["Cart"] != null)
            {
                proizvodi = (List<KupljenProizvod>)Session["Cart"];
            }
            else
            {
                proizvodi = new List<KupljenProizvod>();
            }

            decimal? ukupno = 0;

            foreach (var item in proizvodi)
            {
                CartViewModel odabranProizvod = new CartViewModel();
                odabranProizvod.Id = item.Id;
                odabranProizvod.Kolicina = item.Kolicina;
                odabranProizvod.Naziv = db.UslugeProizvodi.Where(x => x.PK_UslugeProizvodi == item.Id).Select(x => x.Naziv).First();
                odabranProizvod.Tip = db.UslugeProizvodi.Where(x => x.PK_UslugeProizvodi == item.Id).Select(x => x.Tip).First();
                odabranProizvod.Mjera = db.UslugeProizvodi.Where(x => x.PK_UslugeProizvodi == item.Id).Select(x => x.Mjera).First();
                odabranProizvod.CijenaPoMjeri = db.UslugeProizvodi.Where(x => x.PK_UslugeProizvodi == item.Id).Select(x => x.CijenaPoMjeri).First();
                ukupno += (odabranProizvod.Kolicina * odabranProizvod.CijenaPoMjeri);
                sadrzajKosarice.Add(odabranProizvod);
            }

            ViewBag.Ukupno = ukupno;

            return View(sadrzajKosarice);
        }

        [HttpPost]
        public ActionResult RemoveFromCart()
        {
            var keys = Request.Form.AllKeys;
            var Id = int.Parse(Request.Form.Get(keys[0]));
            proizvodi = (List<KupljenProizvod>)Session["Cart"];
            KupljenProizvod itemm = proizvodi.Where(x => x.Id == Id).FirstOrDefault();
            proizvodi.Remove(itemm);
            Session["Cart"] = proizvodi;

            List<CartViewModel> sadrzajKosarice = new List<CartViewModel>();
            decimal? ukupno = 0;

            foreach (var item in proizvodi)
            {
                CartViewModel odabranProizvod = new CartViewModel();
                odabranProizvod.Id = item.Id;
                odabranProizvod.Kolicina = item.Kolicina;
                odabranProizvod.Naziv = db.UslugeProizvodi.Where(x => x.PK_UslugeProizvodi == item.Id).Select(x => x.Naziv).First();
                odabranProizvod.Tip = db.UslugeProizvodi.Where(x => x.PK_UslugeProizvodi == item.Id).Select(x => x.Tip).First();
                odabranProizvod.Mjera = db.UslugeProizvodi.Where(x => x.PK_UslugeProizvodi == item.Id).Select(x => x.Mjera).First();
                odabranProizvod.CijenaPoMjeri = db.UslugeProizvodi.Where(x => x.PK_UslugeProizvodi == item.Id).Select(x => x.CijenaPoMjeri).First();
                ukupno += (odabranProizvod.Kolicina * odabranProizvod.CijenaPoMjeri);
                sadrzajKosarice.Add(odabranProizvod);
            }

            ViewBag.Ukupno = ukupno;

            return View("Cart", sadrzajKosarice);
        }

        [HttpPost]
        public ActionResult AddToCart()
        {
            KupljenProizvod item = new KupljenProizvod();
            var keys = Request.Form.AllKeys;
            item.Id = int.Parse(Request.Form.Get(keys[1]));
            item.Kolicina = decimal.Parse(Request.Form.Get(keys[0]));
            if (Session["Cart"] != null)
            {
                proizvodi = (List<KupljenProizvod>)Session["Cart"];
            }
            proizvodi.Add(item);
            Session["Cart"] = proizvodi;

            return RedirectToAction("Prodaja");
        }

        public ActionResult Terminate()
        {
            Session["RezervacijaId"] = null;
            Session["Cart"] = null;

            return RedirectToAction("Index", "Home");
        }

        public ActionResult Checkout()
        {
            if (Session["Cart"] != null)
            {
                proizvodi = (List<KupljenProizvod>)Session["Cart"];
                int i = (int)Session["RezervacijaId"];

                Racun racun = new Racun()
                {
                    FK_Racun_Rezervacija_PK = (int)Session["RezervacijaId"],
                    isPaid = false
                };
                db.Racun.Add(racun);
                db.SaveChanges();

                foreach (var item in proizvodi)
                {
                    UslugeProizvodiRacun uslugeProizvodiRacun = new UslugeProizvodiRacun()
                    {
                        FK_UslugaRacun_Racun_PK = racun.PK_Racun,
                        FK_UslugaRacun_UslugeProizvodi_PK = item.Id,
                        Kolicina = (decimal?)item.Kolicina
                    };
                    db.UslugeProizvodiRacun.Add(uslugeProizvodiRacun);
                    db.SaveChanges();
                }
            }

            Session["RezervacijaId"] = null;
            Session["Cart"] = null;

            return RedirectToAction("Index", "Home");
        }


        // GET: UslugeProizvodi/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            UslugeProizvodi uslugeProizvodi = db.UslugeProizvodi.Find(id);
            if (uslugeProizvodi == null)
            {
                return HttpNotFound();
            }
            return View(uslugeProizvodi);
        }

        // GET: UslugeProizvodi/Create
        public ActionResult Create()
        {
            ViewBag.FK_UslugeProizvodi_Hotel_PK = new SelectList(db.Hotel
                .OrderBy(d => d.Grad.Drzava.Naziv).ThenBy(g => g.Grad.Naziv), "PK_Hotel", "DrzavaGradHotel"); ;
            return View();
        }

        // POST: UslugeProizvodi/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "PK_UslugeProizvodi,Naziv,Tip,FK_UslugeProizvodi_Hotel_PK,CijenaPoMjeri,Mjera")] UslugeProizvodi uslugeProizvodi)
        {
            if (ModelState.IsValid)
            {
                db.UslugeProizvodi.Add(uslugeProizvodi);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.FK_UslugeProizvodi_Hotel_PK = new SelectList(db.Hotel
                .OrderBy(d => d.Grad.Drzava.Naziv).ThenBy(g => g.Grad.Naziv), "PK_Hotel", "DrzavaGradHotel");
            return View(uslugeProizvodi);
        }

        // GET: UslugeProizvodi/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            UslugeProizvodi uslugeProizvodi = db.UslugeProizvodi.Find(id);
            if (uslugeProizvodi == null)
            {
                return HttpNotFound();
            }
            ViewBag.FK_UslugeProizvodi_Hotel_PK = new SelectList(db.Hotel, "PK_Hotel", "Naziv", uslugeProizvodi.FK_UslugeProizvodi_Hotel_PK);
            return View(uslugeProizvodi);
        }

        // POST: UslugeProizvodi/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "PK_UslugeProizvodi,Naziv,Tip,FK_UslugeProizvodi_Hotel_PK,CijenaPoMjeri,Mjera")] UslugeProizvodi uslugeProizvodi)
        {
            if (ModelState.IsValid)
            {
                db.Entry(uslugeProizvodi).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.FK_UslugeProizvodi_Hotel_PK = new SelectList(db.Hotel, "PK_Hotel", "Naziv", uslugeProizvodi.FK_UslugeProizvodi_Hotel_PK);
            return View(uslugeProizvodi);
        }

        // GET: UslugeProizvodi/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            UslugeProizvodi uslugeProizvodi = db.UslugeProizvodi.Find(id);
            if (uslugeProizvodi == null)
            {
                return HttpNotFound();
            }
            return View(uslugeProizvodi);
        }

        // POST: UslugeProizvodi/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            UslugeProizvodi uslugeProizvodi = db.UslugeProizvodi.Find(id);
            db.UslugeProizvodi.Remove(uslugeProizvodi);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        protected override void OnException(ExceptionContext filterContext)
        {
            Exception ex = filterContext.Exception;
            filterContext.ExceptionHandled = true;

            var controller = filterContext.RouteData.Values["controller"].ToString();
            var action = filterContext.RouteData.Values["action"].ToString();
            var model = new HandleErrorInfo(ex, controller, action);

            MNLog.Error(this.ControllerContext.RouteData.Values["controller"].ToString(), model.Exception.Message);
            Session["ErrorMsg"] = this.ControllerContext.RouteData.Values["controller"].ToString() + " : " + model.Exception.Message;

            filterContext.Result = new ViewResult
            {
                ViewName = "Error",
                ViewData = new ViewDataDictionary(model)
            };
        }
    }
}
