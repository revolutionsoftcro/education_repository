﻿using System.Web.Mvc;

namespace HotelMVC.Controllers
{
    public class HomeController : Controller
    {
        // GET: Home
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Unos()
        {
            return View();
        }

        public ActionResult Rezervacije()
        {
            return View();
        }

        public ActionResult UslugeIProizvodi()
        {
            return View();
        }

        public ActionResult UnosHotela()
        {
            return View();
        }

        public ActionResult UnosSoba()
        {
            return View();
        }

        public ActionResult Rezervacije1()
        {
            return View();
        }
    }
}