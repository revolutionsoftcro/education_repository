﻿using System.Web.Mvc;

namespace HotelMVC.Controllers
{
    public class ErrorController : Controller
    {

        public ActionResult Error()
        {
            return View();
        }
        public ActionResult Error500()
        {
            return View();
        }

        public ActionResult Error404()
        {
            return View();
        }

    }
}