﻿using HotelMVC.Models;
using HotelMVC.ViewModels;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web.Mvc;

namespace HotelMVC.Controllers
{
    public class RezervacijaController : Controller
    {
        private HotelEntities db = new HotelEntities();
        MojNLog MNLog = new MojNLog(new NLog_html());

        // GET: Rezervacija
        public ActionResult Index()
        {
            var rezervacija = db.Rezervacija.Include(r => r.Gost).Include(r => r.Soba).Include(r => r.Soba.Hotel).Include(r => r.Soba.Hotel.Grad);
            return View(rezervacija.ToList().OrderBy(x => x.Soba.Hotel.Naziv).ThenBy(x => x.Gost.Ime).ThenBy(x => x.Gost.Prezime));
        }

        public ActionResult RezervacijeRacuni()
        {
            List<RezervacijeRnViewModel> rezervacije = new List<RezervacijeRnViewModel>();
            foreach (var rezervacija in db.Rezervacija.Include(x => x.Gost).Include(x => x.Soba.Hotel).Include(x => x.Racun))
            {
                var rez = new RezervacijeRnViewModel();
                rez.PK_Rezervacija = rezervacija.PK_Rezervacija;
                rez.Ime = rezervacija.Gost.Ime;
                rez.Prezime = rezervacija.Gost.Prezime;
                rez.Hotel = rezervacija.Soba.Hotel.Naziv;
                rez.UkupnaSuma = 0;
                rez.ZaPlatiti = 0;
                foreach (var racun in rezervacija.Racun)
                {
                    if (racun.SobaRacun.Any())
                    {
                        decimal? proba = racun.SobaRacun.FirstOrDefault().Soba.Cijena;
                        rez.UkupnaSuma += (racun.SobaRacun.FirstOrDefault().Soba.Cijena * ((100 - rezervacija.Popust) / 100));
                        if (racun.isPaid == false)
                        {
                            rez.ZaPlatiti += (racun.SobaRacun.FirstOrDefault().Soba.Cijena * ((100 - rezervacija.Popust) / 100));
                        }
                    }
                    //var test = racun.UslugeProizvodiRacun.Sum(x => x.Kolicina * x.UslugeProizvodi.CijenaPoMjeri);
                    foreach (var podracun in racun.UslugeProizvodiRacun)
                    {
                        rez.UkupnaSuma += podracun.Kolicina * podracun.UslugeProizvodi.CijenaPoMjeri;
                        if (racun.isPaid == false)
                        {
                            rez.ZaPlatiti += podracun.Kolicina * podracun.UslugeProizvodi.CijenaPoMjeri;
                        }
                    }

                }
                rezervacije.Add(rez);
            }
            return View(rezervacije);
        }

        public ActionResult NeplaceniRacuni()
        {
            List<RezervacijeRnViewModel> rezervacije = new List<RezervacijeRnViewModel>();
            foreach (var rezervacija in db.Rezervacija.Include(x => x.Gost).Include(x => x.Soba.Hotel).Include(x => x.Racun))
            {
                var rez = new RezervacijeRnViewModel();
                rez.PK_Rezervacija = rezervacija.PK_Rezervacija;
                rez.Ime = rezervacija.Gost.Ime;
                rez.Prezime = rezervacija.Gost.Prezime;
                rez.Hotel = rezervacija.Soba.Hotel.Naziv;
                rez.UkupnaSuma = 0;
                rez.ZaPlatiti = 0;
                foreach (var racun in rezervacija.Racun)
                {
                    if (racun.SobaRacun.Any())
                    {
                        decimal? proba = racun.SobaRacun.FirstOrDefault().Soba.Cijena;
                        rez.UkupnaSuma += (racun.SobaRacun.FirstOrDefault().Soba.Cijena * ((100 - rezervacija.Popust) / 100));
                        if (racun.isPaid == false)
                        {
                            rez.ZaPlatiti += (racun.SobaRacun.FirstOrDefault().Soba.Cijena * ((100 - rezervacija.Popust) / 100));
                        }
                    }
                    //var test = racun.UslugeProizvodiRacun.Sum(x => x.Kolicina * x.UslugeProizvodi.CijenaPoMjeri);
                    foreach (var podracun in racun.UslugeProizvodiRacun)
                    {
                        rez.UkupnaSuma += podracun.Kolicina * podracun.UslugeProizvodi.CijenaPoMjeri;
                        if (racun.isPaid == false)
                        {
                            rez.ZaPlatiti += podracun.Kolicina * podracun.UslugeProizvodi.CijenaPoMjeri;
                        }
                    }

                }
                if (rez.ZaPlatiti != 0)
                {
                    rezervacije.Add(rez);
                }
            }
            return View("RezervacijeRacuni", rezervacije);
        }

        public ActionResult RacuniView(int? id)
        {
            var racuni = new List<RacuniViewModel>();
            foreach (var racun in db.Racun.Include(x => x.SobaRacun).Include(x => x.UslugeProizvodiRacun).Include(x => x.Rezervacija).Where(x => x.FK_Racun_Rezervacija_PK == id))
            {
                var racuniViewModel = new RacuniViewModel();
                racuniViewModel.PK_Racun = racun.PK_Racun;
                racuniViewModel.isPaid = racun.isPaid;
                racuniViewModel.Iznos = 0;

                if (racun.SobaRacun.Any())
                {
                    decimal? proba = racun.SobaRacun.FirstOrDefault().Soba.Cijena;
                    racuniViewModel.Iznos += (racun.SobaRacun.FirstOrDefault().Soba.Cijena * ((100 - racun.Rezervacija.Popust) / 100));
                }

                foreach (var podracun in racun.UslugeProizvodiRacun)
                {
                    racuniViewModel.Iznos += podracun.Kolicina * podracun.UslugeProizvodi.CijenaPoMjeri;
                }
                racuni.Add(racuniViewModel);
            }


            return View(racuni);
        }

        public ActionResult Plati(int? id)
        {
            var racun = db.Racun.Where(r => r.PK_Racun == id).FirstOrDefault();
            racun.isPaid = true;
            db.Entry(racun).State = EntityState.Modified;
            db.SaveChanges();
            return RedirectToAction("RacuniView", new { id = racun.FK_Racun_Rezervacija_PK });
        }

        // GET: Rezervacija/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Rezervacija rezervacija = db.Rezervacija.Find(id);
            if (rezervacija == null)
            {
                return HttpNotFound();
            }
            return View(rezervacija);
        }

        // GET: Rezervacija/Create
        public ActionResult Create()
        {

            ViewBag.PK_TipSobe = new SelectList(db.TipSobe, "PK_TipSobe", "Naziv");
            ViewBag.PK_Hotel = new SelectList(db.Hotel
                .OrderBy(d => d.Grad.Drzava.Naziv).ThenBy(g => g.Grad.Naziv), "PK_Hotel", "DrzavaGradHotel");
            ViewBag.PK_Gost = new SelectList(db.Gost, "PK_Gost", "GradImePrezime");
            ViewBag.Termin = true;
            ViewBag.Odlazak = true;
            ViewBag.SobaPostoji = true;
            return View();
        }



        // POST: Rezervacija/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(HotelRezervacijaViewModel hotelRezervacijaViewModel)
        {
            ViewBag.PK_TipSobe = new SelectList(db.TipSobe, "PK_TipSobe", "Naziv");
            ViewBag.PK_Hotel = new SelectList(db.Hotel
                .OrderBy(d => d.Grad.Drzava.Naziv).ThenBy(g => g.Grad.Naziv), "PK_Hotel", "DrzavaGradHotel");
            ViewBag.PK_Gost = new SelectList(db.Gost, "PK_Gost", "GradImePrezime");

            if (ModelState.IsValid)
            {
                if (hotelRezervacijaViewModel.Dolazak > hotelRezervacijaViewModel.Odlazak)
                {
                    ViewBag.Odlazak = false;
                    return View(hotelRezervacijaViewModel);
                }


                Soba soba = new Soba();
                bool sobaPostoji = true;
                soba.FK_Soba_TipSobe_PK = hotelRezervacijaViewModel.PK_TipSobe;
                soba.FK_Soba_Hotel_PK = hotelRezervacijaViewModel.PK_Hotel;

                if (!Termin(soba, ref sobaPostoji, hotelRezervacijaViewModel.Dolazak, hotelRezervacijaViewModel.Odlazak))
                {
                    if (!sobaPostoji)
                    {
                        ViewBag.Odlazak = true;
                        ViewBag.Termin = true;
                        ViewBag.SobaPostoji = false;
                        return View(hotelRezervacijaViewModel);
                    }

                    ViewBag.Odlazak = true;
                    ViewBag.SobaPostoji = true;
                    ViewBag.Termin = false;
                    return View(hotelRezervacijaViewModel);
                }


                Rezervacija rezervacija = new Rezervacija()
                {
                    FK_Rezervacija_Soba_PK = soba.PK_Soba,
                    FK_Rezervacija_Gost_PK = hotelRezervacijaViewModel.PK_Gost,
                    Dolazak = hotelRezervacijaViewModel.Dolazak,
                    Odlazak = hotelRezervacijaViewModel.Odlazak,
                    Popust = hotelRezervacijaViewModel.Popust
                };

                if (!db.Rezervacija.Where(r => r.FK_Rezervacija_Gost_PK == rezervacija.FK_Rezervacija_Gost_PK
                && r.FK_Rezervacija_Soba_PK == rezervacija.FK_Rezervacija_Soba_PK && r.Dolazak == rezervacija.Dolazak && r.Odlazak == rezervacija.Odlazak).Any())
                {
                    db.Rezervacija.Add(rezervacija);
                    db.SaveChanges();

                    Racun racun = new Racun()
                    {
                        FK_Racun_Rezervacija_PK = rezervacija.PK_Rezervacija,
                        isPaid = false
                    };
                    db.Racun.Add(racun);
                    db.SaveChanges();

                    SobaRacun sobaRacun = new SobaRacun()
                    {
                        FK_SobaRacun_Racun_PK = racun.PK_Racun,
                        FK_SobaRacun_Soba_PK = soba.PK_Soba
                    };
                    db.SobaRacun.Add(sobaRacun);
                    db.SaveChanges();
                }

                return RedirectToAction("Index");
            }

            ViewBag.Termin = true;
            ViewBag.Odlazak = true;
            ViewBag.SobaPostoji = true;
            return View(hotelRezervacijaViewModel);
        }

        // GET: Rezervacija/Create2
        public ActionResult Create1()
        {
            ViewBag.PK_TipSobe = new SelectList(db.TipSobe, "PK_TipSobe", "Naziv");
            ViewBag.PK_Hotel = new SelectList(db.Hotel
                .OrderBy(d => d.Grad.Drzava.Naziv).ThenBy(g => g.Grad.Naziv), "PK_Hotel", "DrzavaGradHotel");
            ViewBag.PK_Grad = new SelectList(db.Grad, "PK_Grad", "gradoviDrzave");
            ViewBag.Termin = true;
            ViewBag.Odlazak = true;
            ViewBag.SobaPostoji = true;
            return View();
        }

        // POST: Rezervacija/Create2
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create1(HotelRezervacija1ViewModel hotelRezervacijaViewModel)
        {
            ViewBag.PK_TipSobe = new SelectList(db.TipSobe, "PK_TipSobe", "Naziv");
            ViewBag.PK_Hotel = new SelectList(db.Hotel
                .OrderBy(d => d.Grad.Drzava.Naziv).ThenBy(g => g.Grad.Naziv), "PK_Hotel", "DrzavaGradHotel");
            ViewBag.PK_Grad = new SelectList(db.Grad, "PK_Grad", "gradoviDrzave");
            if (ModelState.IsValid)
            {
                if (hotelRezervacijaViewModel.Dolazak > hotelRezervacijaViewModel.Odlazak)
                {
                    ViewBag.Odlazak = false;
                    return View(hotelRezervacijaViewModel);
                }


                Soba soba = new Soba();
                bool sobaPostoji = true;
                soba.FK_Soba_TipSobe_PK = hotelRezervacijaViewModel.PK_TipSobe;
                soba.FK_Soba_Hotel_PK = hotelRezervacijaViewModel.PK_Hotel;

                if (!Termin(soba, ref sobaPostoji, hotelRezervacijaViewModel.Dolazak, hotelRezervacijaViewModel.Odlazak))
                {
                    if (!sobaPostoji)
                    {
                        ViewBag.Odlazak = true;
                        ViewBag.Termin = true;
                        ViewBag.SobaPostoji = false;
                        return View(hotelRezervacijaViewModel);
                    }

                    ViewBag.Odlazak = true;
                    ViewBag.SobaPostoji = true;
                    ViewBag.Termin = false;
                    return View(hotelRezervacijaViewModel);
                }


                Gost gost = new Gost()
                {
                    Ime = hotelRezervacijaViewModel.Ime,
                    Prezime = hotelRezervacijaViewModel.Prezime,
                    Mobitel = hotelRezervacijaViewModel.Mobitel,
                    Email = hotelRezervacijaViewModel.Email,
                    FK_Gost_Grad_PK = hotelRezervacijaViewModel.PK_Grad,
                    Adresa = hotelRezervacijaViewModel.Adresa
                };

                if (db.Gost.Where(g => g.Ime == gost.Ime && g.Prezime == gost.Prezime && g.Mobitel == gost.Mobitel && g.Adresa == gost.Adresa).Any())
                {
                    gost.PK_Gost = db.Gost.Where(g => g.Ime == gost.Ime && g.Prezime == gost.Prezime && g.Mobitel == gost.Mobitel && g.Adresa == gost.Adresa).First().PK_Gost;
                }
                else
                {
                    db.Gost.Add(gost);
                    db.SaveChanges();
                }


                Rezervacija rezervacija = new Rezervacija()
                {
                    FK_Rezervacija_Soba_PK = soba.PK_Soba,
                    FK_Rezervacija_Gost_PK = gost.PK_Gost,
                    Dolazak = hotelRezervacijaViewModel.Dolazak,
                    Odlazak = hotelRezervacijaViewModel.Odlazak,
                    Popust = hotelRezervacijaViewModel.Popust
                };

                if (!db.Rezervacija.Where(r => r.FK_Rezervacija_Gost_PK == rezervacija.FK_Rezervacija_Gost_PK
                && r.FK_Rezervacija_Soba_PK == rezervacija.FK_Rezervacija_Soba_PK && r.Dolazak == rezervacija.Dolazak && r.Odlazak == rezervacija.Odlazak).Any())
                {
                    db.Rezervacija.Add(rezervacija);
                    db.SaveChanges();

                    Racun racun = new Racun()
                    {
                        FK_Racun_Rezervacija_PK = rezervacija.PK_Rezervacija,
                        isPaid = false
                    };
                    db.Racun.Add(racun);
                    db.SaveChanges();

                    SobaRacun sobaRacun = new SobaRacun()
                    {
                        FK_SobaRacun_Racun_PK = racun.PK_Racun,
                        FK_SobaRacun_Soba_PK = soba.PK_Soba
                    };
                    db.SobaRacun.Add(sobaRacun);
                    db.SaveChanges();
                }

                return RedirectToAction("Index");
            }

            ViewBag.SobaPostoji = true;
            ViewBag.Termin = true;
            ViewBag.Odlazak = true;
            return View(hotelRezervacijaViewModel);
        }

        // GET: Rezervacija/Create2
        public ActionResult Create2()
        {
            ViewBag.PK_TipSobe = new SelectList(db.TipSobe, "PK_TipSobe", "Naziv");
            ViewBag.PK_Hotel = new SelectList(db.Hotel
                .OrderBy(d => d.Grad.Drzava.Naziv).ThenBy(g => g.Grad.Naziv), "PK_Hotel", "DrzavaGradHotel");
            ViewBag.Termin = true;
            ViewBag.Odlazak = true;
            ViewBag.SobaPostoji = true;
            return View();
        }

        // POST: Rezervacija/Create2
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create2(HotelRezervacija2ViewModel hotelRezervacijaViewModel)
        {
            ViewBag.PK_TipSobe = new SelectList(db.TipSobe, "PK_TipSobe", "Naziv");
            ViewBag.PK_Hotel = new SelectList(db.Hotel
                .OrderBy(d => d.Grad.Drzava.Naziv).ThenBy(g => g.Grad.Naziv), "PK_Hotel", "DrzavaGradHotel"); ;
            if (ModelState.IsValid)
            {
                if (hotelRezervacijaViewModel.Dolazak > hotelRezervacijaViewModel.Odlazak)
                {
                    ViewBag.Odlazak = false;
                    return View(hotelRezervacijaViewModel);
                }


                Soba soba = new Soba();
                bool sobaPostoji = true;
                soba.FK_Soba_TipSobe_PK = hotelRezervacijaViewModel.PK_TipSobe;
                soba.FK_Soba_Hotel_PK = hotelRezervacijaViewModel.PK_Hotel;

                if (!Termin(soba, ref sobaPostoji, hotelRezervacijaViewModel.Dolazak, hotelRezervacijaViewModel.Odlazak))
                {
                    if (!sobaPostoji)
                    {
                        ViewBag.Odlazak = true;
                        ViewBag.Termin = true;
                        ViewBag.SobaPostoji = false;
                        return View(hotelRezervacijaViewModel);
                    }

                    ViewBag.Odlazak = true;
                    ViewBag.SobaPostoji = true;
                    ViewBag.Termin = false;
                    return View(hotelRezervacijaViewModel);
                }


                Grad grad = new Grad(hotelRezervacijaViewModel.Drzava, hotelRezervacijaViewModel.Grad);

                Gost gost = new Gost()
                {
                    Ime = hotelRezervacijaViewModel.Ime,
                    Prezime = hotelRezervacijaViewModel.Prezime,
                    Mobitel = hotelRezervacijaViewModel.Mobitel,
                    Email = hotelRezervacijaViewModel.Email,
                    FK_Gost_Grad_PK = grad.PK_Grad,
                    Adresa = hotelRezervacijaViewModel.Adresa
                };

                if (db.Gost.Where(g => g.Ime == gost.Ime && g.Prezime == gost.Prezime && g.Mobitel == gost.Mobitel && g.Adresa == gost.Adresa).Any())
                {
                    gost.PK_Gost = db.Gost.Where(g => g.Ime == gost.Ime && g.Prezime == gost.Prezime && g.Mobitel == gost.Mobitel && g.Adresa == gost.Adresa).First().PK_Gost;
                }
                else
                {
                    db.Gost.Add(gost);
                    db.SaveChanges();
                }


                Rezervacija rezervacija = new Rezervacija()
                {
                    FK_Rezervacija_Soba_PK = soba.PK_Soba,
                    FK_Rezervacija_Gost_PK = gost.PK_Gost,
                    Dolazak = hotelRezervacijaViewModel.Dolazak,
                    Odlazak = hotelRezervacijaViewModel.Odlazak,
                    Popust = hotelRezervacijaViewModel.Popust
                };

                if (!db.Rezervacija.Where(r => r.FK_Rezervacija_Gost_PK == rezervacija.FK_Rezervacija_Gost_PK
                && r.FK_Rezervacija_Soba_PK == rezervacija.FK_Rezervacija_Soba_PK && r.Dolazak == rezervacija.Dolazak && r.Odlazak == rezervacija.Odlazak).Any())
                {
                    db.Rezervacija.Add(rezervacija);
                    db.SaveChanges();

                    Racun racun = new Racun()
                    {
                        FK_Racun_Rezervacija_PK = rezervacija.PK_Rezervacija,
                        isPaid = false
                    };
                    db.Racun.Add(racun);
                    db.SaveChanges();

                    SobaRacun sobaRacun = new SobaRacun()
                    {
                        FK_SobaRacun_Racun_PK = racun.PK_Racun,
                        FK_SobaRacun_Soba_PK = soba.PK_Soba
                    };
                    db.SobaRacun.Add(sobaRacun);
                    db.SaveChanges();
                }

                return RedirectToAction("Index");
            }

            ViewBag.SobaPostoji = true;
            ViewBag.Termin = true;
            ViewBag.Odlazak = true;
            return View(hotelRezervacijaViewModel);
        }

        // GET: Rezervacija/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Rezervacija rezervacija = db.Rezervacija.Find(id);
            if (rezervacija == null)
            {
                return HttpNotFound();
            }
            ViewBag.FK_Rezervacija_Gost_PK = new SelectList(db.Gost, "PK_Gost", "Ime", rezervacija.FK_Rezervacija_Gost_PK);
            ViewBag.FK_Rezervacija_Soba_PK = new SelectList(db.Soba, "PK_Soba", "PK_Soba", rezervacija.FK_Rezervacija_Soba_PK);
            return View(rezervacija);
        }

        // POST: Rezervacija/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "PK_Rezervacija,FK_Rezervacija_Soba_PK,FK_Rezervacija_Gost_PK,Dolazak,Odlazak,Popust")] Rezervacija rezervacija)
        {
            if (ModelState.IsValid)
            {
                db.Entry(rezervacija).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.FK_Rezervacija_Gost_PK = new SelectList(db.Gost, "PK_Gost", "Ime", rezervacija.FK_Rezervacija_Gost_PK);
            ViewBag.FK_Rezervacija_Soba_PK = new SelectList(db.Soba, "PK_Soba", "PK_Soba", rezervacija.FK_Rezervacija_Soba_PK);
            return View(rezervacija);
        }

        // GET: Rezervacija/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Rezervacija rezervacija = db.Rezervacija.Find(id);
            if (rezervacija == null)
            {
                return HttpNotFound();
            }
            return View(rezervacija);
        }

        // POST: Rezervacija/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Rezervacija rezervacija = db.Rezervacija.Find(id);
            db.Rezervacija.Remove(rezervacija);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        internal static bool Termin(Soba soba, ref bool sobaPostoji, DateTime dolazak, DateTime odlazak)
        {
            HotelEntities db = new HotelEntities();

            if (!db.Soba.Where(s => s.FK_Soba_Hotel_PK == soba.FK_Soba_Hotel_PK && s.FK_Soba_TipSobe_PK == soba.FK_Soba_TipSobe_PK).Any())
            {
                sobaPostoji = false;
                return false;
            }

            db.Rezervacija.Include(x => x.Soba);
            if (!db.Rezervacija.Where(r => r.Soba.FK_Soba_TipSobe_PK == soba.FK_Soba_TipSobe_PK && r.Soba.FK_Soba_Hotel_PK == soba.FK_Soba_Hotel_PK).Any())
            {
                soba.PK_Soba = db.Soba.Where(s => s.FK_Soba_Hotel_PK == soba.FK_Soba_Hotel_PK && s.FK_Soba_TipSobe_PK == soba.FK_Soba_TipSobe_PK).First().PK_Soba;
                return true;
            }

            if (db.Soba.Where(s => s.FK_Soba_Hotel_PK == soba.FK_Soba_Hotel_PK && s.FK_Soba_TipSobe_PK == soba.FK_Soba_TipSobe_PK &&
                            !(s.Rezervacija.Any(r => !(r.Odlazak <= dolazak || r.Dolazak >= odlazak)))).Any())
            {

                soba.PK_Soba = db.Soba.Where(s => s.FK_Soba_Hotel_PK == soba.FK_Soba_Hotel_PK && s.FK_Soba_TipSobe_PK == soba.FK_Soba_TipSobe_PK
                                     && !(s.Rezervacija.Any(r => !(r.Odlazak <= dolazak || r.Dolazak >= odlazak)))).First().PK_Soba;

                return true;
            }

            return false;
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        protected override void OnException(ExceptionContext filterContext)
        {
            Exception ex = filterContext.Exception;
            filterContext.ExceptionHandled = true;

            var controller = filterContext.RouteData.Values["controller"].ToString();
            var action = filterContext.RouteData.Values["action"].ToString();
            var model = new HandleErrorInfo(ex, controller, action);

            MNLog.Error(this.ControllerContext.RouteData.Values["controller"].ToString(), model.Exception.Message);
            Session["ErrorMsg"] = this.ControllerContext.RouteData.Values["controller"].ToString() + " : " + model.Exception.Message;

            filterContext.Result = new ViewResult
            {
                ViewName = "Error",
                ViewData = new ViewDataDictionary(model)
            };
        }
    }
}
