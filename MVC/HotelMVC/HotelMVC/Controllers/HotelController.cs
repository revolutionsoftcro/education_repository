﻿using HotelMVC.Models;
using HotelMVC.ViewModels;
using System;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web.Mvc;

namespace HotelMVC.Controllers
{
    public class HotelController : Controller
    {
        private HotelEntities db = new HotelEntities();
        MojNLog MNLog = new MojNLog(new NLog_html());

        // GET: Hotel
        public ActionResult Index()
        {
            var hotel = db.Hotel.Include(h => h.Grad).Include(d => d.Grad.Drzava)
                .OrderBy(x => x.Grad.Drzava.Naziv).ThenBy(x => x.Grad.Naziv);
            return View(hotel.ToList());
        }

        // GET: Hotel/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Hotel hotel = db.Hotel.Find(id);
            if (hotel == null)
            {
                return HttpNotFound();
            }
            return View(hotel);
        }

        // GET: Hotel/Create
        public ActionResult Create()
        {

            db.Grad.Include(x => x.Drzava);
            ViewBag.FK_Hotel_Grad_PK = new SelectList(db.Grad.OrderBy(x => x.Drzava.Naziv).ThenBy(x => x.Naziv), "PK_Grad", "gradoviDrzave");
            return View();
        }

        // POST: Hotel/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "PK_Hotel,Naziv,BrojZvjezdica,Telefon,Email,FK_Hotel_Grad_PK,Adresa")] Hotel hotel)
        {
            if (ModelState.IsValid)
            {
                db.Hotel.Add(hotel);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            db.Grad.Include(x => x.Drzava);
            ViewBag.FK_Hotel_Grad_PK = new SelectList(db.Grad.OrderBy(x => x.Drzava.Naziv).ThenBy(x => x.Naziv), "PK_Grad", "gradoviDrzave", hotel.FK_Hotel_Grad_PK);
            return View(hotel);
        }

        // GET: Hotel/Create1
        public ActionResult Create1()
        {
            return View();
        }

        // POST: Hotel/Create1
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create1(HotelGradViewModel viewHotel)
        {
            if (ModelState.IsValid)
            {
                Grad grad = new Grad(viewHotel.Drzava, viewHotel.Grad);

                Hotel hotel = new Hotel()
                {
                    FK_Hotel_Grad_PK = grad.PK_Grad,
                    Naziv = viewHotel.Naziv,
                    BrojZvjezdica = viewHotel.BrojZvjezdica,
                    Telefon = viewHotel.Telefon,
                    Email = viewHotel.Email,
                    Adresa = viewHotel.Adresa
                };
                db.Hotel.Add(hotel);
                db.SaveChanges();
                return RedirectToAction("Index");
            }


            return View(viewHotel);
        }

        // GET: Hotel/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Hotel hotel = db.Hotel.Find(id);
            if (hotel == null)
            {
                return HttpNotFound();
            }
            ViewBag.FK_Hotel_Grad_PK = new SelectList(db.Grad, "PK_Grad", "Naziv", hotel.FK_Hotel_Grad_PK);
            return View(hotel);
        }

        // POST: Hotel/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "PK_Hotel,Naziv,BrojZvjezdica,Telefon,Email,FK_Hotel_Grad_PK,Adresa")] Hotel hotel)
        {
            if (ModelState.IsValid)
            {
                db.Entry(hotel).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.FK_Hotel_Grad_PK = new SelectList(db.Grad, "PK_Grad", "Naziv", hotel.FK_Hotel_Grad_PK);
            return View(hotel);
        }

        // GET: Hotel/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Hotel hotel = db.Hotel.Find(id);
            if (hotel == null)
            {
                return HttpNotFound();
            }
            return View(hotel);
        }

        // POST: Hotel/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Hotel hotel = db.Hotel.Find(id);
            db.Hotel.Remove(hotel);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        protected override void OnException(ExceptionContext filterContext)
        {
            Exception ex = filterContext.Exception;
            filterContext.ExceptionHandled = true;

            var controller = filterContext.RouteData.Values["controller"].ToString();
            var action = filterContext.RouteData.Values["action"].ToString();
            var model = new HandleErrorInfo(ex, controller, action);

            MNLog.Error(this.ControllerContext.RouteData.Values["controller"].ToString(), model.Exception.Message);
            Session["ErrorMsg"] = this.ControllerContext.RouteData.Values["controller"].ToString() + " : " + model.Exception.Message;

            filterContext.Result = new ViewResult
            {
                ViewName = "Error",
                ViewData = new ViewDataDictionary(model)
            };
        }
    }
}
