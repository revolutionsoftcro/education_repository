﻿using HotelMVC.Models;
using HotelMVC.ViewModels;
using System;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web.Mvc;

namespace HotelMVC.Controllers
{
    public class SobaController : Controller
    {
        private HotelEntities db = new HotelEntities();
        MojNLog MNLog = new MojNLog(new NLog_html());

        // GET: Soba
        public ActionResult Index()
        {
            var soba = db.Soba.Include(s => s.Hotel).Include(s => s.TipSobe);
            return View(soba.ToList());
        }

        // GET: Soba/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Soba soba = db.Soba.Find(id);
            if (soba == null)
            {
                return HttpNotFound();
            }
            return View(soba);
        }

        // GET: Soba/Create
        public ActionResult Create()
        {
            ViewBag.FK_Soba_Hotel_PK = new SelectList(db.Hotel
                .OrderBy(d => d.Grad.Drzava.Naziv).ThenBy(g => g.Grad.Naziv), "PK_Hotel", "DrzavaGradHotel");
            ViewBag.FK_Soba_TipSobe_PK = new SelectList(db.TipSobe, "PK_TipSobe", "Naziv");
            return View();
        }

        // POST: Soba/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "PK_Soba,FK_Soba_TipSobe_PK,FK_Soba_Hotel_PK,Cijena")] Soba soba)
        {
            if (ModelState.IsValid)
            {
                db.Soba.Add(soba);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.FK_Soba_Hotel_PK = new SelectList(db.Hotel
                .OrderBy(d => d.Grad.Drzava.Naziv).ThenBy(g => g.Grad.Naziv), "PK_Hotel", "DrzavaGradHotel");
            ViewBag.FK_Soba_TipSobe_PK = new SelectList(db.TipSobe, "PK_TipSobe", "Naziv", soba.FK_Soba_TipSobe_PK);
            return View(soba);
        }

        // GET: Soba/Create1
        public ActionResult Create1()
        {
            db.Hotel.Include(x => x.Grad).Include(y => y.Grad.Drzava);
            ViewBag.FK_Soba_Hotel_PK = new SelectList(db.Hotel
                .OrderBy(d => d.Grad.Drzava.Naziv).ThenBy(g => g.Grad.Naziv), "PK_Hotel", "DrzavaGradHotel");
            return View();
        }

        // POST: Soba/Create1
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create1(SobaTipViewModel viewSobaTip)
        {
            if (ModelState.IsValid)
            {
                TipSobe tipSobe = new TipSobe
                {
                    Naziv = viewSobaTip.TipNaziv,
                    Televizor = viewSobaTip.Televizor,
                    Telefon = viewSobaTip.Telefon,
                    Balkon = viewSobaTip.Balkon,
                    WIFI = viewSobaTip.WIFI
                };
                db.TipSobe.Add(tipSobe);
                db.SaveChanges();
                tipSobe = db.TipSobe.Where(t => t.Naziv.ToLower() == viewSobaTip.TipNaziv.ToLower()).FirstOrDefault();

                Soba soba = new Soba()
                {
                    FK_Soba_Hotel_PK = viewSobaTip.FK_Soba_Hotel_PK,
                    FK_Soba_TipSobe_PK = tipSobe.PK_TipSobe,
                    Cijena = viewSobaTip.Cijena
                };

                db.Soba.Add(soba);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            db.Hotel.Include(x => x.Grad).Include(y => y.Grad.Drzava);
            ViewBag.FK_Soba_Hotel_PK = new SelectList(db.Hotel
                .OrderBy(d => d.Grad.Drzava.Naziv).ThenBy(g => g.Grad.Naziv), "PK_Hotel", "DrzavaGradHotel");
            return View(viewSobaTip);
        }

        // GET: Soba/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Soba soba = db.Soba.Find(id);
            if (soba == null)
            {
                return HttpNotFound();
            }
            ViewBag.FK_Soba_Hotel_PK = new SelectList(db.Hotel, "PK_Hotel", "Naziv", soba.FK_Soba_Hotel_PK);
            ViewBag.FK_Soba_TipSobe_PK = new SelectList(db.TipSobe, "PK_TipSobe", "Naziv", soba.FK_Soba_TipSobe_PK);
            return View(soba);
        }

        // POST: Soba/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "PK_Soba,FK_Soba_TipSobe_PK,FK_Soba_Hotel_PK,Cijena")] Soba soba)
        {
            if (ModelState.IsValid)
            {
                db.Entry(soba).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.FK_Soba_Hotel_PK = new SelectList(db.Hotel, "PK_Hotel", "Naziv", soba.FK_Soba_Hotel_PK);
            ViewBag.FK_Soba_TipSobe_PK = new SelectList(db.TipSobe, "PK_TipSobe", "Naziv", soba.FK_Soba_TipSobe_PK);
            return View(soba);
        }

        // GET: Soba/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Soba soba = db.Soba.Find(id);
            if (soba == null)
            {
                return HttpNotFound();
            }
            return View(soba);
        }

        // POST: Soba/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Soba soba = db.Soba.Find(id);
            db.Soba.Remove(soba);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        protected override void OnException(ExceptionContext filterContext)
        {
            Exception ex = filterContext.Exception;
            filterContext.ExceptionHandled = true;

            var controller = filterContext.RouteData.Values["controller"].ToString();
            var action = filterContext.RouteData.Values["action"].ToString();
            var model = new HandleErrorInfo(ex, controller, action);

            MNLog.Error(this.ControllerContext.RouteData.Values["controller"].ToString(), model.Exception.Message);
            Session["ErrorMsg"] = this.ControllerContext.RouteData.Values["controller"].ToString() + " : " + model.Exception.Message;

            filterContext.Result = new ViewResult
            {
                ViewName = "Error",
                ViewData = new ViewDataDictionary(model)
            };
        }
    }
}
