﻿namespace HotelMVC.Models
{
    public interface IMoj_NLog
    {
        void Info(string controller, string poruka);
        void Error(string controller, string poruka);
    }
}
