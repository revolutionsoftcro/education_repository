﻿using System;
using System.IO;

namespace HotelMVC.Models
{
    public class NLog_html : IMoj_NLog
    {
        public void Error(string controller, string poruka)
        {
            string start = @"C:\Users\trobic\Documents\TomislavRobic-Ucenje\MVC\HotelMVC\HotelMVC\logs\html";
            string end = $"{DateTime.Now.ToShortDateString()}.html";
            string putanja = Path.Combine(start, end);
            string temp = Path.Combine(start, "template.html");

            string text;
            if (!File.Exists(putanja))
            {
                File.Copy(temp, putanja);
                text = System.IO.File.ReadAllText(putanja);
                text = text.Replace("<!-- Datum -->", $"{DateTime.Now.ToShortDateString()}");
                File.WriteAllText(putanja, text);
            }

            text = File.ReadAllText(putanja);
            text = text.Replace("<!-- Tip -->", "<font color=\"red\">Error</font>");
            text = text.Replace("<!-- Vrijeme -->", $"<font color=\"red\">{DateTime.Now}</font>");
            text = text.Replace("<!-- Kontroler -->", $"<font color=\"red\">{controller}</font>");
            text = text.Replace("<!-- Poruka -->", $"<font color=\"red\">Dogodila se greška: {poruka}</font>");
            string red = "\n<tr >\n"
                + "<td><strong><!-- Tip --></strong></td>\n"
                + "<td><!-- Vrijeme --></td>\n"
                + "<td><!-- Kontroler --></td>\n"
                + "<td><!-- Poruka --></td>\n"
                + "</tr>\n"
                + "<!-- red -->";
            text = text.Replace("<!-- red -->", red);
            File.WriteAllText(putanja, text);
        }

        public void Info(string controller, string poruka)
        {
            string start = @"C:\Users\trobic\Documents\TomislavRobic-Ucenje\MVC\HotelMVC\HotelMVC\logs\html";
            string end = $"{DateTime.Now.ToShortDateString()}.html";
            string putanja = Path.Combine(start, end);
            string temp = Path.Combine(start, "template.html");

            string text;
            if (!File.Exists(putanja))
            {
                File.Copy(temp, putanja);
                text = System.IO.File.ReadAllText(putanja);
                text = text.Replace("<!-- Datum -->", $"{DateTime.Now.ToShortDateString()}");
                File.WriteAllText(putanja, text);
            }

            text = File.ReadAllText(putanja);
            text = text.Replace("<!-- Tip -->", "Info");
            text = text.Replace("<!-- Vrijeme -->", $"{DateTime.Now}");
            text = text.Replace("<!-- Kontroler -->", $"{controller}");
            text = text.Replace("<!-- Poruka -->", $"{poruka}");
            string red = "\n<tr >\n"
                + "<td><strong><!-- Tip --></strong></td>\n"
                + "<td><!-- Vrijeme --></td>\n"
                + "<td><!-- Kontroler --></td>\n"
                + "<td><!-- Poruka --></td>\n"
                + "</tr>\n"
                + "<!-- red -->";
            text = text.Replace("<!-- red -->", red);
            File.WriteAllText(putanja, text);
        }
    }
}