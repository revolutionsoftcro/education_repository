﻿using Hotel_2._0.Models;
using Moq;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;

namespace MockData
{
    public static class Mock_Data
    {
        public static void setupSet<T>(ICollection<T> data, Mock<DbSet<T>> set) where T : HotelModel
        {
            var dataQ = data.AsQueryable<T>();
            set.As<IQueryable<T>>().Setup(m => m.Provider).Returns(() => dataQ.Provider);
            set.As<IQueryable<T>>().Setup(m => m.Expression).Returns(() => dataQ.Expression);
            set.As<IQueryable<T>>().Setup(m => m.ElementType).Returns(() => dataQ.ElementType);
            set.As<IQueryable<T>>().Setup(m => m.GetEnumerator()).Returns(() => dataQ.GetEnumerator());
            set.Setup(s => s.Include(It.IsAny<string>())).Returns(set.Object);
            set.Setup(d => d.Add(It.IsAny<T>())).Callback<T>((s) => data.Add(s));
            set.Setup(m => m.Remove(It.IsAny<T>())).Callback((T item) => data.Remove(item));
        }

        public static Mock<Hotel2Entities> Create()
        {
            var context = new Mock<Hotel2Entities>();
            context.Setup(x => x.ChangeState<HotelModel>(new HotelModel()));

            var drzavaData = new List<Drzava>
            {
                new Drzava{PK_Drzava = 1,Naziv = "Hrvatska"},
                new Drzava{PK_Drzava = 2,Naziv = "Rusija"}
            };

            Mock<DbSet<Drzava>> setDrzava = new Mock<DbSet<Drzava>>();

            setupSet(drzavaData, setDrzava);
            setDrzava.Setup(m => m.Find(It.IsAny<object[]>()))
                .Returns<object[]>(ids => drzavaData.FirstOrDefault(d => d.PK_Drzava == (int)ids[0]));
            context.Setup(c => c.Drzava).Returns(setDrzava.Object);

            var gradData = new List<Grad>
            {
                new Grad{PK_Grad = 1,FK_Grad_Drzava_PK = 1, Naziv = "Zagreb"},
                new Grad{PK_Grad = 2,FK_Grad_Drzava_PK = 2, Naziv = "Sankt Petersburg"},
            };

            Mock<DbSet<Grad>> setGradovi = new Mock<DbSet<Grad>>();

            setupSet(gradData, setGradovi);
            setGradovi.Setup(m => m.Find(It.IsAny<object[]>()))
                .Returns<object[]>(ids => gradData.FirstOrDefault(d => d.PK_Grad == (int)ids[0]));
            context.Setup(c => c.Grad).Returns(setGradovi.Object);

            var gostData = new List<Gost>
            {
                new Gost{PK_Gost = 1,FK_Gost_Grad_PK = 1, Ime = "Pero", Prezime = "Perić", Adresa = "Ilica 5", Email = "pperic@gmail.com", Mobitel = "09111652123" },
                new Gost{PK_Gost = 2,FK_Gost_Grad_PK = 2, Ime = "Maja", Prezime = "Makarov", Adresa = "Crveni trg 5", Email = "mmakarov@gmail.com", Mobitel = "07611652123" }
            };

            Mock<DbSet<Gost>> setGost = new Mock<DbSet<Gost>>();

            setupSet(gostData, setGost);
            setGost.Setup(m => m.Find(It.IsAny<object[]>()))
                .Returns<object[]>(ids => gostData.FirstOrDefault(d => d.PK_Gost == (int)ids[0]));
            context.Setup(c => c.Gost).Returns(setGost.Object);

            var hotelData = new List<Hotel>
            {
                new Hotel{PK_Hotel = 1,FK_Hotel_Grad_PK = 1, Naziv = "SZ1", BrojZvjezdica = 4, Adresa = "Ilica 16", Email = "sz1@gmail.com", Telefon = "0111652123" },
                new Hotel{PK_Hotel = 2,FK_Hotel_Grad_PK = 2, Naziv = "SM1", BrojZvjezdica = 5, Adresa = "Crveni trg 1", Email = "sm1@gmail.com", Telefon = "07696652123" }
            };

            Mock<DbSet<Hotel>> setHotel = new Mock<DbSet<Hotel>>();

            setupSet(hotelData, setHotel);
            setHotel.Setup(m => m.Find(It.IsAny<object[]>()))
                .Returns<object[]>(ids => hotelData.FirstOrDefault(d => d.PK_Hotel == (int)ids[0]));
            context.Setup(c => c.Hotel).Returns(setHotel.Object);

            var tipSobeData = new List<TipSobe>
            {
                new TipSobe{PK_TipSobe = 1, Naziv = "Jednosobna", Balkon = false, Telefon = true, Televizor = false, WIFI = true },
                new TipSobe{PK_TipSobe = 2,Naziv = "Dvosobna", Balkon = false, Telefon = true, Televizor = true, WIFI = true }
            };

            Mock<DbSet<TipSobe>> setTipSobe = new Mock<DbSet<TipSobe>>();

            setupSet(tipSobeData, setTipSobe);
            setTipSobe.Setup(m => m.Find(It.IsAny<object[]>()))
                .Returns<object[]>(ids => tipSobeData.FirstOrDefault(d => d.PK_TipSobe == (int)ids[0]));
            context.Setup(c => c.TipSobe).Returns(setTipSobe.Object);

            var sobaData = new List<Soba>
            {
                new Soba {PK_Soba = 1, FK_Soba_TipSobe_PK = 1, FK_Soba_Hotel_PK = 1, Cijena = 200, Hotel = hotelData.Where(h=>h.PK_Hotel == 1).First() },
                new Soba {PK_Soba = 2, FK_Soba_TipSobe_PK = 2, FK_Soba_Hotel_PK = 2, Cijena = 350, Hotel = hotelData.Where(h=>h.PK_Hotel == 2).First() }
            };

            Mock<DbSet<Soba>> setSoba = new Mock<DbSet<Soba>>();

            setupSet(sobaData, setSoba);
            setSoba.Setup(m => m.Find(It.IsAny<object[]>()))
                .Returns<object[]>(ids => sobaData.FirstOrDefault(d => d.PK_Soba == (int)ids[0]));
            context.Setup(c => c.Soba).Returns(setSoba.Object);

            var uslugeProizvodiData = new List<UslugeProizvodi>
            {
                new UslugeProizvodi {PK_UslugeProizvodi = 1, FK_UslugeProizvodi_Hotel_PK = 1, Naziv="Vino", Tip = "Proizvod", Mjera = "Boca 0.75", CijenaPoMjeri = 200},
                new UslugeProizvodi {PK_UslugeProizvodi = 2, FK_UslugeProizvodi_Hotel_PK = 1, Naziv="Masaža", Tip = "Usluga", Mjera = "Sat", CijenaPoMjeri = 150},
                new UslugeProizvodi {PK_UslugeProizvodi = 3, FK_UslugeProizvodi_Hotel_PK = 1, Naziv="Šampanjac", Tip = "Proizvod", Mjera = "Boca 0.75", CijenaPoMjeri = 500},
                new UslugeProizvodi {PK_UslugeProizvodi = 4, FK_UslugeProizvodi_Hotel_PK = 1, Naziv="Čokolada", Tip = "Proizvod", Mjera = "100g", CijenaPoMjeri = 50}
            };

            Mock<DbSet<UslugeProizvodi>> setUslugeProizvodi = new Mock<DbSet<UslugeProizvodi>>();

            setupSet(uslugeProizvodiData, setUslugeProizvodi);
            setUslugeProizvodi.Setup(m => m.Find(It.IsAny<object[]>()))
                .Returns<object[]>(ids => uslugeProizvodiData.FirstOrDefault(d => d.PK_UslugeProizvodi == (int)ids[0]));
            context.Setup(c => c.UslugeProizvodi).Returns(setUslugeProizvodi.Object);

            var rezervacijaData = new List<Rezervacija>
            {
                new Rezervacija {PK_Rezervacija=1, FK_Rezervacija_Gost_PK=1, FK_Rezervacija_Soba_PK=2,
                    Dolazak =new DateTime(2020, 7, 15), Odlazak=new DateTime(2020,7,25),
                    Soba =sobaData.Where(s=>s.PK_Soba==2).First(),
                    Gost=gostData.Where(g=>g.PK_Gost==1).First()},
                new Rezervacija {PK_Rezervacija=2, FK_Rezervacija_Gost_PK=2, FK_Rezervacija_Soba_PK=1,
                    Dolazak =new DateTime(2020, 7, 25), Odlazak=new DateTime(2020,8,5),
                    Soba =sobaData.Where(s=>s.PK_Soba==1).First(),
                    Gost=gostData.Where(g=>g.PK_Gost==1).First()}
            };

            Mock<DbSet<Rezervacija>> setRezervacija = new Mock<DbSet<Rezervacija>>();

            setupSet(rezervacijaData, setRezervacija);
            setRezervacija.Setup(m => m.Find(It.IsAny<object[]>()))
                .Returns<object[]>(ids => rezervacijaData.FirstOrDefault(d => d.PK_Rezervacija == (int)ids[0]));
            context.Setup(c => c.Rezervacija).Returns(setRezervacija.Object);

            var soba = sobaData.Where(s => s.PK_Soba == 1).First();
            soba.Rezervacija = rezervacijaData.Where(r => r.FK_Rezervacija_Soba_PK == 1).ToList();

            var kosaricaData = new List<Kosarica>
            {
                new Kosarica {PK_Kosarica = 1, FK_Kosarica_Rezervacija_PK = 1, Trenutna = true, Otkazana = false, Iznos = 600, Rezervacija = rezervacijaData.Where(r=>r.PK_Rezervacija == 1).First()},
                new Kosarica {PK_Kosarica = 2, FK_Kosarica_Rezervacija_PK = 2, Trenutna = false, Otkazana = true, Iznos = 0, Rezervacija = rezervacijaData.Where(r=>r.PK_Rezervacija == 2).First()}
            };

            var rezervacija = rezervacijaData.Where(r => r.PK_Rezervacija == 1).First();
            rezervacija.Kosarica = kosaricaData.Where(k => k.FK_Kosarica_Rezervacija_PK == 1).ToList();

            Mock<DbSet<Kosarica>> setKosarica = new Mock<DbSet<Kosarica>>();

            setupSet(kosaricaData, setKosarica);
            setKosarica.Setup(m => m.Find(It.IsAny<object[]>()))
                .Returns<object[]>(ids => kosaricaData.FirstOrDefault(d => d.PK_Kosarica == (int)ids[0]));
            context.Setup(c => c.Kosarica).Returns(setKosarica.Object);

            var kosaricaProizvodData = new List<KosaricaProizvod>
            {
                new KosaricaProizvod {PK_KosaricaProizvod = 1, FK_KosaricaProizvod_UslugeProizvodi_PK = 1, FK_KosaricaProizvod_Kosarica_PK = 1, Deleted = false,
                    Kolicina = 3, Iznos = 600, UslugeProizvodi = uslugeProizvodiData.Where(u=>u.PK_UslugeProizvodi == 1).First(), Kosarica = kosaricaData.Where(k=>k.PK_Kosarica == 1).First()},
                new KosaricaProizvod {PK_KosaricaProizvod = 3, FK_KosaricaProizvod_UslugeProizvodi_PK=1, FK_KosaricaProizvod_Kosarica_PK = 1, Deleted = false,
                    Kolicina = 2, Iznos = 1000, UslugeProizvodi=uslugeProizvodiData.Where(u=>u.PK_UslugeProizvodi == 1).First(), Kosarica=kosaricaData.Where(k=>k.PK_Kosarica == 1).First()},
                new KosaricaProizvod {PK_KosaricaProizvod = 2, FK_KosaricaProizvod_UslugeProizvodi_PK=2, FK_KosaricaProizvod_Kosarica_PK = 1, Deleted = true,
                    Kolicina = 2, Iznos = 460, UslugeProizvodi=uslugeProizvodiData.Where(u=>u.PK_UslugeProizvodi == 2).First(), Kosarica = kosaricaData.Where(k=>k.PK_Kosarica == 1).First()}
            };

            Mock<DbSet<KosaricaProizvod>> setKosaricaProizvod = new Mock<DbSet<KosaricaProizvod>>();

            setupSet(kosaricaProizvodData, setKosaricaProizvod);
            setKosaricaProizvod.Setup(m => m.Find(It.IsAny<object[]>()))
                .Returns<object[]>(ids => kosaricaProizvodData.FirstOrDefault(d => d.PK_KosaricaProizvod == (int)ids[0]));
            context.Setup(c => c.KosaricaProizvod).Returns(setKosaricaProizvod.Object);

            var kosarica = kosaricaData.Where(k => k.Trenutna == true).First();
            kosarica.KosaricaProizvod = kosaricaProizvodData.Where(p => p.FK_KosaricaProizvod_Kosarica_PK == kosarica.PK_Kosarica).ToList();

            var racunData = new List<Racun>
            {
                new Racun {PK_Racun = 1, FK_Racun_Rezervacija_PK = 1, Soba = true, Iznos = 1250, isPaid = true},
                new Racun {PK_Racun = 2, FK_Racun_Rezervacija_PK = 1, Soba = false, Iznos = 2550, isPaid = false}
            };

            Mock<DbSet<Racun>> setRacun = new Mock<DbSet<Racun>>();

            setupSet(racunData, setRacun);
            setRacun.Setup(m => m.Find(It.IsAny<object[]>()))
                .Returns<object[]>(ids => racunData.FirstOrDefault(d => d.PK_Racun == (int)ids[0]));
            context.Setup(c => c.Racun).Returns(setRacun.Object);

            rezervacijaData[0].Racun = racunData.Where(r => r.FK_Racun_Rezervacija_PK == 1).ToList();

            var stavkaRacunData = new List<StavkaRacun>
            {
                new StavkaRacun {PK_UslugaRacun = 1, FK_StavkaRacun_Racun_PK = 2, FK_StavkaRacun_UslugeProizvodi_PK = 1, Kolicina = 3, Iznos = 600, Racun = racunData.Where(r=>r.PK_Racun == 2).First()},
                new StavkaRacun {PK_UslugaRacun = 2, FK_StavkaRacun_Racun_PK = 2, FK_StavkaRacun_UslugeProizvodi_PK = 2, Kolicina = 2, Iznos = 450, Racun = racunData.Where(r=>r.PK_Racun == 2).First()}
            };

            var racun = racunData.Where(r => r.Soba == false).First();
            racun.StavkaRacun = stavkaRacunData.Where(r => r.FK_StavkaRacun_Racun_PK == 2).ToList();

            Mock<DbSet<StavkaRacun>> setStavkaRacun = new Mock<DbSet<StavkaRacun>>();

            setupSet(stavkaRacunData, setStavkaRacun);
            setStavkaRacun.Setup(m => m.Find(It.IsAny<object[]>()))
                .Returns<object[]>(ids => stavkaRacunData.FirstOrDefault(d => d.PK_UslugaRacun == (int)ids[0]));
            context.Setup(c => c.StavkaRacun).Returns(setStavkaRacun.Object);

            return context;
        }

        public static void Destroy(Mock<Hotel2Entities> context)
        {
            context.Object.Dispose();
        }
    }
}
