﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Web;

namespace Hotel_2._0.Models
{
    public interface IContext : IDisposable
    {
        DbSet<Drzava> Drzava { get; set; }
        DbSet<Gost> Gost { get; set; }
        DbSet<Grad> Grad { get; set; }
        DbSet<Hotel> Hotel { get; set; }
        DbSet<Kosarica> Kosarica { get; set; }
        DbSet<KosaricaProizvod> KosaricaProizvod { get; set; }
        DbSet<Racun> Racun { get; set; }
        DbSet<Rezervacija> Rezervacija { get; set; }
        DbSet<Soba> Soba { get; set; }
        DbSet<StavkaRacun> StavkaRacun { get; set; }
        DbSet<TipSobe> TipSobe { get; set; }
        DbSet<UslugeProizvodi> UslugeProizvodi { get; set; }

        int SaveChanges();
        void ChangeState<T>(T item) where T : ChangeStateClass;
    }
}
