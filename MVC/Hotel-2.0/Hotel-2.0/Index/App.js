﻿var removeList = ["#DrzavaTable", "#GradTable", "#HotelTable", "#TipSobeTable", "#SobaTable", "#GostTable", "#RezervacijaTable",
    "#DrzavaFormContent", "#DrzavaFormEditContent1", "#DrzavaFormEditContent2", "#GradFormContent", "#GradFormEditContent1", "#GradFormEditContent2",
    "#HotelFormContent", "#HotelFormEditContent1", "#HotelFormEditContent2", "#TipSobeFormContent", "#TipSobeFormEditContent1", "#TipSobeFormEditContent2",
    "#SobaFormContent", "#SobaFormEditContent1", "#SobaFormEditContent2", "#GostFormContent", "#GostFormEditContent1", "#GostFormEditContent2", "DrzavaFormDeleteContent1", "DrzavaFormDeleteContent2",
    "GradFormDeleteContent1", "GradFormDeleteContent2", "HotelFormDeleteContent1", "HotelFormDeleteContent2", "TipSobeFormDeleteContent1", "TipSobeFormDeleteContent2",
    "SobaFormDeleteContent1", "SobaFormDeleteContent2", "GostFormDeleteContent1", "GostFormDeleteContent2", "RezervacijaFormDeleteContent1", "RezervacijaFormDeleteContent2"];

var hideList = ["#GradoviTableDiv", "#DrzaveTableDiv", "#HoteliTableDiv", "#TipoviSobeTableDiv", "#SobeTableDiv", "#SobeTableDiv", "#GostiTableDiv", "#RezervacijeTableDiv",
    "#PostGradForm", "#PostDrzavaForm", "#PostHotelForm", "#PostTipSobeForm", "#PostSobaForm", "#PostGostForm", "#PostRezervacijaForm", "#EditDrzavaForm", "#EditGradForm",
    "#EditHotelForm", "#EditTipSobeForm", "#EditSobaForm", "#EditGostForm", "#DeleteDrzavaForm", "#DeleteGradForm", "#DeleteHotelForm", "#DeleteTipSobeForm",
    "#DeleteSobaForm", "#DeleteGostForm", "#DeleteRezervacijaForm"];

var gradoviOptions = "";
var drzaveOptions = "";
var hoteliOptions = "";
var tipoviSobeOptions = "";
var gostiOptions = "";
var sobeOptions = "";
var rezervacijeOptions = "";

function hideRemove(hideList, removeList) {

    hideList.forEach(function (_hideElement) {
        $(_hideElement).hide();
    })

    removeList.forEach(function (_removeElement) {
        if ($(_removeElement) != null)
            $(_removeElement).remove();
    })
}

function pageLoad() {
    $(".section").hide();
    hideRemove(hideList, removeList);

    $.ajax({
        url: "http://localhost/API/api/drzava",
        method: "GET",
        crossDomain: true
    }).done(function (data) {
        drzaveOptions = '<option value=""></option>';
        data.forEach(function (item) {
            drzaveOptions += ('<option value="' + item.PK_Drzava + '">' + item.Naziv + '</option>');
        });
    });

    $.ajax({
        url: "http://localhost/API/api/grad",
        method: "GET",
        crossDomain: true
    }).done(function (data) {
        gradoviOptions = '<option value=""></option>';
        data.forEach(function (item) {
            gradoviOptions += ('<option value="' + item.Id + '">' + item.Grad + '</option>');
        });
    });

    $.ajax({
        url: "http://localhost/API/api/hotel",
        method: "GET",
        crossDomain: true
    }).done(function (data) {
        hoteliOptions = '<option value=""></option>';
        data.forEach(function (item) {
            hoteliOptions += ('<option value="' + item.Id + '">' + item.Grad + ' | ' + item.Naziv + '</option>');
        });
    });

    $.ajax({
        url: "http://localhost/API/api/tipsobe",
        method: "GET",
        crossDomain: true
    }).done(function (data) {
        tipoviSobeOptions = '<option value=""></option>';
        data.forEach(function (item) {
            tipoviSobeOptions += ('<option value="' + item.PK_TipSobe + '">' + item.Naziv + '</option>');
        });
    });

    $.ajax({
        url: "http://localhost/API/api/soba",
        method: "GET",
        crossDomain: true
    }).done(function (data) {
        sobeOptions = '<option value=""></option>';
        data.forEach(function (item) {
            sobeOptions += ('<option value="' + item.Broj + '">' + item.Hotel + ' | ' + item.TipSobe + ' | ' + item.Broj + '</option>');
        });
    });

    $.ajax({
        url: "http://localhost/API/api/gost",
        method: "GET",
        crossDomain: true
    }).done(function (data) {
        gostiOptions = '<option value=""></option>';
        data.forEach(function (item) {
            gostiOptions += ('<option value="' + item.Id + '">' + item.Ime + ' ' + item.Prezime + '</option>');
        });
    });

    rezervacijeOptions = '<option value=""></option>';
    $.ajax({
        url: "http://localhost/API/api/rezervacija",
        method: "GET",
        crossDomain: true
    }).done(function (data) {
        data.forEach(function (item) {
            $.ajax({
                url: "http://localhost/API/api/hotel/" + item.HotelId,
                method: "GET",
                crossDomain: true
            }).done(function (data2) {
                $.ajax({
                    url: "http://localhost/API/api/gost/" + item.GostId,
                    method: "GET",
                    crossDomain: true
                }).done(function (data3) {
                    rezervacijeOptions += ('<option value="' + item.Id + '">' + data3.Ime + ' ' + data3.Prezime + ' | ' + data2.Naziv + ' | ' + item.BrojSobe + '</option>');
                })
            })
        })
    });
}

pageLoad();

$("#reset").click(function () {
    pageLoad();
})

$("#menu h3").click(function () {
    $(".section").hide();
    hideRemove(hideList, removeList);
    $(this).next().slideToggle("slow");
})

function Get(url, tableDivId, tableId, svojstva, svojstvaDisplay) {
    $(tableDivId).empty();
    $.ajax({
        url: url,
        method: "GET",
        crossDomain: true
    }).done(function (data) {        
        var tableDiv = $(tableDivId);
        var table = ($("<table>", { class: 'table table-dark', id: tableId }));      

        var headerRow = $("<tr>");
        for (var i = 0; i < svojstvaDisplay.length; i++) {
            headerRow.append("<th>" + svojstvaDisplay[i] + "</th>");
        }
        table.append(headerRow);
        
        data.forEach(function (item, i) {
            var tr = $("<tr>");
            for (var i = 0; i < svojstva.length; i++) {
                tr.append("<td>" + item[svojstva[i]] + "</td > ");
            }
            table.append(tr);
        });
        tableDiv.append(table);
        $(tableDivId).slideToggle("slow");
    }).fail(function () {
        alert("Objekti se ne mogu dohvatiti!");
        pageLoad();
    });
}

function Create(url, c_form) {
    var form = $(c_form);
    var data = form.serializeArray().reduce(function (obj, item) {
        obj[item.name] = item.value;
        return obj;
    }, {});
    $.ajax({
        url: url,
        method: "POST",
        data: data,
        crossDomain: true
    }).done(function (data) {
        $(c_form).slideToggle("slow");
        pageLoad();
    }).fail(function (jqXHR, textStatus, errorThrown) {
        alert("Objekt se ne može napraviti! Poruka: " + jqXHR.responseJSON.Message );
        pageLoad();
    });
}

function Edit(url, e_form, id) {
    var form = $(e_form);
    var data = form.serializeArray().reduce(function (obj, item) {
        obj[item.name] = item.value;
        return obj;
    }, {});
    $.ajax({
        url: url + id,
        method: "PUT",
        data: data,
        crossDomain: true
    }).done(function (data) {
        form.slideToggle("slow");
        pageLoad();
    }).fail(function (jqXHR, textStatus, errorThrown) {
        alert("Objekt se ne može promjeniti! Poruka: " + jqXHR.responseJSON.Message);
        pageLoad();
    });
}

function Delete(url, d_form, id) {
    var form = $(d_form);

    $.ajax({
        url: url + id,
        method: "DELETE",
        crossDomain: true
    }).done(function (data) {
        form.slideToggle("slow");
        pageLoad();
    }).fail(function (jqXHR, textStatus, errorThrown) {
        alert("Objekt se ne može obrisati! Poruka: " + jqXHR.responseJSON.Message);
        pageLoad();
    });
}