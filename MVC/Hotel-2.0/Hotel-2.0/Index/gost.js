﻿var g_svojstva = ["Ime", "Prezime", "Mobitel", "Email", "Adresa"];
var g_svojstvaDisplay = ["Ime", "Prezime", "Mobitel","Email", "Adresa"];
var g_url = "http://localhost/API/api/gost/";
var g_tableDiv = "#GostiTableDiv";
var g_table = "#GostTable";
var g_form1 = "#PostGostForm";
var g_form2 = "#EditGostForm";
var g_form3 = "#DeleteGostForm";
var g_formContent = "#GostFormContent"
var g_selectGost = "#selectGost"
var g_formEditContent1 = "#GostFormEditContent1"
var g_formEditContent2 = "#GostFormEditContent2"
var g_formDeleteContent1 = "#GostFormDeleteContent1"
var g_formDeleteContent2 = "#GostFormDeleteContent2"

$(document).on("click", "#GetGost", function () {
    $(g_form1).hide();
    $(g_form2).hide();
    $(g_form3).hide();
    $(g_form1).empty();
    $(g_form2).empty();
    $(g_form3).empty();

    Get(g_url, g_tableDiv, g_table, g_svojstva, g_svojstvaDisplay);
});

$(document).on("click", "#PostGost", function () {
    $(g_tableDiv).hide();
    $(g_form2).hide();
    $(g_form3).hide();
    $(g_tableDiv).empty();
    $(g_form2).empty();
    $(g_form3).empty();

    var form = $(g_form1);
    var formGroup, selectgroup;
    form.empty();

    form.append('<div id="GostFormContent" class="form-group">');
    formGroup = $(g_formContent);
    formGroup.append('<label>Ime</label>');
    formGroup.append("<input class='form-control text-box single-line' type='text' value='' name='Ime'>");
    formGroup.append('<label>Prezime</label>');
    formGroup.append("<input class='form-control text-box single-line' type='text' value='' name='Prezime'>");
    formGroup.append('<label>Mobitel</label>');
    formGroup.append("<input class='form-control text-box single-line' type='text' value='' name='Mobitel'>");
    formGroup.append('<label>Email</label>');
    formGroup.append("<input class='form-control text-box single-line' type='text' value='' name='Email'>");
    formGroup.append('<label>Grad</label>');
    formGroup.append('<select class="form-control" name="GradId" id="selectGrad">');
    selectgroup = $("#selectGrad");
    selectgroup.append(gradoviOptions);
    formGroup.append('<label>Adresa</label>');
    formGroup.append("<input class='form-control text-box single-line' type='text' value='' name='Adresa'>");

    formGroup.append('<button id="GostCreateButton" class="btn btn-primary"> Napravi </button>')
    form.slideToggle("slow");
});

$(document).on("click", "#GostCreateButton", function () {
    event.preventDefault();
    Create(g_url, g_form1);
});

$(document).on("click", "#EditGost", function () {
    $(g_form1).hide();
    $(g_form3).hide();
    $(g_tableDiv).hide();
    $(g_form1).empty();
    $(g_form3).empty();
    $(g_tableDiv).empty();

    var form = $(g_form2);
    var formGroup;

    form.empty();

    form.append('<div id="GostFormEditContent1" class="form-group">');
    formGroup = $(g_formEditContent1);

    formGroup.append('<label>Gost</label>');
    formGroup.append('<select class="form-control" name="Id" id="selectGost">');
    selectgroup = $(g_selectGost);
    selectgroup.append(gostiOptions);

    formGroup.append('<button id="GostEditButton1" class="btn btn-primary"> Odaberi </button>')
    form.slideToggle("slow");
});

$(document).on("click", "#GostEditButton1", function (event) {
    event.preventDefault();
    $(g_form1).hide();
    $(g_form3).hide();
    $(g_tableDiv).hide();
    $(g_form1).empty();
    $(g_form3).empty();
    $(g_tableDiv).empty();

    var form = $(g_form2);
    var formGroup;
    let data = form.serializeArray().reduce(function (obj, item) {
        obj[item.name] = item.value;
        return obj;
    }, {});

    form.empty();

    form.append('<div id="GostFormEditContent2" class="form-group">');
    formGroup = $(g_formEditContent2);
    formGroup.append('<input id="gostIdInput" name="Id" hidden type="number" />')
    var gostId = $("#gostIdInput")
    gostId.val(data.Id);
    formGroup.append('<label>Ime</label>');
    formGroup.append('<input id="gostImeInput" class="form-control text-box single-line" type="text" value="" name="Ime">');
    formGroup.append('<label>Prezime</label>');
    formGroup.append('<input id="gostPrezimeInput" class="form-control text-box single-line" type="text" value="" name="Prezime">');
    formGroup.append('<label>Mobitel</label>');
    formGroup.append('<input id="gostMobitelInput"  class="form-control text-box single-line" type="text" value="" name="Mobitel">');
    formGroup.append('<label>Email</label>');
    formGroup.append('<input id="gostEmailInput"  class="form-control text-box single-line" type="text" value="" name="Email">');
    formGroup.append('<label>Grad</label>');
    formGroup.append('<select class="form-control" name="GradId" id="selectGrad">');
    selectgroup = $("#selectGrad");
    selectgroup.append(gradoviOptions);
    formGroup.append('<label>Adresa</label>');
    formGroup.append('<input id="gostAdresaInput"  class="form-control text-box single-line" type="text" value="" name="Adresa">');

    formGroup.append('<button id="GostEditButton2" class="btn btn-primary"> Promjeni </button>');

    $.ajax({
        url: g_url + data.Id,
        method: "GET",
        crossDomain: true
    }).done(function (data) {
        var id = data.GradId
        console.log(id);
        var grad;
        $.ajax({
            url: gr_url + id,
            method: "GET",
            crossDomain: true
        }).done(function (data2) {
            grad = data2.Grad;
            console.log(grad);
            $("#gostImeInput").val(data.Ime);
            $("#gostPrezimeInput").val(data.Prezime);
            $("#gostMobitelInput").val(data.Mobitel);
            $("#gostEmailInput").val(data.Email);
            $("#selectGrad option:contains('" + grad + "')").prop("selected", true);
            $("#gostAdresaInput").val(data.Adresa);
        });
    });
});

$(document).on("click", "#GostEditButton2", function (event) {
    event.preventDefault();
    var form = $(g_form2);
    let data = form.serializeArray().reduce(function (obj, item) {
        obj[item.name] = item.value;
        return obj;
    }, {});

    Edit(g_url, g_form2, data.Id);
});

$(document).on("click", "#DeleteGost", function () {
    $(g_form1).hide();
    $(g_form2).hide();
    $(g_tableDiv).hide();
    $(g_form1).empty();
    $(g_form2).empty();
    $(g_tableDiv).empty();

    var form = $(g_form3);
    var formGroup;

    form.empty();

    form.append('<div id="GostFormDeleteContent1" class="form-group">');
    formGroup = $(g_formDeleteContent1);

    formGroup.append('<label>Gost</label>');
    formGroup.append('<select class="form-control" name="Id" id="selectGost">');
    selectgroup = $(g_selectGost);
    selectgroup.append(gostiOptions);

    formGroup.append('<button id="GostDeleteButton1" class="btn btn-primary"> Odaberi </button>')
    form.slideToggle("slow");
});

$(document).on("click", "#GostDeleteButton1", function (event) {
    event.preventDefault();
    $(g_form1).hide();
    $(g_form2).hide();
    $(g_tableDiv).hide();
    $(g_form1).empty();
    $(g_form2).empty();
    $(g_tableDiv).empty();

    var form = $(g_form3);
    var formGroup;
    let data = form.serializeArray().reduce(function (obj, item) {
        obj[item.name] = item.value;
        return obj;
    }, {});

    form.empty();

    form.append('<div id="GostFormDeleteContent2" class="form-group">');
    formGroup = $(g_formDeleteContent2);
    formGroup.append("<h2>" + "Želite li obrisati ovaj objekt ?")
    formGroup.append("<h3>" + "Gost:")
    formGroup.append('<input id="gostIdInput" name="Id" hidden type="number" />')
    var gostId = $("#gostIdInput")
    gostId.val(data.Id);

    $.ajax({
        url: g_url + data.Id,
        method: "GET",
        crossDomain: true
    }).done(function (data) {
        $.ajax({
            url: gr_url + data.GradId,
            method: "GET",
            crossDomain: true
        }).done(function (data2) {
            formGroup.append('<div class="row"> <label>Ime: </label> <label>' + data.Ime + '</div>');
            formGroup.append('<div class="row"> <label>Prezime: </label> <label>' + data.Prezime + '</div>');
            formGroup.append('<div class="row"> <label>Mobitel: </label> <label>' + data.Mobitel + '</div>');
            formGroup.append('<div class="row"> <label>Email: </label> <label>' + data.Email + '</div>');
            formGroup.append('<div class="row"> <label>Grad: </label> <label>' + data2.Grad + '</div>');
            formGroup.append('<div class="row"> <label>Adresa: </label> <label>' + data.Adresa + '</div>');

            formGroup.append('<div class="row"> <button id="GostDeleteConfirm" class="btn btn-primary"> Da </button> <button id="GostDeleteCancel" class="btn btn-primary"> Ne </button> </div>');
        });
    });
});

$(document).on("click", "#GostDeleteCancel", function (event) {
    event.preventDefault();
    pageLoad();
});

$(document).on("click", "#GostDeleteConfirm", function (event) {
    event.preventDefault();
    var form = $(g_form3);
    let data = form.serializeArray().reduce(function (obj, item) {
        obj[item.name] = item.value;
        return obj;
    }, {});

    Delete(g_url, g_form3, data.Id);
});