﻿var t_svojstva = ["Naziv", "Televizor", "Telefon", "Balkon", "WIFI"];
var t_svojstvaDisplay = ["Naziv", "Televizor", "Telefon", "Balkon", "WIFI"];
var t_url = "http://localhost/API/api/tipsobe/";
var t_tableDiv = "#TipoviSobeTableDiv";
var t_table = "#TipSobeTable";
var t_form1 = "#PostTipSobeForm";
var t_form2 = "#EditTipSobeForm";
var t_form3 = "#DeleteTipSobeForm";
var t_formContent = "#TipSobeFormContent"
var t_selectBool = '<option value=true>Ima</option><option value=false>Nema</option>'
var t_selectTipSobe = "#selectTipSobe"
var t_formEditContent1 = "#TipSobeFormEditContent1"
var t_formEditContent2 = "#TipSobeFormEditContent2"
var t_formDeleteContent1 = "#TipSobeFormDeleteContent1"
var t_formDeleteContent2 = "#TipSobeFormDeleteContent2"


$(document).on("click", "#GetTipSobe", function () {
    $(t_form1).hide();
    $(t_form2).hide();
    $(t_form3).hide();
    $(t_form1).empty();
    $(t_form2).empty();
    $(t_form3).empty();

    Get(t_url, t_tableDiv, t_table, t_svojstva, t_svojstvaDisplay);
});

$(document).on("click", "#PostTipSobe", function () {
    $(t_tableDiv).hide();
    $(t_form2).hide();
    $(t_form3).hide();
    $(t_tableDiv).empty();
    $(t_form2).empty();
    $(t_form3).empty();

    var form = $(t_form1);
    var formGroup, selectgroup;
    form.empty();

    form.append('<div id="TipSobeFormContent" class="form-group">');
    formGroup = $(t_formContent);
    formGroup.append('<label>Naziv</label>');
    formGroup.append("<input class='form-control text-box single-line' type='text' value='' name='Naziv'>");
    formGroup.append('<label>Televizor</label>');
    formGroup.append('<select class="form-control" name="Televizor" id="selectTelevizor">');
    selectgroup = $("#selectTelevizor");
    selectgroup.append(t_selectBool);
    formGroup.append('<label>Telefon</label>');
    formGroup.append('<select class="form-control" name="Telefon" id="selectTelefon">');
    selectgroup = $("#selectTelefon");
    selectgroup.append(t_selectBool);
    formGroup.append('<label>Balkon</label>');
    formGroup.append('<select class="form-control" name="Balkon" id="selectBalkon">');
    selectgroup = $("#selectBalkon");
    selectgroup.append(t_selectBool);
    formGroup.append('<label>WIFI</label>');
    formGroup.append('<select class="form-control" name="WIFI" id="selectWIFI">');
    selectgroup = $("#selectWIFI");
    selectgroup.append(t_selectBool);
    formGroup.append('<button id="TipSobeCreateButton" class="btn btn-primary"> Napravi </button>')
    form.slideToggle("slow");
});

$(document).on("click", "#TipSobeCreateButton", function () {
    event.preventDefault();
    Create(t_url, t_form1);
});

$(document).on("click", "#EditTipSobe", function () {
    $(t_form1).hide();
    $(t_form3).hide();
    $(t_tableDiv).hide();
    $(t_form1).empty();
    $(t_form3).empty();
    $(t_tableDiv).empty();

    var form = $(t_form2);
    var formGroup;

    form.empty();

    form.append('<div id="TipSobeFormEditContent1" class="form-group">');
    formGroup = $(t_formEditContent1);

    formGroup.append('<label>TipSobe</label>');
    formGroup.append('<select class="form-control" name="PK_TipSobe" id="selectTipSobe">');
    selectgroup = $(t_selectTipSobe);
    selectgroup.append(tipoviSobeOptions);

    formGroup.append('<button id="TipSobeEditButton1" class="btn btn-primary"> Odaberi </button>')
    form.slideToggle("slow");
});

$(document).on("click", "#TipSobeEditButton1", function (event) {
    event.preventDefault();
    $(t_form1).hide();
    $(t_form3).hide();
    $(t_tableDiv).hide();
    $(t_form1).empty();
    $(t_form3).empty();
    $(t_tableDiv).empty();

    var form = $(t_form2);
    var formGroup;
    let data = form.serializeArray().reduce(function (obj, item) {
        obj[item.name] = item.value;
        return obj;
    }, {});

    form.empty();

    form.append('<div id="TipSobeFormEditContent2" class="form-group">');
    formGroup = $(t_formEditContent2);
    formGroup.append('<input id="tipSobeIdInput" name="PK_TipSobe" hidden type="number" />')
    var tipSobeId = $("#tipSobeIdInput")
    tipSobeId.val(data.PK_TipSobe);
    formGroup.append('<label>Naziv</label>');
    formGroup.append('<input id="tipSobeNazivInput" class="form-control text-box single-line" type="text" value="" name="Naziv">');
    formGroup.append('<label>Televizor</label>');
    formGroup.append('<select id="tipSobeTelevizorInput" class="form-control" name="Televizor" id="selectTelevizor">');
    selectgroup = $("#tipSobeTelevizorInput");
    selectgroup.append(t_selectBool);
    formGroup.append('<label>Telefon</label>');
    formGroup.append('<select id="tipSobeTelefonInput" class="form-control" name="Telefon" id="selectTelefon">');
    selectgroup = $("#tipSobeTelefonInput");
    selectgroup.append(t_selectBool);
    formGroup.append('<label>Balkon</label>');
    formGroup.append('<select id="tipSobeBalkonInput" class="form-control" name="Balkon" id="selectBalkon">');
    selectgroup = $("#tipSobeBalkonInput");
    selectgroup.append(t_selectBool);
    formGroup.append('<label>WIFI</label>');
    formGroup.append('<select id="tipSobeWIFIInput" class="form-control" name="WIFI" id="selectWIFI">');
    selectgroup = $("#tipSobeWIFIInput");
    selectgroup.append(t_selectBool);

    formGroup.append('<button id="TipSobeEditButton2" class="btn btn-primary"> Promjeni </button>')

    $.ajax({
        url: t_url + data.PK_TipSobe,
        method: "GET",
        crossDomain: true
    }).done(function (data) {
        $("#tipSobeNazivInput").val(data.Naziv);
        var televizorBool = data.Televizor ? "Ima" : "Nema";
        var telefonBool = data.Telefon ? "Ima" : "Nema";
        var balkonBool = data.Balkon ? "Ima" : "Nema";
        var WIFIBool = data.WIFI ? "Ima" : "Nema";
        $("#tipSobeTelevizorInput option:contains('" + televizorBool + "')").prop("selected", true);
        $("#tipSobeTelefonInput option:contains('" + telefonBool + "')").prop("selected", true);
        $("#tipSobeBalkonInput option:contains('" + balkonBool + "')").prop("selected", true);
        $("#tipSobeWIFIInput option:contains('" + WIFIBool + "')").prop("selected", true);
    });
});

$(document).on("click", "#TipSobeEditButton2", function (event) {
    event.preventDefault();
    var form = $(t_form2);
    let data = form.serializeArray().reduce(function (obj, item) {
        obj[item.name] = item.value;
        return obj;
    }, {});

    Edit(t_url, t_form2, data.PK_TipSobe);
});

$(document).on("click", "#DeleteTipSobe", function () {
    $(t_form1).hide();
    $(t_form2).hide();
    $(t_tableDiv).hide();
    $(t_form1).empty();
    $(t_form2).empty();
    $(t_tableDiv).empty();

    var form = $(t_form3);
    var formGroup;

    form.empty();

    form.append('<div id="TipSobeFormDeleteContent1" class="form-group">');
    formGroup = $(t_formDeleteContent1);

    formGroup.append('<label>TipSobe</label>');
    formGroup.append('<select class="form-control" name="PK_TipSobe" id="selectTipSobe">');
    selectgroup = $(t_selectTipSobe);
    selectgroup.append(tipoviSobeOptions);

    formGroup.append('<button id="TipSobeDeleteButton1" class="btn btn-primary"> Odaberi </button>')
    form.slideToggle("slow");
});

$(document).on("click", "#TipSobeDeleteButton1", function (event) {
    event.preventDefault();
    $(t_form1).hide();
    $(t_form2).hide();
    $(t_tableDiv).hide();
    $(t_form1).empty();
    $(t_form2).empty();
    $(t_tableDiv).empty();

    var form = $(t_form3);
    var formGroup;
    let data = form.serializeArray().reduce(function (obj, item) {
        obj[item.name] = item.value;
        return obj;
    }, {});

    form.empty();

    form.append('<div id="TipSobeFormDeleteContent2" class="form-group">');
    formGroup = $(t_formDeleteContent2);
    formGroup.append("<h2>" + "Želite li obrisati ovaj objekt ?")
    formGroup.append("<h3>" + "Tip sobe:")
    formGroup.append('<input id="tipSobeIdInput" name="PK_TipSobe" hidden type="number" />')
    var tipSobeId = $("#tipSobeIdInput")
    tipSobeId.val(data.PK_TipSobe);

    $.ajax({
        url: t_url + data.PK_TipSobe,
        method: "GET",
        crossDomain: true
    }).done(function (data) {
        var televizorBool = data.Televizor ? "Ima" : "Nema";
        var telefonBool = data.Telefon ? "Ima" : "Nema";
        var balkonBool = data.Balkon ? "Ima" : "Nema";
        var WIFIBool = data.WIFI ? "Ima" : "Nema";

        formGroup.append('<div class="row"> <label>Naziv: </label> <label>' + data.Naziv + '</div>');
        formGroup.append('<div class="row"> <label>Televizor: </label> <label>' + televizorBool + '</div>');
        formGroup.append('<div class="row"> <label>Telefon: </label> <label>' + telefonBool + '</div>');
        formGroup.append('<div class="row"> <label>Balkon: </label> <label>' + balkonBool + '</div>');
        formGroup.append('<div class="row"> <label>WIFI: </label> <label>' + WIFIBool + '</div>');

        formGroup.append('<div class="row"> <button id="TipSobeDeleteConfirm" class="btn btn-primary"> Da </button> <button id="TipSobeDeleteCancel" class="btn btn-primary"> Ne </button> </div>');
    });
});

$(document).on("click", "#TipSobeDeleteCancel", function (event) {
    event.preventDefault();
    pageLoad();
});

$(document).on("click", "#TipSobeDeleteConfirm", function (event) {
    event.preventDefault();
    var form = $(t_form3);
    let data = form.serializeArray().reduce(function (obj, item) {
        obj[item.name] = item.value;
        return obj;
    }, {});

    Delete(t_url, t_form3, data.PK_TipSobe);
});