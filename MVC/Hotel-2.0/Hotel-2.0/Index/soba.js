﻿var s_svojstva = ["Broj", "TipSobe", "Hotel", "Cijena"];
var s_svojstvaDisplay = ["Broj", "Tip sobe", "Hotel", "Cijena"];
var s_url = "http://localhost/API/api/soba/";
var s_tableDiv = "#SobeTableDiv";
var s_table = "#SobaTable";
var s_form1 = "#PostSobaForm";
var s_form2 = "#EditSobaForm";
var s_form3 = "#DeleteSobaForm";
var s_formContent = "#SobaFormContent"
var s_selectSoba = "#selectSoba"
var s_formEditContent1 = "#SobaFormEditContent1"
var s_formEditContent2 = "#SobaFormEditContent2"
var s_formDeleteContent1 = "#SobaFormDeleteContent1"
var s_formDeleteContent2 = "#SobaFormDeleteContent2"

$(document).on("click", "#GetSoba", function () {
    $(s_form1).hide();
    $(s_form2).hide();
    $(s_form3).hide();
    $(s_form1).empty();
    $(s_form2).empty();
    $(s_form3).empty();

    Get(s_url, s_tableDiv, s_table, s_svojstva, s_svojstvaDisplay);
});

$(document).on("click", "#PostSoba", function () {
    $(s_tableDiv).hide();
    $(s_form2).hide();
    $(s_form3).hide();
    $(s_tableDiv).empty();
    $(s_form2).empty();
    $(s_form3).empty();

    var form = $(s_form1);
    var formGroup, selectgroup;
    form.empty();

    form.append('<div id="SobaFormContent" class="form-group">');
    formGroup = $(s_formContent);
    formGroup.append('<label>Hotel</label>');
    formGroup.append('<select class="form-control" name="HotelId" id="selectHotel">');
    selectgroup = $("#selectHotel");
    selectgroup.append(hoteliOptions);
    formGroup.append('<label>Tip</label>');
    formGroup.append('<select class="form-control" name="TipId" id="selectTip">');
    selectgroup = $("#selectTip");
    selectgroup.append(tipoviSobeOptions);
    formGroup.append('<label>Cijena</label>');
    formGroup.append("<input class='form-control text-box single-line' type='text' value='' name='Cijena'>");

    formGroup.append('<button id="SobaCreateButton" class="btn btn-primary"> Napravi </button>')
    form.slideToggle("slow");
});

$(document).on("click", "#SobaCreateButton", function () {
    event.preventDefault();
    Create(s_url, s_form1);
});

$(document).on("click", "#EditSoba", function () {
    $(s_form1).hide();
    $(s_form3).hide();
    $(s_tableDiv).hide();
    $(s_form1).empty();
    $(s_form3).empty();
    $(s_tableDiv).empty();

    var form = $(s_form2);
    var formGroup;

    form.empty();

    form.append('<div id="SobaFormEditContent1" class="form-group">');
    formGroup = $(s_formEditContent1);

    formGroup.append('<label>Soba</label>');
    formGroup.append('<select class="form-control" name="Broj" id="selectSoba">');
    selectgroup = $(s_selectSoba);
    selectgroup.append(sobeOptions);

    formGroup.append('<button id="SobaEditButton1" class="btn btn-primary"> Odaberi </button>')
    form.slideToggle("slow");
});

$(document).on("click", "#SobaEditButton1", function (event) {
    event.preventDefault();
    $(s_form1).hide();
    $(s_form3).hide();
    $(s_tableDiv).hide();
    $(s_form1).empty();
    $(s_form3).empty();
    $(s_tableDiv).empty();

    var form = $(s_form2);
    var formGroup;
    let data = form.serializeArray().reduce(function (obj, item) {
        obj[item.name] = item.value;
        return obj;
    }, {});

    form.empty();

    form.append('<div id="SobaFormEditContent2" class="form-group">');
    formGroup = $(s_formEditContent2);
    formGroup.append('<input id="sobaIdInput" name="Broj" hidden type="number" />')
    var sobaId = $("#sobaIdInput")
    sobaId.val(data.Broj);
    formGroup.append('<label>Hotel</label>');
    formGroup.append('<select class="form-control" name="HotelId" id="selectHotel">');
    selectgroup = $("#selectHotel");
    selectgroup.append(hoteliOptions);
    formGroup.append('<label>Tip</label>');
    formGroup.append('<select class="form-control" name="TipId" id="selectTip">');
    selectgroup = $("#selectTip");
    selectgroup.append(tipoviSobeOptions);
    formGroup.append('<label>Cijena</label>');
    formGroup.append('<input id="sobaCijenaInput" class="form-control text-box single-line" type="text" value="" name="Cijena">');

    formGroup.append('<button id="SobaEditButton2" class="btn btn-primary"> Promjeni </button>')

    $.ajax({
        url: s_url + data.Broj,
        method: "GET",
        crossDomain: true
    }).done(function (data) {
        $("#selectHotel option:contains('" + data.Hotel + "')").prop("selected", true);
        $("#selectTip option:contains('" + data.TipSobe + "')").prop("selected", true);
        $("#sobaCijenaInput").val(data.Cijena);
    });
});

$(document).on("click", "#SobaEditButton2", function (event) {
    event.preventDefault();
    var form = $(s_form2);
    let data = form.serializeArray().reduce(function (obj, item) {
        obj[item.name] = item.value;
        return obj;
    }, {});

    Edit(s_url, s_form2, data.Broj);
});

$(document).on("click", "#DeleteSoba", function () {
    $(s_form1).hide();
    $(s_form2).hide();
    $(s_tableDiv).hide();
    $(s_form1).empty();
    $(s_form2).empty();
    $(s_tableDiv).empty();

    var form = $(s_form3);
    var formGroup;

    form.empty();

    form.append('<div id="SobaFormDeleteContent1" class="form-group">');
    formGroup = $(s_formDeleteContent1);

    formGroup.append('<label>Soba</label>');
    formGroup.append('<select class="form-control" name="Broj" id="selectSoba">');
    selectgroup = $(s_selectSoba);
    selectgroup.append(sobeOptions);

    formGroup.append('<button id="SobaDeleteButton1" class="btn btn-primary"> Odaberi </button>')
    form.slideToggle("slow");
});

$(document).on("click", "#SobaDeleteButton1", function (event) {
    event.preventDefault();
    $(s_form1).hide();
    $(s_form2).hide();
    $(s_tableDiv).hide();
    $(s_form1).empty();
    $(s_form2).empty();
    $(s_tableDiv).empty();

    var form = $(s_form3);
    var formGroup;
    let data = form.serializeArray().reduce(function (obj, item) {
        obj[item.name] = item.value;
        return obj;
    }, {});

    form.empty();

    form.append('<div id="SobaFormDeleteContent2" class="form-group">');
    formGroup = $(s_formDeleteContent2);
    formGroup.append("<h2>" + "Želite li obrisati ovaj objekt ?")
    formGroup.append("<h3>" + "Soba:")
    formGroup.append('<input id="sobaIdInput" name="Broj" hidden type="number" />')
    var sobaId = $("#sobaIdInput")
    sobaId.val(data.Broj);

    $.ajax({
        url: s_url + data.Broj,
        method: "GET",
        crossDomain: true
    }).done(function (data) {
        formGroup.append('<div class="row"> <label>Broj: </label> <label>' + data.Broj + '</div>');
        formGroup.append('<div class="row"> <label>Hotel: </label> <label>' + data.Hotel + '</div>');
        formGroup.append('<div class="row"> <label>Tip sobe: </label> <label>' + data.TipSobe + '</div>');
        formGroup.append('<div class="row"> <label>Cijena: </label> <label>' + data.Cijena + '</div>');

        formGroup.append('<div class="row"> <button id="SobaDeleteConfirm" class="btn btn-primary"> Da </button> <button id="SobaDeleteCancel" class="btn btn-primary"> Ne </button> </div>');
    });
});

$(document).on("click", "#SobaDeleteCancel", function (event) {
    event.preventDefault();
    pageLoad();
});

$(document).on("click", "#SobaDeleteConfirm", function (event) {
    event.preventDefault();
    var form = $(s_form3);
    let data = form.serializeArray().reduce(function (obj, item) {
        obj[item.name] = item.value;
        return obj;
    }, {});

    Delete(s_url, s_form3, data.Broj);
});