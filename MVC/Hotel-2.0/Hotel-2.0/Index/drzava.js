﻿var d_svojstva = ["Naziv"];
var d_svojstvaDisplay = ["Naziv Države"];
var d_url = "http://localhost/API/api/drzava/";
var d_tableDiv = "#DrzaveTableDiv";
var d_table = "#DrzaveTable";
var d_form1 = "#PostDrzavaForm";
var d_form2 = "#EditDrzavaForm";
var d_form3 = "#DeleteDrzavaForm";
var d_formContent = "#DrzavaFormContent";
var d_selectDrzava = "#selectDrzava";
var d_formEditContent1 = "#DrzavaFormEditContent1";
var d_formEditContent2 = "#DrzavaFormEditContent2";
var d_formDeleteContent1 = "#DrzavaFormDeleteContent1";
var d_formDeleteContent2 = "#DrzavaFormDeleteContent2";


$(document).on("click", "#GetDrzava", function () {
    $(d_form2).hide();
    $(d_form1).hide();
    $(d_form3).hide();
    $(d_form2).empty();
    $(d_form1).empty();
    $(d_form3).empty();

    Get(d_url, d_tableDiv, d_table, d_svojstva, d_svojstvaDisplay);
});

$(document).on("click", "#PostDrzava", function () {
    $(d_form2).hide();
    $(d_form3).hide();
    $(d_tableDiv).hide();
    $(d_form2).empty();
    $(d_form3).empty();
    $(d_tableDiv).empty();

    var form = $(d_form1);
    var formGroup;

    form.empty();

    form.append('<div id="DrzavaFormContent" class="form-group">');
    formGroup = $(d_formContent);
    formGroup.append('<label>Naziv</label>');
    formGroup.append("<input class='form-control text-box single-line' type='text' value='' name='Naziv'>");
    formGroup.append('<button id="DrzavaCreateButton" class="btn btn-primary"> Napravi </button>')
    form.slideToggle("slow");

});

$(document).on("click", "#DrzavaCreateButton", function () {
    event.preventDefault();
    Create(d_url, d_form1);
});

$(document).on("click", "#EditDrzava", function () {
    $(d_form1).hide();
    $(d_form3).hide();
    $(d_tableDiv).hide();
    $(d_form1).empty();
    $(d_form3).empty();
    $(d_tableDiv).empty();

    var form = $(d_form2);
    var formGroup;

    form.empty();

    form.append('<div id="DrzavaFormEditContent1" class="form-group">');
    formGroup = $(d_formEditContent1);

    formGroup.append('<label>Država</label>');
    formGroup.append('<select class="form-control" name="PK_Drzava" id="selectDrzava">');
    selectgroup = $(d_selectDrzava);
    selectgroup.append(drzaveOptions);

    formGroup.append('<button id="DrzavaEditButton1" class="btn btn-primary"> Odaberi </button>')
    form.slideToggle("slow");
});

$(document).on("click", "#DrzavaEditButton1", function (event) {
    event.preventDefault();
    $(d_form1).hide();
    $(d_form3).hide()
    $(d_tableDiv).hide();
    $(d_form1).empty();
    $(d_form3).empty();
    $(d_tableDiv).empty();

    var form = $(d_form2);
    var formGroup;
    let data = form.serializeArray().reduce(function (obj, item) {
        obj[item.name] = item.value;
        return obj;
    }, {});

    form.empty();

    form.append('<div id="DrzavaFormEditContent2" class="form-group">');
    formGroup = $(d_formEditContent2);
    formGroup.append('<input id="drzavaIdInput" name="PK_Drzava" hidden type="number" />')
    var drzavaId = $("#drzavaIdInput")
    drzavaId.val(data.PK_Drzava);
    formGroup.append('<label>Naziv</label>');
    formGroup.append("<input id='drzavaNazivInput' class='form-control text-box single-line' type='text' value='' name='Naziv'>");

    formGroup.append('<button id="DrzavaEditButton2" class="btn btn-primary"> Promjeni </button>')

    $.ajax({
        url: d_url + data.PK_Drzava,
        method: "GET",
        crossDomain: true
    }).done(function (data) {
        $("#drzavaNazivInput").val(data.Naziv);
    });
});

$(document).on("click", "#DrzavaEditButton2", function (event) {
    event.preventDefault();
    var form = $(d_form2);
    let data = form.serializeArray().reduce(function (obj, item) {
        obj[item.name] = item.value;
        return obj;
    }, {});

    Edit(d_url, d_form2, data.PK_Drzava);
});

$(document).on("click", "#DeleteDrzava", function () {
    $(d_form1).hide();
    $(d_form2).hide();
    $(d_tableDiv).hide();
    $(d_form1).empty();
    $(d_form2).empty();
    $(d_tableDiv).empty();

    var form = $(d_form3);
    var formGroup;

    form.empty();

    form.append('<div id="DrzavaFormDeleteContent1" class="form-group">');
    formGroup = $(d_formDeleteContent1);

    formGroup.append('<label>Država</label>');
    formGroup.append('<select class="form-control" name="PK_Drzava" id="selectDrzava">');
    selectgroup = $(d_selectDrzava);
    selectgroup.append(drzaveOptions);

    formGroup.append('<button id="DrzavaDeleteButton1" class="btn btn-primary"> Odaberi </button>')
    form.slideToggle("slow");
});

$(document).on("click", "#DrzavaDeleteButton1", function (event) {
    event.preventDefault();
    $(d_form1).hide();
    $(d_form2).hide();
    $(d_tableDiv).hide();
    $(d_form1).empty();
    $(d_form2).empty();
    $(d_tableDiv).empty();

    var form = $(d_form3);
    var formGroup;
    let data = form.serializeArray().reduce(function (obj, item) {
        obj[item.name] = item.value;
        return obj;
    }, {});

    form.empty();

    form.append('<div id="DrzavaFormEditContent2" class="form-group">');
    formGroup = $(d_formEditContent2);
    formGroup.append("<h2>" + "Želite li obrisati ovaj objekt ?")
    formGroup.append("<h3>" + "Država:")
    formGroup.append('<input id="drzavaIdInput" name="PK_Drzava" hidden type="number" />')
    var drzavaId = $("#drzavaIdInput")
    drzavaId.val(data.PK_Drzava);

    $.ajax({
        url: d_url + data.PK_Drzava,
        method: "GET",
        crossDomain: true
    }).done(function (data) {
        formGroup.append('<div class="row"> <label>Naziv: </label> <label>' + data.Naziv + '</div>');
        formGroup.append('<div class="row"> <button id="DrzavaDeleteConfirm" class="btn btn-primary"> Da </button> <button id="DrzavaDeleteCancel" class="btn btn-primary"> Ne </button> </div>');
    });
});

$(document).on("click", "#DrzavaDeleteCancel", function (event) {
    event.preventDefault();
    pageLoad();
});

$(document).on("click", "#DrzavaDeleteConfirm", function (event) {
    event.preventDefault();
    var form = $(d_form3);
    var data = form.serializeArray().reduce(function (obj, item) {
        obj[item.name] = item.value;
        return obj;
    }, {});
    Delete(d_url, d_form3, data.PK_Drzava);
});