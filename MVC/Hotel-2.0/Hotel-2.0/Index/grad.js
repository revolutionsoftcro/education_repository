﻿var gr_svojstva = ["Grad", "Drzava"];
var gr_svojstvaDisplay = ["Naziv grada", "Država"];
var gr_url = "http://localhost/API/api/grad/";
var gr_tableDiv = "#GradoviTableDiv";
var gr_table = "#GradTable";
var gr_form1 = "#PostGradForm";
var gr_form2 = "#EditGradForm"
var gr_form3 = "#DeleteGradForm"
var gr_formContent = "#GradFormContent"
var gr_selectDrzava1 = "#selectDrzava1"
var gr_selectGrad = "#selectGrad"
var gr_selectDrzava2 = "#selectDrzava2"
var gr_formEditContent1 = "#GradFormEditContent1"
var gr_formEditContent2 = "#GradFormEditContent2"
var gr_formDeleteContent1 = "#GradFormDeleteContent1"
var gr_formDeleteContent2 = "#GradFormDeleteContent2"

$(document).on("click", "#GetGrad", function () {
    $(gr_form1).hide();
    $(gr_form2).hide();
    $(gr_form3).hide();
    $(gr_form1).empty();
    $(gr_form2).empty();
    $(gr_form3).empty();

    Get(gr_url, gr_tableDiv, gr_table, gr_svojstva, gr_svojstvaDisplay);
});

$(document).on("click", "#PostGrad", function () {
    $(gr_form2).hide();
    $(gr_form3).hide();
    $(gr_tableDiv).hide();
    $(gr_form2).empty();
    $(gr_form3).empty();
    $(gr_tableDiv).empty();

    var form = $(gr_form1);
    var formGroup, selectgroup;

    form.empty();

    form.append('<div id="GradFormContent" class="form-group">');
    formGroup = $(gr_formContent);
    formGroup.append('<label>Naziv</label>');
    formGroup.append("<input class='form-control text-box single-line' type='text' value='' name='Grad'>");
    formGroup.append('<label>Država</label>');
    formGroup.append('<select class="form-control" name="DrzavaId" id="selectDrzava1">');
    selectgroup = $(gr_selectDrzava1);
    selectgroup.append(drzaveOptions);
    formGroup.append('<button id="GradCreateButton" class="btn btn-primary"> Napravi </button>')
    form.slideToggle("slow");

});

$(document).on("click", "#GradCreateButton", function () {
    event.preventDefault();
    Create(gr_url, gr_form1);
});

$(document).on("click", "#EditGrad", function () {
    $(gr_form1).hide();
    $(gr_tableDiv).hide();
    $(gr_form3).hide();
    $(gr_form1).empty();
    $(gr_tableDiv).empty();
    $(gr_form3).empty();

    var form = $(gr_form2);
    var formGroup;

    form.empty();

    form.append('<div id="GradFormEditContent1" class="form-group">');
    formGroup = $(gr_formEditContent1);

    formGroup.append('<label>Grad</label>');
    formGroup.append('<select class="form-control" name="PK_Grad" id="selectGrad">');
    selectgroup = $(gr_selectGrad);
    selectgroup.append(gradoviOptions);

    formGroup.append('<button id="GradEditButton1" class="btn btn-primary"> Odaberi </button>')
    form.slideToggle("slow");
});

$(document).on("click", "#GradEditButton1", function (event) {
    event.preventDefault();
    $(gr_form1).hide();
    $(gr_form3).hide();
    $(gr_tableDiv).hide();
    $(gr_form1).empty();
    $(gr_form3).empty();
    $(gr_tableDiv).empty();

    var form = $(gr_form2);
    var formGroup;
    let data = form.serializeArray().reduce(function (obj, item) {
        obj[item.name] = item.value;
        return obj;
    }, {});

    form.empty();

    form.append('<div id="GradFormEditContent2" class="form-group">');
    formGroup = $(gr_formEditContent2);
    formGroup.append("<h2>" + "Želite li obrisati ovaj objekt ?")
    formGroup.append("<h3>" + "Grad:")
    formGroup.append('<input id="gradIdInput" name="PK_Grad" hidden type="number" />')
    var gradId = $("#gradIdInput")
    gradId.val(data.PK_Grad);
    formGroup.append('<label>Naziv</label>');
    formGroup.append("<input id='gradNazivInput' class='form-control text-box single-line' type='text' value='' name='Naziv'>");
    formGroup.append('<label>Država</label>');
    formGroup.append('<select id="selectDrzava2" class="form-control" name="FK_Grad_Drzava_PK" id="selectDrzava2">');
    selectgroup = $(gr_selectDrzava2);
    selectgroup.append(drzaveOptions);

    formGroup.append('<button id="GradEditButton2" class="btn btn-primary"> Promjeni </button>')

    $.ajax({
        url: gr_url + data.PK_Grad,
        method: "GET",
        crossDomain: true
    }).done(function (data) {
        $("#gradNazivInput").val(data.Grad);
        $("#selectDrzava2 option:contains('" + data.Drzava + "')").prop("selected", true);
    });
});

$(document).on("click", "#GradEditButton2", function (event) {
    event.preventDefault();
    var form = $(gr_form2);
    let data = form.serializeArray().reduce(function (obj, item) {
        obj[item.name] = item.value;
        return obj;
    }, {});
    Edit(gr_url, gr_form2, data.PK_Grad);
});

$(document).on("click", "#DeleteGrad", function () {
    $(gr_form1).hide();
    $(gr_form2).hide();
    $(gr_tableDiv).hide();
    $(gr_form1).empty();
    $(gr_form2).empty();
    $(gr_tableDiv).empty();

    var form = $(gr_form3);
    var formGroup;

    form.empty();

    form.append('<div id="GradFormDeleteContent1" class="form-group">');
    formGroup = $(gr_formDeleteContent1);

    formGroup.append('<label>Grad</label>');
    formGroup.append('<select class="form-control" name="Id" id="selectGrad">');
    selectgroup = $(gr_selectGrad);
    selectgroup.append(gradoviOptions);

    formGroup.append('<button id="GradDeleteButton1" class="btn btn-primary"> Odaberi </button>')
    form.slideToggle("slow");
});

$(document).on("click", "#GradDeleteButton1", function (event) {
    event.preventDefault();
    $(gr_form1).hide();
    $(gr_form2).hide();
    $(gr_tableDiv).hide();
    $(gr_form1).empty();
    $(gr_form2).empty();
    $(gr_tableDiv).empty();

    var form = $(gr_form3);
    var formGroup;
    let data = form.serializeArray().reduce(function (obj, item) {
        obj[item.name] = item.value;
        return obj;
    }, {});

    form.empty();

    form.append('<div id="GradFormEditContent2" class="form-group">');
    formGroup = $(gr_formEditContent2);
    formGroup.append("<h2>" + "Želite li obrisati ovaj objekt ?")
    formGroup.append("<h3>" + "Grad:")
    formGroup.append('<input id="gradIdInput" name="Id" hidden type="number" />')
    var gradId = $("#gradIdInput")
    gradId.val(data.Id);

    $.ajax({
        url: gr_url + data.Id,
        method: "GET",
        crossDomain: true
    }).done(function (data) {
        formGroup.append('<div class="row"> <label>Naziv: </label> <label>' + data.Grad + '</div>');
        formGroup.append('<div class="row"> <label>Država: </label> <label>' + data.Drzava + '</div>');
        formGroup.append('<div class="row"> <button id="GradDeleteConfirm" class="btn btn-primary"> Da </button> <button id="GradDeleteCancel" class="btn btn-primary"> Ne </button> </div>');
    });
});

$(document).on("click", "#GradDeleteCancel", function (event) {
    event.preventDefault();
    pageLoad();
});

$(document).on("click", "#GradDeleteConfirm", function (event) {
    event.preventDefault();
    var form = $(gr_form3);
    var data = form.serializeArray().reduce(function (obj, item) {
        obj[item.name] = item.value;
        return obj;
    }, {});
    Delete(gr_url, gr_form3, data.Id);
});