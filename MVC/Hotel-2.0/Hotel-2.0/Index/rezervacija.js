﻿var r_svojstva = ["BrojSobe", "Dolazak", "Odlazak", "Popust"];
var r_svojstvaDisplay = ["Broj sobe", "Dolazak", "Odlazak", "Popust"];
var r_url = "http://localhost/API/api/rezervacija/";
var r_tableDiv = "#RezervacijeTableDiv";
var r_table = "#RezervacijaTable";
var r_form1 = "#PostRezervacijaForm";
var r_form2 = "#DeleteRezervacijaForm";
var r_selectRezervacija = "#selectRezervacija"
var r_formContent = "#RezervacijaFormContent"
var r_formDeleteContent1 = "#RezervacijaFormDeleteContent1"
var r_formDeleteContent2 = "#RezervacijaFormDeleteContent2"

$(document).on("click", "#GetRezervacija", function () {
    $(r_form1).hide();
    $(r_form2).hide();
    $(r_form1).empty();
    $(r_form2).empty();

    Get(r_url, r_tableDiv, r_table, r_svojstva, r_svojstvaDisplay);
});

$(document).on("click", "#PostRezervacija", function () {
    $(r_tableDiv).hide();
    $(r_form2).hide();
    $(r_tableDiv).empty();
    $(r_form2).empty();

    var form = $(r_form1);
    var formGroup, selectgroup;
    form.empty();

    form.append('<div id="RezervacijaFormContent" class="form-group">');
    formGroup = $(r_formContent);
    formGroup.append('<label>Gost</label>');
    formGroup.append('<select class="form-control" name="GostId" id="selectGost">');
    selectgroup = $("#selectGost");
    selectgroup.append(gostiOptions);
    formGroup.append('<label>Hotel</label>');
    formGroup.append('<select class="form-control" name="HotelId" id="selectHotel">');
    selectgroup = $("#selectHotel");
    selectgroup.append(hoteliOptions);
    formGroup.append('<label>Tip</label>');
    formGroup.append('<select class="form-control" name="TipSobeId" id="selectTip">');
    selectgroup = $("#selectTip");
    selectgroup.append(tipoviSobeOptions);
    formGroup.append('<label>Dolazak</label>');
    formGroup.append("<input class='form-control text-box single-line' type='date' value='' name='Dolazak'>");
    formGroup.append('<label>Odlazak</label>');
    formGroup.append("<input class='form-control text-box single-line' type='date' value='' name='Odlazak'>");
    formGroup.append('<label>Popust</label>');
    formGroup.append("<input class='form-control text-box single-line' type='number' step='0.01' value='' name='Popust'>");

    formGroup.append('<button id="RezervacijaCreateButton" class="btn btn-primary"> Napravi </button>')
    form.slideToggle("slow");
});

$(document).on("click", "#RezervacijaCreateButton", function () {
    event.preventDefault();
    Create(r_url, r_form1);
});

$(document).on("click", "#DeleteRezervacija", function () {
    $(r_form1).hide();
    $(r_tableDiv).hide();
    $(r_form1).empty();
    $(r_tableDiv).empty();

    var form = $(r_form2);
    var formGroup;

    form.empty();

    form.append('<div id="RezervacijaFormDeleteContent1" class="form-group">');
    formGroup = $(r_formDeleteContent1);

    formGroup.append('<label>Rezervacija</label>');
    formGroup.append('<select class="form-control" name="Id" id="selectRezervacija">');
    selectgroup = $(r_selectRezervacija);
    selectgroup.append(rezervacijeOptions);

    formGroup.append('<button id="RezervacijaDeleteButton1" class="btn btn-primary"> Odaberi </button>')
    form.slideToggle("slow");
});

$(document).on("click", "#RezervacijaDeleteButton1", function (event) {
    event.preventDefault();
    $(r_form1).hide();
    $(r_tableDiv).hide();
    $(r_form1).empty();
    $(r_tableDiv).empty();

    var form = $(r_form2);
    var formGroup;
    let data = form.serializeArray().reduce(function (obj, item) {
        obj[item.name] = item.value;
        return obj;
    }, {});

    form.empty();

    form.append('<div id="RezervacijaFormDeleteContent2" class="form-group">');
    formGroup = $(r_formDeleteContent2);
    formGroup.append("<h2>" + "Želite li obrisati ovaj objekt ?")
    formGroup.append("<h3>" + "Rezervacija:")
    formGroup.append('<input id="rezervacijaIdInput" name="Id" hidden type="number" />')
    var rezervacijaId = $("#rezervacijaIdInput")
    rezervacijaId.val(data.Id);

    $.ajax({
        url: r_url + data.Id,
        method: "GET",
        crossDomain: true
    }).done(function (data1) {
        $.ajax({           
            url: h_url + data1.HotelId,
            method: "GET",
            crossDomain: true            
        }).done(function (data2) {
            $.ajax({
                url: g_url + data1.GostId,
                method: "GET",
                crossDomain: true
            }).done(function (data3) {
                formGroup.append('<div class="row"> <label>Ime: </label> <label>' + data3.Ime + '</div>');
                formGroup.append('<div class="row"> <label>Prezime: </label> <label>' + data3.Prezime + '</div>');
                formGroup.append('<div class="row"> <label>Hotel: </label> <label>' + data2.Naziv + '</div>');
                formGroup.append('<div class="row"> <label>Broj sobe: </label> <label>' + data1.BrojSobe + '</div>');
                formGroup.append('<div class="row"> <label>Dolazak: </label> <label>' + data1.Dolazak + '</div>');
                formGroup.append('<div class="row"> <label>Odlazak: </label> <label>' + data1.Odlazak + '</div>');
                formGroup.append('<div class="row"> <label>Popust: </label> <label>' + data1.Popust + '</div>');

                formGroup.append('<div class="row"> <button id="RezervacijaDeleteConfirm" class="btn btn-primary"> Da </button> <button id="RezervacijaDeleteCancel" class="btn btn-primary"> Ne </button> </div>');
            });
        });
    });
});

$(document).on("click", "#RezervacijaDeleteCancel", function (event) {
    event.preventDefault();
    pageLoad();
});

$(document).on("click", "#RezervacijaDeleteConfirm", function (event) {
    event.preventDefault();
    var form = $(r_form2);
    let data = form.serializeArray().reduce(function (obj, item) {
        obj[item.name] = item.value;
        return obj;
    }, {});

    Delete(r_url, r_form2, data.Id);
});
