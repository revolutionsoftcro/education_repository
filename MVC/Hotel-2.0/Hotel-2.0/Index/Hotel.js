﻿var h_svojstva = ["Naziv", "BrojZvjezdica", "Telefon", "Email", "Grad", "Adresa"];
var h_svojstvaDisplay = ["Naziv", "Broj zvjezdica", "Telefon", "Email", "Grad", "Adresa"];
var h_url = "http://localhost/API/api/hotel/";
var h_tableDiv = "#HoteliTableDiv";
var h_table = "#HotelTable";
var h_form1 = "#PostHotelForm";
var h_form2 = "#EditHotelForm";
var h_form3 = "#DeleteHotelForm";
var h_formContent = "#HotelFormContent";
var h_selectGrad1 = "#selectGrad1";
var h_selectHotel = "#selectHotel"
var h_selectGrad2 = "#selectGrad2"
var h_formEditContent1 = "#HotelFormEditContent1"
var h_formEditContent2 = "#HotelFormEditContent2"
var h_formDeleteContent1 = "#HotelFormDeleteContent1"
var h_formDeleteContent2 = "#HotelFormDeleteContent2"

$(document).on("click", "#GetHotel", function () {
    $(h_form1).hide();
    $(h_form2).hide();
    $(h_form3).hide();
    $(h_form1).empty();
    $(h_form2).empty();
    $(h_form3).empty();

    Get(h_url, h_tableDiv, h_table, h_svojstva, h_svojstvaDisplay);
});

$(document).on("click", "#PostHotel", function () {
    $(h_tableDiv).hide();
    $(h_form2).hide();
    $(h_form3).hide();
    $(h_tableDiv).empty();
    $(h_form2).empty();
    $(h_form3).empty();

    var form = $(h_form1);
    var formGroup, selectgroup;
    form.empty();

    form.append('<div id="HotelFormContent" class="form-group">');
    formGroup = $(h_formContent);
    formGroup.append('<label>Naziv</label>');
    formGroup.append("<input class='form-control text-box single-line' type='text' value='' name='Naziv'>");
    formGroup.append('<label>Broj zvjezdica</label>');
    formGroup.append("<input class='form-control text-box single-line' type='text' value='' name='BrojZvjezdica'>");
    formGroup.append('<label>Telefon</label>');
    formGroup.append("<input class='form-control text-box single-line' type='text' value='' name='Telefon'>");
    formGroup.append('<label>Email</label>');
    formGroup.append("<input class='form-control text-box single-line' type='text' value='' name='Email'>");
    formGroup.append('<label>Grad</label>');
    formGroup.append('<select class="form-control" name="GradId" id="selectGrad1">');
    selectgroup = $(h_selectGrad1);
    selectgroup.append(gradoviOptions);
    formGroup.append('<label>Adresa</label>');
    formGroup.append("<input class='form-control text-box single-line' type='text' value='' name='Adresa'>");
    formGroup.append('<button id="HotelCreateButton" class="btn btn-primary"> Napravi </button>')
    form.slideToggle("slow");
});

$(document).on("click", "#HotelCreateButton", function () {
    event.preventDefault();
    Create(h_url, h_form1);
});

$(document).on("click", "#EditHotel", function () {
    $(h_form1).hide();
    $(h_form3).hide();
    $(h_tableDiv).hide();
    $(h_form1).empty();
    $(h_form3).empty();
    $(h_tableDiv).empty();

    var form = $(h_form2);
    var formGroup;

    form.empty();

    form.append('<div id="HotelFormEditContent1" class="form-group">');
    formGroup = $(h_formEditContent1);

    formGroup.append('<label>Hotel</label>');
    formGroup.append('<select class="form-control" name="Id" id="selectHotel">');
    selectgroup = $(h_selectHotel);
    selectgroup.append(hoteliOptions);

    formGroup.append('<button id="HotelEditButton1" class="btn btn-primary"> Odaberi </button>')
    form.slideToggle("slow");
});

$(document).on("click", "#HotelEditButton1", function (event) {
    event.preventDefault();
    $(h_form1).hide();
    $(h_form3).hide();
    $(h_tableDiv).hide();
    $(h_form1).empty();
    $(h_form3).empty();
    $(h_tableDiv).empty();

    var form = $(h_form2);
    var formGroup;
    let data = form.serializeArray().reduce(function (obj, item) {
        obj[item.name] = item.value;
        return obj;
    }, {});

    form.empty();

    form.append('<div id="HotelFormEditContent2" class="form-group">');
    formGroup = $(h_formEditContent2);
    formGroup.append('<input id="hotelIdInput" name="Id" hidden type="number" />')
    var hotelId = $("#hotelIdInput")
    hotelId.val(data.Id);
    formGroup.append('<label >Naziv</label>');
    formGroup.append('<input id="hotelNazivInput" class="form-control text-box single-line" type="text" value="" name="Naziv">');
    formGroup.append('<label>Broj zvjezdica</label>');
    formGroup.append('<input id="hotelZvjezdiceInput" class="form-control text-box single-line" type="text" value="" name="BrojZvjezdica">');
    formGroup.append('<label>Telefon</label>');
    formGroup.append('<input id="hotelTelefonInput" class="form-control text-box single-line" type="text" value="" name="Telefon">');
    formGroup.append('<label>Email</label>');
    formGroup.append('<input id="hotelEmailInput" class="form-control text-box single-line" type="text" value="" name="Email">');
    formGroup.append('<label>Grad</label>');
    formGroup.append('<select class="form-control" name="GradId" id="selectGrad2">');
    selectgroup = $(h_selectGrad2);
    selectgroup.append(gradoviOptions);
    formGroup.append('<label>Adresa</label>');
    formGroup.append('<input id="hotelAdresaInput" class="form-control text-box single-line" type="text" value="" name="Adresa">');

    formGroup.append('<button id="HotelEditButton2" class="btn btn-primary"> Promjeni </button>')

    $.ajax({
        url: h_url + data.Id,
        method: "GET",
        crossDomain: true
    }).done(function (data) {
        $("#hotelNazivInput").val(data.Naziv);
        $("#hotelZvjezdiceInput").val(data.BrojZvjezdica);
        $("#hotelTelefonInput").val(data.Telefon);
        $("#hotelEmailInput").val(data.Email);
        $("#selectGrad2 option:contains('" + data.Grad + "')").prop("selected", true);
        $("#hotelAdresaInput").val(data.Adresa);
    });
});

$(document).on("click", "#HotelEditButton2", function (event) {
    event.preventDefault();
    var form = $(h_form2);
    let data = form.serializeArray().reduce(function (obj, item) {
        obj[item.name] = item.value;
        return obj;
    }, {});

    Edit(h_url, h_form2, data.Id);
});

$(document).on("click", "#DeleteHotel", function () {
    $(h_form1).hide();
    $(h_form2).hide();
    $(h_tableDiv).hide();
    $(h_form2).empty();
    $(h_tableDiv).empty();
    $(h_form1).empty();

    var form = $(h_form3);
    var formGroup;

    form.empty();

    form.append('<div id="HotelFormDeleteContent1" class="form-group">');
    formGroup = $(h_formDeleteContent1);

    formGroup.append('<label>Hotel</label>');
    formGroup.append('<select class="form-control" name="Id" id="selectHotel">');
    selectgroup = $(h_selectHotel);
    selectgroup.append(hoteliOptions);

    formGroup.append('<button id="HotelDeleteButton1" class="btn btn-primary"> Odaberi </button>')
    form.slideToggle("slow");
});

$(document).on("click", "#HotelDeleteButton1", function (event) {
    event.preventDefault();
    $(h_form1).hide();
    $(h_form2).hide();
    $(h_tableDiv).hide();
    $(h_form1).empty();
    $(h_form2).empty();
    $(h_tableDiv).empty();

    var form = $(h_form3);
    var formGroup;
    let data = form.serializeArray().reduce(function (obj, item) {
        obj[item.name] = item.value;
        return obj;
    }, {});

    form.empty();

    form.append('<div id="HotelFormDeleteContent2" class="form-group">');
    formGroup = $(h_formDeleteContent2);
    formGroup.append("<h2>" + "Želite li obrisati ovaj objekt ?")
    formGroup.append("<h3>" + "Hotel:")
    formGroup.append('<input id="hotelIdInput" name="Id" hidden type="number" />')
    var hotelId = $("#hotelIdInput")
    hotelId.val(data.Id);

    $.ajax({
        url: h_url + data.Id,
        method: "GET",
        crossDomain: true
    }).done(function (data) {
        formGroup.append('<div class="row"> <label>Naziv: </label> <label>' + data.Naziv + '</div>');
        formGroup.append('<div class="row"> <label>Broj zvjezdica: </label> <label>' + data.BrojZvjezdica + '</div>');
        formGroup.append('<div class="row"> <label>Telefon: </label> <label>' + data.Telefon + '</div>');
        formGroup.append('<div class="row"> <label>Email: </label> <label>' + data.Email + '</div>');
        formGroup.append('<div class="row"> <label>Grad: </label> <label>' + data.Grad + '</div>');
        formGroup.append('<div class="row"> <label>Adresa: </label> <label>' + data.Adresa + '</div>');

        formGroup.append('<div class="row"> <button id="HotelDeleteConfirm" class="btn btn-primary"> Da </button> <button id="HotelDeleteCancel" class="btn btn-primary"> Ne </button> </div>');
    });
});

$(document).on("click", "#HotelDeleteCancel", function (event) {
    event.preventDefault();
    pageLoad();
});

$(document).on("click", "#HotelDeleteConfirm", function (event) {
    event.preventDefault();
    var form = $(h_form3);
    let data = form.serializeArray().reduce(function (obj, item) {
        obj[item.name] = item.value;
        return obj;
    }, {});

    Delete(h_url, h_form3, data.Id);
});