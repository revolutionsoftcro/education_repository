﻿using AutoMapper;
using Hotel_2._0.DTO;
using Hotel_2._0.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Hotel_2._0.App_Start
{
    public class MappingProfile:Profile
    {
        public MappingProfile()
        {
            Mapper.CreateMap<Gost, GostDTO>()
                .ForMember(g => g.Id, m => m.MapFrom(g => g.PK_Gost))
                .ForMember(g => g.GradId, m => m.MapFrom(g => g.FK_Gost_Grad_PK));
            Mapper.CreateMap<GostDTO, Gost>()
                .ForMember(g => g.PK_Gost, m => m.MapFrom(g => g.Id))
                .ForMember(g => g.FK_Gost_Grad_PK, m => m.MapFrom(g => g.GradId));

            Mapper.CreateMap<Grad, GradDTO>()
                .ForMember(g => g.Id, m => m.MapFrom(g => g.PK_Grad))
                .ForMember(g => g.Grad, m => m.MapFrom(g => g.Naziv))
                .ForMember(g => g.DrzavaId, m => m.MapFrom(g => g.FK_Grad_Drzava_PK))
                .ForMember(g => g.Drzava, m => m.MapFrom(g => g.Drzava.Naziv));
            Mapper.CreateMap<GradDTO, Grad>()
                .ForMember(g => g.PK_Grad, m => m.MapFrom(g => g.Id))
                .ForMember(g => g.Naziv, m => m.MapFrom(g => g.Grad))
                .ForMember(g => g.FK_Grad_Drzava_PK, m => m.MapFrom(g => (int)g.DrzavaId));

            Mapper.CreateMap<Hotel, HotelDTO>()
                .ForMember(h => h.Id, m => m.MapFrom(h => h.PK_Hotel))
                .ForMember(h => h.GradId, m => m.MapFrom(h => h.FK_Hotel_Grad_PK))
                .ForMember(h => h.Grad, m => m.MapFrom(h => h.Grad.Naziv));
            Mapper.CreateMap<HotelDTO, Hotel>()
                .ForMember(h => h.PK_Hotel, m => m.MapFrom(h => h.Id))
                .ForMember(h => h.FK_Hotel_Grad_PK, m => m.MapFrom(h => h.GradId));

            Mapper.CreateMap<Kosarica, Racun>()
                .ForMember(r => r.FK_Racun_Rezervacija_PK, m => m.MapFrom(k => k.FK_Kosarica_Rezervacija_PK));

            Mapper.CreateMap<KosaricaProizvod, StavkaRacun>()
                .ForMember(s => s.FK_StavkaRacun_UslugeProizvodi_PK, m => m.MapFrom(p => p.FK_KosaricaProizvod_UslugeProizvodi_PK));

            Mapper.CreateMap<KosaricaProizvod, KosaricaProizvodDTO>()
                .ForMember(k => k.Id, m => m.MapFrom(k => k.PK_KosaricaProizvod))
                .ForMember(k => k.KosaricaId, m => m.MapFrom(k => k.FK_KosaricaProizvod_Kosarica_PK))
                .ForMember(k => k.ProizvodId, m => m.MapFrom(k => k.FK_KosaricaProizvod_UslugeProizvodi_PK))
                .ForMember(k => k.Naziv, m => m.MapFrom(k => k.UslugeProizvodi.Naziv));

            Mapper.CreateMap<KosaricaProizvodDTO, KosaricaProizvod>()
                .ForMember(k => k.PK_KosaricaProizvod, m => m.MapFrom(k => k.Id))
                .ForMember(k => k.FK_KosaricaProizvod_Kosarica_PK, m => m.MapFrom(k => k.KosaricaId))
                .ForMember(k => k.FK_KosaricaProizvod_UslugeProizvodi_PK, m => m.MapFrom(k => k.ProizvodId));

            Mapper.CreateMap<Racun, RacunDTO>()
                .ForMember(r => r.Id, m => m.MapFrom(r => r.PK_Racun))
                .ForMember(r => r.RezervacijaId, m => m.MapFrom(r => r.FK_Racun_Rezervacija_PK));

            Mapper.CreateMap<Rezervacija, RezervacijaDTO>()
                .ForMember(r => r.Id, m => m.MapFrom(r => r.PK_Rezervacija))
                .ForMember(r => r.GostId, m => m.MapFrom(r => r.FK_Rezervacija_Gost_PK))
                .ForMember(r=>r.HotelId, m => m.MapFrom(r=>r.Soba.FK_Soba_Hotel_PK))
                .ForMember(r=>r.TipSobeId, m=>m.MapFrom(r=>r.Soba.FK_Soba_TipSobe_PK))
                .ForMember(r => r.BrojSobe, m => m.MapFrom(r => r.FK_Rezervacija_Soba_PK));

            Mapper.CreateMap<RezervacijaDTO, Rezervacija>()
                .ForMember(r => r.PK_Rezervacija, m => m.MapFrom(r => r.Id))
                .ForMember(r => r.FK_Rezervacija_Gost_PK, m => m.MapFrom(r => r.GostId))
                .ForMember(r => r.FK_Rezervacija_Soba_PK, m => m.MapFrom(r => r.BrojSobe));

            Mapper.CreateMap<Soba, SobaDTO>()
                .ForMember(s => s.Broj, m => m.MapFrom(s => s.PK_Soba))
                .ForMember(s => s.HotelId, m => m.MapFrom(s => s.FK_Soba_Hotel_PK))
                .ForMember(s => s.TipId, m => m.MapFrom(s => s.FK_Soba_TipSobe_PK))
                .ForMember(s => s.Hotel, m => m.MapFrom(s => s.Hotel.Naziv))
                .ForMember(s => s.TipSobe, m => m.MapFrom(s => s.TipSobe.Naziv));

            Mapper.CreateMap<SobaDTO, Soba>()
                .ForMember(s => s.PK_Soba, m => m.MapFrom(s => s.Broj))
                .ForMember(s => s.FK_Soba_Hotel_PK, m => m.MapFrom(s => s.HotelId))
                .ForMember(s => s.FK_Soba_TipSobe_PK, m => m.MapFrom(s => s.TipId));

            Mapper.CreateMap<UslugeProizvodi, UslugeProizvodiDTO>()
                .ForMember(p => p.Id, m => m.MapFrom(p => p.PK_UslugeProizvodi))
                .ForMember(p => p.HotelId, m => m.MapFrom(p => p.FK_UslugeProizvodi_Hotel_PK))
                .ForMember(p => p.JedinicnaCijena, m => m.MapFrom(p => p.CijenaPoMjeri));

            Mapper.CreateMap<UslugeProizvodiDTO, UslugeProizvodi>()
                .ForMember(p => p.PK_UslugeProizvodi, m => m.MapFrom(p => p.Id))
                .ForMember(p => p.FK_UslugeProizvodi_Hotel_PK, m => m.MapFrom(p => p.HotelId))
                .ForMember(p => p.CijenaPoMjeri, m => m.MapFrom(p => p.JedinicnaCijena));
        }
    }
}