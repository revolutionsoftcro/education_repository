﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Hotel_2._0.ViewModels
{
    public class FutureDateAttribute : ValidationAttribute
    {
        public override bool IsValid(object value)
        {
            DateTime dateTime;
            var isValid = DateTime.TryParse(Convert.ToString(value), out dateTime);

            return (isValid && dateTime > DateTime.Now);
        }
    }
}