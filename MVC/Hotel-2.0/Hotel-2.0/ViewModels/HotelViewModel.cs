﻿using Hotel_2._0.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Hotel_2._0.ViewModels
{
    public class HotelViewModel:Hotel
    {
        public SelectList ListaGradova { get; set; }
    }
}