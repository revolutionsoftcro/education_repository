﻿using System;
using System.ComponentModel;

namespace Hotel_2._0.ViewModels
{
    public class RacuniViewModel
    {
        [DisplayName("Broj računa")]
        public int PK_Racun { get; set; }
        public decimal? Iznos { get; set; }
        public Nullable<bool> isPaid { get; set; }
    }
}