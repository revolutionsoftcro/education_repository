﻿namespace Hotel_2._0.ViewModels
{
    public class KosaricaProizvodViewModel
    {
        public int Id { get; set; }
        public string Naziv { get; set; }
        public string Tip { get; set; }
        public decimal CijenaPoMjeri { get; set; }
        public string Mjera { get; set; }
        public decimal Kolicina { get; set; }
        public decimal Iznos { get; set; }
    }
}