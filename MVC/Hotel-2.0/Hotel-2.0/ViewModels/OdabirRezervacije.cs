﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace Hotel_2._0.ViewModels
{
    public class OdabirRezervacije
    {
        [DisplayName("Rezervacija")]
        [Required]
        public int PK_Rezervacija { get; set; }

        public SelectList ListaRezervacija { get; set; }
    }
}