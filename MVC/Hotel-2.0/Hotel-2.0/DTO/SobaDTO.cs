﻿namespace Hotel_2._0.DTO
{
    public class SobaDTO
    {
        public int? Broj { get; set; }
        public int TipId { get; set; }
        public int HotelId { get; set; }
        public string TipSobe { get; set; }
        public string Hotel { get; set; }
        public decimal Cijena { get; set; }
    }
}