﻿namespace Hotel_2._0.DTO
{
    public class RacunDTO
    {
        public int? Id { get; set; }
        public int? RezervacijaId { get; set; }
        public decimal Iznos { get; set; }
        public bool isPaid { get; set; }
    }
}