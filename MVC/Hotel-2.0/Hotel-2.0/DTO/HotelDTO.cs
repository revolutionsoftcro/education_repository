﻿namespace Hotel_2._0.DTO
{
    public class HotelDTO
    {
        public int? Id { get; set; }
        public int? GradId { get; set; }
        public string Naziv { get; set; }
        public short BrojZvjezdica { get; set; }
        public string Telefon { get; set; }
        public string Email { get; set; }
        public string Grad { get; set; }
        public string Adresa { get; set; }
    }
}