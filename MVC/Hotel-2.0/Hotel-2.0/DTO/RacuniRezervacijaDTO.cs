﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Hotel_2._0.DTO
{
    public class RacuniRezervacijaDTO
    {
        public int Id { get; set; }
        public string Ime { get; set; }
        public string Prezime { get; set; }
        public string  Hotel { get; set; }
        public int BrojSobe { get; set; }
        public decimal UkupanIznos { get; set; }
        public decimal PreostaliDug { get; set; }
    }
}