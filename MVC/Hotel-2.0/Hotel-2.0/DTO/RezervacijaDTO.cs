﻿using System;

namespace Hotel_2._0.DTO
{
    public class RezervacijaDTO
    {
        public int? Id { get; set; }
        public int GostId { get; set; }
        public int? HotelId { get; set; }
        public int? TipSobeId { get; set; }
        public int? BrojSobe { get; set; }
        public DateTime Dolazak { get; set; }
        public DateTime Odlazak { get; set; }
        public decimal? Popust { get; set; }
    }
}