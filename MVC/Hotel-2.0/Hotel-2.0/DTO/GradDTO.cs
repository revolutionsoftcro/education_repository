﻿namespace Hotel_2._0.DTO
{
    public class GradDTO
    {
        public int? Id { get; set; }
        public int? DrzavaId { get; set; }
        public string Grad { get; set; }
        public string Drzava { get; set; }
    }
}