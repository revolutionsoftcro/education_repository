﻿using Hotel_2._0.Models;
using Hotel_2._0.ViewModels;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web.Mvc;

namespace Hotel_2._0.Controllers
{
    public class GradController : CustomController
    {
        public GradController() : base() {}
        public GradController(Hotel2Entities context) : base(context) {}

        // GET: Grad
        public ActionResult Index()
        {
            IQueryable<Grad> grad = db.Grad.Include(g => g.Drzava);
            return View(grad.AsQueryable());
        }

        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Grad grad = db.Grad.Find(id);
            if (grad == null)
            {
                return HttpNotFound();
            }

            return View(grad);
        }

        // GET: Grad/Create
        public ActionResult Create()
        {
            var model = new GradViewModel();
            model.ListaDrzava = new SelectList(db.Drzava.OrderBy(d => d.Naziv), "PK_Drzava", "Naziv");
            return View(model);
        }

        // POST: Grad/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "PK_Grad,Naziv,FK_Grad_Drzava_PK")] Grad grad)
        {
            if (ModelState.IsValid)
            {
                db.Grad.Add(grad);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            var model = new GradViewModel();
            model.ListaDrzava = new SelectList(db.Drzava.OrderBy(d => d.Naziv), "PK_Drzava", "Naziv");
            return View(model);
        }

        // GET: Grad/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            Grad grad = db.Grad.Find(id);
            if (grad == null)
            {
                return HttpNotFound();
            }
            var model = new GradViewModel();

            model.ListaDrzava = new SelectList(db.Drzava.OrderBy(d => d.Naziv), "PK_Drzava", "Naziv");
            model.PK_Grad = grad.PK_Grad;
            model.Naziv = grad.Naziv;
            model.FK_Grad_Drzava_PK = grad.FK_Grad_Drzava_PK;

            return View(model);
        }

        // POST: Grad/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "PK_Grad,Naziv,FK_Grad_Drzava_PK")] Grad grad)
        {
            if (ModelState.IsValid)
            {
                db.ChangeState<Grad>(grad);
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            var model = new GradViewModel();
            model.ListaDrzava = new SelectList(db.Drzava.OrderBy(d => d.Naziv), "PK_Drzava", "Naziv");
            return View(model);
        }

        // GET: Grad/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Grad grad = db.Grad.Find(id);
            if (grad == null)
            {
                return HttpNotFound();
            }
            return View(grad);
        }

        // POST: Grad/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Grad grad = db.Grad.Find(id);
            db.Grad.Remove(grad);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
