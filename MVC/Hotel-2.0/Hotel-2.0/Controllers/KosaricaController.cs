﻿using Hotel_2._0.Models;
using Hotel_2._0.ViewModels;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web.Mvc;

namespace Hotel_2._0.Controllers
{
    public class KosaricaController : CustomController
    {
        public KosaricaController() : base() { }
        public KosaricaController(Hotel2Entities context) : base(context) { }

        public ActionResult Cart()
        {
            var kosaricaViewModel = new KosaricaViewModel();
            var rezervacija = db.Rezervacija.Include(r => r.Gost).Include(r => r.Soba).Include(r => r.Soba.Hotel).Where(r => r.PK_Rezervacija == r.Kosarica.Where(k => k.Trenutna == true).Select(k => k.FK_Kosarica_Rezervacija_PK).FirstOrDefault()).First();
            kosaricaViewModel.Kupac = rezervacija.HotelGostSoba;
            kosaricaViewModel.UkupnaCijena = db.Kosarica.Where(k => k.Trenutna == true).Select(k => k.Iznos).First();
            kosaricaViewModel.Items = new List<KosaricaProizvodViewModel>();

            foreach (var proizvod in db.KosaricaProizvod.Include(p => p.UslugeProizvodi).Where(k => k.Kosarica.Trenutna == true && k.Deleted == false))
            {
                var kosaricaProizvodViewModel = new KosaricaProizvodViewModel()
                {
                    Naziv = proizvod.UslugeProizvodi.Naziv,
                    Tip = proizvod.UslugeProizvodi.Tip,
                    CijenaPoMjeri = proizvod.UslugeProizvodi.CijenaPoMjeri,
                    Mjera = proizvod.UslugeProizvodi.Mjera,
                    Kolicina = proizvod.Kolicina,
                    Iznos = proizvod.Iznos,
                    Id = proizvod.PK_KosaricaProizvod
                };

                kosaricaViewModel.Items.Add(kosaricaProizvodViewModel);
            }

            return View(kosaricaViewModel);
        }

        public ActionResult Terminate()
        {
            var kosarica = new Kosarica();
            kosarica = db.Kosarica.Where(k => k.Trenutna == true).FirstOrDefault();
            kosarica.Trenutna = false;
            kosarica.Otkazana = true;
            db.ChangeState<Kosarica>(kosarica);
            db.SaveChanges();

            return RedirectToAction("Index", "Home");
        }

        public ActionResult Checkout()
        {
            var kosarica = db.Kosarica.Where(k => k.Trenutna == true).FirstOrDefault();
            if (db.Kosarica.Where(k => k.Trenutna == true && k.Iznos != 0).Any())
            {
                var racun = new RacunController(db);
                racun.NapraviRn();
            }
            else
            {
                kosarica.Otkazana = true;
            }

            kosarica.Trenutna = false;
            db.ChangeState<Kosarica>(kosarica);
            db.SaveChanges();
            return RedirectToAction("Index", "Home");
        }

        internal void Napravi(int pK_Rezervacija)
        {
            if (db.Kosarica.Where(k => k.Trenutna == true).Any())
            {
                Kosarica kosarica_t = db.Kosarica.ToList().Where(k => k.Trenutna == true).First();
                kosarica_t.Trenutna = false;
                kosarica_t.Otkazana = true;
                db.ChangeState<Kosarica>(kosarica_t);
                db.SaveChanges();
            }

            Kosarica kosarica = new Kosarica()
            {
                FK_Kosarica_Rezervacija_PK = pK_Rezervacija,
                Trenutna = true,
                Otkazana = false,
                Iznos = 0,
                Datum_izrade = DateTime.Now
            };

            db.Kosarica.Add(kosarica);
            db.SaveChanges();
        }


        internal void UpdateIznos(int kosaricaId, decimal iznos)
        {
            var kosarica = db.Kosarica.Where(k => k.PK_Kosarica == kosaricaId).FirstOrDefault();
            kosarica.Iznos += iznos;
            db.ChangeState<Kosarica>(kosarica);
            db.SaveChanges();
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
