﻿using System.Web.Mvc;

namespace Hotel_2._0.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Unos()
        {
            return View();
        }

        public ActionResult Rezervacije()
        {
            return View();
        }

    }
}