﻿using Hotel_2._0.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Web.Mvc;

namespace Hotel_2._0.Controllers
{
    public class CustomController : Controller
    {
        MojNLog MNLog = new MojNLog(new NLog_html());
        protected Hotel2Entities db;

        public CustomController()
        {
            db = new Hotel2Entities();
        }

        public CustomController(Hotel2Entities context)
        {
            db = context;
        }

        protected List<T> _pretvori<T>(DbSet<T> nesto) where T : HotelModel
        {
            List<T> data = new List<T>();

            foreach (var d in nesto)
            {
                data.Add(d);
            }

            return data;
        }

        protected override void OnException(ExceptionContext filterContext)
        {
            Exception ex = filterContext.Exception;
            filterContext.ExceptionHandled = true;

            var controller = filterContext.RouteData.Values["controller"].ToString();
            var action = filterContext.RouteData.Values["action"].ToString();
            var model = new HandleErrorInfo(ex, controller, action);

            MNLog.Error(this.ControllerContext.RouteData.Values["controller"].ToString(), model.Exception.Message);

            filterContext.Result = new ViewResult
            {
                ViewName = "Error",
                ViewData = new ViewDataDictionary(model)
            };
        }
    }
}