﻿using Hotel_2._0.Models;
using Hotel_2._0.ViewModels;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web.Mvc;

namespace Hotel_2._0.Controllers
{
    public class RacunController : CustomController
    {
        public RacunController() : base() { }
        public RacunController(Hotel2Entities context) : base(context) { }

        public ActionResult RacuniView(int? id)
        {
            var racuni = new List<RacuniViewModel>();
            foreach (var racun in db.Racun.Include(x => x.StavkaRacun).Include(x => x.Rezervacija).Where(x => x.FK_Racun_Rezervacija_PK == id))
            {
                var racuniViewModel = new RacuniViewModel();
                racuniViewModel.PK_Racun = racun.PK_Racun;
                racuniViewModel.isPaid = racun.isPaid;
                racuniViewModel.Iznos = 0;

                if (racun.Soba == true)
                {
                    racuniViewModel.Iznos += racun.Iznos;
                }
                else
                {
                    foreach (var podracun in racun.StavkaRacun)
                    {
                        racuniViewModel.Iznos += podracun.Iznos;
                    }
                }

                racuni.Add(racuniViewModel);
            }


            return View(racuni);
        }

        public ActionResult Plati(int? id)
        {
            var racun = db.Racun.Where(r => r.PK_Racun == id).FirstOrDefault();
            racun.isPaid = true;
            db.ChangeState<Racun>(racun);
            db.SaveChanges();
            return RedirectToAction("RacuniView", new { id = racun.FK_Racun_Rezervacija_PK });
        }

        public void NapraviSobaRn(int rezervacija, decimal iznos, decimal? popust)
        {
            var rezervacija_s = db.Rezervacija.Where(r => r.PK_Rezervacija == rezervacija).First();
            
            if (popust != null)
            {
                iznos = iznos * ((100 - (decimal)popust) / 100);
            }
            Racun racun = new Racun()
            {
                FK_Racun_Rezervacija_PK = rezervacija,
                Iznos = iznos,
                Soba = true,
                isPaid = false,
                Datum_izrade = DateTime.Now
            };
            db.Racun.Add(racun);
            db.SaveChanges();
        }

        internal void NapraviRn()
        {
            var racun = new Racun
            {
                FK_Racun_Rezervacija_PK = db.Kosarica.Where(k => k.Trenutna == true).Select(k => k.FK_Kosarica_Rezervacija_PK).FirstOrDefault(),
                Soba = false,
                isPaid = false,
                Iznos = db.Kosarica.Where(k => k.Trenutna == true).Select(k => k.Iznos).FirstOrDefault(),
                Datum_izrade = DateTime.Now
            };
            db.Racun.Add(racun);
            db.SaveChanges();
            var stavke = new StavkaRacunController(db);
            stavke.NapraviStavke(racun.PK_Racun);

        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }


    }
}
