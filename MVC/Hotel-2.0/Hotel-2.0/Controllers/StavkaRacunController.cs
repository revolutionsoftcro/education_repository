﻿using Hotel_2._0.Models;
using System;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web.Mvc;

namespace Hotel_2._0.Controllers
{
    public class StavkaRacunController : CustomController
    {
        public StavkaRacunController() : base() { }
        public StavkaRacunController(Hotel2Entities context) : base(context) { }

        internal void NapraviStavke(int pK_Racun)
        {
            foreach (var proizvod in db.Kosarica.Where(k => k.Trenutna == true).Select(p => p.KosaricaProizvod).First())
            {
                if (proizvod.Deleted == false)
                {
                    var stavka = new StavkaRacun()
                    {
                        FK_StavkaRacun_Racun_PK = pK_Racun,
                        FK_StavkaRacun_UslugeProizvodi_PK = proizvod.FK_KosaricaProizvod_UslugeProizvodi_PK,
                        Kolicina = proizvod.Kolicina,
                        Iznos = proizvod.Iznos,
                        Datum_izrade = DateTime.Now
                    };

                    db.StavkaRacun.Add(stavka);
                    db.SaveChanges();
                }
            }
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
