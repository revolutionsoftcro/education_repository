﻿using Hotel_2._0.Models;
using Hotel_2._0.ViewModels;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web.Mvc;

namespace Hotel_2._0.Controllers
{
    public class SobaController : CustomController
    {
        public SobaController() : base() { }
        public SobaController(Hotel2Entities context) : base(context) { }

        // GET: Soba
        public ActionResult Index()
        {
            var soba = db.Soba.Include(s => s.Hotel).Include(s => s.TipSobe);
            return View(soba.ToList());
        }

        // GET: Soba/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Soba soba = db.Soba.Find(id);
            if (soba == null)
            {
                return HttpNotFound();
            }
            return View(soba);
        }

        // GET: Soba/Create
        public ActionResult Create()
        {
            var model = new SobaViewModel();
            model.ListaHotela = new SelectList(db.Hotel.OrderBy(h => h.Naziv), "PK_Hotel", "Naziv");
            model.ListaTipovaSoba = new SelectList(db.TipSobe, "PK_TipSobe", "Naziv");
            return View(model);
        }

        // POST: Soba/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "PK_Soba,FK_Soba_TipSobe_PK,FK_Soba_Hotel_PK,Cijena")] Soba soba)
        {
            if (ModelState.IsValid)
            {
                db.Soba.Add(soba);
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            var model = new SobaViewModel();
            model.ListaHotela = new SelectList(db.Hotel.OrderBy(h => h.Naziv), "PK_Hotel", "Naziv");
            model.ListaTipovaSoba = new SelectList(db.TipSobe, "PK_TipSobe", "Naziv");
            return View(model);
        }

        // GET: Soba/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Soba soba = db.Soba.Find(id);
            if (soba == null)
            {
                return HttpNotFound();
            }
            var model = new SobaViewModel();
            model.ListaHotela = new SelectList(db.Hotel.OrderBy(h => h.Naziv), "PK_Hotel", "Naziv");
            model.ListaTipovaSoba = new SelectList(db.TipSobe, "PK_TipSobe", "Naziv");
            return View(model);
        }

        // POST: Soba/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "PK_Soba,FK_Soba_TipSobe_PK,FK_Soba_Hotel_PK,Cijena")] Soba soba)
        {
            if (ModelState.IsValid)
            {
                db.ChangeState<Soba>(soba);
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            var model = new SobaViewModel();
            model.ListaHotela = new SelectList(db.Hotel.OrderBy(h => h.Naziv), "PK_Hotel", "Naziv");
            model.ListaTipovaSoba = new SelectList(db.TipSobe, "PK_TipSobe", "Naziv");
            return View(model);
        }

        // GET: Soba/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Soba soba = db.Soba.Find(id);
            if (soba == null)
            {
                return HttpNotFound();
            }
            return View(soba);
        }

        // POST: Soba/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Soba soba = db.Soba.Find(id);
            db.Soba.Remove(soba);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
