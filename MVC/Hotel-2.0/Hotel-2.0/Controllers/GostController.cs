﻿using Hotel_2._0.Models;
using Hotel_2._0.ViewModels;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web.Mvc;

namespace Hotel_2._0.Controllers
{
    public class GostController : CustomController
    {
        public GostController() : base() { }
        public GostController(Hotel2Entities context) : base(context) { }

        // GET: Gost
        public ActionResult Index()
        {
            var gost = db.Gost.Include(g => g.Grad);
            return View(gost.ToList());
        }

        // GET: Gost/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Gost gost = db.Gost.Find(id);
            if (gost == null)
            {
                return HttpNotFound();
            }
            return View(gost);
        }

        // GET: Gost/Create
        public ActionResult Create()
        {
            var model = new GostViewModel();
            model.ListaGradova = new SelectList(db.Grad.OrderBy(g => g.Naziv), "PK_Grad", "Naziv");
            return View(model);
        }

        // POST: Gost/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "PK_Gost,Ime,Prezime,Mobitel,Email,FK_Gost_Grad_PK,Adresa")] Gost gost)
        {
            if (ModelState.IsValid)
            {
                db.Gost.Add(gost);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            var model = new GostViewModel();
            model.ListaGradova = new SelectList(db.Grad.OrderBy(g => g.Naziv), "PK_Grad", "Naziv");
            return View(model);
        }

        // GET: Gost/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Gost gost = db.Gost.Find(id);
            if (gost == null)
            {
                return HttpNotFound();
            }
            var model = new GostViewModel();
            model.ListaGradova = new SelectList(db.Grad.OrderBy(g => g.Naziv), "PK_Grad", "Naziv");
            return View(model);
        }

        // POST: Gost/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "PK_Gost,Ime,Prezime,Mobitel,Email,FK_Gost_Grad_PK,Adresa")] Gost gost)
        {
            if (ModelState.IsValid)
            {
                db.ChangeState<Gost>(gost);
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            var model = new GostViewModel();
            model.ListaGradova = new SelectList(db.Grad.OrderBy(g => g.Naziv), "PK_Grad", "Naziv");
            return View(model);
        }

        // GET: Gost/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Gost gost = db.Gost.Find(id);
            if (gost == null)
            {
                return HttpNotFound();
            }
            return View(gost);
        }

        // POST: Gost/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Gost gost = db.Gost.Find(id);
            db.Gost.Remove(gost);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
