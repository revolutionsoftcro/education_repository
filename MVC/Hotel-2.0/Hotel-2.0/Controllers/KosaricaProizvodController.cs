﻿using Hotel_2._0.Models;
using Hotel_2._0.ViewModels;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web.Mvc;

namespace Hotel_2._0.Controllers
{
    public class KosaricaProizvodController : CustomController
    {
        public KosaricaProizvodController() : base() { }
        public KosaricaProizvodController(Hotel2Entities context) : base(context) { }

        [HttpPost]
        public ActionResult AddToCart(decimal kolicina, int proizvodID)
        {
            var item = new KosaricaProizvod()
            {
                FK_KosaricaProizvod_Kosarica_PK = db.Kosarica.Where(k => k.Trenutna == true).Select(k => k.PK_Kosarica).FirstOrDefault(),
                FK_KosaricaProizvod_UslugeProizvodi_PK = proizvodID,
                Deleted = false,
                Kolicina = kolicina,
                Iznos = kolicina * db.UslugeProizvodi.Where(p => p.PK_UslugeProizvodi == proizvodID).Select(c => c.CijenaPoMjeri).FirstOrDefault(),
                Datum_izrade = DateTime.Now
            };

            db.KosaricaProizvod.Add(item);
            db.SaveChanges();

            var kosarica = new KosaricaController(db);
            kosarica.UpdateIznos(item.FK_KosaricaProizvod_Kosarica_PK, item.Iznos);

            return RedirectToAction("Prodaja", "UslugeProizvodi");
        }

        [HttpPost]
        public ActionResult RemoveFromCart(int proizvodID)
        {
            var proizvod= new KosaricaProizvod();
            proizvod= db.KosaricaProizvod.Where(p => p.PK_KosaricaProizvod == proizvodID).FirstOrDefault();
            proizvod.Deleted = true;
            proizvod.Datum_brisanja = DateTime.Now;
            db.ChangeState<KosaricaProizvod>(proizvod);
            db.SaveChanges();

            var kosarica = new KosaricaController(db);
            kosarica.UpdateIznos(proizvod.FK_KosaricaProizvod_Kosarica_PK, (0 - proizvod.Iznos));

            return RedirectToAction("Cart", "Kosarica");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
