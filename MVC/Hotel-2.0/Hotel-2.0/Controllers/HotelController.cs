﻿using Hotel_2._0.Models;
using Hotel_2._0.ViewModels;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web.Mvc;

namespace Hotel_2._0.Controllers
{
    public class HotelController : CustomController
    {
        public HotelController() : base() { }
        public HotelController(Hotel2Entities context) : base(context) { }

        // GET: Hotel
        public ActionResult Index()
        {
            var hotel = db.Hotel.Include(h => h.Grad);
            return View(hotel.ToList());
        }

        // GET: Hotel/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Hotel hotel = db.Hotel.Find(id);
            if (hotel == null)
            {
                return HttpNotFound();
            }
            return View(hotel);
        }

        // GET: Hotel/Create
        public ActionResult Create()
        {
            var model = new HotelViewModel();
            model.ListaGradova = new SelectList(db.Grad.OrderBy(g => g.Naziv), "PK_Grad", "Naziv");
            return View(model);
        }

        // POST: Hotel/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "PK_Hotel,Naziv,BrojZvjezdica,Telefon,Email,FK_Hotel_Grad_PK,Adresa")] Hotel hotel)
        {
            if (ModelState.IsValid)
            {
                db.Hotel.Add(hotel);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            var model = new HotelViewModel();
            model.ListaGradova = new SelectList(db.Grad.OrderBy(g => g.Naziv), "PK_Grad", "Naziv");
            return View(model);
        }

        // GET: Hotel/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Hotel hotel = db.Hotel.Find(id);
            if (hotel == null)
            {
                return HttpNotFound();
            }
            var model = new HotelViewModel();
            model.ListaGradova = new SelectList(db.Grad.OrderBy(g => g.Naziv), "PK_Grad", "Naziv");
            return View(model);
        }

        // POST: Hotel/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "PK_Hotel,Naziv,BrojZvjezdica,Telefon,Email,FK_Hotel_Grad_PK,Adresa")] Hotel hotel)
        {
            if (ModelState.IsValid)
            {
                db.ChangeState<Hotel>(hotel);
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            var model = new HotelViewModel();
            model.ListaGradova = new SelectList(db.Grad.OrderBy(g => g.Naziv), "PK_Grad", "Naziv");
            return View(model);
        }

        // GET: Hotel/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Hotel hotel = db.Hotel.Find(id);
            if (hotel == null)
            {
                return HttpNotFound();
            }
            return View(hotel);
        }

        // POST: Hotel/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Hotel hotel = db.Hotel.Find(id);
            db.Hotel.Remove(hotel);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
