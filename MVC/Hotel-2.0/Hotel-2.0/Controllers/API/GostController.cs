﻿using AutoMapper;
using Hotel_2._0.DTO;
using Hotel_2._0.Models;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Web.Http;
using System.Web.Http.Cors;
using System.Web.Http.Description;

namespace Hotel_2._0.Controllers.API
{
    public class GostController :  BaseController
    {
        private Hotel2Entities db = new Hotel2Entities();

        // GET: api/Gost
        [HttpGet]
        public IEnumerable<GostDTO> Gost()
        {
            return db.Gost.ToList().Select(Mapper.Map<Gost,GostDTO>);
        }

        // GET: api/Gost/5
        [ResponseType(typeof(GostDTO))]
        [HttpGet]
        public IHttpActionResult Gost(int id)
        {
            Gost gost = db.Gost.Find(id);
            if (gost == null)
            {
                return NotFound();
            }

            return Ok(Mapper.Map<Gost,GostDTO>(gost));
        }

        // PUT: api/Gost/5
        [ResponseType(typeof(void))]
        [HttpPut]
        public IHttpActionResult Gost(int id, GostDTO gostDTO)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if(id!=gostDTO.Id)
            {
                return BadRequest("Id se ne podudara!");
            }

            if (!db.Grad.Where(g => g.PK_Grad == gostDTO.GradId).Any())
            {
                return BadRequest("Grad ne postoji u bazi!");
            }

            db.Entry(Mapper.Map<GostDTO, Gost>(gostDTO)).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!GostExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Gost
        [ResponseType(typeof(GostDTO))]
        [HttpPost]
        public IHttpActionResult Gost(GostDTO gostDTO)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (!db.Grad.Where(g => g.PK_Grad == gostDTO.GradId).Any())
            {
                return BadRequest("Grad ne postoji u bazi!");
            }

            var gost = Mapper.Map<GostDTO, Gost>(gostDTO);
            db.Gost.Add(gost);
            db.SaveChanges();
            return CreatedAtRoute("DefaultApi", new { id = gost.PK_Gost }, Mapper.Map<Gost,GostDTO>(gost));
        }

        // DELETE: api/Gost/5
        [ResponseType(typeof(Gost))]
        [HttpDelete]
        public IHttpActionResult DGost(int id)
        {
            Gost gost = db.Gost.Find(id);
            if (gost == null)
            {
                return NotFound();
            }

            db.Gost.Remove(gost);
            db.SaveChanges();

            return Ok(Mapper.Map<Gost,GostDTO>(gost));
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool GostExists(int id)
        {
            return db.Gost.Count(e => e.PK_Gost == id) > 0;
        }
    }
}