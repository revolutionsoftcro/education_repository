﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using Hotel_2._0.Models;

namespace Hotel_2._0.Controllers.API
{
    public class TipSobeController : BaseController
    {
        private Hotel2Entities db = new Hotel2Entities();

        // GET: api/TipSobe
        [HttpGet]
        public IQueryable<TipSobe> TipSobe()
        {
            return db.TipSobe;
        }

        // GET: api/TipSobe/5
        [ResponseType(typeof(TipSobe))]
        [HttpGet]
        public IHttpActionResult TipSobe(int id)
        {
            TipSobe tipSobe = db.TipSobe.Find(id);
            if (tipSobe == null)
            {
                return NotFound();
            }

            return Ok(tipSobe);
        }

        // PUT: api/TipSobe/5
        [ResponseType(typeof(void))]
        [HttpPut]
        public IHttpActionResult TipSobe(int id, TipSobe tipSobe)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != tipSobe.PK_TipSobe)
            {
                return BadRequest();
            }

            db.Entry(tipSobe).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!TipSobeExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/TipSobe
        [ResponseType(typeof(TipSobe))]
        [HttpPost]
        public IHttpActionResult TipSobe(TipSobe tipSobe)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.TipSobe.Add(tipSobe);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = tipSobe.PK_TipSobe }, tipSobe);
        }

        // DELETE: api/TipSobe/5
        [ResponseType(typeof(TipSobe))]
        [HttpDelete]
        public IHttpActionResult DTipSobe(int id)
        {
            TipSobe tipSobe = db.TipSobe.Find(id);
            if (tipSobe == null)
            {
                return NotFound();
            }

            db.TipSobe.Remove(tipSobe);
            db.SaveChanges();

            return Ok(tipSobe);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool TipSobeExists(int id)
        {
            return db.TipSobe.Count(e => e.PK_TipSobe == id) > 0;
        }
    }
}