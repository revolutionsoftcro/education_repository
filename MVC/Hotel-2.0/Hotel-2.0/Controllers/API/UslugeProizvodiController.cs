﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using AutoMapper;
using Hotel_2._0.DTO;
using Hotel_2._0.Models;

namespace Hotel_2._0.Controllers.API
{
    public class UslugeProizvodiController : BaseController
    {
        private Hotel2Entities db = new Hotel2Entities();

        // GET: api/UslugeProizvodi
        [HttpGet]
        public IEnumerable<UslugeProizvodiDTO> UslugeProizvodi()
        {
            return db.UslugeProizvodi.ToList().Select(Mapper.Map<UslugeProizvodi, UslugeProizvodiDTO>);
        }

        // GET: api/UslugeProizvodi/5
        [ResponseType(typeof(UslugeProizvodiDTO))]
        [HttpGet]
        public IHttpActionResult UslugeProizvodi(int id)
        {
            UslugeProizvodi proizvod = db.UslugeProizvodi.Find(id);
            if (proizvod == null)
            {
                return NotFound();
            }

            return Ok(Mapper.Map<UslugeProizvodi, UslugeProizvodiDTO>(proizvod));
        }

        // PUT: api/UslugeProizvodi/5
        [ResponseType(typeof(void))]
        [HttpPut]
        public IHttpActionResult UslugeProizvodi(int id, UslugeProizvodiDTO uslugeProizvodiDTO)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != uslugeProizvodiDTO.Id)
            {
                return BadRequest();
            }

            if (!db.Hotel.Where(h => h.PK_Hotel == uslugeProizvodiDTO.HotelId).Any())
            {
                return BadRequest("Hotel ne postoji u bazi!");
            }

            db.Entry(Mapper.Map<UslugeProizvodiDTO,UslugeProizvodi>(uslugeProizvodiDTO)).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!UslugeProizvodiExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/UslugeProizvodi
        [ResponseType(typeof(UslugeProizvodiDTO))]
        [HttpPost]
        public IHttpActionResult UslugeProizvodi(UslugeProizvodiDTO uslugeProizvodiDTO)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if(!db.Hotel.Where(h=>h.PK_Hotel==uslugeProizvodiDTO.HotelId).Any())
            {
                return BadRequest("Hotel ne postoji u bazi!");
            }

            var proizvod = Mapper.Map<UslugeProizvodiDTO, UslugeProizvodi>(uslugeProizvodiDTO);
            db.UslugeProizvodi.Add(proizvod);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = proizvod.PK_UslugeProizvodi }, Mapper.Map<UslugeProizvodi, UslugeProizvodiDTO>(proizvod));
        }

        // DELETE: api/UslugeProizvodi/5
        [ResponseType(typeof(UslugeProizvodi))]
        [HttpDelete]
        public IHttpActionResult DUslugeProizvodi(int id)
        {
            UslugeProizvodi uslugeProizvodi = db.UslugeProizvodi.Find(id);
            if (uslugeProizvodi == null)
            {
                return NotFound();
            }

            if(db.KosaricaProizvod.Where(k=>k.FK_KosaricaProizvod_UslugeProizvodi_PK==id).Any())
            {
                return BadRequest("Taj proizvod je u košarici!");
            }

            if(db.StavkaRacun.Where(s=>s.FK_StavkaRacun_UslugeProizvodi_PK==id).Any())
            {
                return BadRequest("Taj proizvod je na računu!");
            }

            db.UslugeProizvodi.Remove(uslugeProizvodi);
            db.SaveChanges();

            return Ok(uslugeProizvodi);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool UslugeProizvodiExists(int id)
        {
            return db.UslugeProizvodi.Count(e => e.PK_UslugeProizvodi == id) > 0;
        }
    }
}