﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using AutoMapper;
using Hotel_2._0.DTO;
using Hotel_2._0.Models;

namespace Hotel_2._0.Controllers.API
{
    public class HotelController : BaseController
    {
        private Hotel2Entities db = new Hotel2Entities();

        // GET: api/Hotel
        [HttpGet]
        public IEnumerable<HotelDTO> Hotel()
        {
            return db.Hotel.Include(g=>g.Grad).ToList().Select(Mapper.Map<Hotel, HotelDTO>);
        }

        // GET: api/Hotel/5
        [HttpGet]
        [ResponseType(typeof(HotelDTO))]
        public IHttpActionResult Hotel(int id)
        {
            if (!db.Hotel.Where(h=>h.PK_Hotel==id).Any())
            {
                return NotFound();
            }

            return Ok(Mapper.Map<Hotel,HotelDTO>(db.Hotel.Include(h=>h.Grad).Where(h=>h.PK_Hotel==id).FirstOrDefault()));
        }

        // PUT: api/Hotel/5
        [ResponseType(typeof(void))]
        [HttpPut]
        public IHttpActionResult Hotel(int id, HotelDTO hotelDTO)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != hotelDTO.Id)
            {
                return BadRequest();
            }

            if (!db.Grad.Where(g => g.PK_Grad == hotelDTO.GradId).Any())
            {
                return BadRequest("Grad ne postoji u bazi!");
            }

            db.Entry(Mapper.Map<HotelDTO,Hotel>(hotelDTO)).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!HotelExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Hotel
        [ResponseType(typeof(Hotel))]
        [HttpPost]
        public IHttpActionResult Hotel(HotelDTO hotelDTO)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (!db.Grad.Where(g=>g.PK_Grad==hotelDTO.GradId).Any())
            {
                return BadRequest("Grad ne postoji u bazi!");
            }

            var hotel = Mapper.Map<HotelDTO, Hotel>(hotelDTO);
            db.Hotel.Add(hotel);
            db.SaveChanges();
            hotelDTO.Id = hotel.PK_Hotel;

            return CreatedAtRoute("DefaultApi", new { id = hotel.PK_Hotel }, hotelDTO);
        }

        // DELETE: api/Hotel/5
        [ResponseType(typeof(Hotel))]
        [HttpDelete]
        public IHttpActionResult DHotel(int id)
        {
            Hotel hotel = db.Hotel.Find(id);
            if (hotel == null)
            {
                return NotFound();
            }

            if(db.Soba.Where(s=>s.FK_Soba_Hotel_PK==id).Any())
            {
                return BadRequest("U hotelu ima soba!");
            }

            if (db.UslugeProizvodi.Where(p => p.FK_UslugeProizvodi_Hotel_PK == id).Any())
            {
                return BadRequest("U hotelu ima usluga/proizvoda!");
            }

            db.Hotel.Remove(hotel);
            db.SaveChanges();

            return Ok(hotel);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool HotelExists(int id)
        {
            return db.Hotel.Count(e => e.PK_Hotel == id) > 0;
        }
    }
}