﻿using AutoMapper;
using Hotel_2._0.Models;
using System;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Web.Http;
using System.Web.Http.Description;

namespace Hotel_2._0.Controllers.API
{
    [RoutePrefix("api/kosarica")]
    public class KosaricaController : BaseController
    {
        private Hotel2Entities db = new Hotel2Entities();

        // GET: api/Kosarica
        [HttpGet]
        public IQueryable<Kosarica> Kosarica()
        {
            return db.Kosarica;
        }

        // GET: api/Kosarica/5
        [ResponseType(typeof(Kosarica))]
        [HttpGet]
        public IHttpActionResult Kosarica(int id)
        {
            Kosarica kosarica = db.Kosarica.Find(id);
            if (kosarica == null)
            {
                return NotFound();
            }

            return Ok(kosarica);
        }

        [ResponseType(typeof(Kosarica))]
        [Route("prodaja")]
        [HttpPost]
        public IHttpActionResult Prodaja([FromBody]int id)
        {
            if (!db.Rezervacija.Where(r => r.PK_Rezervacija == id).Any())
            {
                return BadRequest("Rezervacije nema u bazi!");
            }

            if (db.Kosarica.Where(k => k.Trenutna == true).Any())
            {
                var kosarica_t = db.Kosarica.Where(k => k.Trenutna == true).FirstOrDefault();
                kosarica_t.Trenutna = false;
                kosarica_t.Otkazana = true;
                db.Entry(kosarica_t).State = EntityState.Modified;
            }

            var kosarica = new Kosarica()
            {
                FK_Kosarica_Rezervacija_PK = id,
                Iznos = 0,
                Otkazana = false,
                Trenutna = true,
                Datum_izrade = DateTime.Now
            };

            db.Kosarica.Add(kosarica);
            db.SaveChanges();

            return Content(HttpStatusCode.Created, kosarica);
        }

        [ResponseType(typeof(void))]
        [Route("terminate")]
        [HttpPut]
        public IHttpActionResult Terminate()
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (!db.Kosarica.Where(k => k.Trenutna == true).Any())
            {
                return BadRequest("Ne postoji otvorena prodaja!");
            }

            var kosarica = db.Kosarica.Where(k => k.Trenutna == true).FirstOrDefault();
            kosarica.Trenutna = false;
            kosarica.Otkazana = true;

            db.Entry(kosarica).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!KosaricaExists(kosarica.PK_Kosarica))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        [ResponseType(typeof(void))]
        [Route("checkout")]
        [HttpPut]
        public IHttpActionResult Checkout()
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (!db.Kosarica.Where(k => k.Trenutna == true).Any())
            {
                return BadRequest("Ne postoji otvorena prodaja!");
            }

            var kosarica = db.Kosarica.Include(k=>k.KosaricaProizvod).Where(k => k.Trenutna == true).FirstOrDefault();
            kosarica.Trenutna = false;
            if (kosarica.Iznos==0)
            {
                kosarica.Otkazana = true;
            }

            db.Entry(kosarica).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!KosaricaExists(kosarica.PK_Kosarica))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            var racun = Mapper.Map<Kosarica, Racun>(kosarica);
            racun.Soba = false;
            racun.isPaid = false;
            racun.Datum_izrade = DateTime.Now;

            db.Racun.Add(racun);
            db.SaveChanges();

            foreach(var proizvod in kosarica.KosaricaProizvod)
            {
                if (proizvod.Deleted == false)
                {
                    var stavka = Mapper.Map<KosaricaProizvod, StavkaRacun>(proizvod);
                    stavka.FK_StavkaRacun_Racun_PK = racun.PK_Racun;
                    stavka.Datum_izrade = DateTime.Now;

                    db.StavkaRacun.Add(stavka);
                    db.SaveChanges();
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }


        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool KosaricaExists(int id)
        {
            return db.Kosarica.Count(e => e.PK_Kosarica == id) > 0;
        }
    }
}