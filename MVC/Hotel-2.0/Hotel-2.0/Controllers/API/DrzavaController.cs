﻿using Hotel_2._0.Models;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Web.Http;
using System.Web.Http.Description;

namespace Hotel_2._0.Controllers.API
{
    public class DrzavaController : BaseController
    {
        private Hotel2Entities db = new Hotel2Entities();

        // GET: api/Drzava
        [HttpGet]
        public List<Drzava> GetDrzava()
        {
            return db.Drzava.ToList();
        }

        // GET: api/Drzava/5
        [ResponseType(typeof(Drzava))]
        [HttpGet]
        public IHttpActionResult Drzava(int id)
        {
            Drzava drzava = db.Drzava.Find(id);
            if (drzava == null)
            {
                return NotFound();
            }

            return Ok(drzava);
        }

        // PUT: api/Drzava/5
        [ResponseType(typeof(void))]
        [HttpPut]
        public IHttpActionResult Drzava(int id, Drzava drzava)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != drzava.PK_Drzava)
            {
                return BadRequest();
            }

            db.Entry(drzava).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!DrzavaExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Drzava
        [ResponseType(typeof(Drzava))]
        [HttpPost]
        public IHttpActionResult PostDrzava(Drzava drzava)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.Drzava.Add(drzava);
            db.SaveChanges();
            return CreatedAtRoute("DefaultApi", new { id = drzava.PK_Drzava }, drzava);
        }

        // DELETE: api/Drzava/5
        [ResponseType(typeof(Drzava))]
        [HttpDelete]
        public IHttpActionResult DDrzava(int id)
        {
            Drzava drzava = db.Drzava.Find(id);
            if (drzava == null)
            {
                return NotFound();
            }

            if(db.Grad.Where(g=>g.FK_Grad_Drzava_PK==id).Any())
            {
                return BadRequest("Postoje gradovi u toj državi!");
            }

            db.Drzava.Remove(drzava);
            db.SaveChanges();

            return Ok(drzava);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool DrzavaExists(int id)
        {
            return db.Drzava.Count(e => e.PK_Drzava == id) > 0;
        }
    }
}