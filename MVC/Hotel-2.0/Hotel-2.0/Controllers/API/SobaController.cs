﻿using AutoMapper;
using Hotel_2._0.DTO;
using Hotel_2._0.Models;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Web.Http;
using System.Web.Http.Description;

namespace Hotel_2._0.Controllers.API
{
    public class SobaController : BaseController
    {
        private Hotel2Entities db = new Hotel2Entities();

        // GET: api/Soba
        [HttpGet]
        public IEnumerable<SobaDTO> Soba()
        {
            return db.Soba.Include(s=>s.TipSobe).Include(s=>s.Hotel).ToList().Select(Mapper.Map<Soba,SobaDTO>);
        }

        // GET: api/Soba/5
        [ResponseType(typeof(SobaDTO))]
        [HttpGet]
        public IHttpActionResult Soba(int id)
        {
            var soba = db.Soba.Include(h => h.Hotel).Include(t => t.TipSobe).Where(s => s.PK_Soba == id).FirstOrDefault();

            if (soba == null)
            {
                return NotFound();
            }

            return Ok(Mapper.Map<Soba,SobaDTO>(soba));
        }

        // PUT: api/Soba/5
        [ResponseType(typeof(void))]
        [HttpPut]
        public IHttpActionResult Soba(int id, SobaDTO sobaDTO)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != sobaDTO.Broj)
            {
                return BadRequest();
            }

            if (!db.Hotel.Where(h => h.PK_Hotel == sobaDTO.HotelId).Any())
            {
                return BadRequest("Hotel ne postoji u bazi!");
            }

            if (!db.TipSobe.Where(h => h.PK_TipSobe == sobaDTO.TipId).Any())
            {
                return BadRequest("Tip sobe ne postoji u bazi!");
            }


            db.Entry(Mapper.Map<SobaDTO,Soba>(sobaDTO)).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!SobaExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Soba
        [ResponseType(typeof(SobaDTO))]
        [HttpPost]
        public IHttpActionResult Soba(SobaDTO sobaDTO)
        {
            if (!db.Hotel.Where(h => h.PK_Hotel == sobaDTO.HotelId).Any())
            {
                return BadRequest("Hotel ne postoji u bazi!");
            }

            if (!db.TipSobe.Where(h => h.PK_TipSobe == sobaDTO.TipId).Any())
            {
                return BadRequest("Tip sobe ne postoji u bazi!");
            }

            var soba = Mapper.Map<SobaDTO, Soba>(sobaDTO);

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.Soba.Add(soba);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = soba.PK_Soba }, Mapper.Map<Soba,SobaDTO>(soba));
        }

        // DELETE: api/Soba/5
        [ResponseType(typeof(SobaDTO))]
        [HttpDelete]
        public IHttpActionResult DSoba(int id)
        {
            Soba soba = db.Soba.Find(id);
            if (soba == null)
            {
                return NotFound();
            }

            if (db.Rezervacija.Where(r=>r.FK_Rezervacija_Soba_PK==id).Any())
            {
                return BadRequest("Za tu sobu postoji rezervacija!");
            }

            db.Soba.Remove(soba);
            db.SaveChanges();

            return Ok(Mapper.Map<Soba,SobaDTO>(soba));
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool SobaExists(int id)
        {
            return db.Soba.Count(e => e.PK_Soba == id) > 0;
        }
    }
}