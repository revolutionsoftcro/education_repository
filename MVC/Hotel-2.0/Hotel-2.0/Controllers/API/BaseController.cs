﻿using System.Web.Http;
using System.Web.Http.Cors;

namespace Hotel_2._0.Controllers.API
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class BaseController : ApiController
    {
    }
}
