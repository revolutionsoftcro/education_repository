﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using AutoMapper;
using Hotel_2._0.DTO;
using Hotel_2._0.Models;

namespace Hotel_2._0.Controllers.API
{
    [RoutePrefix("api/racun")]
    public class RacunController : BaseController
    {
        private Hotel2Entities db = new Hotel2Entities();

        // GET: api/Racun
        [HttpGet]
        public IEnumerable<RacunDTO> Racun()
        {
            return db.Racun.ToList().Select(Mapper.Map<Racun,RacunDTO>);
        }

        // GET: api/Racun/5
        [ResponseType(typeof(RacunDTO))]
        [HttpGet]
        public IHttpActionResult Racun(int id)
        {
            Racun racun = db.Racun.Find(id);
            if (racun == null)
            {
                return NotFound();
            }

            return Ok(Mapper.Map<Racun,RacunDTO>(racun));
        }

        [Route("rezervacija")]
        [ResponseType(typeof(List<RacunDTO>))]
        [HttpGet]
        public IHttpActionResult RacunRezervacija([FromBody]int id)
        {
            if (!db.Rezervacija.Where(r=>r.PK_Rezervacija==id).Any())
            {
                return BadRequest("Rezervacija ne postoji u bazi");
            }

            return Ok(db.Racun.Where(r => r.FK_Racun_Rezervacija_PK == id).ToList().Select(Mapper.Map<Racun, RacunDTO>)); 
        }


        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool RacunExists(int id)
        {
            return db.Racun.Count(e => e.PK_Racun == id) > 0;
        }
    }
}