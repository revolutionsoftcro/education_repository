﻿using AutoMapper;
using Hotel_2._0.DTO;
using Hotel_2._0.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web.Http;
using System.Web.Http.Description;

namespace Hotel_2._0.Controllers.API
{
    [RoutePrefix("api/rezervacija")]
    public class RezervacijaController : BaseController
    {
        private Hotel2Entities db = new Hotel2Entities();

        // GET: api/Rezervacija
        [HttpGet]
        public IEnumerable<RezervacijaDTO> Rezervacija()
        {
            return db.Rezervacija.Include(r=>r.Soba).ToList().Select(Mapper.Map<Rezervacija, RezervacijaDTO>);
        }

        [Route("racuni-rezervacija")]
        [HttpGet]
        public List<RacuniRezervacijaDTO> Racuni()
        {
            var rezervacije = new List<RacuniRezervacijaDTO>();

            foreach (var rezervacija in db.Rezervacija.Include(g=>g.Gost).Include(h=>h.Soba).Include(h=>h.Soba.Hotel).Include(r=>r.Racun))
            {
                var rezervacija1 = new RacuniRezervacijaDTO()
                {
                    Id = rezervacija.PK_Rezervacija,
                    Ime=rezervacija.Gost.Ime,
                    Prezime=rezervacija.Gost.Prezime,
                    Hotel=rezervacija.Soba.Hotel.Naziv,
                    BrojSobe=rezervacija.Soba.PK_Soba,
                    UkupanIznos = 0,
                    PreostaliDug = 0,                    
                };

                foreach (var racun in rezervacija.Racun)
                {
                    if (racun.Soba == true)
                    {
                        rezervacija1.UkupanIznos += racun.Iznos;
                        if (racun.isPaid == false)
                        {
                            rezervacija1.PreostaliDug += racun.Iznos;
                        }
                    }
                    else
                    {
                        var racun1 = db.Racun.Include(r => r.StavkaRacun).Where(r => r.PK_Racun == racun.PK_Racun).FirstOrDefault();
                        foreach (var podracun in racun1.StavkaRacun)
                        {
                            rezervacija1.UkupanIznos += podracun.Iznos;
                            if (racun.isPaid == false)
                            {
                                rezervacija1.PreostaliDug += podracun.Iznos;
                            }
                        }
                    }
                }

                rezervacije.Add(rezervacija1);
            }

            return rezervacije;
        }

        // GET: api/Rezervacija/5
        [ResponseType(typeof(Rezervacija))]
        [HttpGet]
        public IHttpActionResult Rezervacija(int id)
        {
            Rezervacija rezervacija = db.Rezervacija.Include(r => r.Soba).Where(r => r.PK_Rezervacija == id).FirstOrDefault(); 
            if (rezervacija == null)
            {
                return NotFound();
            }

            return Ok(Mapper.Map<Rezervacija,RezervacijaDTO>(rezervacija));
        }


        // POST: api/Rezervacija
        [ResponseType(typeof(RezervacijaDTO))]
        [HttpPost]
        public IHttpActionResult Rezervacija(RezervacijaDTO rezervacijaDTO)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (rezervacijaDTO.Dolazak < DateTime.Now)
            {
                return BadRequest("Ne možete napraviti rezervaciju za datum koji je već prošao!");
            }

            if (rezervacijaDTO.Dolazak>rezervacijaDTO.Odlazak)
            {
                return BadRequest("Datum odlaska mora biti nakon datuma dolaska!");
            }

            if (!db.Gost.Where(g=>g.PK_Gost==rezervacijaDTO.GostId).Any())
            {
                return BadRequest("Taj gost ne postoji u bazi!");
            }

            if (!db.Hotel.Where(g => g.PK_Hotel == rezervacijaDTO.HotelId).Any())
            {
                return BadRequest("Taj hotel ne postoji u bazi!");
            }

            bool sobaPostoji = db.Soba.Where(s => s.FK_Soba_Hotel_PK == rezervacijaDTO.HotelId && s.FK_Soba_TipSobe_PK == rezervacijaDTO.TipSobeId).Any();

            if (!(sobaPostoji))
            {
                return BadRequest("Taj tip sobe ne postoji u odabranom hotelu!");
            }

            bool terminPostoji = db.Soba.Where(s => s.FK_Soba_Hotel_PK == rezervacijaDTO.HotelId && s.FK_Soba_TipSobe_PK == rezervacijaDTO.TipSobeId
                                 && !(s.Rezervacija.Any(r => !(r.Odlazak <= rezervacijaDTO.Dolazak || r.Dolazak >= rezervacijaDTO.Odlazak)))).Any();

            if (!terminPostoji)
            {
                return BadRequest("Nema slobodnih soba tog tipa za odabrani termin!");
            }

            var soba = db.Soba.Where(s => s.FK_Soba_Hotel_PK == rezervacijaDTO.HotelId && s.FK_Soba_TipSobe_PK == rezervacijaDTO.TipSobeId
                                   && !(s.Rezervacija.Any(r => !(r.Odlazak <= rezervacijaDTO.Dolazak || r.Dolazak >= rezervacijaDTO.Odlazak)))).FirstOrDefault();


            var rezervacija = Mapper.Map<RezervacijaDTO, Rezervacija>(rezervacijaDTO);
            rezervacija.FK_Rezervacija_Soba_PK = soba.PK_Soba;
            rezervacija.Datum_izrade = DateTime.Now;

            db.Rezervacija.Add(rezervacija);
            db.SaveChanges();

            decimal iznos = soba.Cijena * (int)(rezervacija.Odlazak - rezervacija.Dolazak).TotalDays;
            if (rezervacija.Popust != null)
            {
                iznos = iznos * ((100 - (decimal)rezervacija.Popust) / 100);
            }
            Racun racun = new Racun()
            {
                FK_Racun_Rezervacija_PK = rezervacija.PK_Rezervacija,
                Iznos = iznos,
                Soba = true,
                isPaid = false,
                Datum_izrade = DateTime.Now
            };
            db.Racun.Add(racun);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = rezervacija.PK_Rezervacija }, Mapper.Map<Rezervacija,RezervacijaDTO>(rezervacija));
        }

        // DELETE: api/Rezervacija/5
        [ResponseType(typeof(RezervacijaDTO))]
        [HttpDelete]
        public IHttpActionResult DRezervacija(int id)
        {
            Rezervacija rezervacija = db.Rezervacija.Find(id);
            if (rezervacija == null)
            {
                return NotFound();
            }

            if (rezervacija.Dolazak < DateTime.Now)
            {
                return BadRequest("Ne može se izbrisati rezervacija jer je već prošao datum dolaska!");
            }

            var racun = db.Racun.Where(r => r.FK_Racun_Rezervacija_PK == id).FirstOrDefault();
            db.Racun.Remove(racun);
            db.SaveChanges();
            db.Rezervacija.Remove(rezervacija);
            db.SaveChanges();

            return Ok(Mapper.Map<Rezervacija,RezervacijaDTO>(rezervacija));
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool RezervacijaExists(int id)
        {
            return db.Rezervacija.Count(e => e.PK_Rezervacija == id) > 0;
        }
    }
}