﻿using AutoMapper;
using Hotel_2._0.DTO;
using Hotel_2._0.Models;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Web.Http;
using System.Web.Http.Description;

namespace Hotel_2._0.Controllers.API
{
    public class GradController : BaseController
    {
        private Hotel2Entities db = new Hotel2Entities();

        // GET: api/Grad
        [HttpGet]
        public IEnumerable<GradDTO> Grad()
        {
             return db.Grad.Include(g=>g.Drzava).ToList().Select(Mapper.Map<Grad,GradDTO>);
        }

        // GET: api/Grad/5
        [ResponseType(typeof(GradDTO))]
        [HttpGet]
        public IHttpActionResult Grad(int id)
        {
            if (!db.Grad.Include(d => d.Drzava).Where(g => g.PK_Grad == id).Any())
            {
                return NotFound();
            }

            return Ok(Mapper.Map<Grad,GradDTO>(db.Grad.Include(d => d.Drzava).Where(g => g.PK_Grad == id).FirstOrDefault()));
        }

        // PUT: api/Grad/5
        [HttpPut]
        public IHttpActionResult Grad([FromBody] Grad grad, int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != grad.PK_Grad)
            {
                return BadRequest();
            }

            if (!db.Drzava.Where(d => d.PK_Drzava == grad.FK_Grad_Drzava_PK).Any())
            {
                return BadRequest("Ta država ne postoji u bazi");
            }

            db.Entry(grad).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!GradExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Grad
        [ResponseType(typeof(GradDTO))]
        [HttpPost]
        public IHttpActionResult Grad(GradDTO gradDTO)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            if (!db.Drzava.Where(d => d.PK_Drzava == gradDTO.DrzavaId).Any())
            {
                return BadRequest("Država ne postoji u bazi!");
            }

            var grad = Mapper.Map<GradDTO, Grad>(gradDTO);

            db.Grad.Add(grad);
            db.SaveChanges();
            gradDTO= Mapper.Map<Grad, GradDTO>(db.Grad.Include(d => d.Drzava).Where(g => g.PK_Grad == grad.PK_Grad).FirstOrDefault());

            return CreatedAtRoute("DefaultApi", new { id = grad.PK_Grad }, gradDTO);
        }

        // DELETE: api/Grad/5
        [ResponseType(typeof(Grad))]
        [HttpDelete]
        public IHttpActionResult DGrad(int id)
        {
            Grad grad = db.Grad.Find(id);
            if (grad == null)
            {
                return NotFound();
            }

            if(db.Hotel.Where(h=>h.FK_Hotel_Grad_PK==id).Any())
            {
                return BadRequest("U tom gradu ima hotela!");
            }

            if (db.Gost.Where(g=>g.FK_Gost_Grad_PK == id).Any())
            {
                return BadRequest("Postoje gosti iz tog grada!");
            }

            db.Grad.Remove(grad);
            db.SaveChanges();

            return Ok(grad);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool GradExists(int id)
        {
            return db.Grad.Count(e => e.PK_Grad == id) > 0;
        }
    }

    public class TestModel
    {
        public int Name { get; set; }
    }
}