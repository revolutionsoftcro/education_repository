﻿using AutoMapper;
using Hotel_2._0.DTO;
using Hotel_2._0.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Web.Http;
using System.Web.Http.Description;

namespace Hotel_2._0.Controllers.API
{
    [RoutePrefix("api/kosaricaproizvod")]
    public class KosaricaProizvodController : BaseController
    {
        private Hotel2Entities db = new Hotel2Entities();

        // GET: api/KosaricaProizvod
        [HttpGet]
        public IEnumerable<KosaricaProizvodDTO> KosaricaProizvod()
        {
            return db.KosaricaProizvod.ToList().Select(Mapper.Map<KosaricaProizvod, KosaricaProizvodDTO>);
        }

        // GET: api/KosaricaProizvod/5
        [ResponseType(typeof(KosaricaProizvodDTO))]
        [HttpGet]
        public IHttpActionResult KosaricaProizvod(int id)
        {
            KosaricaProizvod kosaricaProizvod = db.KosaricaProizvod.Where(p=>p.PK_KosaricaProizvod==id).Include(p=>p.UslugeProizvodi).FirstOrDefault();
            if (kosaricaProizvod == null)
            {
                return NotFound();
            }

            return Ok(Mapper.Map<KosaricaProizvod, KosaricaProizvodDTO>(kosaricaProizvod));
        }

        [ResponseType(typeof(void))]
        [HttpPut]
        [Route("izbrisi")]
        public IHttpActionResult Izbrisi([FromBody]int id)
        {


            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if(!db.Kosarica.Where(k=>k.Trenutna==true).Any())
            {
                return BadRequest("Ne postoji otvorena prodaja!");
            }

            if (!db.KosaricaProizvod.Where(p=>p.PK_KosaricaProizvod==id).Any())
            {
                return BadRequest("Ne postoji kosaricaProizvod sa tim id-om!");
            }

            KosaricaProizvod kosaricaProizvod = db.KosaricaProizvod.Where(p => p.PK_KosaricaProizvod == id).FirstOrDefault();
            kosaricaProizvod.Deleted = true;
            kosaricaProizvod.Datum_brisanja = DateTime.Now;

            db.Entry(kosaricaProizvod).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!KosaricaProizvodExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            var kosarica = db.Kosarica.Where(k => k.Trenutna == true).FirstOrDefault();
            kosarica.Iznos -= kosaricaProizvod.Iznos;
            db.Entry(kosarica).State = EntityState.Modified;
            db.SaveChanges();

            return StatusCode(HttpStatusCode.NoContent);
        }

        [Route("dodaj")]
        [ResponseType(typeof(KosaricaProizvodDTO))]
        [HttpPost]
        public IHttpActionResult DodajProizvod(KosaricaProizvodDTO kosaricaProizvodDTO)
        {
            if (!db.UslugeProizvodi.Where(p => p.PK_UslugeProizvodi == kosaricaProizvodDTO.ProizvodId).Any())
            {
                return BadRequest("Proizvod/usluga ne postoji u bazi!");
            }

            if (!db.Kosarica.Where(k => k.Trenutna == true).Any())
            {
                return BadRequest("Ne postoji otvorena prodaja! Morate prvo zapoćeti prodaju!");
            }

            var trenutna = db.Kosarica.Where(k => k.Trenutna == true).Select(k => k.PK_Kosarica).FirstOrDefault();

            var kosaricaProizvod = new KosaricaProizvod();
            var kosarica = db.Kosarica.Where(k => k.PK_Kosarica == trenutna).FirstOrDefault();
            var noviIznos = db.UslugeProizvodi.Where(p => p.PK_UslugeProizvodi == kosaricaProizvodDTO.ProizvodId).Select(p => p.CijenaPoMjeri).FirstOrDefault() * kosaricaProizvodDTO.Kolicina;

            if (db.KosaricaProizvod.Where(k => k.FK_KosaricaProizvod_Kosarica_PK == trenutna && k.FK_KosaricaProizvod_UslugeProizvodi_PK == kosaricaProizvodDTO.ProizvodId && k.Deleted == false).Any())
            {
                kosaricaProizvod = db.KosaricaProizvod.Where(k => k.FK_KosaricaProizvod_Kosarica_PK == trenutna && k.FK_KosaricaProizvod_UslugeProizvodi_PK == kosaricaProizvodDTO.ProizvodId && k.Deleted == false).FirstOrDefault();

                kosaricaProizvod.Kolicina += kosaricaProizvodDTO.Kolicina;
                kosaricaProizvod.Iznos += noviIznos;
                db.Entry(kosaricaProizvod).State = EntityState.Modified;
                kosarica.Iznos += noviIznos;
                db.Entry(kosarica).State = EntityState.Modified;
                db.SaveChanges();
                kosaricaProizvod = db.KosaricaProizvod.Where(k => k.PK_KosaricaProizvod == kosaricaProizvod.PK_KosaricaProizvod).Include(k => k.UslugeProizvodi).FirstOrDefault();

                return Content(HttpStatusCode.Created, Mapper.Map<KosaricaProizvod, KosaricaProizvodDTO>(kosaricaProizvod));
            }

            kosaricaProizvod = Mapper.Map<KosaricaProizvodDTO, KosaricaProizvod>(kosaricaProizvodDTO);
            kosaricaProizvod.FK_KosaricaProizvod_Kosarica_PK = db.Kosarica.Where(k => k.Trenutna == true).Select(k => k.PK_Kosarica).FirstOrDefault();
            kosaricaProizvod.Iznos = noviIznos;
            kosaricaProizvod.Deleted = false;
            kosaricaProizvod.Datum_izrade = DateTime.Now;

            db.KosaricaProizvod.Add(kosaricaProizvod);
            kosarica.Iznos += noviIznos;
            db.Entry(kosarica).State = EntityState.Modified;

            db.SaveChanges();
            kosaricaProizvod = db.KosaricaProizvod.Where(k => k.PK_KosaricaProizvod == kosaricaProizvod.PK_KosaricaProizvod).Include(k => k.UslugeProizvodi).FirstOrDefault();

            return Content(HttpStatusCode.Created, Mapper.Map<KosaricaProizvod, KosaricaProizvodDTO>(kosaricaProizvod));

        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool KosaricaProizvodExists(int id)
        {
            return db.KosaricaProizvod.Count(e => e.PK_KosaricaProizvod == id) > 0;
        }
    }
}