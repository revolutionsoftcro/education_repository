﻿using Hotel_2._0.Models;
using Hotel_2._0.ViewModels;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web.Mvc;

namespace Hotel_2._0.Controllers
{
    public class RezervacijaController : CustomController
    {
        public RezervacijaController() : base() { }
        public RezervacijaController(Hotel2Entities context) : base(context) { }
        public ActionResult Create()
        {
            var model = new RezervacijaViewModel();
            model.ListaGostiju = new SelectList(db.Gost.Include(g => g.Grad).OrderBy(g => g.Ime).ThenBy(g => g.Prezime), "PK_Gost", "ImePrezimeGrad");
            model.ListaHotela = new SelectList(db.Hotel.OrderBy(h => h.Naziv), "PK_Hotel", "Naziv");
            model.ListaTipova = new SelectList(db.TipSobe, "PK_TipSobe", "Naziv");
            model.TerminBool = true;
            model.OdlazakBool = true;
            model.TipSobeBool = true;
            return View(model);
        }

        [HttpPost]
        public ActionResult Create(RezervacijaViewModel model)
        {
            if (ModelState.IsValid)
            {
                model.ListaGostiju = new SelectList(db.Gost.Include(g=>g.Grad).OrderBy(g => g.Ime).ThenBy(g => g.Prezime), "PK_Gost", "ImePrezimeGrad");
                model.ListaHotela = new SelectList(db.Hotel.OrderBy(h => h.Naziv), "PK_Hotel", "Naziv");
                model.ListaTipova = new SelectList(db.TipSobe, "PK_TipSobe", "Naziv");
                model.TerminBool = true;
                model.OdlazakBool = true;
                model.TipSobeBool = true;

                if (model.Dolazak > model.Odlazak)
                {                   
                    model.OdlazakBool = false;
                    return View(model);
                }

                if (!(db.Soba.Where(s => s.FK_Soba_Hotel_PK == model.PK_Hotel && s.FK_Soba_TipSobe_PK == model.PK_TipSobe).Any()))
                {
                    model.TipSobeBool = false;
                    return View(model);
                }

                Soba soba = new Soba();

                if (db.Soba.Where(s => s.FK_Soba_Hotel_PK == model.PK_Hotel && !(s.Rezervacija.Any(r => !(r.Odlazak <= model.Dolazak || r.Dolazak >= model.Odlazak)))).Any())
                {
                    soba = db.Soba.Where(s => s.FK_Soba_Hotel_PK == model.PK_Hotel && !(s.Rezervacija.Any(r => !(r.Odlazak <= model.Dolazak || r.Dolazak >= model.Odlazak)))).FirstOrDefault();
                }
                else
                {
                    model.TerminBool = false;
                    return View(model);
                }

                Rezervacija rezervacija = new Rezervacija()
                {
                    FK_Rezervacija_Gost_PK = model.FK_Rezervacija_Gost_PK,
                    FK_Rezervacija_Soba_PK = soba.PK_Soba,
                    Dolazak = (DateTime)model.Dolazak,
                    Odlazak = (DateTime)model.Odlazak,
                    Popust = model.Popust,
                    Datum_izrade = DateTime.Now
                };

                db.Rezervacija.Add(rezervacija);
                db.SaveChanges();

                var racunSobe = new RacunController(db);
                racunSobe.NapraviSobaRn(rezervacija.PK_Rezervacija, soba.Cijena * (int)(rezervacija.Odlazak - rezervacija.Dolazak).TotalDays, rezervacija.Popust);
                return RedirectToAction("RezervacijeRacuni");

            }
            model.ListaGostiju = new SelectList(db.Gost.Include(g => g.Grad).OrderBy(g => g.Ime).ThenBy(g => g.Prezime), "PK_Gost", "ImePrezimeGrad");
            model.ListaHotela = new SelectList(db.Hotel.OrderBy(h => h.Naziv), "PK_Hotel", "Naziv");
            model.ListaTipova = new SelectList(db.TipSobe, "PK_TipSobe", "Naziv");
            model.TerminBool = true;
            model.OdlazakBool = true;
            model.TipSobeBool = true;
            return View(model);
        }


        public ActionResult RezervacijeRacuni()
        {
            List<RezervacijeRnViewModel> rezervacije = new List<RezervacijeRnViewModel>();
            foreach (var rezervacija in db.Rezervacija.Include(x => x.Gost).Include(x => x.Soba.Hotel).Include(x => x.Racun))
            {
                var rez = new RezervacijeRnViewModel();
                rez.PK_Rezervacija = rezervacija.PK_Rezervacija;
                rez.Ime = rezervacija.Gost.Ime;
                rez.Prezime = rezervacija.Gost.Prezime;
                rez.Hotel = rezervacija.Soba.Hotel.Naziv;
                rez.UkupnaSuma = 0;
                rez.ZaPlatiti = 0;
                foreach (var racun in rezervacija.Racun)
                {
                    if (racun.Soba == true)
                    {
                        rez.UkupnaSuma += racun.Iznos;
                        if (racun.isPaid == false)
                        {
                            rez.ZaPlatiti += racun.Iznos;
                        }
                    }
                    else
                    {
                        var racun1 = db.Racun.Include(r => r.StavkaRacun).Where(r => r.PK_Racun == racun.PK_Racun).FirstOrDefault();
                        foreach (var podracun in racun1.StavkaRacun)
                        {
                            rez.UkupnaSuma += podracun.Iznos;
                            if (racun.isPaid == false)
                            {
                                rez.ZaPlatiti += podracun.Iznos;
                            }
                        }
                    }
                }
                rezervacije.Add(rez);
            }
            return View(rezervacije);
        }

        public ActionResult NeplaceniRacuni()
        {
            List<RezervacijeRnViewModel> rezervacije = new List<RezervacijeRnViewModel>();
            foreach (var rezervacija in db.Rezervacija.Include(x => x.Gost).Include(x => x.Soba.Hotel).Include(x => x.Racun))
            {
                var rez = new RezervacijeRnViewModel();
                rez.PK_Rezervacija = rezervacija.PK_Rezervacija;
                rez.Ime = rezervacija.Gost.Ime;
                rez.Prezime = rezervacija.Gost.Prezime;
                rez.Hotel = rezervacija.Soba.Hotel.Naziv;
                rez.UkupnaSuma = 0;
                rez.ZaPlatiti = 0;
                foreach (var racun in rezervacija.Racun)
                {
                    if (racun.Soba == true)
                    {
                        rez.UkupnaSuma += racun.Iznos;
                        if (racun.isPaid == false)
                        {
                            rez.ZaPlatiti += racun.Iznos;
                        }
                    }
                    else
                    {
                        var racun1 = db.Racun.Include(r => r.StavkaRacun).Where(r => r.PK_Racun == racun.PK_Racun).FirstOrDefault();
                        foreach (var podracun in racun1.StavkaRacun)
                        {
                            rez.UkupnaSuma += podracun.Iznos;
                            if (racun.isPaid == false)
                            {
                                rez.ZaPlatiti += podracun.Iznos;
                            }
                        }
                    }
                }

                if (rez.ZaPlatiti != 0)
                {
                    rezervacije.Add(rez);
                }
            }
            return View("RezervacijeRacuni", rezervacije);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
