﻿var h_number = 1;
var h_url = 'http://localhost/API/api/hotel/';
var h_svojstva = ["Naziv", "BrojZvjezdica", "Telefon", "Email", "Grad", "Adresa", "Id"];
var h_svojstvaDisplay = ["Naziv", "Broj zvjezdica", "Telefon", "Email", "Grad", "Adresa"];
var h_formContent = '#HotelFormContent'
var h_selectGrad = '#selectGrad'
var h_form = "#entitetForm";
var h_Id = '#hoteli'
var h_napraviId = '#napraviHotel';
var h_Table = '<h1 class="entitet">Hoteli</h1><button id="napraviHotel" class="btn btn-primary pozicijaGumb" > Napravi novi</button ><table id="entitetTable">'

function loadHoteli() {
    $(sadrzajId).empty();
    $(sadrzajId).append(h_Table);
    napraviTablicu(h_url, h_svojstva, h_svojstvaDisplay, h_number);
}

$(document).on("click", h_Id, function () {
    loadHoteli();
});

$(document).on("click", h_napraviId, function () {
    $(sadrzajId).empty();
    $(sadrzajId).append('<h1 align="center"> Unos novog hotela:');
    $(sadrzajId).append('<form id="entitetForm">');


    var form = $(h_form);
    var formGroup, selectgroup;

    form.append('<div id="HotelFormContent" class="form-group text-center">');
    formGroup = $(h_formContent);
    formGroup.append('<label class="posebni-label">Naziv</label>');
    formGroup.append('<input class="kontrolaForme text-box single-line" type="text" value="" name="Naziv">');
    formGroup.append('<label class="posebni-label">Broj zvjezdica</label>');
    formGroup.append('<input class="kontrolaForme text-box single-line" type="text" value="" name="BrojZvjezdica">');
    formGroup.append('<label class="posebni-label">Telefon</label>');
    formGroup.append('<input class="kontrolaForme text-box single-line" type="text" value="" name="Telefon">');
    formGroup.append('<label class="posebni-label">Email</label>');
    formGroup.append('<input class="kontrolaForme text-box single-line" type="text" value="" name="Email">');
    formGroup.append('<label class="posebni-label">Grad</label>');
    formGroup.append('<select class="kontrolaForme" name="GradId" id="selectGrad">');
    selectgroup = $(h_selectGrad);
    selectgroup.append(gradoviOptions);
    formGroup.append('<label class="posebni-label">Adresa</label>');
    formGroup.append('<input class="kontrolaForme text-box single-line" type="text" value="" name="Adresa">');
    formGroup.append('<button id="HotelCreateButton" class="btn btn-primary mt-3 mr-3"> Unesi </button>')
});

$(document).on("click", "#HotelCreateButton", function () {
    event.preventDefault();
    Create(h_url, h_number);
});

$(document).on("click", ".buttonPromjeni1", function () {
    event.preventDefault();
    var id = $(this).parent().attr('id');
    $(sadrzajId).empty();
    $(sadrzajId).append('<h1 align="center"> Promjena hotela:');
    $(sadrzajId).append('<form id="entitetForm">');


    var form = $(h_form);
    var formGroup, selectgroup;

    form.append('<div id="HotelFormContent" class="form-group text-center">');
    formGroup = $(h_formContent);
    formGroup.append('<input id="hotelIdInput" name="Id" hidden type="number" />')
    var hotelId = $("#hotelIdInput")
    hotelId.val(id);
    formGroup.append('<label class="posebni-label">Naziv</label>');
    formGroup.append('<input id="hotelNazivInput" class="kontrolaForme text-box single-line" type="text" value="" name="Naziv">');
    formGroup.append('<label class="posebni-label">Broj zvjezdica</label>');
    formGroup.append('<input id="hotelZvjezdiceInput" class="kontrolaForme text-box single-line" type="text" value="" name="BrojZvjezdica">');
    formGroup.append('<label class="posebni-label">Telefon</label>');
    formGroup.append('<input id="hotelTelefonInput" class="kontrolaForme text-box single-line" type="text" value="" name="Telefon">');
    formGroup.append('<label class="posebni-label">Email</label>');
    formGroup.append('<input id="hotelEmailInput" class="kontrolaForme text-box single-line" type="text" value="" name="Email">');
    formGroup.append('<label class="posebni-label">Grad</label>');
    formGroup.append('<select class="kontrolaForme" name="GradId" id="selectGrad">');
    selectgroup = $(h_selectGrad);
    selectgroup.append(gradoviOptions);
    formGroup.append('<label class="posebni-label">Adresa</label>');
    formGroup.append('<input id="hotelAdresaInput" class="kontrolaForme text-box single-line" type="text" value="" name="Adresa">');
    formGroup.append('<button id="HotelPromjeniButton" class="btn btn-primary mt-3 mr-3"> Promjeni </button>')

    $.ajax({
        url: h_url + id,
        method: "GET",
        crossDomain: true
    }).done(function (data) {
        $("#hotelNazivInput").val(data.Naziv);
        $("#hotelZvjezdiceInput").val(data.BrojZvjezdica);
        $("#hotelTelefonInput").val(data.Telefon);
        $("#hotelEmailInput").val(data.Email);
        $("#selectGrad option:contains('" + data.Grad + "')").prop("selected", true);
        $("#hotelAdresaInput").val(data.Adresa);
    });
});

$(document).on("click", "#HotelPromjeniButton", function () {
    event.preventDefault();
    var form = $(h_form);
    let data = form.serializeArray().reduce(function (obj, item) {
        obj[item.name] = item.value;
        return obj;
    }, {});

    Edit(h_url, data.Id, h_number);
});

$(document).on("click", ".buttonObrisi1", function () {
    event.preventDefault();
    var id = $(this).parent().attr('id');
    $.ajax({
        url: h_url + id,
        method: "GET",
        crossDomain: true
    }).done(function (data) {
        var r = confirm(`Jeste li sigurni da želite obrisati ovaj hotel? \n Naziv: ${data.Naziv} \n Grad: ${data.Grad}`);
        if (r == true) {
            Delete(h_url, id, h_number);
        }
    });
});