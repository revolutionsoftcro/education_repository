﻿var gumbiOdabir1 = '<button id="administracija" class="btn btn-default" class="pozicijaGumb">Administracija</button>' + '<button id="recepcija" class="btn btn-primary" class="pozicijaGumb"> Recepcija</button>';
var gumbiOdabir2 = '<button id="administracija" class="btn btn-primary" class="pozicijaGumb">Administracija</button>' + '<button id="recepcija" class="btn btn-default" class="pozicijaGumb"> Recepcija</button>';
var kazalo1 = '<li id="odabirGumbi"></li><li><a href="#" id="rezervacije"><i class="zmdi zmdi-widgets"></i> Rezervacije</a></li><li><a href="#" id="gosti"><i class="zmdi zmdi-calendar"></i> Gosti</a></li>';
var kazalo2 = '<li id="odabirGumbi"></li><li><a href="#" id="hoteli"><i class="zmdi zmdi-widgets"></i> Hoteli</a></li><li><a href="#" id="tipoviSoba"><i class="zmdi zmdi-calendar"></i> Tipovi Soba</a></li><li><a href="#" id="sobe"><i class="zmdi zmdi-info-outline"></i> Sobe' +
              '</a></li><li><a href="#" id="drzave"><i class="zmdi zmdi-view-dashboard"></i> Države</a></li><li><a href="#" id="gradovi"><i class="zmdi zmdi-link"></i> Gradovi</a></li>';
var stranicaNaslovna = '<img src="images/hotel.gif" class="image" /><br><h1 class="naslovna">Dobro došli u aplikaciju Omega hoteli!</h1>';
var kazaloId = '#kazalo';
var gumbiOdabirId = '#odabirGumbi';
var sadrzajId = '#sadrzaj';
var naslovnaId = '#naslovna';
var administracijaId = '#administracija';
var recepcijaId = '#recepcija';
var tableId = '#entitetTable';
var formId = '#entitetForm';
var gradoviOptions = "";
var drzaveOptions = "";
var hoteliOptions = "";
var tipoviSobeOptions = "";
var gostiOptions = "";
var sobeOptions = "";
var rezervacijeOptions = "";

var gr_url = 'http://localhost/API/api/grad/';
//var g_url = 'http://localhost/API/api/gost/';
var t_url = 'http://localhost/API/api/tipsobe/';


function loadNaslovna() {
    $(sadrzajId).empty();
    $(sadrzajId).append(stranicaNaslovna);
}

function pageLoad(i) {

    $.ajax({
        url: "http://localhost/API/api/drzava",
        method: "GET",
        crossDomain: true
    }).done(function (data) {
        drzaveOptions = '<option value=""></option>';
        data.forEach(function (item) {
            drzaveOptions += ('<option value="' + item.PK_Drzava + '">' + item.Naziv + '</option>');
        });
    });

    $.ajax({
        url: "http://localhost/API/api/grad",
        method: "GET",
        crossDomain: true
    }).done(function (data) {
        gradoviOptions = '<option value=""></option>';
        data.forEach(function (item) {
            gradoviOptions += ('<option value="' + item.Id + '">' + item.Grad + '</option>');
        });
    });


    $.ajax({
        url: "http://localhost/API/api/hotel",
        method: "GET",
        crossDomain: true
    }).done(function (data) {
        hoteliOptions = '<option value=""></option>';
        data.forEach(function (item) {
            hoteliOptions += ('<option value="' + item.Id + '">' + item.Grad + ' | ' + item.Naziv + '</option>');
        });
    });

    $.ajax({
        url: "http://localhost/API/api/tipsobe",
        method: "GET",
        crossDomain: true
    }).done(function (data) {
        tipoviSobeOptions = '<option value=""></option>';
        data.forEach(function (item) {
            tipoviSobeOptions += ('<option value="' + item.PK_TipSobe + '">' + item.Naziv + '</option>');
        });
    });

    $.ajax({
        url: "http://localhost/API/api/soba",
        method: "GET",
        crossDomain: true
    }).done(function (data) {
        sobeOptions = '<option value=""></option>';
        data.forEach(function (item) {
            sobeOptions += ('<option value="' + item.Broj + '">' + item.Hotel + ' | ' + item.TipSobe + ' | ' + item.Broj + '</option>');
        });
    });

    $.ajax({
        url: "http://localhost/API/api/gost",
        method: "GET",
        crossDomain: true
    }).done(function (data) {
        gostiOptions = '<option value=""></option>';
        data.forEach(function (item) {
            gostiOptions += ('<option value="' + item.Id + '">' + item.Ime + ' ' + item.Prezime + '</option>');
        });
    });

    rezervacijeOptions = '<option value=""></option>';
    $.ajax({
        url: "http://localhost/API/api/rezervacija",
        method: "GET",
        crossDomain: true
    }).done(function (data) {
        data.forEach(function (item) {
            $.ajax({
                url: "http://localhost/API/api/hotel/" + item.HotelId,
                method: "GET",
                crossDomain: true
            }).done(function (data2) {
                $.ajax({
                    url: "http://localhost/API/api/gost/" + item.GostId,
                    method: "GET",
                    crossDomain: true
                }).done(function (data3) {
                    rezervacijeOptions += ('<option value="' + item.Id + '">' + data3.Ime + ' ' + data3.Prezime + ' | ' + data2.Naziv + ' | ' + item.BrojSobe + '</option>');
                })
            })
        })
    });


    if (i === 0) {
        $(kazaloId).empty();
        $(kazaloId).append(kazalo1);
        $(gumbiOdabirId).append(gumbiOdabir1);
        loadNaslovna();
    }
    else if (i === 1) {
        $(kazaloId).empty();
        $(kazaloId).append(kazalo2);
        $(gumbiOdabirId).append(gumbiOdabir2);
        loadHoteli();
    }
    else if (i === 2) {
        $(kazaloId).empty();
        $(kazaloId).append(kazalo1);
        $(gumbiOdabirId).append(gumbiOdabir1);
        loadRezervacije();
    }
    else if (i === 3) {
        $(kazaloId).empty();
        $(kazaloId).append(kazalo1);
        $(gumbiOdabirId).append(gumbiOdabir1);
        loadGosti();
    }
    else if (i === 4) {
        $(kazaloId).empty();
        $(kazaloId).append(kazalo2);
        $(gumbiOdabirId).append(gumbiOdabir2);
        loadTipoviSoba();
    }
    else if (i === 5) {
        $(kazaloId).empty();
        $(kazaloId).append(kazalo2);
        $(gumbiOdabirId).append(gumbiOdabir2);
        loadSobe();
    }
    else if (i === 6) {
        $(kazaloId).empty();
        $(kazaloId).append(kazalo2);
        $(gumbiOdabirId).append(gumbiOdabir2);
        loadGradovi();
    }
    else if (i === 7) {
        $(kazaloId).empty();
        $(kazaloId).append(kazalo2);
        $(gumbiOdabirId).append(gumbiOdabir2);
        loadDrzave();
    }
}

pageLoad(0);

$(document).on("click", administracijaId, function () {
    $(kazaloId).empty();
    $(kazaloId).append(kazalo2);
    $(gumbiOdabirId).append(gumbiOdabir2);
    loadNaslovna();
});

$(document).on("click", recepcijaId, function ()  {
    $(kazaloId).empty();
    $(kazaloId).append(kazalo1);
    $(gumbiOdabirId).append(gumbiOdabir1);
    loadNaslovna();
});

$(document).on("click", naslovnaId, function () {
    loadNaslovna();
}); 

function napraviTablicu(url, svojstva, svojstvaDisplay, ind) {
    $.ajax({
        url: url,
        method: "GET",
        crossDomain: true
    }).done(function (data) {  
        var table = $(tableId);
        table.append('<thead id="tableHeader">');
        var thead = $('#tableHeader');
        var headerRow = $('<tr class="table100-head">');
        for (var i = 0; i < svojstvaDisplay.length; i++) {
            headerRow.append('<th>' + svojstvaDisplay[i] + '</th>');
        }
        headerRow.append("<th>");
        thead.append(headerRow);

        table.append('<tbody id="tableBody">');
        var tbody = $('#tableBody');
        data.forEach(function (item, i) {
            if (ind === 2) {
                $.ajax({
                    url: h_url + item.HotelId,
                    method: "GET",
                    crossDomain: true
                }).done(function (data1) {
                    $.ajax({
                        url: g_url + item.GostId,
                        method: "GET",
                        crossDomain: true
                    }).done(function (data2) {
                        $.ajax({
                            url: t_url + item.TipSobeId,
                            method: "GET",
                            crossDomain: true
                        }).done(function (data3) {
                            item.Ime = data2.Ime;
                            item.Prezime = data2.Prezime;
                            item.Hotel = data1.Naziv;
                            item.TipSobe = data3.Naziv;
                            var tr = $("<tr>");
                            var i;
                            for (i = 0; i < svojstva.length - 1; i++) {
                                tr.append("<td>" + item[svojstva[i]] + "</td > ");
                            }
                            tr.append(`<td id="${item[svojstva[i]]}"><button class="buttonObrisi${ind} buttonObrisi">Obriši</button></td >`)
                            tbody.append(tr);
                        });
                    });
                });
            } else if (ind === 3) {
                $.ajax({
                    url: gr_url + item.GradId,
                    method: "GET",
                    crossDomain: true
                }).done(function (data1) {
                    item.Grad = data1.Grad;
                    var tr = $("<tr>");
                    var i;
                    for (i = 0; i < svojstva.length - 1; i++) {
                        tr.append("<td>" + item[svojstva[i]] + "</td > ");
                    }
                    tr.append(`<td id="${item[svojstva[i]]}"><button class= "buttonPromjeni${ind} buttonPromjeni"> Promjeni</button><button class="buttonObrisi${ind} buttonObrisi">Obriši</button></td >`)
                    tbody.append(tr);
                });
            } else if (ind == 4) {
                var tr = $("<tr>");
                var i;
                for (i = 0; i < svojstva.length - 1; i++) {
                    if (svojstva[i] != "Naziv" && item[svojstva[i]]) {
                        tr.append("<td> Ima </td > ");
                    } else if (svojstva[i] != "Naziv" && !item[svojstva[i]]) {
                        tr.append("<td> Nema </td > ");
                    } else {
                        tr.append("<td>" + item[svojstva[i]] + "</td > ");
                    }                    
                }
                tr.append(`<td id="${item[svojstva[i]]}"><button class= "buttonPromjeni${ind} buttonPromjeni"> Promjeni</button><button class="buttonObrisi${ind} buttonObrisi">Obriši</button></td >`)
                tbody.append(tr); 
            } else {
                var tr = $("<tr>");
                var i;
                for (i = 0; i < svojstva.length - 1; i++) {
                    tr.append("<td>" + item[svojstva[i]] + "</td > ");
                }
                tr.append(`<td id="${item[svojstva[i]]}"><button class= "buttonPromjeni${ind} buttonPromjeni"> Promjeni</button><button class="buttonObrisi${ind} buttonObrisi">Obriši</button></td >`)
                tbody.append(tr); 
            }       
        });
    }).fail(function () {
        alert("Objekti se ne mogu dohvatiti!");
        pageLoad(0);
    });
}

function Create(url, i) {
    var form = $(formId);
    var data = form.serializeArray().reduce(function (obj, item) {
        obj[item.name] = item.value;
        return obj;
    }, {});
    $.ajax({
        url: url,
        method: "POST",
        data: data,
        crossDomain: true
    }).done(function (data) {
        pageLoad(i);
    }).fail(function (jqXHR, textStatus, errorThrown) {
        alert("Objekt se ne može napraviti! Poruka: " + jqXHR.responseJSON.Message);
        pageLoad(i);
    });
}

function Edit(url, id, i) {
    var form = $(formId);
    var data = form.serializeArray().reduce(function (obj, item) {
        obj[item.name] = item.value;
        return obj;
    }, {});
    $.ajax({
        url: url + id,
        method: "PUT",
        data: data,
        crossDomain: true
    }).done(function (data) {
        pageLoad(i);
    }).fail(function (jqXHR, textStatus, errorThrown) {
        alert("Objekt se ne može promjeniti! Poruka: " + jqXHR.responseJSON.Message);
        pageLoad(i);
    });
}

function Delete(url, id, i) {
    $.ajax({
        url: url + id,
        method: "DELETE",
        crossDomain: true
    }).done(function (data) {
        pageLoad(i);
    }).fail(function (jqXHR, textStatus, errorThrown) {
        alert("Objekt se ne može obrisati! Poruka: " + jqXHR.responseJSON.Message);
        pageLoad(i);
    });
}