﻿var d_number = 7;
var d_url = 'http://localhost/API/api/drzava/';
var d_svojstva = ["Naziv", "PK_Drzava"];
var d_svojstvaDisplay = ["Naziv drzave"];
var d_formContent = '#DrzavaFormContent'
var d_form = "#entitetForm";
var d_Id = '#drzave'
var d_napraviId = '#napraviDrzava';
var d_Table = '<h1 class="entitet">Države</h1><button id="napraviDrzava" class="btn btn-primary pozicijaGumb" > Napravi novu</button ><table id="entitetTable">'

function loadDrzave() {
    $(sadrzajId).empty();
    $(sadrzajId).append(d_Table);
    napraviTablicu(d_url, d_svojstva, d_svojstvaDisplay, d_number);
}

$(document).on("click", d_Id, function () {
    loadDrzave();
});

$(document).on("click", d_napraviId, function () {
    $(sadrzajId).empty();
    $(sadrzajId).append('<h1 align="center"> Unos nove države:');
    $(sadrzajId).append('<form id="entitetForm">');


    var form = $(d_form);
    var formGroup, selectgroup;

    form.append('<div id="DrzavaFormContent" class="form-group text-center">');
    formGroup = $(d_formContent);
    formGroup.append('<label class="posebni-label">Naziv</label>');
    formGroup.append('<input class="kontrolaForme text-box single-line" type="text" value="" name="Naziv">');
    formGroup.append('<button id="DrzavaCreateButton" class="btn btn-primary mt-3 mr-3"> Unesi </button>')
});

$(document).on("click", "#DrzavaCreateButton", function () {
    event.preventDefault();
    Create(d_url, d_number);
});

$(document).on("click", ".buttonPromjeni7", function () {
    event.preventDefault();
    var id = $(this).parent().attr('id');
    $(sadrzajId).empty();
    $(sadrzajId).append('<h1 align="center"> Promjena države:');
    $(sadrzajId).append('<form id="entitetForm">');

    var form = $(d_form);
    var formGroup, selectgroup;

    form.append('<div id="DrzavaFormContent" class="form-group text-center">');
    formGroup = $(d_formContent);
    formGroup.append('<input id="drzavaIdInput" name="PK_Drzava" hidden type="number" />')
    var drzavaId = $("#drzavaIdInput")
    drzavaId.val(id);
    formGroup.append('<label class="posebni-label">Naziv</label>');
    formGroup.append('<input id="drzavaNazivInput" class="kontrolaForme text-box single-line" type="text" value="" name="Naziv">');
    formGroup.append('<button id="DrzavaPromjeniButton" class="btn btn-primary mt-3 mr-3"> Promjeni </button>')

    $.ajax({
        url: d_url + id,
        method: "GET",
        crossDomain: true
    }).done(function (data) {
        $("#drzavaNazivInput").val(data.Naziv);
    });
});

$(document).on("click", "#DrzavaPromjeniButton", function () {
    event.preventDefault();
    var form = $(gr_form);
    let data = form.serializeArray().reduce(function (obj, item) {
        obj[item.name] = item.value;
        return obj;
    }, {});

    Edit(d_url, data.PK_Drzava, d_number);
});

$(document).on("click", ".buttonObrisi7", function () {
    event.preventDefault();
    var id = $(this).parent().attr('id');
    $.ajax({
        url: d_url + id,
        method: "GET",
        crossDomain: true
    }).done(function (data) {
        var r = confirm(`Jeste li sigurni da želite obrisati ovu državu? \n Država: ${data.Naziv}`);
        if (r == true) {
            Delete(d_url, id, d_number);
        }
    });
});