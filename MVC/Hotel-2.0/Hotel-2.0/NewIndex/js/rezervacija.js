﻿var r_number = 2;
var r_url = 'http://localhost/API/api/rezervacija/';
var r_svojstva = ["Ime", "Prezime", "Hotel", "TipSobe", "BrojSobe",  "Dolazak", "Odlazak", "Popust", "Id"];
var r_svojstvaDisplay = ["Ime", "Prezime", "Hotel", "Tip sobe", "Broj sobe", "Dolazak", "Odlazak", "Popust"];
var r_formContent = '#RezervacijaFormContent';
var r_selectGrad = '#selectGrad';
var r_form = "#entitetForm";
var r_Id = '#rezervacije';
var r_napraviId = '#napraviRezervaciju';
var r_Table = '<h1 class="entitet">Rezervacije</h1><button id="napraviRezervaciju" class="btn btn-primary pozicijaGumb" > Napravi novu</button ><table id="entitetTable">';

function loadRezervacije() {
    $(sadrzajId).empty();
    $(sadrzajId).append(r_Table);
    napraviTablicu(r_url, r_svojstva, r_svojstvaDisplay, r_number);
}

$(document).on("click", r_Id, function () {
    loadRezervacije();
});

$(document).on("click", r_napraviId, function () {
    $(sadrzajId).empty();
    $(sadrzajId).append('<h1 align="center"> Unos nove rezervacije:');
    $(sadrzajId).append('<form id="entitetForm">');


    var form = $(h_form);
    var formGroup, selectgroup;

    form.append('<div id="RezervacijaFormContent" class="form-group text-center">');
    formGroup = $(r_formContent);
    formGroup.append('<label class="posebni-label">Gost</label>');
    formGroup.append('<select class="kontrolaForme" name="GostId" id="selectGost">');
    selectgroup = $("#selectGost");
    selectgroup.append(gostiOptions);
    formGroup.append('<label class="posebni-label">Hotel</label>');
    formGroup.append('<select class="kontrolaForme" name="HotelId" id="selectHotel">');
    selectgroup = $("#selectHotel");
    selectgroup.append(hoteliOptions);
    formGroup.append('<label class="posebni-label">Tip</label>');
    formGroup.append('<select class="kontrolaForme" name="TipSobeId" id="selectTip">');
    selectgroup = $("#selectTip");
    selectgroup.append(tipoviSobeOptions);
    formGroup.append('<label class="posebni-label">Dolazak</label>');
    formGroup.append("<input class='kontrolaForme text-box single-line' type='date' value='' name='Dolazak'>");
    formGroup.append('<label class="posebni-label">Odlazak</label>');
    formGroup.append("<input class='kontrolaForme text-box single-line' type='date' value='' name='Odlazak'>");
    formGroup.append('<label class="posebni-label">Popust</label>');
    formGroup.append("<input class='kontrolaForme text-box single-line' type='number' step='0.01' value='' name='Popust'>");

    formGroup.append('<button id="RezervacijaCreateButton" class="btn btn-primary"> Unesi </button>')
});

$(document).on("click", "#RezervacijaCreateButton", function () {
    event.preventDefault();
    Create(r_url, r_number);
});

$(document).on("click", ".buttonObrisi2", function () {
    event.preventDefault();
    var id = $(this).parent().attr('id');
    $.ajax({
        url: r_url + id,
        method: "GET",
        crossDomain: true
    }).done(function (data) {
        $.ajax({
            url: h_url + data.HotelId,
            method: "GET",
            crossDomain: true
        }).done(function (data1) {
            $.ajax({
                url: g_url + data.GostId,
                method: "GET",
                crossDomain: true
            }).done(function (data2) {
                var r = confirm(`Jeste li sigurni da želite obrisati ovu Rezervaciju? \n Ime i prezime: ${data2.Ime} ${data2.Prezime} \n Hotel: ${data1.Naziv} \n Dolazak: ${data.Dolazak} \n Odlazak: ${data.Odlazak}`);
                if (r == true) {
                    Delete(r_url, id, r_number);
                }
            });
        });
    });
});