﻿var t_number = 4;
var t_url = 'http://localhost/API/api/tipsobe/';
var t_svojstva = ["Naziv", "Televizor", "Telefon", "Balkon", "WIFI", "PK_TipSobe"];
var t_svojstvaDisplay = ["Naziv", "Televizor", "Telefon", "Balkon", "WIFI"];
var t_formContent = '#TipSobeFormContent'
var t_selectBool = '<option value=true>Ima</option><option value=false>Nema</option>'
var t_form = '#entitetForm';
var t_Id = '#tipoviSoba'
var t_napraviId = '#napraviTipSobe';
var t_Table = '<h1 class="entitet">Tipovi soba</h1><button id="napraviTipSobe" class="btn btn-primary pozicijaGumb" > Napravi novi</button ><table id="entitetTable">'

function loadTipoviSoba() {
    $(sadrzajId).empty();
    $(sadrzajId).append(t_Table);
    napraviTablicu(t_url, t_svojstva, t_svojstvaDisplay, t_number);
}

$(document).on("click", t_Id, function () {
    loadTipoviSoba();
});

$(document).on("click", t_napraviId, function () {
    $(sadrzajId).empty();
    $(sadrzajId).append('<h1 align="center"> Unos novog tipa sobe:');
    $(sadrzajId).append('<form id="entitetForm">');


    var form = $(h_form);
    var formGroup, selectgroup;

    form.append('<div id="TipSobeFormContent" class="form-group text-center">');
    formGroup = $(t_formContent);
    formGroup.append('<label class="posebni-label">Naziv</label>');
    formGroup.append("<input class='kontrolaForme text-box single-line' type='text' value='' name='Naziv'>");
    formGroup.append('<label class="posebni-label">Televizor</label>');
    formGroup.append('<select class="kontrolaForme" name="Televizor" id="selectTelevizor">');
    selectgroup = $("#selectTelevizor");
    selectgroup.append(t_selectBool);
    formGroup.append('<label class="posebni-label">Telefon</label>');
    formGroup.append('<select class="kontrolaForme" name="Telefon" id="selectTelefon">');
    selectgroup = $("#selectTelefon");
    selectgroup.append(t_selectBool);
    formGroup.append('<label class="posebni-label">Balkon</label>');
    formGroup.append('<select class="kontrolaForme" name="Balkon" id="selectBalkon">');
    selectgroup = $("#selectBalkon");
    selectgroup.append(t_selectBool);
    formGroup.append('<label class="posebni-label">WIFI</label>');
    formGroup.append('<select class="kontrolaForme" name="WIFI" id="selectWIFI">');
    selectgroup = $("#selectWIFI");
    selectgroup.append(t_selectBool);
    formGroup.append('<button id="TipSobeCreateButton" class="btn btn-primary"> Unesi </button>')
});

$(document).on("click", "#TipSobeCreateButton", function () {
    event.preventDefault();
    Create(t_url, t_number);
});

$(document).on("click", ".buttonPromjeni4", function () {
    event.preventDefault();
    var id = $(this).parent().attr('id');
    $(sadrzajId).empty();
    $(sadrzajId).append('<h1 align="center"> Promjena tipa sobe:');
    $(sadrzajId).append('<form id="entitetForm">');


    var form = $(t_form);
    var formGroup, selectgroup;

    form.append('<div id="TipSobeFormContent" class="form-group text-center">');
    formGroup = $(t_formContent);
    formGroup.append('<input id="tipSobeIdInput" name="PK_TipSobe" hidden type="number" />')
    var tipSobeId = $("#tipSobeIdInput")
    tipSobeId.val(id);
    formGroup.append('<label class="posebni-label">Naziv</label>');
    formGroup.append('<input id="tipSobeNazivInput" class="kontrolaForme text-box single-line" type="text" value="" name="Naziv">');
    formGroup.append('<label class="posebni-label">Televizor</label>');
    formGroup.append('<select  class="kontrolaForme" name="Televizor" id="selectTelevizor">');
    selectgroup = $("#selectTelevizor");
    selectgroup.append(t_selectBool);
    formGroup.append('<label class="posebni-label">Telefon</label>');
    formGroup.append('<select class="kontrolaForme" name="Telefon" id="selectTelefon">');
    selectgroup = $("#selectTelefon");
    selectgroup.append(t_selectBool);
    formGroup.append('<label class="posebni-label">Balkon</label>');
    formGroup.append('<select class="kontrolaForme" name="Balkon" id="selectBalkon">');
    selectgroup = $("#selectBalkon");
    selectgroup.append(t_selectBool);
    formGroup.append('<label class="posebni-label">WIFI</label>');
    formGroup.append('<select class="kontrolaForme" name="WIFI" id="selectWIFI">');
    selectgroup = $("#selectWIFI");
    selectgroup.append(t_selectBool);
    formGroup.append('<button id="TipSobePromjeniButton" class="btn btn-primary"> Promjeni </button>')

    $.ajax({
        url: t_url + id,
        method: "GET",
        crossDomain: true
    }).done(function (data) {
        $("#tipSobeNazivInput").val(data.Naziv);
        var televizorBool = data.Televizor ? "Ima" : "Nema";
        var telefonBool = data.Telefon ? "Ima" : "Nema";
        var balkonBool = data.Balkon ? "Ima" : "Nema";
        var WIFIBool = data.WIFI ? "Ima" : "Nema";
        $("#selectTelevizor option:contains('" + televizorBool + "')").prop("selected", true);
        $("#selectTelefon option:contains('" + telefonBool + "')").prop("selected", true);
        $("#selectBalkon option:contains('" + balkonBool + "')").prop("selected", true);
        $("#selectWIFI option:contains('" + WIFIBool + "')").prop("selected", true);
    });
});

$(document).on("click", "#TipSobePromjeniButton", function () {
    event.preventDefault();
    var form = $(t_form);
    let data = form.serializeArray().reduce(function (obj, item) {
        obj[item.name] = item.value;
        return obj;
    }, {});

    Edit(t_url, data.PK_TipSobe, t_number);
});

$(document).on("click", ".buttonObrisi4", function () {
    event.preventDefault();
    var id = $(this).parent().attr('id');
    $.ajax({
        url: t_url + id,
        method: "GET",
        crossDomain: true
    }).done(function (data) {
        var r = confirm(`Jeste li sigurni da želite obrisati ovaj tip sobe? \n Naziv: ${data.Naziv}`);
        if (r == true) {
            Delete(t_url, id, t_number);
        }
    });
});