﻿var s_number = 5;
var s_url = 'http://localhost/API/api/soba/';
var s_svojstva = ["Broj", "TipSobe", "Hotel", "Cijena", "Broj"];
var s_svojstvaDisplay = ["Broj", "Tip sobe", "Hotel", "Cijena"];
var s_formContent = '#SobaFormContent'
var s_form = "#entitetForm";
var s_Id = '#sobe'
var s_napraviId = '#napraviSoba';
var s_Table = '<h1 class="entitet">Sobe</h1><button id="napraviSoba" class="btn btn-primary pozicijaGumb" > Napravi novu</button ><table id="entitetTable">'

function loadSobe() {
    $(sadrzajId).empty();
    $(sadrzajId).append(s_Table);
    napraviTablicu(s_url, s_svojstva, s_svojstvaDisplay, s_number);
}

$(document).on("click", s_Id, function () {
    loadSobe();
});

$(document).on("click", s_napraviId, function () {
    $(sadrzajId).empty();
    $(sadrzajId).append('<h1 align="center"> Unos nove sobe:');
    $(sadrzajId).append('<form id="entitetForm">');


    var form = $(s_form);
    var formGroup, selectgroup;

    form.append('<div id="SobaFormContent" class="form-group text-center">');
    formGroup = $(s_formContent);
    formGroup.append('<labe class="posebni-label"l>Hotel</label>');
    formGroup.append('<select  class="kontrolaForme" name="HotelId" id="selectHotel">');
    selectgroup = $("#selectHotel");
    selectgroup.append(hoteliOptions);
    formGroup.append('<label class="posebni-label">Tip</label>');
    formGroup.append('<select class="kontrolaForme" name="TipId" id="selectTip">');
    selectgroup = $("#selectTip");
    selectgroup.append(tipoviSobeOptions);
    formGroup.append('<label class="posebni-label">Cijena</label>');
    formGroup.append("<input class='kontrolaForme text-box single-line' type='text' value='' name='Cijena'>");
    formGroup.append('<button id="SobaCreateButton" class="btn btn-primary mt-3 mr-3"> Unesi </button>')
});

$(document).on("click", "#SobaCreateButton", function () {
    event.preventDefault();
    Create(s_url, s_number);
});

$(document).on("click", ".buttonPromjeni5", function () {
    event.preventDefault();
    var id = $(this).parent().attr('id');
    $(sadrzajId).empty();
    $(sadrzajId).append('<h1 align="center"> Promjena sobe:');
    $(sadrzajId).append('<form id="entitetForm">');


    var form = $(s_form);
    var formGroup, selectgroup;

    form.append('<div id="SobaFormContent" class="form-group text-center">');
    formGroup = $(s_formContent);
    formGroup.append('<input id="sobaIdInput" name="Broj" hidden type="number" />')
    var sobaId = $("#sobaIdInput")
    sobaId.val(id);
    formGroup.append('<labe class="posebni-label"l>Hotel</label>');
    formGroup.append('<select  class="kontrolaForme" name="HotelId" id="selectHotel">');
    selectgroup = $("#selectHotel");
    selectgroup.append(hoteliOptions);
    formGroup.append('<label class="posebni-label">Tip</label>');
    formGroup.append('<select class="kontrolaForme" name="TipId" id="selectTip">');
    selectgroup = $("#selectTip");
    selectgroup.append(tipoviSobeOptions);
    formGroup.append('<label class="posebni-label">Cijena</label>');
    formGroup.append('<input id="sobaCijenaInput" class="kontrolaForme text-box single-line" type="text" value="" name="Cijena">');
    formGroup.append('<button id="SobaPromjeniButton" class="btn btn-primary mt-3 mr-3"> Promjeni </button>')

    $.ajax({
        url: s_url + id,
        method: "GET",
        crossDomain: true
    }).done(function (data) {
        $("#selectHotel option:contains('" + data.Hotel + "')").prop("selected", true);
        $("#selectTip option:contains('" + data.TipSobe + "')").prop("selected", true);
        $("#sobaCijenaInput").val(data.Cijena);
    });
});

$(document).on("click", "#SobaPromjeniButton", function () {
    event.preventDefault();
    var form = $(s_form);
    let data = form.serializeArray().reduce(function (obj, item) {
        obj[item.name] = item.value;
        return obj;
    }, {});

    Edit(s_url, data.Broj, s_number);
});

$(document).on("click", ".buttonObrisi5", function () {
    event.preventDefault();
    var id = $(this).parent().attr('id');
    $.ajax({
        url: s_url + id,
        method: "GET",
        crossDomain: true
    }).done(function (data) {
        var r = confirm(`Jeste li sigurni da želite obrisati ovu sobu? \n Broj: ${data.Broj} \n Tip sobe: ${data.TipSobe} \n Hotel: ${data.Hotel}` );
        if (r == true) {
            Delete(s_url, id, s_number);
        }
    });
});