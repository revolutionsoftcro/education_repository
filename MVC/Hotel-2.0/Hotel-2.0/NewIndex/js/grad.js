﻿var gr_number = 6;
var gr_url = 'http://localhost/API/api/grad/';
var gr_svojstva = ["Grad", "Drzava", "Id"];
var gr_svojstvaDisplay = ["Naziv grada", "Država"];
var gr_formContent = '#GradFormContent'
var gr_form = "#entitetForm";
var gr_Id = '#gradovi'
var gr_napraviId = '#napraviGrad';
var gr_Table = '<h1 class="entitet">Gradovi</h1><button id="napraviGrad" class="btn btn-primary pozicijaGumb" > Napravi novi</button ><table id="entitetTable">'

function loadGradovi() {
    $(sadrzajId).empty();
    $(sadrzajId).append(gr_Table);
    napraviTablicu(gr_url, gr_svojstva, gr_svojstvaDisplay, gr_number);
}

$(document).on("click", gr_Id, function () {
    loadGradovi();
});

$(document).on("click", gr_napraviId, function () {
    $(sadrzajId).empty();
    $(sadrzajId).append('<h1 align="center"> Unos novog grada:');
    $(sadrzajId).append('<form id="entitetForm">');


    var form = $(gr_form);
    var formGroup, selectgroup;

    form.append('<div id="GradFormContent" class="form-group text-center">');
    formGroup = $(gr_formContent);
    formGroup.append('<label class="posebni-label">Naziv</label>');
    formGroup.append("<input class='kontrolaForme text-box single-line' type='text' value='' name='Grad'>");
    formGroup.append('<label class="posebni-label">Država</label>');
    formGroup.append('<select class="kontrolaForme" name="DrzavaId" id="selectDrzava">');
    selectgroup = $("#selectDrzava");
    selectgroup.append(drzaveOptions);
    formGroup.append('<button id="GradCreateButton" class="btn btn-primary mt-3 mr-3"> Unesi </button>')
});

$(document).on("click", "#GradCreateButton", function () {
    event.preventDefault();
    Create(gr_url, gr_number);
});

$(document).on("click", ".buttonPromjeni6", function () {
    event.preventDefault();
    var id = $(this).parent().attr('id');
    $(sadrzajId).empty();
    $(sadrzajId).append('<h1 align="center"> Promjena grada:');
    $(sadrzajId).append('<form id="entitetForm">');

    var form = $(gr_form);
    var formGroup, selectgroup;

    form.append('<div id="GradFormContent" class="form-group text-center">');
    formGroup = $(gr_formContent);
    formGroup.append('<input id="gradIdInput" name="PK_Grad" hidden type="number" />')
    var gradId = $("#gradIdInput")
    gradId.val(id);
    formGroup.append('<label class="posebni-label">Naziv</label>');
    formGroup.append('<input id="gradNazivInput" class="kontrolaForme text-box single-line" type="text" value="" name="Naziv">');
    formGroup.append('<label class="posebni-label">Država</label>');
    formGroup.append('<select class="kontrolaForme" name="FK_Grad_Drzava_PK" id="selectDrzava">');
    selectgroup = $("#selectDrzava");
    selectgroup.append(drzaveOptions);
    formGroup.append('<button id="GradPromjeniButton" class="btn btn-primary mt-3 mr-3"> Promjeni </button>')

    $.ajax({
        url: gr_url + id,
        method: "GET",
        crossDomain: true
    }).done(function (data) {
        $("#gradNazivInput").val(data.Grad);
        $("#selectDrzava option:contains('" + data.Drzava + "')").prop("selected", true);
    });
});

$(document).on("click", "#GradPromjeniButton", function () {
    event.preventDefault();
    var form = $(gr_form);
    let data = form.serializeArray().reduce(function (obj, item) {
        obj[item.name] = item.value;
        return obj;
    }, {});

    Edit(gr_url, data.PK_Grad, gr_number);
});

$(document).on("click", ".buttonObrisi6", function () {
    event.preventDefault();
    var id = $(this).parent().attr('id');
    $.ajax({
        url: gr_url + id,
        method: "GET",
        crossDomain: true
    }).done(function (data) {
        var r = confirm(`Jeste li sigurni da želite obrisati ovaj grad? \n Grad: ${data.Grad} \n Država: ${data.Drzava} `);
        if (r == true) {
            Delete(gr_url, id, gr_number);
        }
    });
});