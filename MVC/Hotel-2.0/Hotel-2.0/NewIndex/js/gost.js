﻿var g_number = 3;
var g_url = 'http://localhost/API/api/gost/';
var g_svojstva = ["Ime", "Prezime", "Mobitel", "Email", "Grad", "Adresa", "Id"];
var g_svojstvaDisplay = ["Ime", "Prezime", "Mobitel", "Email", "Grad", "Adresa"];
var g_formContent = '#GostFormContent'
var g_selectGrad = '#selectGrad'
var g_form = "#entitetForm";
var g_Id = '#gosti'
var g_napraviId = '#napraviGost';
var g_Table = '<h1 class="entitet">Gosti</h1><button id="napraviGost" class="btn btn-primary pozicijaGumb" > Napravi novog</button ><table id="entitetTable">'

function loadGosti() {
    $(sadrzajId).empty();
    $(sadrzajId).append(g_Table);
    napraviTablicu(g_url, g_svojstva, g_svojstvaDisplay, g_number);
}

$(document).on("click", g_Id, function () {
    loadGosti();
});

$(document).on("click", g_napraviId, function () {
    $(sadrzajId).empty();
    $(sadrzajId).append('<h1 align="center"> Unos novog gosta:');
    $(sadrzajId).append('<form id="entitetForm">');


    var form = $(g_form);
    var formGroup, selectgroup;

    form.append('<div id="GostFormContent" class="form-group text-center">');
    formGroup = $(g_formContent);
    formGroup.append('<label class="posebni-label">Ime</label>');
    formGroup.append("<input class='kontrolaForme text-box single-line' type='text' value='' name='Ime'>");
    formGroup.append('<label class="posebni-label">Prezime</label>');
    formGroup.append("<input class='kontrolaForme text-box single-line' type='text' value='' name='Prezime'>");
    formGroup.append('<label class="posebni-label">Mobitel</label>');
    formGroup.append("<input class='kontrolaForme text-box single-line' type='text' value='' name='Mobitel'>");
    formGroup.append('<label class="posebni-label">Email</label>');
    formGroup.append("<input class='kontrolaForme text-box single-line' type='text' value='' name='Email'>");
    formGroup.append('<label class="posebni-label">Grad</label>');
    formGroup.append('<select class="kontrolaForme" name="GradId" id="selectGrad">');
    selectgroup = $("#selectGrad");
    selectgroup.append(gradoviOptions);
    formGroup.append('<label class="posebni-label">Adresa</label>');
    formGroup.append("<input class='kontrolaForme text-box single-line' type='text' value='' name='Adresa'>");

    formGroup.append('<button id="GostCreateButton" class="btn btn-primary"> Unesi </button>')
});

$(document).on("click", "#GostCreateButton", function () {
    event.preventDefault();
    Create(g_url, g_number);
});

$(document).on("click", ".buttonPromjeni3", function () {
    event.preventDefault();
    var id = $(this).parent().attr('id');
    $(sadrzajId).empty();
    $(sadrzajId).append('<h1 align="center"> Promjena gosta:');
    $(sadrzajId).append('<form id="entitetForm">');


    var form = $(g_form);
    var formGroup, selectgroup;

    form.append('<div id="GostFormContent" class="form-group text-center">');
    formGroup = $(g_formContent);
    formGroup.append('<input id="gostIdInput" name="Id" hidden type="number" />')
    var gostId = $("#gostIdInput")
    gostId.val(id);
    formGroup.append('<label class="posebni-label">Ime</label>');
    formGroup.append('<input id="gostImeInput" class="kontrolaForme text-box single-line" type="text" value="" name="Ime">');
    formGroup.append('<label class="posebni-label">Prezime</label>');
    formGroup.append('<input id="gostPrezimeInput" class="kontrolaForme text-box single-line" type="text" value="" name="Prezime">');
    formGroup.append('<label class="posebni-label">Mobitel</label>');
    formGroup.append('<input id="gostMobitelInput" class="kontrolaForme text-box single-line" type="text" value="" name="Mobitel">');
    formGroup.append('<label class="posebni-label">Email</label>');
    formGroup.append('<input id="gostEmailInput" class="kontrolaForme text-box single-line" type="text" value="" name="Email">');
    formGroup.append('<label class="posebni-label">Grad</label>');
    formGroup.append('<select class="kontrolaForme" name="GradId" id="selectGrad">');
    selectgroup = $("#selectGrad");
    selectgroup.append(gradoviOptions);
    formGroup.append('<label class="posebni-label">Adresa</label>');
    formGroup.append('<input id="gostAdresaInput" class="kontrolaForme text-box single-line" type="text" value="" name="Adresa">');

    formGroup.append('<button id="GostPromjeniButton" class="btn btn-primary"> Promjeni </button>')

    $.ajax({
        url: g_url + id,
        method: "GET",
        crossDomain: true
    }).done(function (data) {
        $.ajax({
            url: gr_url + data.GradId,
            method: "GET",
            crossDomain: true
        }).done(function (data1) {
            $("#gostImeInput").val(data.Ime);
            $("#gostPrezimeInput").val(data.Prezime);
            $("#gostMobitelInput").val(data.Mobitel);
            $("#gostEmailInput").val(data.Email);
            $("#selectGrad option:contains('" + data1.Grad + "')").prop("selected", true);
            $("#gostAdresaInput").val(data.Adresa);
        });
    });
});

$(document).on("click", "#GostPromjeniButton", function () {
    event.preventDefault();
    var form = $(g_form);
    let data = form.serializeArray().reduce(function (obj, item) {
        obj[item.name] = item.value;
        return obj;
    }, {});

    Edit(g_url, data.Id, g_number);
});

$(document).on("click", ".buttonObrisi3", function () {
    event.preventDefault();
    var id = $(this).parent().attr('id');
    $.ajax({
        url: g_url + id,
        method: "GET",
        crossDomain: true
    }).done(function (data) {
        $.ajax({
            url: gr_url + data.GradId,
            method: "GET",
            crossDomain: true
        }).done(function (data1) {
            var r = confirm(`Jeste li sigurni da želite obrisati ovog gosta? \n Ime i prezime: ${data.Ime} ${data.Prezime} \n Grad: ${data1.Grad}`);
            if (r == true) {
                Delete(g_url, id, g_number);
            }
        });
    });
});