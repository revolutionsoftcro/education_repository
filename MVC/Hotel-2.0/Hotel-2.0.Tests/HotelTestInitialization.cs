﻿using Hotel_2._0.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using MockData;

namespace Hotel_2._0.Tests
{
    [TestClass]
    public class HotelTestInitialization
    {
        public static Mock<Hotel2Entities> context { get; set; }

        [AssemblyInitialize]
        public static void AssemblyInitialize(TestContext tc)
        {
            context = Mock_Data.Create();
        }

        [AssemblyCleanup]
        public static void AssemblyCleanup()
        {
            Mock_Data.Destroy(context);
        }
    }
}
