﻿using System;
using System.Web.Mvc;
using Hotel_2._0.Controllers;
using Hotel_2._0.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace Hotel_2._0.Tests.Controllers
{
    [TestClass]
    public class KosaricaProizvodControllerTest
    {
        protected static Mock<Hotel2Entities> testContext = HotelTestInitialization.context;
        protected KosaricaProizvodController kosaricaProizvodController = new KosaricaProizvodController(testContext.Object);

        [TestMethod]
        public void AddToCartVracaRedirect()
        {
            var result = kosaricaProizvodController.AddToCart(2,4);
            Assert.IsInstanceOfType(result, typeof(RedirectToRouteResult), "Nije redirectalo za deleteConfirmed!");
        }

        [TestMethod]
        public void RemoveFromCartVracaRedirect()
        {
            var result = kosaricaProizvodController.RemoveFromCart(1);
            Assert.IsInstanceOfType(result, typeof(RedirectToRouteResult), "Nije redirectalo za deleteConfirmed!");
        }
    }
}
