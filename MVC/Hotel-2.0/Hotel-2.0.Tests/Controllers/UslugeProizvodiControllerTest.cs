﻿using Hotel_2._0.Controllers;
using Hotel_2._0.Models;
using Hotel_2._0.ViewModels;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web.Mvc;

namespace Hotel_2._0.Tests.Controllers
{
    [TestClass]
    public class UslugeProizvodiControllerTest
    {
        protected static Mock<Hotel2Entities> testContext = HotelTestInitialization.context;
        protected UslugeProizvodiController uslugeProizvodiController = new UslugeProizvodiController(testContext.Object);

        [TestMethod]
        public void ConstructorNotNull()
        {
            var result = new UslugeProizvodiController();
            Assert.IsNotNull(result, "View is null!");
        }

        [TestMethod]
        public void IndexVracaView()
        {
            var result = uslugeProizvodiController.Index() as ViewResult;
            Assert.IsNotNull(result, "View is null!");
            Assert.IsInstanceOfType(result, typeof(ViewResult), "Ne vraca se ViewResult!");
            Assert.IsInstanceOfType(result.Model, typeof(IEnumerable<UslugeProizvodi>), "Model je krivi!");
        }

        [TestMethod]
        public void DetailsNullId()
        {
            int? i = null;
            var result = uslugeProizvodiController.Details(i) as HttpStatusCodeResult;
            Assert.AreEqual((int)HttpStatusCode.BadRequest, result.StatusCode, "Details ne vraća Bad Request za null id!");
        }

        [TestMethod]
        public void DetailsNepostojecauslugeProizvodi()
        {
            var result = uslugeProizvodiController.Details(5) as HttpStatusCodeResult;
            Assert.IsInstanceOfType(result, typeof(HttpNotFoundResult), "Nepostojeća uslugeProizvodi ne vraća NotFound()!");
        }

        [TestMethod]
        public void DetailsVracaView()
        {
            var view = uslugeProizvodiController.Details(1) as ViewResult;

            Assert.IsNotNull(view, "View je null!");
            Assert.IsInstanceOfType(view, typeof(ViewResult), "Details ne vraća pravi View!");
            Assert.IsInstanceOfType(view.Model, typeof(UslugeProizvodi), "Details nema dobar model!");
        }

        [TestMethod]
        public void CreateVracaView()
        {
            var result = uslugeProizvodiController.Create() as ViewResult;
            Assert.IsNotNull(result, "View is null!");
            Assert.IsInstanceOfType(result, typeof(ViewResult), "Ne vraca se ViewResult!");
        }

        [TestMethod]
        public void CreatePostVracaRedirect()
        {
            var result = uslugeProizvodiController.Create(new UslugeProizvodi { PK_UslugeProizvodi = 3, FK_UslugeProizvodi_Hotel_PK = 2, Naziv = "Šampanjac", Tip = "Proizvod", Mjera = "Boca 0.75", CijenaPoMjeri = 450 });
            Assert.IsInstanceOfType(result, typeof(RedirectToRouteResult), "Nije redirectalo za nepotpun model!");
        }

        [TestMethod]
        public void CreatePostVracaView()
        {
            var controller = uslugeProizvodiController;
            controller.ModelState.AddModelError("Naziv", "Naziv je obavezan");
            var result = uslugeProizvodiController.Create(new UslugeProizvodi { PK_UslugeProizvodi = 4, FK_UslugeProizvodi_Hotel_PK = 2, Naziv = "Najam auta", Tip = "Usluga", Mjera = "Dan", CijenaPoMjeri = 750 }) as ViewResult;
            Assert.IsInstanceOfType(result, typeof(ViewResult), "Ne vraca se ViewResult!");
            Assert.IsInstanceOfType(result.Model, typeof(UslugeProizvodi), "Model je krivi!");
            controller.ModelState.Clear();
        }

        [TestMethod]
        public void EditNullId()
        {
            int? i = null;
            var result = uslugeProizvodiController.Edit(i) as HttpStatusCodeResult;
            Assert.AreEqual((int)HttpStatusCode.BadRequest, result.StatusCode, "Edit ne vraća Bad Request za null id!");
        }

        [TestMethod]
        public void EditNepostojeciUslugeProizvodi()
        {
            var result = uslugeProizvodiController.Edit(5) as HttpStatusCodeResult;
            Assert.IsInstanceOfType(result, typeof(HttpNotFoundResult), "Nepostojeći uslugeProizvodi ne vraća NotFound()!");
        }

        [TestMethod]
        public void EditVracaView()
        {
            var view = uslugeProizvodiController.Edit(1) as ViewResult;

            Assert.IsNotNull(view, "View je null!");
            Assert.IsInstanceOfType(view, typeof(ViewResult), "Edit ne vraća pravi View!");
            Assert.IsInstanceOfType(view.Model, typeof(UslugeProizvodi), "Edit nema dobar model!");
        }

        [TestMethod]
        public void EditPostVracaRedirect()
        {
            var result = uslugeProizvodiController.Edit(testContext.Object.UslugeProizvodi.Find(1));
            Assert.IsInstanceOfType(result, typeof(RedirectToRouteResult), "Nije redirectalo za nepotpun model!");
        }

        [TestMethod]
        public void EditPostVracaView()
        {
            var controller = uslugeProizvodiController;
            controller.ModelState.AddModelError("Naziv", "Naziv je obavezan");
            var result = uslugeProizvodiController.Edit(testContext.Object.UslugeProizvodi.Find(1)) as ViewResult;
            Assert.IsInstanceOfType(result, typeof(ViewResult), "Ne vraca se ViewResult!");
            Assert.IsInstanceOfType(result.Model, typeof(UslugeProizvodi), "Model je krivi!");
            controller.ModelState.Clear();
        }

        [TestMethod]
        public void DeleteNullId()
        {
            int? i = null;
            var result = uslugeProizvodiController.Delete(i) as HttpStatusCodeResult;
            Assert.AreEqual((int)HttpStatusCode.BadRequest, result.StatusCode, "Edit ne vraća Bad Request za null id!");
        }

        [TestMethod]
        public void DeleteNepostojeciUslugeProizvod()
        {
            var result = uslugeProizvodiController.Delete(5) as HttpStatusCodeResult;
            Assert.IsInstanceOfType(result, typeof(HttpNotFoundResult), "Nepostojeći tip sobe ne vraća NotFound()!");
        }

        [TestMethod]
        public void DeleteVracaView()
        {
            var view = uslugeProizvodiController.Delete(1) as ViewResult;

            Assert.IsNotNull(view, "View je null!");
            Assert.IsInstanceOfType(view, typeof(ViewResult), "Edit ne vraća pravi View!");
            Assert.IsInstanceOfType(view.Model, typeof(UslugeProizvodi), "Edit nema dobar model!");
        }

        [TestMethod]
        public void DeleteConfirmedVracaRedirect()
        {
            var proizvod = testContext.Object.UslugeProizvodi.Where(p => p.PK_UslugeProizvodi == 1).First();
            var result = uslugeProizvodiController.DeleteConfirmed(1);
            testContext.Object.UslugeProizvodi.Add(proizvod);
            Assert.IsInstanceOfType(result, typeof(RedirectToRouteResult), "Nije redirectalo za deleteConfirmed!");
        }

        [TestMethod]
        public void OdabirRezervacijeVracaView()
        {
            var result = uslugeProizvodiController.OdabirRezervacije() as ViewResult;
            Assert.IsNotNull(result, "View is null!");
            Assert.IsInstanceOfType(result, typeof(ViewResult), "Ne vraca se ViewResult!");
        }

        [TestMethod]
        public void ProdajaVracaView()
        {
            var result = uslugeProizvodiController.Prodaja() as ViewResult;
            Assert.IsNotNull(result, "View is null!");
            Assert.IsInstanceOfType(result, typeof(ViewResult), "Ne vraca se ViewResult!");
        }


        [TestMethod]
        public void ProdajaPostVracaView()
        {
            var kosarica = testContext.Object.Kosarica.Where(k => k.Trenutna == true).First();
            var result = uslugeProizvodiController.Prodaja(new OdabirRezervacije() { PK_Rezervacija = 1 }) as ViewResult;
            kosarica.Otkazana = false;
            kosarica.Trenutna = true;
            Assert.IsNotNull(result, "View is null!");
            Assert.IsInstanceOfType(result, typeof(ViewResult), "Ne vraca se ViewResult!");
        }
    }
}
