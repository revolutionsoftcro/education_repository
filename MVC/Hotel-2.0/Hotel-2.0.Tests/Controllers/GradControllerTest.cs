﻿using Hotel_2._0.Controllers;
using Hotel_2._0.Models;
using Hotel_2._0.ViewModels;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web.Mvc;

namespace Hotel_2._0.Tests.Controllers
{
    [TestClass]
    public class GradControllerTest
    {
        protected static Mock<Hotel2Entities> testContext = HotelTestInitialization.context;
        protected GradController gradController = new GradController(testContext.Object);

        [TestMethod]
        public void ConstructorNotNull()
        {
            var result =new GradController();
            Assert.IsNotNull(result, "View is null!");
        }

        [TestMethod]
        public void IndexVracaView()
        {
            var result = gradController.Index() as ViewResult;
            Assert.IsNotNull(result, "View is null!");
            Assert.IsInstanceOfType(result, typeof(ViewResult), "Ne vraca se ViewResult!");
            Assert.IsInstanceOfType(result.Model, typeof(IEnumerable<Grad>), "Model je krivi!");
        }

        [TestMethod]
        public void DetailsNullId()
        {
            int? i = null;
            var result = gradController.Details(i) as HttpStatusCodeResult;
            Assert.AreEqual((int)HttpStatusCode.BadRequest, result.StatusCode, "Details ne vraća Bad Request za null id!");
        }

        [TestMethod]
        public void DetailsNepostojeciGrad()
        {
            var result = gradController.Details(5) as HttpStatusCodeResult;
            Assert.IsInstanceOfType(result, typeof(HttpNotFoundResult), "Nepostojeći grad ne vraća NotFound()!");
        }

        [TestMethod]
        public void DetailsVracaView()
        {
            var view = gradController.Details(1) as ViewResult;

            Assert.IsNotNull(view, "View je null!");
            Assert.IsInstanceOfType(view, typeof(ViewResult), "Details ne vraća pravi View!");
            Assert.IsInstanceOfType(view.Model, typeof(Grad), "Details nema dobar model!");
        }

        [TestMethod]
        public void CreateVracaView()
        {
            var result = gradController.Create() as ViewResult;
            Assert.IsNotNull(result, "View is null!");
            Assert.IsInstanceOfType(result, typeof(ViewResult), "Ne vraca se ViewResult!");
            Assert.IsInstanceOfType(result.Model, typeof(Grad), "Model je krivi!");
        }

        [TestMethod]
        public void CreatePostVracaRedirect()
        {
            var result = gradController.Create(new Grad { FK_Grad_Drzava_PK = 1, Naziv = "Kastel Stari" });
            Assert.IsInstanceOfType(result, typeof(RedirectToRouteResult), "Nije redirectalo za nepotpun model!");
        }

        [TestMethod]
        public void CreatePostVracaView()
        {
            var controller = gradController;
            controller.ModelState.AddModelError("Naziv", "Naziv je obavezan");
            var result = gradController.Create(new Grad { FK_Grad_Drzava_PK = 1, Naziv = "Kastel Novi" }) as ViewResult;
            Assert.IsInstanceOfType(result, typeof(ViewResult), "Ne vraca se ViewResult!");
            Assert.IsInstanceOfType(result.Model, typeof(Grad), "Model je krivi!");
            controller.ModelState.Clear();
        }

        [TestMethod]
        public void EditNullId()
        {
            int? i = null;
            var result = gradController.Edit(i) as HttpStatusCodeResult;
            Assert.AreEqual((int)HttpStatusCode.BadRequest, result.StatusCode, "Edit ne vraća Bad Request za null id!");
        }

        [TestMethod]
        public void EditNepostojeciGrad()
        {
            var result = gradController.Edit(5) as HttpStatusCodeResult;
            Assert.IsInstanceOfType(result, typeof(HttpNotFoundResult), "Nepostojeći grad ne vraća NotFound()!");
        }

        [TestMethod]
        public void EditVracaView()
        {
            var view = gradController.Edit(2) as ViewResult;

            Assert.IsNotNull(view, "View je null!");
            Assert.IsInstanceOfType(view, typeof(ViewResult), "Edit ne vraća pravi View!");
            Assert.IsInstanceOfType(view.Model, typeof(GradViewModel), "Edit nema dobar model!");
        }

        [TestMethod]
        public void EditPostVracaRedirect()
        {
            var result = gradController.Edit(testContext.Object.Grad.Find(3));
            Assert.IsInstanceOfType(result, typeof(RedirectToRouteResult), "Nije redirectalo za nepotpun model!");
        }

        [TestMethod]
        public void EditPostVracaView()
        {
            var controller = gradController;
            controller.ModelState.AddModelError("Naziv", "Naziv je obavezan");
            var result = gradController.Edit(testContext.Object.Grad.Find(3)) as ViewResult;
            Assert.IsInstanceOfType(result, typeof(ViewResult), "Ne vraca se ViewResult!");
            Assert.IsInstanceOfType(result.Model, typeof(Grad), "Model je krivi!");
            controller.ModelState.Clear();
        }

        [TestMethod]
        public void DeleteNullId()
        {
            int? i = null;
            var result = gradController.Delete(i) as HttpStatusCodeResult;
            Assert.AreEqual((int)HttpStatusCode.BadRequest, result.StatusCode, "Edit ne vraća Bad Request za null id!");
        }

        [TestMethod]
        public void DeleteNepostojeciGrad()
        {
            var result = gradController.Delete(5) as HttpStatusCodeResult;
            Assert.IsInstanceOfType(result, typeof(HttpNotFoundResult), "Nepostojeći grad ne vraća NotFound()!");
        }

        [TestMethod]
        public void DeleteVracaView()
        {
            var view = gradController.Delete(2) as ViewResult;

            Assert.IsNotNull(view, "View je null!");
            Assert.IsInstanceOfType(view, typeof(ViewResult), "Edit ne vraća pravi View!");
            Assert.IsInstanceOfType(view.Model, typeof(Grad), "Edit nema dobar model!");
        }

        [TestMethod]
        public void DeleteConfirmedVracaRedirect()
        {
            var grad = testContext.Object.Grad.Where(g=>g.PK_Grad==2).First();
            var result = gradController.DeleteConfirmed(2);
            testContext.Object.Grad.Add(grad);
            Assert.IsInstanceOfType(result, typeof(RedirectToRouteResult), "Nije redirectalo za deleteConfirmed!");
        }
    }
}
