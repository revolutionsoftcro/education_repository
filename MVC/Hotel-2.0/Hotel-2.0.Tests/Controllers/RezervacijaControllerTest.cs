﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Hotel_2._0.Controllers;
using Hotel_2._0.Models;
using Hotel_2._0.ViewModels;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace Hotel_2._0.Tests.Controllers
{
    [TestClass]
    public class RezervacijaControllerTest
    {
        protected static Mock<Hotel2Entities> testContext = HotelTestInitialization.context;
        protected RezervacijaController rezervacijaController = new RezervacijaController(testContext.Object);

        [TestMethod]
        public void CreateVracaView()
        {
            var result = rezervacijaController.Create() as ViewResult;
            Assert.IsNotNull(result, "View is null!");
            Assert.IsInstanceOfType(result, typeof(ViewResult), "Ne vraca se ViewResult!");
            Assert.IsInstanceOfType(result.Model, typeof(RezervacijaViewModel), "Model je krivi!");
        }

        [TestMethod]
        public void CreatePostOdlazakVracaView()
        {
            var result = rezervacijaController.Create(new RezervacijaViewModel { PK_Rezervacija=3, Dolazak=new DateTime(2019,5,13), Odlazak = new DateTime(2019,5,11) }) as ViewResult;
            Assert.IsFalse(((RezervacijaViewModel)result.Model).OdlazakBool);
            Assert.IsNotNull(result, "View is null!");
            Assert.IsInstanceOfType(result, typeof(ViewResult), "Ne vraca se ViewResult!");
            Assert.IsInstanceOfType(result.Model, typeof(RezervacijaViewModel), "Model je krivi!");
        }

        [TestMethod]
        public void CreatePostTipSobeVracaView()
        {
            var result = rezervacijaController.Create(new RezervacijaViewModel { PK_Rezervacija = 3, Dolazak = new DateTime(2019, 5, 13), Odlazak = new DateTime(2019, 5, 20), PK_Hotel=1, PK_TipSobe=2 }) as ViewResult;
            Assert.IsFalse(((RezervacijaViewModel)result.Model).TipSobeBool);
            Assert.IsNotNull(result, "View is null!");
            Assert.IsInstanceOfType(result, typeof(ViewResult), "Ne vraca se ViewResult!");
            Assert.IsInstanceOfType(result.Model, typeof(RezervacijaViewModel), "Model je krivi!");
        }

        [TestMethod]
        public void CreatePostTerminVracaView()
        {
            var result = rezervacijaController.Create(new RezervacijaViewModel { PK_Rezervacija = 3, Dolazak = new DateTime(2020, 7, 25), Odlazak = new DateTime(2020, 7, 27), PK_Hotel = 1, PK_TipSobe = 1 }) as ViewResult;
            Assert.IsFalse(((RezervacijaViewModel)result.Model).TerminBool);
            Assert.IsNotNull(result, "View is null!");
            Assert.IsInstanceOfType(result, typeof(ViewResult), "Ne vraca se ViewResult!");
            Assert.IsInstanceOfType(result.Model, typeof(RezervacijaViewModel), "Model je krivi!");
        }

        [TestMethod]
        public void CreatePostFalseModelVracaView()
        {
            var controller = rezervacijaController;
            controller.ModelState.AddModelError("Naziv", "Naziv je obavezan");
            var result = rezervacijaController.Create(new RezervacijaViewModel { PK_Rezervacija = 3, Dolazak = new DateTime(2020, 7, 25), Odlazak = new DateTime(2020, 7, 27), PK_Hotel = 1, PK_TipSobe = 1 }) as ViewResult;
            controller.ModelState.Clear();
            Assert.IsNotNull(result, "View is null!");
            Assert.IsInstanceOfType(result, typeof(ViewResult), "Ne vraca se ViewResult!");
            Assert.IsInstanceOfType(result.Model, typeof(RezervacijaViewModel), "Model je krivi!");
        }

        [TestMethod]
        public void CreatePostVracaRedirect()
        {
            var rezervacija = new RezervacijaViewModel { PK_Rezervacija = 3, Dolazak = new DateTime(2019, 9, 25), Odlazak = new DateTime(2019, 9, 27), PK_Hotel = 1, PK_TipSobe = 1 };
            var result = rezervacijaController.Create(rezervacija);
            Assert.IsInstanceOfType(result, typeof(RedirectToRouteResult), "Nije redirectalo za dobru rezervaciju!");
            testContext.Object.Rezervacija.Remove(testContext.Object.Rezervacija.Last());
        }

        [TestMethod]
        public void RezervacijeRacuniVracaView()
        {
            foreach (var k in testContext.Object.Rezervacija)
            {
                var i = k.PK_Rezervacija;
            }
            var result = rezervacijaController.RezervacijeRacuni() as ViewResult;
            Assert.IsNotNull(result, "View is null!");
            Assert.IsInstanceOfType(result, typeof(ViewResult), "Ne vraca se ViewResult!");
            Assert.IsInstanceOfType(result.Model, typeof(IEnumerable<RezervacijeRnViewModel>), "Model je krivi!");
        }

        [TestMethod]
        public void NeplaceniRacuniVracaView()
        {
            var result = rezervacijaController.NeplaceniRacuni() as ViewResult;
            Assert.IsNotNull(result, "View is null!");
            Assert.IsInstanceOfType(result, typeof(ViewResult), "Ne vraca se ViewResult!");
            Assert.IsInstanceOfType(result.Model, typeof(IEnumerable<RezervacijeRnViewModel>), "Model je krivi!");
        }
    }
}
