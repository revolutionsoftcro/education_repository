﻿using Hotel_2._0.Controllers;
using Hotel_2._0.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web.Mvc;

namespace Hotel_2._0.Tests.Controllers
{
    [TestClass]
    public class HotelControllerTest
    {
        protected static Mock<Hotel2Entities> testContext = HotelTestInitialization.context;
        protected HotelController hotelController = new HotelController(testContext.Object);

        [TestMethod]
        public void ConstructorNotNull()
        {
            var result = new HotelController();
            Assert.IsNotNull(result, "View is null!");
        }

        [TestMethod]
        public void IndexVracaView()
        {
            var result = hotelController.Index() as ViewResult;
            Assert.IsNotNull(result, "View is null!");
            Assert.IsInstanceOfType(result, typeof(ViewResult), "Ne vraca se ViewResult!");
            Assert.IsInstanceOfType(result.Model, typeof(IEnumerable<Hotel>), "Model je krivi!");
        }

        [TestMethod]
        public void DetailsNullId()
        {
            int? i = null;
            var result = hotelController.Details(i) as HttpStatusCodeResult;
            Assert.AreEqual((int)HttpStatusCode.BadRequest, result.StatusCode, "Details ne vraća Bad Request za null id!");
        }

        [TestMethod]
        public void DetailsNepostojecihotel()
        {
            var result = hotelController.Details(5) as HttpStatusCodeResult;
            Assert.IsInstanceOfType(result, typeof(HttpNotFoundResult), "Nepostojeći hotel ne vraća NotFound()!");
        }

        [TestMethod]
        public void DetailsVracaView()
        {
            var view = hotelController.Details(1) as ViewResult;

            Assert.IsNotNull(view, "View je null!");
            Assert.IsInstanceOfType(view, typeof(ViewResult), "Details ne vraća pravi View!");
            Assert.IsInstanceOfType(view.Model, typeof(Hotel), "Details nema dobar model!");
        }

        [TestMethod]
        public void CreateVracaView()
        {
            var result = hotelController.Create() as ViewResult;
            Assert.IsNotNull(result, "View is null!");
            Assert.IsInstanceOfType(result, typeof(ViewResult), "Ne vraca se ViewResult!");
        }

        [TestMethod]
        public void CreatePostVracaRedirect()
        {
            var result = hotelController.Create(new Hotel { PK_Hotel = 1, FK_Hotel_Grad_PK = 2, Naziv = "SM2", BrojZvjezdica = 5, Adresa = "Moskovska 15", Email = "sm2@gmail.com", Telefon = "07211652123" });
            Assert.IsInstanceOfType(result, typeof(RedirectToRouteResult), "Nije redirectalo za nepotpun model!");
        }

        [TestMethod]
        public void CreatePostVracaView()
        {
            var controller = hotelController;
            controller.ModelState.AddModelError("Naziv", "Naziv je obavezan");
            var result = hotelController.Create(new Hotel { PK_Hotel = 4, FK_Hotel_Grad_PK = 1, Naziv = "SZ2", BrojZvjezdica = 4, Adresa = "Ilica 30", Email = "sz2@gmail.com", Telefon = "0111652123" }) as ViewResult;
            Assert.IsInstanceOfType(result, typeof(ViewResult), "Ne vraca se ViewResult!");
            Assert.IsInstanceOfType(result.Model, typeof(Hotel), "Model je krivi!");
            controller.ModelState.Clear();
        }

        [TestMethod]
        public void EditNullId()
        {
            int? i = null;
            var result = hotelController.Edit(i) as HttpStatusCodeResult;
            Assert.AreEqual((int)HttpStatusCode.BadRequest, result.StatusCode, "Edit ne vraća Bad Request za null id!");
        }

        [TestMethod]
        public void EditNepostojecihotel()
        {
            var result = hotelController.Edit(5) as HttpStatusCodeResult;
            Assert.IsInstanceOfType(result, typeof(HttpNotFoundResult), "Nepostojeći hotel ne vraća NotFound()!");
        }

        [TestMethod]
        public void EditVracaView()
        {
            var view = hotelController.Edit(1) as ViewResult;

            Assert.IsNotNull(view, "View je null!");
            Assert.IsInstanceOfType(view, typeof(ViewResult), "Edit ne vraća pravi View!");
            Assert.IsInstanceOfType(view.Model, typeof(Hotel), "Edit nema dobar model!");
        }

        [TestMethod]
        public void EditPostVracaRedirect()
        {
            var result = hotelController.Edit(testContext.Object.Hotel.Find(1));
            Assert.IsInstanceOfType(result, typeof(RedirectToRouteResult), "Nije redirectalo za nepotpun model!");
        }

        [TestMethod]
        public void EditPostVracaView()
        {
            var controller = hotelController;
            controller.ModelState.AddModelError("Naziv", "Naziv je obavezan");
            var result = hotelController.Edit(testContext.Object.Hotel.Find(1)) as ViewResult;
            Assert.IsInstanceOfType(result, typeof(ViewResult), "Ne vraca se ViewResult!");
            Assert.IsInstanceOfType(result.Model, typeof(Hotel), "Model je krivi!");
            controller.ModelState.Clear();
        }

        [TestMethod]
        public void DeleteNullId()
        {
            int? i = null;
            var result = hotelController.Delete(i) as HttpStatusCodeResult;
            Assert.AreEqual((int)HttpStatusCode.BadRequest, result.StatusCode, "Edit ne vraća Bad Request za null id!");
        }

        [TestMethod]
        public void DeleteNepostojeciDrzava()
        {
            var result = hotelController.Delete(5) as HttpStatusCodeResult;
            Assert.IsInstanceOfType(result, typeof(HttpNotFoundResult), "Nepostojeća država ne vraća NotFound()!");
        }

        [TestMethod]
        public void DeleteVracaView()
        {
            var view = hotelController.Delete(1) as ViewResult;

            Assert.IsNotNull(view, "View je null!");
            Assert.IsInstanceOfType(view, typeof(ViewResult), "Edit ne vraća pravi View!");
            Assert.IsInstanceOfType(view.Model, typeof(Hotel), "Edit nema dobar model!");
        }

        [TestMethod]
        public void DeleteConfirmedVracaRedirect()
        {
            var hotel = testContext.Object.Hotel.Where(h=>h.PK_Hotel==1).First();
            var result = hotelController.DeleteConfirmed(1);
            testContext.Object.Hotel.Add(hotel);
            Assert.IsInstanceOfType(result, typeof(RedirectToRouteResult), "Nije redirectalo za deleteConfirmed!");
        }
    }
}
