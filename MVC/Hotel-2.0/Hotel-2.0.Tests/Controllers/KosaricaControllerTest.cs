﻿using System;
using System.Linq;
using System.Web.Mvc;
using Hotel_2._0.Controllers;
using Hotel_2._0.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace Hotel_2._0.Tests.Controllers
{
    [TestClass]
    public class KosaricaControllerTest
    {
        protected static Mock<Hotel2Entities> testContext = HotelTestInitialization.context;
        protected KosaricaController kosaricaController = new KosaricaController(testContext.Object);

        [TestMethod]
        public void ConstructorNotNull()
        {
            var result = new KosaricaController();
            Assert.IsNotNull(result, "View is null!");
        }

        [TestMethod]
        public void CartVracaView()
        {
            var result = kosaricaController.Cart() as ViewResult;
            Assert.IsNotNull(result, "View is null!");
            Assert.IsInstanceOfType(result, typeof(ViewResult), "Ne vraca se ViewResult!");
        }

        [TestMethod]
        public void TerminateVracaRedirect()
        {
            var kosarica = testContext.Object.Kosarica.Where(k => k.Trenutna == true).First();
            var result = kosaricaController.Terminate();
            kosarica.Otkazana = false;
            kosarica.Trenutna = true;
            Assert.IsInstanceOfType(result, typeof(RedirectToRouteResult), "Nije redirectalo za nepotpun model!");
        }

        [TestMethod]
        public void CheckoutVracaRedirect()
        {
            var kosarica = testContext.Object.Kosarica.Where(k => k.Trenutna == true).First();
            var result = kosaricaController.Checkout();
            kosarica.Otkazana = false;
            kosarica.Trenutna = true;
            Assert.IsInstanceOfType(result, typeof(RedirectToRouteResult), "Nije redirectalo za nepotpun model!");
        }

        [TestMethod]
        public void CheckoutVracaRedirect2()
        {
            var kosarica1 = testContext.Object.Kosarica.Where(k => k.Trenutna == true).First();
            var kosarica2 = testContext.Object.Kosarica.Where(k => k.Trenutna == false).First();
            kosarica1.Trenutna = false;
            kosarica2.Trenutna = true;

            var result = kosaricaController.Checkout();

            kosarica1.Trenutna = true;
            kosarica2.Trenutna = false;
            Assert.IsInstanceOfType(result, typeof(RedirectToRouteResult), "Nije redirectalo za nepotpun model!");
        }
    }
}
