﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Hotel_2._0.Controllers;
using Hotel_2._0.Models;
using Hotel_2._0.ViewModels;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace Hotel_2._0.Tests.Controllers
{
    [TestClass]
    public class RacunControllerTest
    {
        protected static Mock<Hotel2Entities> testContext = HotelTestInitialization.context;
        protected RacunController racunController = new RacunController(testContext.Object);

        [TestMethod]
        public void RacuniViewVracaView()
        {
            var result = racunController.RacuniView(1) as ViewResult;
            Assert.IsNotNull(result, "View is null!");
            Assert.IsInstanceOfType(result, typeof(ViewResult), "Ne vraca se ViewResult!");
            Assert.IsInstanceOfType(result.Model, typeof(IEnumerable<RacuniViewModel>), "Model je krivi!");
        }

        [TestMethod]
        public void PlatiVracaRedirect()
        {
            var racun = testContext.Object.Racun.Where(r => r.PK_Racun == 2).First();
            var result = racunController.Plati(2);
            racun.isPaid = false;
            Assert.IsInstanceOfType(result, typeof(RedirectToRouteResult), "Nije redirectalo za Plati!");
        }
    }
}
