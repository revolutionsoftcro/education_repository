﻿using Hotel_2._0.Controllers;
using Hotel_2._0.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web.Mvc;

namespace Hotel_2._0.Tests.Controllers
{
    [TestClass]
    public class TipSobeControllerTest
    {
        protected static Mock<Hotel2Entities> testContext = HotelTestInitialization.context;
        protected TipSobeController tipSobeController = new TipSobeController(testContext.Object);

        [TestMethod]
        public void ConstructorNotNull()
        {
            var result = new TipSobeController();
            Assert.IsNotNull(result, "View is null!");
        }

        [TestMethod]
        public void IndexVracaView()
        {
            var result = tipSobeController.Index() as ViewResult;
            Assert.IsNotNull(result, "View is null!");
            Assert.IsInstanceOfType(result, typeof(ViewResult), "Ne vraca se ViewResult!");
            Assert.IsInstanceOfType(result.Model, typeof(IEnumerable<TipSobe>), "Model je krivi!");
        }

        [TestMethod]
        public void DetailsNullId()
        {
            int? i = null;
            var result = tipSobeController.Details(i) as HttpStatusCodeResult;
            Assert.AreEqual((int)HttpStatusCode.BadRequest, result.StatusCode, "Details ne vraća Bad Request za null id!");
        }

        [TestMethod]
        public void DetailsNepostojeciTipSobe()
        {
            var result = tipSobeController.Details(5) as HttpStatusCodeResult;
            Assert.IsInstanceOfType(result, typeof(HttpNotFoundResult), "Nepostojeći TipSobe ne vraća NotFound()!");
        }

        [TestMethod]
        public void DetailsVracaView()
        {
            var view = tipSobeController.Details(1) as ViewResult;

            Assert.IsNotNull(view, "View je null!");
            Assert.IsInstanceOfType(view, typeof(ViewResult), "Details ne vraća pravi View!");
            Assert.IsInstanceOfType(view.Model, typeof(TipSobe), "Details nema dobar model!");
        }

        [TestMethod]
        public void CreateVracaView()
        {
            var result = tipSobeController.Create() as ViewResult;
            Assert.IsNotNull(result, "View is null!");
            Assert.IsInstanceOfType(result, typeof(ViewResult), "Ne vraca se ViewResult!");
        }

        [TestMethod]
        public void CreatePostVracaRedirect()
        {
            var result = tipSobeController.Create(new TipSobe { PK_TipSobe = 3, Naziv = "Jednosobna Luxury", Balkon = true, Telefon = true, Televizor = true, WIFI = true });
            Assert.IsInstanceOfType(result, typeof(RedirectToRouteResult), "Nije redirectalo za nepotpun model!");
        }

        [TestMethod]
        public void CreatePostVracaView()
        {
            var controller = tipSobeController;
            controller.ModelState.AddModelError("Naziv", "Naziv je obavezan");
            var result = tipSobeController.Create(new TipSobe { PK_TipSobe = 4, Naziv = "Dvosobna Luxury", Balkon = true, Telefon = true, Televizor = true, WIFI = true }) as ViewResult;
            Assert.IsInstanceOfType(result, typeof(ViewResult), "Ne vraca se ViewResult!");
            Assert.IsInstanceOfType(result.Model, typeof(TipSobe), "Model je krivi!");
            controller.ModelState.Clear();
        }

        [TestMethod]
        public void EditNullId()
        {
            int? i = null;
            var result = tipSobeController.Edit(i) as HttpStatusCodeResult;
            Assert.AreEqual((int)HttpStatusCode.BadRequest, result.StatusCode, "Edit ne vraća Bad Request za null id!");
        }

        [TestMethod]
        public void EditNepostojeciTipSobe()
        {
            var result = tipSobeController.Edit(5) as HttpStatusCodeResult;
            Assert.IsInstanceOfType(result, typeof(HttpNotFoundResult), "Nepostojeći tip sobe ne vraća NotFound()!");
        }

        [TestMethod]
        public void EditVracaView()
        {
            var view = tipSobeController.Edit(1) as ViewResult;

            Assert.IsNotNull(view, "View je null!");
            Assert.IsInstanceOfType(view, typeof(ViewResult), "Edit ne vraća pravi View!");
            Assert.IsInstanceOfType(view.Model, typeof(TipSobe), "Edit nema dobar model!");
        }

        [TestMethod]
        public void EditPostVracaRedirect()
        {
            var result = tipSobeController.Edit(testContext.Object.TipSobe.Find(1));
            Assert.IsInstanceOfType(result, typeof(RedirectToRouteResult), "Nije redirectalo za nepotpun model!");
        }

        [TestMethod]
        public void EditPostVracaView()
        {
            var controller = tipSobeController;
            controller.ModelState.AddModelError("Naziv", "Naziv je obavezan");
            var result = tipSobeController.Edit(testContext.Object.TipSobe.Find(1)) as ViewResult;
            Assert.IsInstanceOfType(result, typeof(ViewResult), "Ne vraca se ViewResult!");
            Assert.IsInstanceOfType(result.Model, typeof(TipSobe), "Model je krivi!");
            controller.ModelState.Clear();
        }

        [TestMethod]
        public void DeleteNullId()
        {
            int? i = null;
            var result = tipSobeController.Delete(i) as HttpStatusCodeResult;
            Assert.AreEqual((int)HttpStatusCode.BadRequest, result.StatusCode, "Edit ne vraća Bad Request za null id!");
        }

        [TestMethod]
        public void DeleteNepostojeciDrzava()
        {
            var result = tipSobeController.Delete(5) as HttpStatusCodeResult;
            Assert.IsInstanceOfType(result, typeof(HttpNotFoundResult), "Nepostojeći tip sobe ne vraća NotFound()!");
        }

        [TestMethod]
        public void DeleteVracaView()
        {
            var view = tipSobeController.Delete(1) as ViewResult;

            Assert.IsNotNull(view, "View je null!");
            Assert.IsInstanceOfType(view, typeof(ViewResult), "Edit ne vraća pravi View!");
            Assert.IsInstanceOfType(view.Model, typeof(TipSobe), "Edit nema dobar model!");
        }

        [TestMethod]
        public void DeleteConfirmedVracaRedirect()
        {
            var tipSobe = testContext.Object.TipSobe.Where(t => t.PK_TipSobe == 1).First();
            var result = tipSobeController.DeleteConfirmed(1);
            testContext.Object.TipSobe.Add(tipSobe);
            Assert.IsInstanceOfType(result, typeof(RedirectToRouteResult), "Nije redirectalo za deleteConfirmed!");
        }
    }
}
