﻿using Hotel_2._0.Controllers;
using Hotel_2._0.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web.Mvc;

namespace Hotel_2._0.Tests.Controllers
{
    [TestClass]
    public class GostControllerTest
    {
        protected static Mock<Hotel2Entities> testContext = HotelTestInitialization.context;
        protected GostController gostController = new GostController(testContext.Object);

        [TestMethod]
        public void ConstructorNotNull()
        {
            var result = new GostController();
            Assert.IsNotNull(result, "View is null!");
        }

        [TestMethod]
        public void IndexVracaView()
        {
            var result = gostController.Index() as ViewResult;
            Assert.IsNotNull(result, "View is null!");
            Assert.IsInstanceOfType(result, typeof(ViewResult), "Ne vraca se ViewResult!");
            Assert.IsInstanceOfType(result.Model, typeof(IEnumerable<Gost>), "Model je krivi!");
        }

        [TestMethod]
        public void DetailsNullId()
        {
            int? i = null;
            var result = gostController.Details(i) as HttpStatusCodeResult;
            Assert.AreEqual((int)HttpStatusCode.BadRequest, result.StatusCode, "Details ne vraća Bad Request za null id!");
        }

        [TestMethod]
        public void DetailsNepostojeciGost()
        {
            var result = gostController.Details(5) as HttpStatusCodeResult;
            Assert.IsInstanceOfType(result, typeof(HttpNotFoundResult), "Nepostojeći gost ne vraća NotFound()!");
        }

        [TestMethod]
        public void DetailsVracaView()
        {
            var view = gostController.Details(1) as ViewResult;

            Assert.IsNotNull(view, "View je null!");
            Assert.IsInstanceOfType(view, typeof(ViewResult), "Details ne vraća pravi View!");
            Assert.IsInstanceOfType(view.Model, typeof(Gost), "Details nema dobar model!");
        }

        [TestMethod]
        public void CreateVracaView()
        {
            var result = gostController.Create() as ViewResult;
            Assert.IsNotNull(result, "View is null!");
            Assert.IsInstanceOfType(result, typeof(ViewResult), "Ne vraca se ViewResult!");
        }

        [TestMethod]
        public void CreatePostVracaRedirect()
        {
            var result = gostController.Create(new Gost { PK_Gost = 3, FK_Gost_Grad_PK=1, Ime="Ivan", Prezime="Horvat", Adresa="Maksimirska 4", Email="ihoravat@gmail.com", Mobitel="09976541231" });
            Assert.IsInstanceOfType(result, typeof(RedirectToRouteResult), "Nije redirectalo za nepotpun model!");
        }

        [TestMethod]
        public void CreatePostVracaView()
        {
            var controller = gostController;
            controller.ModelState.AddModelError("Naziv", "Naziv je obavezan");
            var result = gostController.Create(new Gost { PK_Gost = 4, FK_Gost_Grad_PK = 2, Ime = "Ivana", Prezime = "Ivanov", Adresa = "Moskovska 4", Email = "iivanov@gmail.com", Mobitel = "067234241231" }) as ViewResult;
            Assert.IsInstanceOfType(result, typeof(ViewResult), "Ne vraca se ViewResult!");
            Assert.IsInstanceOfType(result.Model, typeof(Gost), "Model je krivi!");
            controller.ModelState.Clear();
        }

        [TestMethod]
        public void EditNullId()
        {
            int? i = null;
            var result = gostController.Edit(i) as HttpStatusCodeResult;
            Assert.AreEqual((int)HttpStatusCode.BadRequest, result.StatusCode, "Edit ne vraća Bad Request za null id!");
        }

        [TestMethod]
        public void EditNepostojeciGost()
        {
            var result = gostController.Edit(5) as HttpStatusCodeResult;
            Assert.IsInstanceOfType(result, typeof(HttpNotFoundResult), "Nepostojeći gost ne vraća NotFound()!");
        }

        [TestMethod]
        public void EditVracaView()
        {
            var view = gostController.Edit(1) as ViewResult;

            Assert.IsNotNull(view, "View je null!");
            Assert.IsInstanceOfType(view, typeof(ViewResult), "Edit ne vraća pravi View!");
            Assert.IsInstanceOfType(view.Model, typeof(Gost), "Edit nema dobar model!");
        }

        [TestMethod]
        public void EditPostVracaRedirect()
        {
            var result = gostController.Edit(testContext.Object.Gost.Find(1));
            Assert.IsInstanceOfType(result, typeof(RedirectToRouteResult), "Nije redirectalo za nepotpun model!");
        }

        [TestMethod]
        public void EditPostVracaView()
        {
            var controller = gostController;
            controller.ModelState.AddModelError("Naziv", "Naziv je obavezan");
            var result = gostController.Edit(testContext.Object.Gost.Find(1)) as ViewResult;
            Assert.IsInstanceOfType(result, typeof(ViewResult), "Ne vraca se ViewResult!");
            Assert.IsInstanceOfType(result.Model, typeof(Gost), "Model je krivi!");
            controller.ModelState.Clear();
        }

        [TestMethod]
        public void DeleteNullId()
        {
            int? i = null;
            var result = gostController.Delete(i) as HttpStatusCodeResult;
            Assert.AreEqual((int)HttpStatusCode.BadRequest, result.StatusCode, "Edit ne vraća Bad Request za null id!");
        }

        [TestMethod]
        public void DeleteNepostojeciDrzava()
        {
            var result = gostController.Delete(5) as HttpStatusCodeResult;
            Assert.IsInstanceOfType(result, typeof(HttpNotFoundResult), "Nepostojeća država ne vraća NotFound()!");
        }

        [TestMethod]
        public void DeleteVracaView()
        {
            var view = gostController.Delete(1) as ViewResult;

            Assert.IsNotNull(view, "View je null!");
            Assert.IsInstanceOfType(view, typeof(ViewResult), "Edit ne vraća pravi View!");
            Assert.IsInstanceOfType(view.Model, typeof(Gost), "Edit nema dobar model!");
        }

        [TestMethod]
        public void DeleteConfirmedVracaRedirect()
        {
            var gost = testContext.Object.Gost.Where(g=>g.PK_Gost==1).First();
            var result = gostController.DeleteConfirmed(1);
            testContext.Object.Gost.Add(gost);
            Assert.IsInstanceOfType(result, typeof(RedirectToRouteResult), "Nije redirectalo za deleteConfirmed!");
        }
    }
}
